/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Aug/2021
 * Time:  16h:05m
 * Description: None
 */
//=============================================================================
package com.common
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import org.mongodb.scala.bson.BsonValue
import org.mongodb.scala.{Document, MongoCollection}

import java.io.{BufferedReader, File, FileReader}
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
object DatabaseImporter extends MyLogger {
  //---------------------------------------------------------------------------
  final val CSV_EXTENSION_FILE = ".csv"
  final val GZ_EXTENSION_FILE = ".gz"
}
//=============================================================================
import com.common.DatabaseImporter._
//=============================================================================
trait DatabaseImporter extends MyLogger {
  //---------------------------------------------------------------------------
  val inputDir: String
  val mongoDB: MongoDB
  val maxBufferedDocument : Int
  val colNameSeq : Seq[LineEntry]
  val lineDivider: String
  val headerInCsv: Boolean
  val coreCount: Int
  //---------------------------------------------------------------------------
  protected val iDir = Path.ensureEndWithFileSeparator(inputDir)
  //---------------------------------------------------------------------------
  private def getDocument(line: String) : Option[Document] = {
    val seq = line.split(lineDivider)
    var r : (String,BsonValue) = null

    val accessingByCol = colNameSeq.head.colPos != -1

    if (accessingByCol) {
      val row = colNameSeq flatMap { entry =>
        val value = seq(entry.colPos)
        Try { r = LineEntry.entryToDocCol(entry, value)}
        match {
          case Success(_) => Some(r)
          case Failure(ex) => error(s"Error processing entry: '$value' with entry: $entry" + ex.toString)
            None
        }
      }
      Some(Document(row.toList))
    }
    else {
      val row = seq.take(colNameSeq.length).zipWithIndex flatMap { case (value,i) =>
        Try { r = LineEntry.entryToDocCol(colNameSeq(i), value.trim)}
        match {
          case Success(_) => Some(r)
          case Failure(ex) => error(s"Error processing entry: '$value' with entry: ${colNameSeq(i)}" +  ex.toString)
            None
        }
      }
      Some(Document(row.toList))
    }
  }
  //---------------------------------------------------------------------------
  protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    val name = Path.getOnlyFilename(path)
    info(s"Processing file: '$name'")
    val br = new BufferedReader(new FileReader(new File(path)))
    val docBuffer = ArrayBuffer[Document]()
    var line = br.readLine()
    if (headerInCsv)  line = br.readLine()
    while(line != null &&  line.length()!=0 ) {
      val doc = getDocument(line)
      if (doc.isEmpty) return error(s"Error processing file: '$path'")
      else {
        docBuffer += doc.get
        if (docBuffer.length > maxBufferedDocument) {
          coll.insertMany(docBuffer.toArray).results()
          docBuffer.clear()
        }
      }
      line = br.readLine()
    }
    br.close()

    //store the remain buffered docs
    if (docBuffer.length > 0) {
      coll.insertMany(docBuffer.toArray).results()
      docBuffer.clear()
    }
    true
  }
  //---------------------------------------------------------------------------
  class MyParallelTask(pathSeq : List[String], coll: MongoCollection[Document]) extends ParallelTask[String](
    pathSeq
    , threadCount = coreCount
    , isItemProcessingThreadSafe = true
    , message = Some("importing file")) {
    //-----------------------------------------------------------------------
    def userProcessSingleItem(s: String): Unit = {
      Try {
        processFile(s, coll)
      }
      match {
        case Success(_) =>
        case Failure(ex) => error(s"Error processing file:'${Path.getOnlyFilename(s)}'" +  ex.toString)
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def createIndexes() : Unit
  //---------------------------------------------------------------------------
  def run(dropCollection : Boolean = true
          , fileExtension: String = CSV_EXTENSION_FILE) : Unit =
    if (Path.directoryExist(iDir)) {
      if (dropCollection) mongoDB.dropCollection()
      val pathSeq = Path.getSortedFileList(iDir,fileExtension) map (_.getAbsolutePath)
      new MyParallelTask(pathSeq, mongoDB.getCollection)

      info(s"Total documents inserted in: ${mongoDB.getBasicConnectionInfo()}: ${mongoDB.getCollection.countDocuments().results.head}")
      mongoDB.close()
      createIndexes
    }
    else error(s"Input directory '$iDir' does not exists")
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file DatabaseImporter.scala
//=============================================================================
