/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  30/Aug/2021
 * Time:  19h:39m
 * Description: http://vizier.u-strasbg.fr/vizier/doc/sesame.htx
 */
//=============================================================================
package com.common
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.path.Path
//=============================================================================
import scalaj.http._
import java.io.{BufferedWriter, File, FileWriter}
import scala.io.Source
//=============================================================================
//=============================================================================
object ObjectNameResolver {
  //---------------------------------------------------------------------------
  private val nameResolverUrl = MyConf.c.getString("ObjectNameResolver.sesame.url")
  private val nameResolverCacheDir = MyConf.c.getString("ObjectNameResolver.sesame.cacheDir")

  private val connectionTimeoutMs = MyConf.c.getInt("ObjectNameResolver.sesame.connectionTimeoutMs")
  private val readTimeoutMs = MyConf.c.getInt("ObjectNameResolver.sesame.readTimeoutMs")
  //---------------------------------------------------------------------------
  final val CATALOG_RESULT_GAIA_2 = "GaiaDR3 DR2"
  final val CATALOG_QUERY_2MASS = "2Mass"
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.common.ObjectNameResolver._
case class ObjectNameResolver() extends MyLogger{
  //---------------------------------------------------------------------------
  Path.ensureDirectoryExist(nameResolverCacheDir)
  val cacheDir = Path.ensureEndWithFileSeparator(nameResolverCacheDir)
  //---------------------------------------------------------------------------
  private def findName(fileName: String, catalog: String) = {
    val r = Source.fromFile(fileName).getLines.find(s=> s.contains(catalog))
    if (r.isEmpty) None
    else Some(r.get.split(catalog).last.trim)
  }
  //---------------------------------------------------------------------------
  private def httpGet(url: String) : Option[String] = {
    info(s"Querying object name resolver: '$url'")
    val result: HttpResponse[String] =  Http(url)
      .charset("UTF-8")
      .timeout(connectionTimeoutMs,readTimeoutMs)
      .asString
    if (result.code != 200) return None
    if (result.body.isEmpty) None
    else Some(result.body)
  }
  //---------------------------------------------------------------------------
  def query(objectName :String, catalogQuery: String, catalogResult: String) : Option[String] = {
    val cachedFileName = cacheDir + objectName
    if (MyFile.fileExist(cachedFileName))
      info(s"Loading form cache the object name resolver: $objectName")
    else {
      val r = httpGet(nameResolverUrl + catalogQuery + " " + objectName)
      val bw = new BufferedWriter(new FileWriter(new File(cachedFileName)))
      if(!r.isEmpty) bw.write(r.get)
      bw.close()
    }
    findName(cachedFileName, catalogResult)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ObjectNameResolver.scala
//=============================================================================