package com.common
//=============================================================================
import com.common.logger.MyLogger
import com.common.csv.CsvFile._
//=============================================================================
import java.util.concurrent.atomic.AtomicInteger
import org.mongodb.scala.bson.{BsonBoolean, BsonInt64}
import org.mongodb.scala.bson.{BsonDouble, BsonInt32, BsonString, BsonValue}
//=============================================================================
object LineEntry extends MyLogger {
  //---------------------------------------------------------------------------
  private val id = new AtomicInteger(-1)
  //---------------------------------------------------------------------------
  def resetID() = id.set(-1)
  //---------------------------------------------------------------------------
  def getNewID: Int = id.addAndGet(1)
  //---------------------------------------------------------------------------
  def getCurrentID: Int = id.get
  //---------------------------------------------------------------------------
  def entryToDocCol(entry: LineEntry, value: String): (String, BsonValue) = {
    val name = entry.name
    entry.dataType match {

      case "boolean" =>
        if (value.isEmpty) name -> new BsonBoolean(false)
        else name -> new BsonBoolean(value.toLowerCase.trim == "true")

      case "byte" =>
        if (value.isEmpty) name -> new BsonInt32(-1)
        else name -> new BsonInt32(value.toInt)

      case "short" =>
        if (value.isEmpty) name -> new BsonInt32(-1)
        else name -> new BsonInt32(value.toInt)

      case "int" =>
        if (value.isEmpty) name -> new BsonInt32(-1)
        else name -> new BsonInt32(value.toInt)

      case "long" =>
        if (value.isEmpty) name -> new BsonInt64(-1)
        else name -> new BsonInt64(value.toLong)

      case "float" =>
        if (value.isEmpty) name -> new BsonDouble(Float.NaN)
        else name -> new BsonDouble(value.toDouble)

      case "double" =>
        if (value.isEmpty) name -> new BsonDouble(Double.NaN)
        else name -> new BsonDouble(value.toDouble)

      case "string" =>
        if (value.isEmpty) name -> new BsonString("")
        else name -> new BsonString(value
          .replaceAll("\"","")
          .replaceAll("'","")
          .trim)

      case "boolean_or_min_value_of_data_type" => name -> BsonBoolean(getBooleanOrDefault(value))

      case "byte_or_min_value_of_data_type"    => name -> BsonInt32(getByteOrDefault(value))

      case "short_or_min_value_of_data_type"   => name -> BsonInt32(getShortOrDefault(value))

      case "int_or_min_value_of_data_type"     => name -> BsonInt32(getIntOrDefault(value))

      case "long_or_min_value_of_data_type"    => name -> BsonInt64(getLongOrDefault(value))

      case "float_or_min_value_of_data_type"   => name -> BsonDouble(getDoubleOrDefault(value))

      case "double_or_min_value_of_data_type"  => name -> BsonDouble(getDoubleOrDefault(value))

      case _ =>
        error(s"Unknown data type on entry: '$entry'")
        name -> new BsonString(value)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class LineEntry(name: String, dataType: String, colPos: Int = LineEntry.getNewID)
//=============================================================================
//=============================================================================
//End of file LineEntry.scala
//=============================================================================