/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Aug/2021
 * Time:  10h:05m
 * Description: None
 */
//=============================================================================
package com.common
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.database.gaia.GaiaDB
import com.common.jpl.horizons.Horizons
import com.common.util.path.Path
import com.henosis.database.MPO_DB
import com.henosis.database.gaia.crossMatch.hipparcos.GaiaCrossMatchHipparcosDB
import com.henosis.database.gaia.crossMatch.panSTARRS.GaiaCrossMatchPanSTARRS_DB
import com.henosis.database.gaia.crossMatch.twoMass.GaiaCrossMatch2MassDB
import com.henosis.database.hipparcos.HipparcosDB
import com.henosis.database.mboss.MbossDB
import com.henosis.database.mpc.MpcDB
import com.henosis.database.panStarrs.PanSTARRS_DB
import com.henosis.database.paper.mpo.candal.Candal_2019_DB
import com.henosis.database.paper.mpo.carry.Carry_2016_DB
import com.henosis.database.paper.mpo.colazo.Colazo_2021_DB
import com.henosis.database.paper.mpo.mahlke.Mahlke_2021_DB
import com.henosis.database.paper.mpo.oszkiewicz.Oszkiewicz_2011_DB
import com.henosis.database.paper.mpo.peixinho.Peixhino_2015_DB
import com.henosis.database.paper.star.landolt.{Landolt_2009_DB, Landolt_2013_DB}
import com.henosis.database.paper.mpo.popescu.Popescu_2018_DB
import com.henosis.database.paper.mpo.veres.Veres_2015_DB
import com.henosis.database.paper.star.smart.Smart_2021_DB
import com.henosis.database.paper.star.stauffer.Stauffer_2010_DB
import com.henosis.database.twoMass.TwoMassDB
//=============================================================================
//=============================================================================
case class DatabaseHub() {
  //---------------------------------------------------------------------------
  //horizons
  val horizons = Horizons().db

  //GAIA early data release 3
  val gaiaEDR_3_Conf = MyConf(MyConf.c.getString("Database.gaia_edr_3"))

  val gaiaEDR_3_DB = GaiaDB(gaiaEDR_3_Conf)
  val gaiaEDR_3_CrossMatchTwoMassDB  = GaiaCrossMatch2MassDB(gaiaEDR_3_Conf)
  val gaiaEDR_3_CrossMatchPanStarrDB = GaiaCrossMatchPanSTARRS_DB(gaiaEDR_3_Conf)
  val gaiaEDR_3_CrossMatchHipparcosDB = GaiaCrossMatchHipparcosDB(gaiaEDR_3_Conf)

  //GAIA data release 3
  val gaiaDR_3_Conf = MyConf(MyConf.c.getString("Database.gaia_dr_3"))
  val gaiaDR_3_DB = GaiaDB(gaiaDR_3_Conf)

  val gaiaDR_3_DB_sso_observationDB          = com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_observation.DB(MyConf(MyConf.c.getString("Database.gaia_dr_3_sso_observation")))
  val gaiaDR_3_DB_sso_reflectance_spectrumDB = com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_reflectance_spectrum.DB(MyConf(MyConf.c.getString("Database.gaia_dr_3_sso_reflectance_spectrum")))
  val gaiaDR_3_DB_sso_sourceDB               = com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_source.DB(MyConf(MyConf.c.getString("Database.gaia_dr_3_sso_source")))
  val gaiaDR_3_DB_astrophysicalParameters    = com.henosis.database.gaia.source.gaiaDR3.astrophysical.astrophysical_parameters.DB(MyConf(MyConf.c.getString("Database.gaia_dr_3_astrophysical_parameters")))

  //synthetic_photometry_gspc
  val gaiaDR_3_DB_spgDB                      = com.henosis.database.gaia.source.gaiaDR3.performance_verification.synthetic_photometry_gspc.DB(MyConf(MyConf.c.getString("Database.gaia_dr_3_synthetic_photometry_gspc")))

  //2MASS
  val twoMassDB = TwoMassDB(MyConf(MyConf.c.getString("Database.2Mass")))

  //PanSTARRS
  val panSTARRS_DB = PanSTARRS_DB(MyConf(MyConf.c.getString("Database.panSTARRS")))

  //hipparcos
  val hipparcosDB = HipparcosDB(MyConf(MyConf.c.getString("Database.hipparcos")))

  //mpo DB
  val mpoDB = MPO_DB()

  //mpc
  val mpcDB = MpcDB()

  //mboss
  val mbossDB = MbossDB()

  //lcdb
  val lcdbPdsColorIndexDB = com.common.database.mongoDB.database.lcdb.pds.colorIndex.LcdbColorIndexDB()
  val lcdbMpcColorIndexDB = com.common.database.mongoDB.database.lcdb.mpc.colorIndex.LcdbColorIndexDB()

  //star papers
  val landolt_2009_DB  = Landolt_2009_DB()
  val landolt_2013_DB  = Landolt_2013_DB()
  val stauffer_2010_DB = Stauffer_2010_DB()
  val smart_2021_DB    = Smart_2021_DB()

  //mpo papers
  val popescu_2018_DB      = Popescu_2018_DB()
  val colazo_2021_DB       = Colazo_2021_DB()
  val mahlke_2021_DB       = Mahlke_2021_DB()
  val oszkiewicz_2011_DB   = Oszkiewicz_2011_DB()
  val veres_2015_DB        = Veres_2015_DB()
  val carry_2016_DB        = Carry_2016_DB()
  val candal_2019_DB       = Candal_2019_DB()
  val peixhino_2015_DB     = Peixhino_2015_DB()
  //---------------------------------------------------------------------------
  def close() = {

    //delete temporal directories
    Path.deleteDirectoryIfExist(PanSTARRS_DB.queryTemporalDir)

    //close databases
    gaiaEDR_3_DB.close()
    gaiaEDR_3_CrossMatchTwoMassDB.close()
    gaiaEDR_3_CrossMatchPanStarrDB.close()
    gaiaEDR_3_CrossMatchHipparcosDB.close()

    gaiaDR_3_DB.close()

    twoMassDB.close()
    panSTARRS_DB.close()
    mpcDB.close()

    landolt_2009_DB.close()
    landolt_2013_DB.close()
    stauffer_2010_DB.close()

    mpoDB.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DatabaseHub.scala
//=============================================================================
