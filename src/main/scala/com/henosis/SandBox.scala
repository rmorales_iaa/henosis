/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Jul/2021
 * Time:  20h:32m
 * Description: None
 */
//=============================================================================
package com.henosis
//=============================================================================
import com.common.configuration.MyConf
import com.common.coordinate.conversion.Conversion
import com.common.database.mongoDB.MongoDB
import com.common.database.mongoDB.database.standadStar.LandoltDB
import com.common.database.mongoDB.observatories.ObservatoriesDB
import com.common.database.postgreDB.PostgreScalaDB
import com.common.database.postgreDB.gaia_dr_3.{SourceMinimal, SourceReduced}
import com.common.image.focusType.ImageFocusMPO
import com.common.jpl.Spice
import com.common.jpl.horizons.Horizons
import com.common.logger.MyLogger
import com.common.pds.pdsToMongo.PdsToMongo
import com.common.util.mail.MailAgent
import com.common.util.path.Path
import com.common.{DatabaseHub, LineEntry}
import com.henosis.algoritms.crossmatch.ApassGaiaDR_3.{ApassGaiaDR_3, QueryItemApass}
import com.henosis.algoritms.crossmatch.VR.CrossMatchVR
import com.henosis.algoritms.paper.mpo.alvarez.Alvarez_2021
import com.henosis.algoritms.paper.mpo.popescu.Popescu_2018
import com.henosis.algoritms.paper.star.landolt.{Landolt_2009, Landolt_2013}
import com.henosis.algoritms.paper.star.smart.Smart_2021
import com.henosis.algoritms.paper.star.stauffer.Stauffer_2010
import com.henosis.database.MPO_DB
import com.henosis.database.apass.ApassDR_10_Importer
import com.henosis.database.astorb.AstorbImporter
import com.henosis.database.carmencita.CarmencitaImporter
import com.henosis.database.hipparcos.HipparcosImporter
import com.henosis.database.mboss.MbossImporter
import com.henosis.database.mpc.MpcToMongo
import com.henosis.database.observatoriesCodes.ObservatoriesImporter
import com.henosis.database.paper.mpo.candal.Candal_2019_DB_Importer
import com.henosis.database.paper.mpo.carry.Carry_2016_DB_Importer
import com.henosis.database.paper.mpo.colazo.Colazo_2021_DB_Importer
import com.henosis.database.paper.mpo.mahlke.Mahlke_2021_DB_Importer
import com.henosis.database.paper.mpo.oszkiewicz.Oszkiewicz_2011_DB_Importer
import com.henosis.database.paper.mpo.peixinho.Peixhino_2015_DB_Importer
import com.henosis.database.paper.mpo.popescu.Popescu_2018_DB_Importer
import com.henosis.database.paper.mpo.veres.Veres_2015_DB_Importer
import com.henosis.database.paper.star.landolt.{Landolt_2009_DB_Importer, Landolt_2013_DB_Importer}
import com.henosis.database.paper.star.smart.Smart_2021_DB_Importer
import com.henosis.database.paper.star.stauffer.Stauffer_2010_DB_Importer
import com.henosis.database.paper.star.torres.Torres_2019_DB_Importer
import com.henosis.database.sdsssMoc_4.SdssMoc_4_ToMongo
import com.henosis.database.tessToMongo.TessToMongo
import com.henosis.database.tnoColor.TnoColorImporter
import com.henosis.database.wise.allSky.WiseAllSkyImporter
import com.henosis.database.wise.reject.WiseRejectImporter
import com.henosis.database.wise.source.WiseSourceImporter
//=============================================================================
import com.henosis.ephemeris.{OrbitalElement, Orekit}
import org.orekit.bodies.CelestialBodyFactory
import org.orekit.data.{DataContext, DirectoryCrawler}
import org.orekit.frames.FramesFactory
import org.orekit.time.{AbsoluteDate, TimeScalesFactory}
import org.orekit.utils.Constants

import java.io.File
import java.time.LocalDateTime
import scala.sys.process._
//=============================================================================
//=============================================================================
object SandBox extends MyLogger {
  //---------------------------------------------------------------------------
  //https://forum.orekit.org/t/generating-a-jpl-spice-kernel-using-orekit-ephemeris/1531/6
  def OG19_Ephemeris(dbHub: DatabaseHub): Unit = {
    Orekit.generateSpk(dbHub
      , 470599  //2008 OG19
      , LocalDateTime.parse("2014-01-01T00:00:00")
      , LocalDateTime.parse("2014-12-31T00:00:00"))
  }
  //--------------------------------------------------------------------------
  def sendMail() = {
    val mailAgent = MailAgent()

    val from = "rmorales@iaa.es"
    val to = "rmorales@iaa.es"
    val subject= "deleteme"
    val content =" Please deleteme"
    val cc = ""
    mailAgent.send(
      from
      , to
      , subject
      , content
      , cc
      , filename = Some("/home/rafa/Downloads/uxmal Notes")
    )
  }

  //---------------------------------------------------------------------------
  def spline(): Unit ={
    Alvarez_2021.test_1()
    //Alvarez_2021.simulate()
  }
  //---------------------------------------------------------------------------
  def sunPosition(timeStamp: LocalDateTime) = {
    /*
        Spice.init()
        val k = Spice.getHeliocentricRange(SUN_CENTER_OBSERVER_SPK_ID_CODE
          , "399800"  //J86
          , timeStamp
        )

        Spice.close()
        println(k)
    */


    //https://www.orekit.org/site-orekit-tutorials-10.3/tutorials/propagation.html
    val orekitData = new File("/home/rafa/proyecto/henosis/input/orekit-data-master/")
    val manager = DataContext.getDefault.getDataProvidersManager
    manager.addProvider(new DirectoryCrawler(orekitData))
    val au2m =  Constants.IAU_2012_ASTRONOMICAL_UNIT
    val Sun = CelestialBodyFactory.getSun


    val epoch = new AbsoluteDate(2002, 7, 15, 0, 0, 0, TimeScalesFactory.getUTC)
    val p = Sun.getPVCoordinates(epoch,  FramesFactory.getEME2000).getPosition

    //val groundPointPos = Earth.(geodeticPoint)

    println(s" ${p.getX / au2m} ${p.getY / au2m} ${p.getZ / au2m} ")
    println()
  }
  //---------------------------------------------------------------------------
  def ephemerisCalculation(dbHub: DatabaseHub): Unit = {
    //https://www.orekit.org/site-orekit-tutorials-10.3/tutorials/propagation.html
    val orekitData = new File("/home/rafa/proyecto/henosis/input/orekit-data-master/")
    val manager = DataContext.getDefault.getDataProvidersManager
    manager.addProvider(new DirectoryCrawler(orekitData))

    //all values fixed in the example: Celestial mechanics. Jeremy Tatum. Chapter 10
    val Ceres = OrbitalElement( 80.48632d                 //longitude of the ascending node                                  (deg)
      , 10.58347d                //inclination to the ecliptic (plane of the Earth's orbit)        (deg)
      , 73.98440d                //argument (aka longitude) of perihelion                          (deg)
      , 189.275000d              //mean anomaly (0 at perihelion increases uniformly with time)    (deg)
      , 2.7664122d               //semi-major axis, or mean distance from Sun (aka mean distance)  (au)
      , 0.0791158d               //eccentricity (0=circle, 0-1=ellipse, 1=parabola)                (no units)
      , 2452400.5000000d         //2002-05-06 00:00:00 epoch of the osculating elements                                                       (Julian day)
      , meanMotion = 0.21420457d
    )

    // 470599 (2008 OG19) 2
    val OG19 = dbHub.mpcDB.getOrbitalElement(470599).get


    val t = LocalDateTime.parse("2002-07-15T00:00:00")
    val (r,alfa,delta) = Ceres.getGeocentricRectangularCoordinates(t)
    //val (r,alfa,delta) = OG19.getGeocentricRectangularCoordinates(t)

    println(Conversion.DD_to_HMS(alfa))
    println(Conversion.DD_to_DMS(delta))

    println()

  }
  //---------------------------------------------------------------------------
  def importHorizons() = Horizons().importData
  //---------------------------------------------------------------------------
  def importMpc() = MpcToMongo.importData()
  //---------------------------------------------------------------------------
  def importMpcObservatories(): Unit = {

    val db = ObservatoriesDB()

    ObservatoriesImporter(
      MyConf(MyConf.c.getString("Database.mpcObservatories"))
      , "output/tmp")
  }
  //---------------------------------------------------------------------------
  def import2Mass(db: MPO_DB) =
    PdsToMongo.importData2Mass("/home/rafa/Downloads/work_on_users/rene/pds/2MASS/ast_ext2mass.txt"
      , "/home/rafa/Downloads/work_on_users/rene/pds/2MASS/ast_ext2mass.csv"
      , db)
  //---------------------------------------------------------------------------
  def importSdssMoc4(db: MPO_DB) = SdssMoc_4_ToMongo.importData(db, "/home/rafa/Downloads/work_on_users/alvaro/paper/sdssmoc/ADR4.csv")
  //---------------------------------------------------------------------------
  def importTess(db: MPO_DB) =
    TessToMongo.importData("/home/rafa/Downloads/work_on_users/rene/tess/lightcurves_spectra/", db)
  //---------------------------------------------------------------------------
  def importAstorb(db: MPO_DB) = AstorbImporter.importData(db)
  //---------------------------------------------------------------------------
  def importApassDr10() = {
    ApassDR_10_Importer(
      MyConf(MyConf.c.getString("Database.apassDR_10"))
      , "/fastData/tmp/apass_DR10/split/")
  }

  //---------------------------------------------------------------------------
  def importCarmencita() = {
    CarmencitaImporter("/home/rafa/Downloads/work_with_users/matilde/carmencita/")
  }

  //---------------------------------------------------------------------------
  def importMboss() = {
    //http://www.eso.org/~ohainaut/MBOSS/mbossTables.html
    val conf = MyConf(MyConf.c.getString("Database.mboss"))
    MbossImporter(
        conf
      , "/home/rafa/Downloads/work_with_users/rafa/database/mboss/")
  }
  //---------------------------------------------------------------------------
  def gaiaQuery( ) = {
    //com.henosis.algoritms.query.gaiaDR_3.Query.matilde_1()
    com.henosis.algoritms.query.gaiaDR_3.Query.matilde_2()
  }
  //---------------------------------------------------------------------------
  def importWISE(): Unit = {


    //source
    LineEntry.resetID()
    WiseSourceImporter(
      MyConf(MyConf.c.getString("Database.wise"))
      , "/mnt/uxmal_groups/mpo/tmp/wise/source/irsa.ipac.caltech.edu/data/download/wise-allwise/")


    /*

    //photometry. required 1.2 PetaBytes
    LineEntry.resetID()
    WisePhotometryImporter(
      MyConf(MyConf.c.getString("Database.wise"))
      , "/mnt/uxmal_groups/mpo/tmp/wise/photometry/irsa.ipac.caltech.edu/data/download/wise-allwise-mep/")
   */


  //all sky
    LineEntry.resetID()
    WiseAllSkyImporter(
      MyConf(MyConf.c.getString("Database.wise"))
      , "/mnt/uxmal_groups/mpo/tmp/wise/all-sky/irsa.ipac.caltech.edu/data/download/wise-allsky/")



    //reject
    LineEntry.resetID()
    WiseRejectImporter(
      MyConf(MyConf.c.getString("Database.wise"))
      , "/mnt/uxmal_groups/mpo/tmp/wise/rejected/irsa.ipac.caltech.edu/data/download/wise-allwise/reject/")

  }
  //---------------------------------------------------------------------------
  def importGaiaCrossMatchHipparcos() =
    com.henosis.database.gaia.crossMatch.hipparcos.HipparcosDB_Importer(MyConf(MyConf.c.getString("Database.gedr3"))
      , "/home/rafa/Downloads/work_on_users/matilde/catalogs/hipparcos/gaia_cross_match/")
  //---------------------------------------------------------------------------
  def importHipparcos(): Unit = {
    HipparcosImporter(
      MyConf(MyConf.c.getString("Database.hipparcos"))
      , "/home/rafa/Downloads/work_on_users/matilde/catalogs/hipparcos/")
  }
  //---------------------------------------------------------------------------
  def importGaiaCrossMatch2Mass() =
    com.henosis.database.gaia.crossMatch.twoMass.TwoMassDB_Importer(MyConf(MyConf.c.getString("Database.2Mass"))
      , "/home/rafa/uxmal/rafa_uxmal/Downloads/catalogs/cdn.gea.esac.esa.int/GaiaDR3/gedr3/cross_match/tmasspscxsc_best_neighbour/")
  //---------------------------------------------------------------------------
  def importGaiaCrossMatchPanstarrs() =
    com.henosis.database.gaia.crossMatch.panSTARRS.PanSTARRS_DB_Importer(MyConf(MyConf.c.getString("Database.panSTARRS"))
      , "/home/rafa/Downloads/cdn.gea.esac.esa.int/GaiaDR3/gedr3/cross_match/panstarrs1_best_neighbour"
    )
  //---------------------------------------------------------------------------
  def importGaiaDR_4_Source() =
    com.henosis.database.gaia.source.gaiaDR4.gaia_source.DB_Importer(MyConf(MyConf.c.getString("Database.gaia_dr_4"))
      , "/home/rafa/Downloads/deleteme/kkkk/")
  //---------------------------------------------------------------------------
  def importGaia_DR_3_sso_source() =
    com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_source.DB_Importer(MyConf(MyConf.c.getString("Database.gaia_dr_3_sso_source"))
      , "/home/rafa/Downloads/work_on_users/rene/cdn.gea.esac.esa.int/GaiaDR3/gdr3/Solar_system/sso_source/")
  //---------------------------------------------------------------------------
  def importGaia_DR_3_sso_observation() =
    com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_observation.DB_Importer(MyConf(MyConf.c.getString("Database.gaia_dr_3_sso_observation"))
      , "/home/rafa/Downloads/work_on_users/rene/cdn.gea.esac.esa.int/GaiaDR3/gdr3/Solar_system/sso_observation/")

  //---------------------------------------------------------------------------
  def importGaia_DR_3_sso_reflectance_spectrum() =
    com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_reflectance_spectrum.DB_Importer(MyConf(MyConf.c.getString("Database.gaia_dr_3_sso_reflectance_spectrum"))
      , "/home/rafa/Downloads/work_on_users/rene/cdn.gea.esac.esa.int/GaiaDR3/gdr3/Solar_system/sso_reflectance_spectrum/")
  //---------------------------------------------------------------------------
  def importGaia_DR_3_synthetic_photometry_gspc() =
    com.henosis.database.gaia.source.gaiaDR3.performance_verification.synthetic_photometry_gspc.DB_Importer(MyConf(MyConf.c.getString("Database.gaia_dr_3_synthetic_photometry_gspc"))
      , "/home/rafa/Downloads/work_on_users/rene/cdn.gea.esac.esa.int/GaiaDR3/gdr3/Performance_verification/synthetic_photometry_gspc/")

  //---------------------------------------------------------------------------
  def importGaia_DR_3_astrophysical_parameters() =
    com.henosis.database.gaia.source.gaiaDR3.astrophysical.astrophysical_parameters.DB_Importer(MyConf(MyConf.c.getString("Database.gaia_dr_3_astrophysical_parameters"))
    //, "/home/rafa/Downloads/deleteme/deleteme/")
    , "/media/data/tmp/cdn.gea.esac.esa.int/Gaia/gdr3/Astrophysical_parameters/astrophysical_parameters/")
  //---------------------------------------------------------------------------
  def landolt_2009_CrossMatch(dbHub: DatabaseHub): Unit = {
    val algorithmsConfDir = MyConf.c.getString("Algorithms.configurationDir")
    val conf = MyConf(algorithmsConfDir + "/star/" + "landolt_2009.conf")
    Landolt_2009(dbHub
      , "output/landot_2009_cross_match.csv"
      , conf)
      .crossMatch(dbHub.landolt_2009_DB)
  }
  //---------------------------------------------------------------------------
  def landolt_2013_CrossMatch(dbHub: DatabaseHub): Unit = {
    val algorithmsConfDir = MyConf.c.getString("Algorithms.configurationDir")
    val conf = MyConf(algorithmsConfDir + "/star/" + "landolt_2013.conf")
    Landolt_2013(dbHub
      , "output/landot_2013_cross_match.csv"
      , conf)
      .crossMatch(dbHub.landolt_2013_DB)
  }
  //---------------------------------------------------------------------------
  def stauffer_2010_CrossMatch(dbHub: DatabaseHub): Unit = {
    val algorithmsConfDir = MyConf.c.getString("Algorithms.configurationDir")
    val conf = MyConf(algorithmsConfDir + "/star/" + "stauffer_2010.conf")
    Stauffer_2010(dbHub
      , "output/stauffer_2010_cross_match.csv"
      , conf)
      .crossMatch(dbHub.stauffer_2010_DB)
  }
  //---------------------------------------------------------------------------
  def popescu_2018_CrossMatch(dbHub: DatabaseHub): Unit = {
    val algorithmsConfDir = MyConf.c.getString("AsteroidsCrossMatch.configurationDir")
    val conf = MyConf(algorithmsConfDir + "/mpo/" + "popescu_2018.conf")
    Popescu_2018(dbHub
      , "output/popescu_2018_cross_match.csv"
      , conf)
      .crossMatch(dbHub.popescu_2018_DB)
  }

  //---------------------------------------------------------------------------
  def alvarez_2021(dbHub: DatabaseHub): Unit = {
    val algorithmsConfDir = MyConf.c.getString("Algorithms.configurationDir")
    val conf = MyConf(algorithmsConfDir + "/mpo/" + "Alvarez_2021.conf")
    Alvarez_2021(dbHub
      , "output/alvarez_2021.csv"
      , conf)
  }
  //---------------------------------------------------------------------------
  def smart_2021(dbHub: DatabaseHub): Unit = {
    val algorithmsConfDir = MyConf.c.getString("Algorithms.configurationDir")
    val conf = MyConf(algorithmsConfDir + "/star/" + "smart_2021.conf")
    Smart_2021(dbHub
      , "output/smart_2021.csv"
      , conf)
      .crossMatch(dbHub.smart_2021_DB)
  }

  //---------------------------------------------------------------------------
  def gaia_dr_3_sso_observation(dbHub: DatabaseHub): Unit = {
    Spice.init()
    com.henosis.algoritms.gaia.gaia_dr_3.solarSystem.Observation()
    Spice.close()
  }
  //---------------------------------------------------------------------------
  def importPaperLandolt_2009(db: MongoDB) =
    Landolt_2009_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/matilde/paper/landolt/2009/table2_and_part_of_5.dat")
  //---------------------------------------------------------------------------
  def importPaperLandolt_2013(db: MongoDB) =
    Landolt_2013_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/matilde/paper/landolt/2013/table2_y_3_with_2MASS_names_and_proper_motions.dat")
  //---------------------------------------------------------------------------
  def importStauffer_2010(db: MongoDB) =
    Stauffer_2010_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/matilde/paper/stauffer/2010/table1.dat")
  //---------------------------------------------------------------------------
  def importTorres_2019(db: MongoDB) =
    Torres_2019_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/matilde/paper/torres_2019/table3.dat")
  //---------------------------------------------------------------------------
  def importPopescu_2018(db: MongoDB) =
    Popescu_2018_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/alvaro/paper/movis-popescu/2018/MOVIS-CTax.csv")
  //---------------------------------------------------------------------------
  def importColazo_2021(db: MongoDB) =
    Colazo_2021_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/alvaro/paper/colazo/2021/H-G_gaia_gmag.csv")
  //---------------------------------------------------------------------------
  def importMahlke_2021(db: MongoDB) =
    Mahlke_2021_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/alvaro/paper/mahlke/2021/ATLAS-phase.dat")
  //---------------------------------------------------------------------------
  def importOszkiewicz_2011(db: MongoDB) =
    Oszkiewicz_2011_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/alvaro/paper/oszkiewicz/2011/aams.csv")
  //---------------------------------------------------------------------------
  def importVeres_2015(db: MongoDB) =
    Veres_2015_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/alvaro/paper/veres/2015/PS1_aster_dat_V1.0.csv")
  //---------------------------------------------------------------------------
  def importCarry_2016(db: MongoDB) = {
    // Carry_2016_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/alvaro/paper/svomoc/svomoc.csv")
    Carry_2016_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/alvaro/paper/svomoc-carry/svomoc_unique.csv")
  }
  //---------------------------------------------------------------------------
  def importSmart_2021(db: MongoDB) =
    Smart_2021_DB_Importer.importData(db, "/home/rafa/Downloads/work_on_users/matilde/paper/smart/2021/table1c.dat")

  //---------------------------------------------------------------------------
  def importCandal_2019(db: MongoDB) =
    Candal_2019_DB_Importer.importData(db, "/home/rafa/Downloads/work_with_users/alvaro/asteroid/database/abs_colours.dat")

  //---------------------------------------------------------------------------
  def importPeixhino_2015(db: MongoDB) =
    Peixhino_2015_DB_Importer.importData(db, "/home/rafa/Downloads/work_with_users/estela/databases/peixhno/tables.csv")

  //---------------------------------------------------------------------------
  def crossMatchApassDR_10_GaiaDR_3() = {

    info(s"\tApass DR10 cross match Gaia DR3")

   val oDir = "output/apassDR_10_crosmatch_gaiaDR_3"
   val radiusNoPmMas = 30 * 10000
   val radiusMas = 1000
   val observingDate = "2000-01-01"
   val minID = 1
   val maxID = 132361259
   val idSlice = 100 * 1000
   val idIterationCount = maxID / idSlice
   val idRemainIteration = maxID / idSlice
   var startID =  minID

   for(i<-1 to idIterationCount) {
     val endID = startID + idSlice - 1
     info(s"Iteration: $i of $idIterationCount from id: $startID to: $endID")
     val querySeq = QueryItemApass.loadDatabase(startID, endID)
     ApassGaiaDR_3(
       Path.resetDirectory(oDir + "/" + startID + "-" + endID )
       , Conversion.mas_to_DD(radiusNoPmMas)
       , Conversion.mas_to_DD(radiusMas)
       , observingDate + "T00:00:00"
       , querySeq)
     startID += idSlice
    }
    //remain id
    if (idRemainIteration > 0) {
      info(s"Last iteration processing remain ids:$idRemainIteration from id: $startID to: $maxID")
      val querySeq = QueryItemApass.loadDatabase(startID, maxID)
      ApassGaiaDR_3(
        Path.resetDirectory(oDir + "/" + startID + "-" + maxID)
        , Conversion.mas_to_DD(radiusNoPmMas)
        , Conversion.mas_to_DD(radiusMas)
        , observingDate + "T00:00:00"
        , querySeq)
    }
  }
  //---------------------------------------------------------------------------
  def hdf5JoinCsv() {

    val inputDir =  "/mnt/uxmal_groups/stars/claret/atmosphere/csv/"
    //val inputDir = "/home/rafa/Downloads/deleteme/kk4/"
    val outputDir = "/mnt/uxmal_groups/stars/claret/atmospherec_out/"

    com.henosis.algoritms.claret.hdf5.JoinCsv.process(inputDir, outputDir)
  }

  //---------------------------------------------------------------------------
  private def pdsLCDB() {
    val dir = "/home/rafa/Downloads/work_with_users/rafa/database/lcdb/pds/ast-lightcurve-database_V4_0/data_color_index/"
    val conf = MyConf(MyConf.c.getString("Database.lcdbColorIndex"))
    com.henosis.database.lcdb.pds.colorIndex.LcdbColorindexImporter(conf, dir)
  }

  //---------------------------------------------------------------------------
  private def mpcLCDB() {


    //https://ssd.jpl.nasa.gov/tools/sbdb_lookup.html#/?sstr=pholus
    //ASTEROID LIGHTCURVE DATABASE(LCDB) COLOR INDEX(Complete)
    //GENERATED: Oct 01 , 2023
    val dir = "/home/rafa/Downloads/work_with_users/rafa/database/lcdb/mpc/"

    //download latest
    /*
    val zipFile = "LCLIST_PUB_CURRENT.zip"
    val command = s"cd $dir;rm $zipFile;wget https://minplanobs.org/mpinfo/datazips/$zipFile;unzip $zipFile"

    val processBuilder = sys.process.Process(Seq("bash", "-c", s"$command"))
    processBuilder.!
     */

    val conf = MyConf(MyConf.c.getString("Database.lcdbColorIndex"))
    com.henosis.database.lcdb.mpc.colorIndex.LcdbColorindexImporter(conf, dir)
    println()
  }
  //---------------------------------------------------------------------------
  def lcdb() {
    //pdsLCDB
    mpcLCDB
  }
  //---------------------------------------------------------------------------
  def tnoColorImporter(dbHub: DatabaseHub) = {

    TnoColorImporter.importData(
        dbHub.mpoDB
       , dbHub.mpoDB.tnoColor
       ,"/home/rafa/Downloads/work_with_users/rafa/database/tno_cent_colors/EAR_A_COMPIL_3_TNO_CEN_COLOR_V10_0/subdir_data/tnocencol/tnocencol.tab"
      , isMultiRun = false)

    TnoColorImporter.importData(
      dbHub.mpoDB
      , dbHub.mpoDB.tnoColor
      , "/home/rafa/Downloads/work_with_users/rafa/database/tno_cent_colors/EAR_A_COMPIL_3_TNO_CEN_COLOR_V10_0/subdir_data/multirun/multirun.tab"
      , isMultiRun = true
      , dropCollection = false)
  }
  //---------------------------------------------------------------------------
  def crossMatchVR(dbHub: DatabaseHub) = {
    CrossMatchVR(dbHub).create()
  }

  //---------------------------------------------------------------------------
  def checkLandolt() = {
    val db = LandoltDB()
    val b = db.getStandardStar("GD 226", 1.8590583333333333, 1.8590583333333333 ,33.30532777777778, 33.30532777777778)
    println()
  }
  //---------------------------------------------------------------------------
  def postgre(): Unit = {
    import scalasql.Sc
    val conf = MyConf(MyConf.c.getString("Database.gaia_dr_3"))

    val postgre = PostgreScalaDB(MyConf.c.getString("Database.postgre.url")
                            , conf.getString("Database.name"))

//    val r = postgre.rowCount(SourceMinimal.tableName)

/*
    postgre.createIndex(SourceMinimal.tableName
                         , Array(SourceMinimal.RA_COL_NAME,SourceMinimal.DEC_COL_NAME))
*/

    postgre.createIndex(SourceReduced.tableName
                        , Array(SourceMinimal.PARALLAX_COL_NAME)
                        , unique = false)

  }
  //---------------------------------------------------------------------------
  def getImageFocus() = {

    val mpcID = 727813

    val mpo = ImageFocusMPO.build(
        id = mpcID.toString
      , allowSPK_NotStoredInDB = true)

    println()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SandBox.scala
//=============================================================================
