/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  09/Feb/2022
  * Time:  21h:22m
  * Description: None
  */
//=============================================================================
package com.henosis.ephemeris
//=============================================================================
import org.apache.commons.math3.util.{FastMath, MathUtils}
//=============================================================================
object KeplerianOrbit {
  //---------------------------------------------------------------------------
  //http://bhavik3.github.io/result_Orekit/xref/Orekit/src/main/java/org/orekit/orbits/KeplerianOrbit.html
  private val k1 = 3 * FastMath.PI + 2
  private val k2 = FastMath.PI - 1
  private val k3 = 6 * FastMath.PI - 1
  private val A  = 3 * k2 * k2 / k1
  private val B  = k3 * k3 / (6 * k1)
  //---------------------------------------------------------------------------
  //http://bhavik3.github.io/result_Orekit/xref/Orekit/src/main/java/org/orekit/orbits/KeplerianOrbit.html
  private def eMeSinE(E: Double, e: Double) = {
    var x = (1 - e) * FastMath.sin(E)
    val mE2 = -E * E
    var term = E
    var d = 0
    // the inequality test below IS intentional and should NOT be replaced by a check with a small tolerance
    var x0 = Double.NaN
    while ( {
      x != x0
    }) {
      d += 2
      term *= mE2 / (d * (d + 1))
      x0 = x
      x = x - term
    }
    x
  }
  //---------------------------------------------------------------------------
  //http://bhavik3.github.io/result_Orekit/xref/Orekit/src/main/java/org/orekit/orbits/KeplerianOrbit.html
  //M: Mean anomaly (in radians)
  //e: eccentricity (no units)
  def estimateEccentricAnomaly(M: Double, e: Double) = {
    val reducedM = MathUtils.normalizeAngle(M, 0.0) // reduce M to [-PI PI) interval
    // compute start value according to A. W. Odell and R. H. Gooding S12 starter
    var E = .0
    if (FastMath.abs(reducedM) < 1.0 / 6.0) E = reducedM + e * (FastMath.cbrt(6 * reducedM) - reducedM)
    else if (reducedM < 0) {
      val w = FastMath.PI + reducedM
      E = reducedM + e * (A * w / (B - w) - FastMath.PI - reducedM)
    }
    else {
      val w = FastMath.PI - reducedM
      E = reducedM + e * (FastMath.PI - A * w / (B - w) - reducedM)
    }
    val e1 = 1 - e
    val noCancellationRisk = (e1 + E * E / 6) >= 0.1
    // perform two iterations, each consisting of one Halley step and one Newton-Raphson step
    for (j <- 0 until 2) {
      var f = .0
      var fd = .0
      val fdd = e * FastMath.sin(E)
      val fddd = e * FastMath.cos(E)
      if (noCancellationRisk) {
        f = (E - fdd) - reducedM
        fd = 1 - fddd
      }
      else {
        f = eMeSinE(E,e) - reducedM
        val s = FastMath.sin(0.5 * E)
        fd = e1 + 2 * e * s * s
      }
      val dee = f * fd / (0.5 * f * fdd - fd * fd)
      // update eccentric anomaly, using expressions that limit underflow problems
      val w = fd + 0.5 * dee * (fdd + dee * fddd / 3)
      fd += dee * (fdd + 0.5 * dee * fddd)
      E -= (f - dee * (fd - w)) / fd
    }
    // expand the result back to original range
    E += M - reducedM
    E
  }
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/5287814/solving-keplers-equation-computationally
  // am=mean anomaly
  // ec=eccentricity
  // dp=number of decimal places
  def solveIteratively(am: Double,ec: Double,dp:Double = 10,_maxiter: Int = 100) = {

    var i=0
    val delta=Math.pow(10,-dp)
    var E = 0D
    var F = 0D

    // some attempt to optimize prediction
    if (ec<0.8) E=am
    else E= am + Math.sin(am)
    F = E - ec*Math.sin(E) - am
    while ((Math.abs(F)>delta) && (i<_maxiter)) {
      E = E - F/(1.0-(ec* Math.cos(E) ))
      F = E - ec * Math.sin(E) - am
      i = i + 1
    }
    Math.round(E*Math.pow(10,dp))/Math.pow(10,dp)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file KeplerianOrbit.scala
//=============================================================================
