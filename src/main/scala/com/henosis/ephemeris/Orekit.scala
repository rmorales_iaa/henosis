/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  17/Feb/2022
  * Time:  00h:29m
  * Description: None
  */
//=============================================================================
package com.henosis.ephemeris
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.common.jpl.horizons.Horizons
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.util.time.Time
import com.common.util.util.Util
import com.henosis.ephemeris.OrbitalElement.getObliquityOfTheEclipticAngle
//=============================================================================
import org.apache.commons.math3.util.FastMath.{cos, sin, toRadians}
import org.hipparchus.ode.nonstiff.DormandPrince853Integrator
import org.hipparchus.util.FastMath
import org.orekit.bodies.CelestialBodyFactory
import org.orekit.errors.OrekitException
import org.orekit.forces.gravity.{NewtonianAttraction, ThirdBodyAttraction}
import org.orekit.propagation.numerical.NumericalPropagator
import org.orekit.orbits.{KeplerianOrbit, Orbit, OrbitType, PositionAngleType}
import org.orekit.propagation.SpacecraftState
import org.orekit.time.{AbsoluteDate, TimeScalesFactory}
import org.orekit.utils.Constants
import org.orekit.data.{DataContext, DirectoryCrawler}
import java.io.{BufferedWriter, File, FileWriter, PrintWriter}
import java.time.LocalDateTime
import scala.io.Source
//=============================================================================
//=============================================================================
object Orekit extends  MyLogger {
  //---------------------------------------------------------------------------
  private val au2m =  Constants.IAU_2012_ASTRONOMICAL_UNIT            // astronomical unit to meters
  //---------------------------------------------------------------------------
  private final val MKSPK = MyConf.c.getString("JplSpice.mkspk")
  //---------------------------------------------------------------------------
  def init() = {
    //https://www.orekit.org/site-orekit-tutorials-10.3/tutorials/propagation.html
    info("Initializing orekit")
    val orekitData = new File(MyConf.c.getString("Orekit.path"))
    val manager = DataContext.getDefault.getDataProvidersManager
    manager.addProvider(new DirectoryCrawler(orekitData))
  }
  //---------------------------------------------------------------------------
  private def addPerturberSeq(orb: Orbit, propagator: NumericalPropagator): Unit = {
    propagator.addForceModel(new NewtonianAttraction(orb.getMu))
    propagator.addForceModel(new ThirdBodyAttraction(CelestialBodyFactory.getEarthMoonBarycenter))
    propagator.addForceModel(new ThirdBodyAttraction(CelestialBodyFactory.getMars))
    propagator.addForceModel(new ThirdBodyAttraction(CelestialBodyFactory.getJupiter))
    propagator.addForceModel(new ThirdBodyAttraction(CelestialBodyFactory.getSaturn))
    propagator.addForceModel(new ThirdBodyAttraction(CelestialBodyFactory.getUranus))
    propagator.addForceModel(new ThirdBodyAttraction(CelestialBodyFactory.getNeptune))
    propagator.addForceModel(new ThirdBodyAttraction(CelestialBodyFactory.getPluto))
  }
  //---------------------------------------------------------------------------
  def getIntegrator(orb: Orbit, orbitType: OrbitType) = {
    val tolerance = NumericalPropagator.tolerances(0.001, orb, orbitType)
    val integrator = new DormandPrince853Integrator(1.000, 30 * 86400.0, tolerance(0), tolerance(1))
    integrator.setInitialStepSize(86400.0)
    integrator
  }
  //---------------------------------------------------------------------------
  @throws[OrekitException]
  private def getPropagator(orb: Orbit,  orbitType: OrbitType) = {
    val integrator = getIntegrator(orb, orbitType)
    val propagator = new NumericalPropagator(integrator)
    propagator.setOrbitType(OrbitType.CARTESIAN)
    addPerturberSeq(orb, propagator)
    propagator.setInitialState(new SpacecraftState(orb))
    propagator
  }
  //---------------------------------------------------------------------------
  //(x,y,z) all in au
  def getHeliocentricEquatorialCoordinates(timestamp: LocalDateTime, x: Double ,y: Double ,z: Double) = {
    //it is just a rotation on X axis (X is fixed, Y and Z axis are rotated) of the obliquity of the ecliptic applied on heliocentric ecliptic coordinates
    val obEc = toRadians(getObliquityOfTheEclipticAngle(timestamp))
    val sinObEc = sin(obEc)
    val cosObEc = cos(obEc)

    //Celestial mechanics. Jeremy Tatum. Eq. 10.7.10
    val y2 = (y * cosObEc) + (-sinObEc * z)
    val z2 = (y * sinObEc) + (cosObEc * z)
    (x,y2,z2)
  }
  //---------------------------------------------------------------------------
  private def generateSPK(mpoName: String, csvFilename: String, skpID: Long, spkFileName: String, spkConfFileName: String) = {

    val absolutePath = Path.getCurrentPath() + "/"
    val pw = new PrintWriter(new File(spkConfFileName))

    pw.write("""\begindata""" + "\n")
    pw.write("INPUT_DATA_TYPE   = 'STATES'\n")
    pw.write("OUTPUT_SPK_TYPE   = 12\n")
    pw.write(s"OBJECT_ID         = $skpID\n")
    pw.write(s"OBJECT_NAME       = '$mpoName'\n")
    pw.write("CENTER_ID         = 10\n")
    pw.write("CENTER_Name       = 'SUN'\n")
    pw.write("REF_FRAME_NAME    = 'J2000'\n")
    pw.write("PRODUCER_ID       = 'rmorales@iaa.es IAA-CSIC'\n")
    pw.write("DATA_ORDER        = 'EPOCH X Y Z VX VY VZ'\n")
    pw.write("INPUT_DATA_UNITS  = ('ANGLES=DEGREES' 'DISTANCES=KM')\n")
    pw.write("DATA_DELIMITER    = ' '\n")
    pw.write("LINES_PER_RECORD  = 1\n")
    pw.write("IGNORE_FIRST_LINE = 0\n")
    pw.write("LEAPSECONDS_FILE  = '/home/rafa/proyecto/m2/input/spice/kernels/lsk/latest_leapseconds.tls'\n")
    pw.write("PCK_FILE          = '/home/rafa/proyecto/m2/input/spice/kernels/pck/earth_latest_high_prec.bpc'\n")
    pw.write("POLYNOM_DEGREE    = 7\n")
    pw.write(s"SEGMENT_ID        = '${mpoName}_SPK_STATES_12'\n")
    pw.write("""\begintext""" + "\n")

    pw.close()

    MyFile.deleteFileIfExist(absolutePath + spkFileName)
    Util.runShellSimple(s"$MKSPK -setup $absolutePath$spkConfFileName -input $absolutePath$csvFilename -output $absolutePath$spkFileName")
  }
  //---------------------------------------------------------------------------
  //https://forum.orekit.org/t/generating-a-jpl-spice-kernel-using-orekit-ephemeris/1531/6
  def generateSpk(dbHub: DatabaseHub
                  , mpcID: Int
                  , _startDate: LocalDateTime
                  , _endDate: LocalDateTime
                  ,  dateStepInSeconds: Int = Time.SECONDS_IN_ONE_DAY //
                  , outputDir: String  = "output/spk"): Unit = {
    //-----------------------------------------------------------------
    val mpo = dbHub.mpcDB.getOrbitalElement(mpcID).get
    //-----------------------------------------------------------------
    val spkID= Horizons().getSPK_ID_ByMpcID(mpcID)
    val dSpk = Path.resetDirectory(outputDir)
    val dTmp = Path.resetDirectory(s"$outputDir/tmp/")
    val ephemerisCsvFileName    = s"$dTmp$mpcID.csv"
    val spkConfFileName         = s"$dTmp$mpcID.conf"
    val mpoName                 = mpo.mpcDesignation.replaceAll(" ","_")
    val spkFileName             = s"$dSpk${spkID}_$mpoName.bsp"  //bsp is the binary form of a SPK
    //-----------------------------------------------------------------
    val a = mpo.a * au2m                                   // semi-major axis [m]
    val e = mpo.e                                          // orbital eccentricity
    val i    = FastMath.toRadians(mpo.i)                   // inclination [rad]
    val peri = FastMath.toRadians(mpo.w)                   // argument of perihelion [rad]
    val node = FastMath.toRadians(mpo.N)                   // longitude of the ascending node [rad]
    val m    = FastMath.toRadians(mpo.M)                   //  mean anomaly [rad]
    val ep   = Time.fromJulian(mpo.epoch)                  // epoch of the orbital elements
    //-----------------------------------------------------------------
    val timeScale   = TimeScalesFactory.getTDB
    val sunBody     = CelestialBodyFactory.getSun                      // heliocentric
    val mu          = sunBody.getGM()                                  // central attraction coefficient (m³/s²)
    val anomalyType = PositionAngleType.MEAN
    val epoch       = new AbsoluteDate(ep.getYear, ep.getMonthValue, ep.getDayOfMonth, ep.getHour, ep.getMinute, ep.getSecond, timeScale)
    //-----------------------------------------------------------------
    val startDate   = new AbsoluteDate(_startDate.toString, timeScale)
    val endDate     = new AbsoluteDate(_endDate.toString, timeScale)
    val dateStep    = dateStepInSeconds
    //-----------------------------------------------------------------
    //sun-center ecliptic coordinate system (inertial frame)
    val inertialFrame =  sunBody.getInertiallyOrientedFrame()
    //-----------------------------------------------------------------
    //orbit
    val orbit = new KeplerianOrbit(a, e, i, peri, node, m, anomalyType, inertialFrame, epoch, mu);
    //-----------------------------------------------------------------
    //propagator
    val ephemerisPropagator = getPropagator(orbit, OrbitType.KEPLERIAN)
    //-----------------------------------------------------------------
    info(s"Generating the ephemeris file:'$ephemerisCsvFileName'")
    //csv ephemeris generation
    val pw = new PrintWriter(new File(ephemerisCsvFileName))
    var currentDate = startDate
    while (currentDate.isBefore(endDate)) {
      val state = ephemerisPropagator.propagate(currentDate).getPVCoordinates
      val p = state.getPosition //m
      val v = state.getVelocity //m/s
      val timeStamp = LocalDateTime.parse(currentDate.toString.take(19))

      //in KM
      val (x,y,z) = getHeliocentricEquatorialCoordinates(
        timeStamp
        , p.getX / 1000
        , p.getY / 1000
        , p.getZ / 1000)

      //in KM/s
      val (vx,vy,vz) = getHeliocentricEquatorialCoordinates(
        timeStamp
        , v.getX  / 1000
        , v.getY  / 1000
        , v.getZ  / 1000)

      //write position in au, speed in au/s
      val line = timeStamp +
        s" $x $y $z " +
        s" $vx $vy $vz"

      pw.write(line + "\n")
      currentDate = currentDate.shiftedBy(dateStep)
    }
    pw.close()
    info(s"Generating the spk file:'$ephemerisCsvFileName'")
    generateSPK(mpoName, ephemerisCsvFileName, spkID, spkFileName, spkConfFileName)
  }
  //---------------------------------------------------------------------------
  def processJplVectorTable() = {
    val in = "/home/rafa/Downloads/horizons_results.txt"
    val out = "/home/rafa/proyecto/henosis/output/spk/tmp/470599.csv"
    val bufferedSource = Source.fromFile(in)
    //-------------------------------------------------------------------------
    def process(): Unit = {
      val bw = new BufferedWriter(new FileWriter(new File(out)))
      bufferedSource.getLines.toArray.drop(37).foreach { line=>
        if(line.contains("$$EOE")) return
        val split  = line.trim.replaceAll("\\s +", " ").split(",")
        bw.write(split(1)
          .replace("A.D. ","")
          .replace("-"," ")+ " ")
        bw.write(split(2) + " ")
        bw.write(split(3) + " ")
        bw.write(split(4) + " ")
        bw.write(split(5) + " ")
        bw.write(split(6) + " ")
        bw.write(split(7)+ "\n")
      }
      bw.close()
    }
    //-------------------------------------------------------------------------
    process()
    bufferedSource.close
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Orekit.scala
//=============================================================================
