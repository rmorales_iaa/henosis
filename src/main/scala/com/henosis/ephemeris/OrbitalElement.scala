/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  09/Feb/2022
  * Time:  12h:10m
  * Description: Based on: //https://www.stjarnhimlen.se/comp/ppcomp.html
  */
//=============================================================================
package com.henosis.ephemeris
//=============================================================================
import com.common.logger.MyLogger
import com.common.util.time.Time
import com.common.util.util.Util
import org.apache.commons.math3.util.FastMath._
import org.orekit.bodies.CelestialBodyFactory
import org.orekit.frames.FramesFactory
import org.orekit.time.{AbsoluteDate, TimeScalesFactory}
import org.orekit.utils.Constants
//=============================================================================
import java.time.LocalDateTime
//=============================================================================
object OrbitalElement extends MyLogger{
  //---------------------------------------------------------------------------
  //https://en.wikipedia.org/wiki/IAU_(1976)_System_of_Astronomical_Constants
  private final val OBLIQUITY_OF_THE_ECLIPTIC = 23.439291d //23°26'21.448"
  private val au2m =  Constants.IAU_2012_ASTRONOMICAL_UNIT
  private val Sun = CelestialBodyFactory.getSun
  //---------------------------------------------------------------------------
  //inclination of Earth's equator (projected on celestial sphere) with respect to the ecliptic
  //https://en.wikipedia.org/wiki/Ecliptic
  def getObliquityOfTheEclipticAngle(timestamp: LocalDateTime) = OBLIQUITY_OF_THE_ECLIPTIC - 3.563E-7 * Time.getTimeScale(timestamp)
  //---------------------------------------------------------------------------
  //http://www.stjarnhimlen.se/comp/tutorial.html
  def normalizeAngle(x: Double) = {
    val r = x - Util.truncate(x / 360d) * 360d
    if (r < 0d) r + 360 else r
  }
  //---------------------------------------------------------------------------
  //(x,y,z) all in au
  //sun coordinates for the timeStamp referred to mean equator and  equinox J2000.0
  def getSunPosition(t: LocalDateTime) = {
    val epoch = new AbsoluteDate(t.getYear, t.getMonthValue, t.getDayOfMonth, t.getHour, t.getMinute, t.getSecond, TimeScalesFactory.getUTC)
    val p = Sun.getPVCoordinates(epoch, FramesFactory.getEME2000).getPosition
    (p.getX / au2m, p.getY / au2m, p.getZ / au2m)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import OrbitalElement._
case class OrbitalElement(  N: Double     //longitude of the ascending node                                 (deg)
                          , i: Double     //inclination to the ecliptic (plane of the Earth's orbit)        (deg)
                          , w: Double     //argument (aka longitude) of perihelion                          (deg)
                          , M: Double     //mean anomaly (0 at perihelion increases uniformly with time)    (deg)
                          , a: Double     //semi-major axis, or mean distance from Sun (aka mean distance)  (au)
                          , e: Double     //eccentricity (0=circle, 0-1=ellipse, 1=parabola)                (no units)
                          , epoch: Double //epoch of the osculating elements                                (Julian day)
                          , meanMotion    : Double  = Double.NaN //Optional: mpc mean motion                (degrees per sidereal day)
                          , mpcDesignation: String  = "" //Optional: mpc main designation
                          , mpcID         : String  = "" //Optional: mpc ID
                         ) {
  //---------------------------------------------------------------------------
  private val n = getMeanMotion() //in degrees per day
  private var directionCosineMatrix : Array[Double] = null
  //---------------------------------------------------------------------------
  private def getDirectionCosineMatrix() = {
    if (directionCosineMatrix == null) {
      val cosN = cos(toRadians(N))
      val sinN = sin(toRadians(N))

      val cosW = cos(toRadians(w))
      val sinW = sin(toRadians(w))

      val cosI = cos(toRadians(i))
      val sinI = sin(toRadians(i))

      val ecl = toRadians(OBLIQUITY_OF_THE_ECLIPTIC)
      val sinEcl = sin(ecl)
      val cosEcl = cos(ecl)

      val a = cosN * cosW
      val b = sinN * sinW
      val c = cosN * sinW
      val d = sinN * cosW
      val f = sinN * sinI
      val j = b * cosI
      val m = sinI * sinEcl
      val s = sinI * cosEcl
      val u = cosI * sinEcl
      val h = cosI * cosEcl

      val k = c * cosI
      val l = d * cosI
      val q = a * cosI

      val n = m * sinW
      val p = m * cosW
      val v = m * cosN
      val t = s * cosW
      val y = s * cosN
      val g = s * sinW

      val Px =  a - j  //Celestial mechanics. Jeremy Tatum. Eq. 10.9.41
      val Qx = -c - l  //Celestial mechanics. Jeremy Tatum. Eq. 10.9.42
      val Rx = f       //Celestial mechanics. Jeremy Tatum. Eq. 10.9.47

      val Py = (d + k)  * cosEcl - n //Celestial mechanics. Jeremy Tatum. Eq. 10.9.43
      val Qy = (-b + q) * cosEcl - p //Celestial mechanics. Jeremy Tatum. Eq. 10.9.44
      val Ry = -y - u                 //Celestial mechanics. Jeremy Tatum. Eq. 10.9.48

      val Pz = (d + k) * sinEcl + g  //Celestial mechanics. Jeremy Tatum. Eq. 10.9.45
      val Qz = (-b + q) * sinEcl + t //Celestial mechanics. Jeremy Tatum. Eq. 10.9.46
      val Rz = -v + h                //Celestial mechanics. Jeremy Tatum. Eq. 10.9.49

      directionCosineMatrix = Array(
         Px, Qx, Rx
        ,Py, Qy, Ry
        ,Pz, Qz, Rz)
    }
    directionCosineMatrix
  }
  //---------------------------------------------------------------------------
  //Celestial mechanics. Jeremy Tatum. Eq. 10.7.1
  def getMeanMotion() = {
    if (meanMotion.isNaN) calculateMeanMotion
    else meanMotion
  }
  //---------------------------------------------------------------------------
  //Celestial mechanics. Jeremy Tatum. Eq. 10.7.1
  def calculateMeanMotion() = (360 / Math.sqrt(a * a * a)) / Time.SIDEREAL_YEAR_PER_YEAR
  //---------------------------------------------------------------------------
  def get_w1 = N + w                      //longitude of perihelion
  //---------------------------------------------------------------------------
  def get_L  = M + get_w1                 //mean longitude
  //---------------------------------------------------------------------------
  def get_q  = a * (1-e)                  //perihelion distance
  //---------------------------------------------------------------------------
  def get_Q  = a * (1+e)                  //aphelion distance
  //---------------------------------------------------------------------------
  def get_P  = Math.pow(a ,1.5)           //orbital period (years if a is in AU, astronomical units)
  //---------------------------------------------------------------------------
  def get_T  = epoch - ((M/360d) / get_P) //time of perihelion
  //---------------------------------------------------------------------------
  //Celestial mechanics. Jeremy Tatum. Eq. 10.7.1
  //result in degrees
  private def getMeanAnomalyAt(timestamp: LocalDateTime) =
    normalizeAngle((n * (Time.toJulian(timestamp) - epoch)) + M)
  //---------------------------------------------------------------------------
  //result in radians!
  private def getEccentricAnomaly(timestamp: LocalDateTime) =
    KeplerianOrbit.estimateEccentricAnomaly(toRadians(getMeanAnomalyAt(timestamp))
                                          , e)
  //---------------------------------------------------------------------------
  //(TrueAnomaly(degrees),heliocentric distance(au))
  private def getTrueAnomalyAndHelioCentricDistance(timestamp: LocalDateTime) = {
    val E = getEccentricAnomaly(timestamp)  //E in radians!

    //Celestial mechanics. Jeremy Tatum. Eq. 2.3.17b
    val v = normalizeAngle(toDegrees(atan2(sqrt(1 - (e * e)) * sin(E), cos(E) - e)))

    //Celestial mechanics. Jeremy Tatum. Eq. 2.3.16
    val cosV = (cos(E) - e) / (1 - (e * cos(E))) //v in radians!!

    val r = a * (1 - (e * e)) / (1 + e * cosV)  //heliocentric distance
    (v,r)
  }
  //---------------------------------------------------------------------------
  //(x,y,z) all in au
  def getHeliocentricEclipticCoordinates(timestamp: LocalDateTime) = {

    val (v,r) = getTrueAnomalyAndHelioCentricDistance(timestamp) //v in degrees

    val o  = toRadians(N)
    val fi = toRadians(w) + toRadians(v) //argument of latitude in radians
    val ir = toRadians(i)

    val s_o = sin(o)
    val c_o = cos(o)

    val s_fi = sin(fi)
    val c_fi = cos(fi)

    val s_i = sin(ir)
    val c_i = cos(ir)

    val x = r * (c_o * c_fi - s_o * s_fi * c_i)  //Celestial mechanics. Jeremy Tatum. Eq. 10.7.6
    val y = r * (s_o * c_fi + c_o * s_fi * c_i)  //Celestial mechanics. Jeremy Tatum. Eq. 10.7.8
    val z = r * s_fi * s_i                       //Celestial mechanics. Jeremy Tatum. Eq. 10.7.9

    (x,y,z)  //in au
  }
  //---------------------------------------------------------------------------
  //(x,y,z) all in au
  def getHeliocentricEquatorialCoordinates(timestamp: LocalDateTime) = {
    //it is just a rotation on X axis (X is fixed, Y and Z axis are rotated) of the obliquity of the ecliptic applied on heliocentric ecliptic coordinates
    val (x,y,z) = getHeliocentricEclipticCoordinates(timestamp)
    val obEc = toRadians(getObliquityOfTheEclipticAngle(timestamp))
    val sinObEc = sin(obEc)
    val cosObEc = cos(obEc)

    //Celestial mechanics. Jeremy Tatum. Eq. 10.7.10
    val y2 = (y * cosObEc) + (-sinObEc * z)
    val z2 = (y * sinObEc) + (cosObEc * z)
    (x,y2,z2)
  }

  //---------------------------------------------------------------------------
  //(r,alfa,delta) in (au, decimal degrees, decimal degrees)
  // alfa = right ascension
  // delta = declination
  def getGeocentricRectangularCoordinates(timestamp: LocalDateTime) = {
    //it is just a translation from Sun to Earth of the mpo heliocentric equatorial coordinates

    //mpo heliocentric equatorial coordinates
    val (xAsteroid,yAsteroid,zAsteroid) = getHeliocentricEquatorialCoordinates(timestamp)

    //solar coordinates for the timeStamp referred to mean equator and equinox J2000.0
    //in the example of Jeremy Tatum Sun position at (x= −0.3861944, y= +0.8626457, z= +0.3749996)
    val (xSun,ySun,zSun) = getSunPosition(timestamp)

    //geocentric equatorial coordinates
    //Celestial mechanics. Jeremy Tatum. Eq. 10.7.11, 10.7.12 and 10.7.13
    val (x,y,z) = (xAsteroid + xSun, yAsteroid + ySun, zAsteroid + zSun)

    //Celestial mechanics. Jeremy Tatum. Eq. 10.7.14, 10.7.15 and 10.7.16
    //problems with the quadrants when calculating inverse of trigonometrics functions. See Jeremy Tatum section 10-8 Quadrant Problems
    //see https://en.wikipedia.org/wiki/Spherical_coordinate_system
    val r = sqrt((x * x) + (y * y) + (z * z))
    val alfa =  normalizeAngle(toDegrees(Math.atan2(y, x)))   //right ascension
    val delta = toDegrees(asin(z / r))                        //declination

    (r,alfa,delta)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file OrbitalElement.scala
//=============================================================================
