/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  03/dic/2018
  * Time:  01h:38m
  * Description: https://github.com/scallop/scallop
  */
//=============================================================================
package com.henosis.commandLine
//=============================================================================
import com.henosis.commandLine.CommandLineParser.COMMAND_SPK_SYNC
import com.common.util.file.MyFile.fileExist
import com.common.util.path.Path.directoryExist
import com.common.util.time.Time
import org.rogach.scallop._
//=============================================================================
//=============================================================================
//=============================================================================
object CommandLineParser {
  //---------------------------------------------------------------------------
  final val COMMAND_GAIA_3_IMPORT                   = "gaia3-import"
  final val COMMAND_SPK_SYNC                        = "spk-sync"
  final val COMMAND_MPO_STATS                       = "mpo-stats"
  final val COMMAND_QUERY_GAIA_DR3                  = "query-gaiadr3"
  //---------------------------------------------------------------------------
  final val VALID_COMMAND_SEQ = Seq(
    COMMAND_GAIA_3_IMPORT
  , COMMAND_SPK_SYNC
  , COMMAND_MPO_STATS
  , COMMAND_QUERY_GAIA_DR3)
  //---------------------------------------------------------------------------
}
//=============================================================================
//-----------------------------------------------------------------------------
import CommandLineParser._
class CommandLineParser(args: Array[String]) extends ScallopConf(args) {

  val rawVersion = "0.0.1"
  version(rawVersion)
  banner(s"""henosis syntax
           |henosis [--configuration-file configurationFile] COMMAND COMMAND_PARAMETERS
           |[$COMMAND_GAIA_3_IMPORT]
           |[$COMMAND_SPK_SYNC]
           |[$COMMAND_MPO_STATS]
           |[$COMMAND_QUERY_GAIA_DR3]
           |
           | Example 0: Import to  mongo database all csv stored in directory 'dir' containing GAIA DR 3 data
           |  java -jar henosis.jar --$COMMAND_GAIA_3_IMPORT dir
           |------------------------------------------------------------------
           | Example 0.1: Import to PostgreSQL database the csv that stores the GAIA DR 3 data
           |  java -jar henosis.jar --$COMMAND_GAIA_3_IMPORT csv  --use-postgre
           |------------------------------------------------------------------
           | Example 1: Synchronize local SPK repository with JPL remote repository (Small Body Database).
           | New versions and new SPK will be updated in local
           |  java -jar henosis.jar --$COMMAND_SPK_SYNC
           |------------------------------------------------------------------
           | Example 2: For all images stored in the MongoDB 'astrometry' find the MPO's present in each image
           | considering only the MPOs kernels stored in the database.
           |  java -jar henosis.jar --$COMMAND_MPO_STATS
           |  -----------------------------------------------------------------
           | Example 2.1: In all FITS images in the set of directories 'd1' and 'd2', astrochecker is being used to
           |  locate any possible MPO (blind mode)
           |  java -jar henosis.jar --$COMMAND_MPO_STATS --dir-seq d1 d2
           |  do not use SPK database to locate the MPO
           | ------------------------------------------------------------------
           | Example 3: Query GAIA DR3 using a CSV file 'csv'.
           |            The first two columns must contain the right ascension and declination coordinates in
           |            decimal degrees or ('hour:minute:second','degree minute second').
           |            The columns use as divider the character ','.
           |            The results will be stored in the directory 'd' (it will lost all previous content)
           |            The query used first the search radius without proper motion 'r_1' and after apply the
           |            one with proper motion 'r_2'.
           |            The date '2002-03-26' is considered the observing date of oaa csv data
           |
           |  java -jar henosis.jar --$COMMAND_QUERY_GAIA_DR3 csv --odir d --search-radius-no-pm-mas r_1 --search-radius-pm-mas r_2  --date 2002-03-26 --col-divider ","
           | ------------------------------------------------------------------
           |Options:
           |""".stripMargin)
  footer("\n'henosis' ends")

  val configurationFile = opt[String](
    default = Some("input/configuration/main.conf")
    ,  short = 'c'
    ,  descr = " configuration file\n")

  val spkSync = opt[Boolean](required = false
    , short = 's'
    , descr = "Synchronize SPK local repository with JPL's remote repository\n"
    , default = Some(false))

  val mpoStats = opt[Boolean](required = false
    , short = 'a'
    , descr = "Get the statistics of all mpo's present in the 'astrometry' database\n"
    , default = Some(false))

  val gaia3Import = opt[String](required = false
    , short = 'g'
    , descr = "Import all csv files from GAIA 3\n")

  val dir = opt[String](required = false
    , noshort = true
    , descr = "Name of a directory\n")

  val dirSeq = opt[List[String]](
    required = false
    , noshort = true
    , descr = "Sequence of directories\n")

  val odir = opt[String](required = false
    , noshort = true
    , descr = "Output directory\n")

  val csv = opt[String](required = false
    , noshort = true
    , descr = "csv file name\n")

  val colDivider = opt[String](required = false
    , noshort = true
    , descr = "column divider in the csv file\n")

  val queryGaiadr3 = opt[String](required = false
    , noshort = true
    , descr = "Query GAIA DR3 using a CSV file\n")

  val searchRadiusNoPmMas = opt[Double](required = false
    , noshort = true
    , descr = "Search radius in milli arc sec without proper motion\n")

  val searchRadiusPmMas = opt[Double](required = false
    , noshort = true
    , descr = "Search radius in milli arc sec with proper motion\n")

  val date = opt[String](required = false
    , noshort = true
    , descr = "a date in the format: YYYY-MM-DD i.e. 2000-11-29\n")

  val usePostgre = opt[Boolean](
    required = false
    , noshort = true
    , descr = "use postgreSQL database instead mongoDB\n")
  //---------------------------------------------------------------------------
  validate (configurationFile) { f =>
    if (!fileExist(f) && (!directoryExist(f))) Left(s"Invalid user argument 'configurationFile'. The configuration file '$f' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (dir) { d =>
    if (!directoryExist(d)) Left(s"Invalid user argument 'dir'. The directory '$d' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (spkSync) { _ => Right() }
  //---------------------------------------------------------------------------
  validate (mpoStats) { _ => Right() }
  //---------------------------------------------------------------------------
  validate(date) { d =>
    if (!Time.isValidLocalDate(d)) Left(s"Invalid user argument '$date'. The date '$d' fas not the expected format: YYYY-MM-DD")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate(searchRadiusNoPmMas) { v =>
    if (v < searchRadiusPmMas.toOption.get) Left(s"Invalid user argument '$searchRadiusNoPmMas'. The search radius without proper motion:$v bust be less than the one with it: ${searchRadiusPmMas.toOption.get}")
    else Right()
  }
  //---------------------------------------------------------------------------
  mutuallyExclusive(spkSync, mpoStats, gaia3Import, queryGaiadr3)
  requireOne       (spkSync, mpoStats, gaia3Import, queryGaiadr3)
  verify()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommandLineParser.scala
//=============================================================================