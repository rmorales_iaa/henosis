/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Feb/2020
 * Time:  15h:19m
 * Description: None
 */
//=============================================================================
package com.henosis
//=============================================================================
import com.henosis.commandLine.CommandLineParser
import com.common.configuration.MyConf
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
object Main extends MyLogger {
  //-------------------------------------------------------------------------
  //Version
  private final val MAJOR_VERSION = 0
  private final val MINOR_VERSION = 0
  private final val COMPILATION_VERSION = 1
  private final val DATE_VERSION = "15 Feb 2020"
  final val VERSION = s"$MAJOR_VERSION.$MINOR_VERSION.$COMPILATION_VERSION"
  final val VERSION_AND_DATE = s"$VERSION ($DATE_VERSION)"
  final val AUTHOR="Rafael Morales (rmorales [at] iaa [dot] com)"
  final val FILIATION="UDIT. IAA-CSIC. 2018"
  final val LICENSE="http://www.apache.org/licenses/LICENSE-2.0.txt"
  //---------------------------------------------------------------------------
  def main(args: Array[String]): Unit = {
    //-------------------------------------------------------------------------
    val userArgs = if (args.isEmpty) Array("--help") else args
    val cl = new CommandLineParser(userArgs)
    MyConf.c = MyConf(cl.configurationFile())
    if (MyConf.c == null || !MyConf.c.isLoaded )
      fatal(s"Error. Error parsing configuration file '${cl.configurationFile()}'")
    else {
      info(s"----------------------------- henosis: astronomical sources data hub $VERSION starts ----------------------------")
      Henosis().run(cl)
      info(s"----------------------------- henosis: astronomical sources data hub $VERSION ends -------------------------------")
      System.exit(0)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Main.scala
//=============================================================================
