/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Feb/2020
 * Time:  00h:44m
 * Description: None
 */
//=============================================================================
package com.henosis
//=============================================================================
import ch.qos.logback.classic.{Level, LoggerContext}
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.common.coordinate.conversion.Conversion
import com.common.hardware.cpu.CPU
import com.common.jpl.Spice
import com.common.jpl.horizons.db.smallBody.SmallBodyDB
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.common.util.time.Time
import com.common.util.time.Time.zoneID_UTC
import com.henosis.algoritms.crossmatch.KeplerGaiaDR_3.KeplerGaiaDR3
import com.henosis.algoritms.mpoInFov.MPO_Stats
import com.henosis.commandLine.CommandLineParser
import com.henosis.commandLine.CommandLineParser._
import com.henosis.database.gaia.source.gaiaDR3
//=============================================================================
import org.slf4j.LoggerFactory
import java.time.Instant
//=============================================================================
//=============================================================================
object Henosis {
  //---------------------------------------------------------------------------
  //timing
  var startMs : Long = System.currentTimeMillis
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
import com.henosis.Henosis._
case class Henosis() extends App with MyLogger {
  //---------------------------------------------------------------------------
  private def initialActions(): Unit = {
    startMs = System.currentTimeMillis

    //disable reactor logging up to level error
    LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext].getLogger("reactor.util").setLevel(Level.ERROR)

    //disable mongo logging up to level error
    LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext].getLogger("org.mongodb.driver").setLevel(Level.ERROR)

    //pds
    LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext].getLogger("gov.nasa.pds").setLevel(Level.ERROR)

    //hikari (slick postgres pool manager)
    LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext].getLogger("com.zaxxer.hikari").setLevel(Level.WARN)

    //slick: scala manager of postgres
    LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext].getLogger("slick").setLevel(Level.WARN)

    //set time zone to UTC
    Instant.now.atZone(zoneID_UTC)

    //spice kernel init
    Spice.init()
  }
  //---------------------------------------------------------------------------
  private def finalActions(): Unit = {
    Spice.close()
    warning(s"henosis: the mpo data hub elapsed time: ${Time.getFormattedElapseTimeFromStart(startMs)} " )
  }
  //---------------------------------------------------------------------------
  private def commandGaia_3_Import(cl: CommandLineParser): Unit = {
    val dir = cl.gaia3Import()
    val usePostgre =  cl.usePostgre()

    info(s"\tCore count                       : '${CPU.getCoreCount()}'")
    info(s"\tPath                             : '$dir'")

    if (usePostgre)
      info(s"\tUsing PostgreSQL database        : 'true'")
    else
      info(s"\tUsing Mongo database        : 'true'")

    gaiaDR3.gaia_source.DB_Importer(MyConf(MyConf.c.getString("Database.gaia_dr_3"))
                                    , dir
                                    , usePostgre)
  }
  //---------------------------------------------------------------------------
  private def commandSpkSync(cl: CommandLineParser): Unit = {
    info(s"\tCore count                       : '${CPU.getCoreCount()}'")
    info(s"\tSPK synchronization with JPL")
    SmallBodyDB().syncWithRemoteDB()
  }
  //---------------------------------------------------------------------------
  private def commandMpoStats(cl: CommandLineParser): Unit = {

    val inSeqDir  = if (cl.dirSeq.isSupplied) Some(cl.dirSeq()) else None

    info(s"\tCalculate MPO's statistics on database images")
    info(s"\tCore count                                : '${CPU.getCoreCount()}'")
    if (inSeqDir.isDefined)
       info(s"\tImage directory to process in blind mode  : '${inSeqDir.get.mkString("{",",","}")}'")
    MPO_Stats(inSeqDir)
  }
  //---------------------------------------------------------------------------
  private def commandQueryGaiaDR3(cl: CommandLineParser, dHub:DatabaseHub): Unit = {
    info(s"\tQuery GAIA DR3 with a search radius")
    //--------------------------------------------------------------------
    val csv            = cl.queryGaiadr3()
    val oDir           = cl.odir()
    val colDivider     = cl.colDivider()
    val radiusNoPmMas  = cl.searchRadiusNoPmMas()
    val radiusMas      = cl.searchRadiusPmMas()
    val observingDate  = cl.date()
    //--------------------------------------------------------------------
    info(s"\tCore count                                           : '${CPU.getCoreCount()}'")
    info(s"\tInput csv                                            : '$csv'")
    info(s"\tOutput dir                                           : '$oDir'")
    info(s"\tColumn divider in the csv file                       : '$colDivider'")
    info(s"\tSearch radius in milli arc sec without proper motion : '$radiusNoPmMas'")
    info(s"\tSearch radius in milli arc sec with proper motion    : '$radiusMas'")
    info(s"\tObserving date                                       : '$observingDate'")

    KeplerGaiaDR3(
        csv
      , Path.resetDirectory(oDir)
      , colDivider
      , Conversion.mas_to_DD(radiusNoPmMas)
      , Conversion.mas_to_DD(radiusMas)
      , observingDate + "T12:00:00")
  }
  //---------------------------------------------------------------------------
  def commandManager(cl: CommandLineParser, dHub:DatabaseHub): Unit = {
    //common arguments
    info(s"\tConfiguration file               : '${cl.configurationFile()}'")
    val command = {
      if (cl.gaia3Import.isSupplied)        COMMAND_GAIA_3_IMPORT
      else if (cl.spkSync.isSupplied)       COMMAND_SPK_SYNC
      else if (cl.mpoStats.isSupplied)      COMMAND_MPO_STATS
      else if (cl.queryGaiadr3.isSupplied)  COMMAND_QUERY_GAIA_DR3
    }

    info(s"\tCommand                          : '$command'")

    //commands
    command match {
      //-----------------------------------------------------------------------
      case COMMAND_GAIA_3_IMPORT             => commandGaia_3_Import(cl)
      case COMMAND_SPK_SYNC                  => commandSpkSync(cl)
      case COMMAND_MPO_STATS                 => commandMpoStats(cl)
      case COMMAND_QUERY_GAIA_DR3            => commandQueryGaiaDR3(cl,dHub)
      case c                                 => error(s"Unknown command '$c'")
      //-----------------------------------------------------------------------
    }
  }
  //--------------------------------------------------------------------------
  def run(cl: CommandLineParser): Unit = {

    initialActions()
    val dbHub = DatabaseHub()

    commandManager(cl, dbHub)

    //SandBox.getImageFocus()


   // SandBox.gaiaQuery()
   // SandBox.importWISE()

  //  SandBox.postgre()


     //SandBox.importHorizons()      //updated daily
     //SandBox.importMpc()         //updated daily
    //SandBox.importMpcObservatories()         //updated daily



    //SandBox.checkLandolt()
    //SandBox.crossMatchVR(dbHub)
//
    //SandBox.importPeixhino_2015(dbHub.peixhino_2015_DB)

   // SandBox.tnoColorImporter(dbHub)
    //SandBox.importCandal_2019(dbHub.candal_2019_DB)


    //SandBox.importMboss
    //SandBox.lcdb()

     //SandBox.hdf5JoinCsv()



    //-----------------------
    //import a new database
  // val csv = CsvRead("/home/rafa/Downloads/carmencita.103.csv", hasHeader = true,itemDivider = ",")
 //   csv.read()
    //csv.printScalaClassDefinition("CarmencitaSource")
    //csv.printScalaLoadClass("CarmencitaSource")
   // csv.printValidHeader()
    //-----------------------



    //SandBox.importCarmencita()
    //SandBox.importApassDr10()
   // SandBox.importLcdbSummary()
    //SandBox.importMboss_2()

    //SandBox.parseJplResponse()

   // SandBox.crossMatchApassDR_10_GaiaDR_3()

   // SandBox.smart_2021(dbHub)

   // SandBox.gaia_dr_3_sso_observation(dbHub)

    //SandBox.importSmart_2021(dbHub.smart_2021_DB)

   // SandBox.testGaia()


   // SandBox.importGaiaCrossMatchHipparcos()
    //SandBox.importHipparcos()
    //SandBox.importHorizons()

    //  SandBox.sendMail()

//     MOP_Fov.getStats("/home/rafa/Downloads/mpo_at_image.csv")


    //SandBox.fixSPK_names

    //SandBox.spark()

    //import data from database

    //

    //gaia 3
    //SandBox.importGaia_DR_3_sso_source()
    //SandBox.importGaia_DR_3_sso_observation
    //SandBox.importGaia_DR_3_sso_reflectance_spectrum
    //SandBox.importGaia_DR_3_synthetic_photometry_gspc
    //SandBox.importGaia_DR_3_astrophysical_parameters

    //gaia 4 (whe it s ready. Use directly the compressed csv"
  //  SandBox.importGaiaDR_4_Source()

    //MOP_Fov("/home/rafa/proyecto/m2/input/spice/kernels/spk/objects_ok/")
    //SandBox.ephemerisCalculation(dbHub)

    //SandBox.OG19_Ephemeris(dbHub)
   // Orekit.processJplVectorTable()

     //SandBox.spkDownload()
      //importMpcDistant(dbHub.horizons)
   //  SandBox.findImagesFov()

     //importAstorb(dbHub.mpoDB)

   // import2Mass(dbHub.mpoDB)
    //importGaia(dbHub.mpoDB)
    //importTess(dbHub.mpoDB)
    //importAstorb(dbHub.mpoDB)

    //importSdssMoc4(dbHub.mpoDB)
    // importWarnerLightCurve(dHub)

    //import data from paper catalog
    //SandBox.importGaiaSource()
   // SandBox.importPaperLandolt_2009(Landolt_2009_DB(dbHub))
   // SandBox.importPaperLandolt_2013(Landolt_2013_DB(dbHub))
    //SandBox.importStauffer_2010(Stauffer_2010_DB(dbHub))
    //SandBox.importPopescu_2018(Popescu_2018_DB(DatabaseHub.mpoPaperConf))
    //SandBox.importColazo_2021(Colazo_2021_DB(DatabaseHub.mpoPaperConf))
   // SandBox.importMahlke_2021(Mahlke_2021_DB(DatabaseHub.mpoPaperConf))
    //SandBox.importOszkiewicz_2011(Oszkiewicz_2011_DB(DatabaseHub.mpoPaperConf))
    //SandBox.importVeres_2015(Veres_2015_DB(DatabaseHub.mpoPaperConf))
    //SandBox.importCarry_2016(Carry_2016_DB(DatabaseHub.mpoPaperConf))


    //algoritms
    //SandBox.importGaiaCrossMatch2Mass()
    //SandBox.importGaiaCrossMatchPanstarrs()
    //SandBox.landolt_2009_CrossMatch(dbHub)
   //SandBox.landolt_2013_CrossMatch(dbHub)
    //SandBox.stauffer_2010_CrossMatch(dbHub)
    //SandBox.popescu_2018_CrossMatch(dbHub)
   // SandBox.alvarez_2021(dbHub)


    dbHub.close()

    finalActions()
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Henosis.scala
//=============================================================================
