/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/Feb/2024
 * Time:  12h:36m
 * Description: None
 */
package com.henosis.algoritms.claret.hdf5
//=============================================================================
import com.common.hardware.cpu.CPU
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import scala.io.Source
//=============================================================================
//=============================================================================
object JoinCsv extends MyLogger {
  //---------------------------------------------------------------------------
  private final val OUTPUT_FILE_EXTENSION = ".asc"
  //---------------------------------------------------------------------------
  private final val PHOENIX_RF      = "PHOENIX_RF"
  private final val DOT_HRRF        = ".HRRF"
  //---------------------------------------------------------------------------
  private final val TEFF_CSV        = "teff.csv"
  private final val LOGG_CSV        = "logg.csv"
  private final val MU_CSV          = "mu.csv"
  private final val WAVELENGTHS_CSV = "wl.csv"
  private final val INTENSITIES_CSV = "Intensities.csv"
  //---------------------------------------------------------------------------
  private final val SORTED_CSV_TO_JOIN = Seq (
      TEFF_CSV
    , LOGG_CSV
    , MU_CSV
    , WAVELENGTHS_CSV
    , INTENSITIES_CSV
  )
  //---------------------------------------------------------------------------
  private final val MU_OUTPUT_MAX_VALUE_PER_LINE    = 10
  //---------------------------------------------------------------------------
  private final val INTENSITIES_OUTPUT_VALUE_PER_BLOCK  = 128
  //---------------------------------------------------------------------------
  private class ProcessImageDirSeq(dirNameSeq: Array[String]
                                   , outputDir: String) extends ParallelTask[String](
    dirNameSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(dirName: String) = {
      processDirectory(dirName, outputDir)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def formatValue(value: String) =
     value.toDouble.formatted("%.6e").replace("e", "E")
  //---------------------------------------------------------------------------
  private def readSingleLine(inputCsv: String) = {
    val bufferedSource = Source.fromFile(inputCsv)
    val line = bufferedSource.getLines.toSeq.head
    bufferedSource.close
    line
  }
  //---------------------------------------------------------------------------
  private def writeTEFF(inputCsv:String, bw: BufferedWriter) =
    bw.write(readSingleLine(inputCsv))
  //---------------------------------------------------------------------------
  private def writeLOGG(inputCsv: String, bw: BufferedWriter) =
    bw.write(readSingleLine(inputCsv))
  //---------------------------------------------------------------------------
  private def writeMU(inputCsv: String, bw: BufferedWriter) = {
    val valueSeqSeq = readSingleLine(inputCsv)
      .split("\t")
      .grouped(MU_OUTPUT_MAX_VALUE_PER_LINE)
      .toArray

    valueSeqSeq.foreach { valueSeq=>
      bw.write(valueSeq
               .map(formatValue(_))
               .mkString(" "))
      bw.write("\n")
    }
  }
  //---------------------------------------------------------------------------
  private def writeWaveLengthAndIntensities(wavelengthCsv: String
                                            , intensityCsv: String
                                            , bw: BufferedWriter) = {
    val wavelengthSeq = readSingleLine(wavelengthCsv).split("\t")
    val intensitiesCsv = Source.fromFile(intensityCsv)

    intensitiesCsv
      .getLines()
      .zipWithIndex
      .foreach { case (intensityLine,i) =>

      //wavelength
      bw.write(" ")
      bw.write(wavelengthSeq(i))
      bw.write("\n")

      //intensities
      intensityLine.split("\t")
        .grouped(MU_OUTPUT_MAX_VALUE_PER_LINE)
        .toArray
        .foreach { valueSeq =>
          bw.write("   ")
          bw.write(valueSeq
            .map(formatValue(_))
            .mkString("   "))
          bw.write("\n")
        }
    }
    intensitiesCsv.close
  }
  //---------------------------------------------------------------------------
  private def processDirectory(dirName: String
                               , outputDir: String) = {
    info(s"Processing directory '$dirName'")
    val pathSeq = dirName.split("/")
    val outputJoinedFileName = outputDir + pathSeq(pathSeq.length-2)
                               .replaceAll(DOT_HRRF,"") +
                                s"$OUTPUT_FILE_EXTENSION"
    val csvFilePathSeq = Path.getSortedFileList(dirName,".csv").map( _.getAbsolutePath)
    val csvFileNameSeq = csvFilePathSeq.map( s=>  Path.getOnlyFilename(s))

    //check the presence of all csv
    val csvSeq = SORTED_CSV_TO_JOIN.zipWithIndex.flatMap { case (csvFileName,i) =>
      val index = csvFileNameSeq.indexOf(csvFileName)

      if (index == -1) {
        error(s"Directory '$dirName' missing csv: '$csvFileName'")
        None
      }
      else Some((i,csvFilePathSeq(index)))
    }.sortWith(_._1 < _._1)
     .map (_._2)

    if (csvSeq.length == SORTED_CSV_TO_JOIN.length) {
      val bw = new BufferedWriter(new FileWriter(new File(outputJoinedFileName)))
      var k = 0;

      //teff and logg
      writeTEFF(csvSeq(k),bw);k+=1
      bw.write("        ")
      writeLOGG(csvSeq(k),bw);k+=1
      bw.write("\n")

      //mu
      writeMU(csvSeq(k),bw);k+=1

      //wavelength and intensities
      writeWaveLengthAndIntensities(csvSeq(k), csvSeq(k+1), bw);
      bw.write("\n")

      //close file
      bw.close()
    }
    warning(s"Generated file: '$outputJoinedFileName'")
  }
  //---------------------------------------------------------------------------
  def process(inputDir: String
              , outputDir: String) = {
    val subDirSeq = Path.getSubDirectoryList(inputDir) map (_.getAbsolutePath + s"/$PHOENIX_RF/")
    if (subDirSeq.length > 0 ) {
      new ProcessImageDirSeq(subDirSeq.toArray, Path.resetDirectory(outputDir))
    }

  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file JoinCsv.scala
//=============================================================================