/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2024
 * Time:  18h:46m
 * Description: GAIA DR 3 querises
 * fields description:
 * https://gea.esac.esa.int/archive/documentation/GDR3/Gaia_archive/chap_datamodel/sec_dm_main_source_catalogue/ssec_dm_gaia_source.html
 */
package com.henosis.algoritms.query.gaiaDR_3
//=============================================================================
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.database.gaia.GaiaDB
import com.common.database.mongoDB.database.gaia.GaiaDB._
import org.mongodb.scala.Document
import org.mongodb.scala.model.Aggregates._
import org.mongodb.scala.model.Filters.{and, gt, lt}
import org.mongodb.scala.model.Projections.{computed, fields, include}
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object Query {
  //---------------------------------------------------------------------------
  private val gaiaDB = GaiaDB()
  private val sep = ","
  //---------------------------------------------------------------------------
  private def runQuery(csvFileName: String
                       , fieldSeq: Seq[String]
                       , docSeq: Seq[Document]): Unit = {
    info("Querying GAIA DR3")

    val totalItemToProcess = docSeq.length
    info(s"Result of the GAIA DR3 query:'$totalItemToProcess' records")

    //create result file
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))

    //write header
    val colNameSeq = fieldSeq.mkString(sep)
    bw.write(s"$colNameSeq\n")

    //write content
    var currentItemPercentage = 0L
    var lastItemPercentage = -1d
    var itemProcessed = 0d
    docSeq.foreach { doc=>
      fieldSeq.foreach { fieldName =>
        bw.write(doc(fieldName).toString + sep)
      }
      bw.write(s"\n")

      //report info
      itemProcessed += 1
      val currentItemPercentage = Math.round((itemProcessed / totalItemToProcess) * 100)

      if (currentItemPercentage > lastItemPercentage) {
        info(s"Processed: $currentItemPercentage% items")
        lastItemPercentage = currentItemPercentage
      }
    }
    //close file
    bw.close()
  }
  //---------------------------------------------------------------------------
  def matilde_1() = {
    val csvFileName = "output/parallax_gt_10.csv"
    val fieldSeq = Seq(GAIA_COL_ID
                       , GAIA_COL_RA
                       , GAIA_COL_DEC
                       , GAIA_COL_PARALLAX
                       , GAIA_COL_PARALLAX_ERROR
                       , GAIA_COL_PARALLAX_OVER_ERROR)
    val query = gaiaDB.collection.find(
      and(gt(GAIA_COL_PARALLAX, 10)
      ))
    runQuery(
      csvFileName
      , fieldSeq
      , query.getResultDocumentSeq)
   }

  //---------------------------------------------------------------------------
  def matilde_2() = {
    val csvFileName = "output/parallax_group_2.csv"
    val fieldSeq = Seq(
        GAIA_COL_ID
      , GAIA_COL_RA
      , GAIA_COL_DEC
      , GAIA_COL_PARALLAX
      , GAIA_COL_PARALLAX_ERROR
      , GAIA_COL_PARALLAX_OVER_ERROR
      , GAIA_COL_RUWE
      , GAIA_COL_PHOTO_G_MEAN_FLUX
      , GAIA_COL_PHOTO_BP_MEAN_FLUX
      , GAIA_COL_PHOTO_RP_MEAN_FLUX
      , GAIA_COL_PHOT_G_MEAN_FLUX_OVER_ERROR
      , GAIA_COL_PHOT_BP_MEAN_FLUX_OVER_ERROR
      , GAIA_COL_PHOT_RP_MEAN_FLUX_OVER_ERROR
      , GAIA_COL_ASTROMETRIC_CHI_2_AL
      , GAIA_COL_ASTROMETRIC_N_GOOD_OBS_AL
    )

    info("Querying GAIA DR3")

    val query = gaiaDB.collection.aggregate(
      List(
        //----------------------------------------------------------------------
        // Filter by magnitude error
        `match`(and(
          gt(GAIA_COL_PARALLAX, 10),
          gt(GAIA_COL_PARALLAX_ERROR, 10),
          gt(GAIA_COL_RUWE, 1.4),
          gt(GAIA_COL_PHOT_BP_MEAN_FLUX_OVER_ERROR, 10),
          gt(GAIA_COL_PHOT_RP_MEAN_FLUX_OVER_ERROR, 10),
          gt(GAIA_COL_ASTROMETRIC_N_GOOD_OBS_AL, 5)
        )),
        //----------------------------------------------------------------------
        // Project stage for calculating `total3`
        project(fields(
          computed("computedValue",
            Document(
              "$sqrt" -> Document(
                "$divide" -> List("$astrometric_chi2_al", "$astrometric_n_good_obs_al")
              )
            )
          ),
          include(fieldSeq: _*) // Include the fields from fieldSeq
        )),
        //----------------------------------------------------------------------
        // Match stage after calculating computedValue
        `match`(
          lt("computedValue", 1.96)
        )
        //----------------------------------------------------------------------
      )  //end of list
    ) //end of aggregate

    runQuery(
      csvFileName
      , fieldSeq
      , query.getResultDocumentSeq)

  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Query.scala
//=============================================================================