/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Feb/2020
 * Time:  00h:44m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms
//=============================================================================
import com.henosis.database.MPO_DB
import com.common.logger.MyLogger
import com.common.database.mongoDB.Helpers._
import com.common.database.mongoDB.MongoDB
import com.henosis.database.mpc.MpcToMongo
import com.mongodb.client.model.CollationStrength
import org.mongodb.scala.model.Collation
//=============================================================================
import org.mongodb.scala.model.Filters.{equal, in}
//=============================================================================
object AsteroidsCrossMatch{
  //---------------------------------------------------------------------------
  val collationCaseInsensitive = Collation
    .builder()
    .locale("en")
    .collationStrength(CollationStrength.PRIMARY).build
  //---------------------------------------------------------------------------
}
//=============================================================================
import AsteroidsCrossMatch._
case class AsteroidsCrossMatch(db: MPO_DB) extends MyLogger {
  //---------------------------------------------------------------------------
  val tess   = db.collTessMoc
  val gaia2  = db.collGaiaMoc
  val mpc    = db.collMpc

  val tessDbName = tess.namespace.getDatabaseName
  val gaia2DbName = gaia2.namespace.getDatabaseName
  val mpcDbName = mpc.namespace.getDatabaseName

  val tessCollName = tess.namespace.getCollectionName
  val gaia2CollName = gaia2.namespace.getCollectionName
  val mpcCollName = mpc.namespace.getCollectionName
  //---------------------------------------------------------------------------
  def mpcGetShortByMpcID(mpcID: String) = {
    val r = mpc.find(equal(MpcToMongo.COL_NAME_MPC_ID, mpcID))
      .projection(MongoDB.getProjectionColList(Seq(MpcToMongo.COL_NAME_MPC_ID, MpcToMongo.COL_NAME_NAME, MpcToMongo.COL_NAME_OTHER_DESIGNATION)))
      .collation(collationCaseInsensitive)
      .getResultDocumentSeq
    if (r.length > 0) Some(r)
    else None
  }
  //---------------------------------------------------------------------------
  def mpcGetByMpcID(mpcID: String) = {
    val r = mpc.find(equal(MpcToMongo.COL_NAME_MPC_ID, mpcID))
      .collation(collationCaseInsensitive)
      .getResultDocumentSeq
    if (r.length > 0) Some(r)
    else None
  }
  //---------------------------------------------------------------------------
  def mpcGetByName(name: String) = {
    val r = mpc.find(equal(MpcToMongo.COL_NAME_NAME, name))
      .collation(collationCaseInsensitive)
      .getResultDocumentSeq
    if (r.length > 0) Some(r)
    else {
      val s = mpc.find(in(MpcToMongo.COL_NAME_OTHER_DESIGNATION, name))
        .collation(collationCaseInsensitive)
        .getResultDocumentSeq
      if (s.length > 0) Some(s)
      else None
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MPO_CrossMatch.scala
//=============================================================================
