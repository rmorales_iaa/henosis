/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  12/Oct/2022
  * Time:  10h:51m
  * Description: https://gaia-kepler.fun/
  * https://exoplanetarchive.ipac.caltech.edu/docs/program_interfaces.html
  */
//=============================================================================
package com.henosis.algoritms.crossmatch.ApassGaiaDR_3
//=============================================================================

import com.common.database.postgreDB.gaia_dr_3.Source
import com.common.util.path.Path
import com.henosis.database.gaia.source.gaiaDR3.gaia_source.DB
import com.henosis.database.gaia.source.gaiaDR3.gaia_source.DB.QueryItem
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object ApassGaiaDR_3 {
  //---------------------------------------------------------------------------
  private val sep = ","
  //---------------------------------------------------------------------------
  def writHeader(bw: BufferedWriter) = {
    bw.write(
      s"apassdr10_id$sep " +
      s"apassdr10_ra[J2000]$sep" +
      s"apassdr10_dec[J2000]$sep" +
      s"apassdr10_gdr3_dist_deg$sep" +
      s"apassdr10_gdr3_dist_arcs$sep" +
      s"gdr3_id$sep" +
      s"gdr3_ra[J2016]$sep" +
      s"gdr3_dec[J2016]$sep" +
      s"gdr3_ref_epoch$sep" +
      s"gdr3_parallax$sep" +
      s"gdr3_parallax_error$sep" +
      s"gdr3_pm_ra$sep" +
      s"gdr3_pm_ra_error$sep" +
      s"gdr3_pm_dec$sep" +
      s"gdr3_pm_dec_error$sep" +
      s"gdr3_ra_pm[J2000]$sep" +
      s"gdr3_dec_pm[J2000]$sep" +
      s"gdr3_phot_g_mean_mag$sep" +
      s"gdr3_phot_bp_mean_mag$sep" +
      s"gdr3_phot_rp_mean_mag$sep" +
      s"gdr3_bp_g$sep" +
      s"gdr3_bp_rp$sep" +
      "\n")
  }
  //---------------------------------------------------------------------------
  def writeUniqueMatch(bw: BufferedWriter
                       , keplerItem: QueryItem[_]
                       , gas: Source
                       , observingDate: String) = {
    //-------------------------------------------------------------------------
    val apass = keplerItem.item.asInstanceOf[ItemApass]
    val dist = gas.getDistanceTo(apass.getRaDec(),observingDate)
    val raDecPM = gas.getRaDecWithPM(observingDate)

    bw.write(
     s"${keplerItem.id}$sep" +
     s"${keplerItem.raDec.x}$sep" +
     s"${keplerItem.raDec.y}$sep" +
     s"$dist$sep" +
     s"${dist * 3600}$sep" +
     s"${gas._id}$sep" +
     s"${gas.ra}$sep" +
     s"${gas.dec}$sep" +
     s"${gas.ref_epoch}$sep" +
     s"${gas.parallax}$sep" +
     s"${gas.parallax_error}$sep" +
     s"${gas.pmra}$sep" +
     s"${gas.pmra_error}$sep" +
     s"${gas.pmdec}$sep" +
     s"${gas.pmdec_error}$sep" +
     s"${raDecPM.x}$sep" +
     s"${raDecPM.y}$sep" +
     s"${gas.phot_g_mean_mag}$sep" +
     s"${gas.phot_bp_mean_mag}$sep" +
     s"${gas.phot_rp_mean_mag}$sep" +
     s"${gas.bp_g}$sep" +
     s"${gas.bp_rp}$sep")
  }
  //---------------------------------------------------------------------------
  def writeNoneMatch(bw: BufferedWriter
                     , keplerItem: QueryItem[_]) =
    bw.write(s"${keplerItem.id}$sep " +
      s"${keplerItem.raDec.x}$sep" +
      s"${keplerItem.raDec.y}$sep" +
      "\n")
  //---------------------------------------------------------------------------
}
//=============================================================================
import ApassGaiaDR_3._
case class ApassGaiaDR_3(outputDir: String
                        , radiusNoPmMas: Double
                        , radiusMas: Double
                        , observingDate: String
                        , querySeq: Array[QueryItem[_]]) {
  //---------------------------------------------------------------------------
  private val map = DB.query(radiusNoPmMas
                        , radiusMas
                        , observingDate
                        , querySeq)
  //---------------------------------------------------------------------------
  writeOutputCsv(Path.resetDirectory(outputDir), querySeq, map)
  //---------------------------------------------------------------------------
  private def writeMultipleOutput(oDir: String
                                , keplerItem: QueryItem[_]
                                , gasSeq:Array[Source]
                                , observingDate: String) = {
    val bw = new BufferedWriter(new FileWriter(new File(oDir + "apass_" + keplerItem.id + s"_gaia_${gasSeq.length}" + ".csv")))
    writHeader(bw)

    gasSeq.foreach { gas =>
      writeUniqueMatch(bw,keplerItem,gas,observingDate)
      bw.write("\n")
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
  private def writeOutputCsv(oDir: String
                             , queryItemSeq: Array[QueryItem[_]]
                             , gasMap: scala.collection.concurrent.Map[String, Array[Source]]) = {

    val bwUnique = new BufferedWriter(new FileWriter(new File(oDir + "/unique_match.csv")))
    writHeader(bwUnique)

    val bwNone = new BufferedWriter(new FileWriter(new File(oDir + "/none_match.csv")))
    writHeader(bwNone)

    val oMultiple = oDir + "/multiple_match/"
    Path.ensureDirectoryExist(oMultiple)

    queryItemSeq.foreach { apass =>
      val queryID = apass.id
      if (gasMap.contains(queryID)) {
        val gasSeq = gasMap(queryID)
        gasSeq.length match {
          case 0 => writeNoneMatch(bwNone, apass)
          case 1 =>
            writeUniqueMatch(bwUnique, apass, gasSeq.head, observingDate)
            bwUnique.write("\n")
          case _ => writeMultipleOutput(oMultiple, apass, gasSeq, observingDate)
        }
      }
    }
    bwUnique.close()
    bwNone.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ApassGaiaDR_3.scala
//=============================================================================
