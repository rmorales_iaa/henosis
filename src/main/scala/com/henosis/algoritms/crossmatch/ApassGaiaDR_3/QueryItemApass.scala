/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  12/Oct/2022
  * Time:  10h:54m
  * Description: None
  */
//=============================================================================
package com.henosis.algoritms.crossmatch.ApassGaiaDR_3
//=============================================================================
import com.common.database.mongoDB.database.apass.ApassDB
import com.common.database.mongoDB.database.apass.ApassDB._
import com.common.geometry.point.Point2D_Double
import com.henosis.database.gaia.source.gaiaDR3.gaia_source.DB.QueryItem
//=============================================================================
//=============================================================================
case class ItemApass(id:Long, ra: Double, dec: Double) {
  //---------------------------------------------------------------------------
  def getRaDec() = Point2D_Double(ra,dec)
  //---------------------------------------------------------------------------
}
//=============================================================================
object QueryItemApass {
  //---------------------------------------------------------------------------
  def loadDatabase(minID: Int,maxID: Int) = {
    val db = ApassDB()

    (db.findRangeID(minID,maxID) map { source =>
      QueryItemApass(ItemApass(
        source._id
        , source.ra
        , source.dec
      ))
    }).toArray.asInstanceOf[Array[QueryItem[_]]]
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class QueryItemApass(item: ItemApass) extends QueryItem[ItemApass] {
  //---------------------------------------------------------------------------
  val id = item.id.toString
  val raDec = item.getRaDec()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file QueryItemApass.scala
//=============================================================================
