/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  12/Oct/2022
  * Time:  10h:54m
  * Description: None
  */
//=============================================================================
package com.henosis.algoritms.crossmatch.KeplerGaiaDR_3
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.geometry.point.Point2D_Double
import com.henosis.database.gaia.source.gaiaDR3.gaia_source.DB.QueryItem
//=============================================================================
//=============================================================================
object QueryItemKepler {
  //---------------------------------------------------------------------------
  private def getRa(s: String) =
    if (s.contains(":")) Conversion.HMS_to_DD(s)
    else
      if (s.contains(" ")) Conversion.HMS_to_DD(s)
      else s.toDouble
  //---------------------------------------------------------------------------
  private def getDec(s: String) =
    if (s.contains(":")) Conversion.DMS_to_DD(s)
    else
      if (s.contains(" ")) Conversion.DMS_to_DD(s)
      else s.toDouble
  //---------------------------------------------------------------------------
  def loadQueryItemSeq(csvFileName: String, colDivider: String) = {
    val bufferedSource = scala.io.Source.fromFile(csvFileName)
    val queryItemSeq = bufferedSource.getLines.zipWithIndex.flatMap { case (line, i) =>
      if (i == 0) None
      else {
        val colSeq = line
          .split(colDivider)
          .filter(s=> !s.isEmpty)

        Some(QueryItemKepler(ItemKepler(colSeq(0).toInt
                                      , getRa(colSeq(7))
                                      , getDec(colSeq(8))
                                      , colSeq(9).toFloat)))
      }
    }.toArray
    bufferedSource.close
    queryItemSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class ItemKepler(kepid:Int, ra: Double, dec: Double, magnitude: Float) {
  //---------------------------------------------------------------------------
  def getRaDec() = Point2D_Double(ra,dec)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class QueryItemKepler(item: ItemKepler) extends QueryItem[ItemKepler] {
  //---------------------------------------------------------------------------
  val id = item.kepid.toString
  val raDec = item.getRaDec()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file QueryItemApass.scala
//=============================================================================
