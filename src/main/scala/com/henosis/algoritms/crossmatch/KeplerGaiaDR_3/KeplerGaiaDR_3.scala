/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  12/Oct/2022
  * Time:  10h:51m
  * Description: https://gaia-kepler.fun/
  * https://exoplanetarchive.ipac.caltech.edu/docs/program_interfaces.html
  */
//=============================================================================
package com.henosis.algoritms.crossmatch.KeplerGaiaDR_3
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.postgreDB.gaia_dr_3.Source
import com.common.util.path.Path
import com.henosis.database.gaia.source.gaiaDR3.astrophysical.astrophysical_parameters.Source.GaiaDR_3_Astrophysical_Parameters
import com.henosis.database.gaia.source.gaiaDR3.gaia_source.DB
import com.henosis.database.gaia.source.gaiaDR3.gaia_source.DB.QueryItem
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object KeplerGaiaDR3 {
  //---------------------------------------------------------------------------
  private val sep = ","
  //---------------------------------------------------------------------------
  val NEGATIVE_INFINITE = -3.4028235E+038
  val NEGATIVE_INFINITE_2 = -3.4028234663852886E38

  val NEGATIVE_INFINITE_SEP = NEGATIVE_INFINITE + sep
  //---------------------------------------------------------------------------
  def writHeader(bw: BufferedWriter) ={
    bw.write(
      s"kep_id$sep " +
      s"kep_ra[J2000]$sep" +
      s"kep_dec[J2000]$sep" +
      s"kep_mag$sep" +
      s"kep_gdr3_dist_deg$sep" +
      s"kep_gdr3_dist_arcs$sep" +
      s"gdr3_id$sep" +
      s"gdr3_ra[J2016]$sep" +
      s"gdr3_dec[J2016]$sep" +
      s"gdr3_ref_epoch$sep" +
      s"gdr3_parallax$sep" +
      s"gdr3_parallax_error$sep" +
      s"gdr3_pm_ra$sep" +
      s"gdr3_pm_ra_error$sep" +
      s"gdr3_pm_dec$sep" +
      s"gdr3_pm_dec_error$sep" +
      s"gdr3_ra_pm[J2000]$sep" +
      s"gdr3_dec_pm[J2000]$sep" +
      s"gdr3_phot_g_mean_mag$sep" +
      s"gdr3_phot_bp_mean_mag$sep" +
      s"gdr3_phot_rp_mean_mag$sep" +
      s"gdr3_bp_g$sep" +
      s"gdr3_bp_rp$sep" +
      s"gdr3_teff_gspphot$sep" +
      s"gdr3_lum_flame(log10)$sep" +
      s"gdr3_lum_flame_lower(log10)$sep" +
      s"gdr3_lum_flame_upper(log10)$sep" +
      s"gdr3_radius_gspphot$sep" +
      s"gdr3_radius_flame$sep" +
      s"gdr3_logg_gspphot$sep" +
      s"gdr3_mh_gspphot$sep" +
      s"gdr3_distance_gspphot$sep" +
      s"gdr3_azero_gspphot$sep" +
      s"gdr3_ag_gspphot$sep" +
      s"gdr3_abp_gspphot$sep" +
      s"gdr3_arp_gspphot$sep" +
      s"gdr3_ebpminrp_gspphot$sep" +
      s"gdr3_mg_gspphot$sep" +
      s"gdr3_bc_flame$sep" +
      "\n")
  }
  //---------------------------------------------------------------------------
  def writeUniqueMatch(bw: BufferedWriter
                       , keplerItem: QueryItem[_]
                       , gas: Source
                       , physicalParameters: GaiaDR_3_Astrophysical_Parameters
                       , observingDate: String) = {

    //-------------------------------------------------------------------------
    def getLog_10_Lum(lum:Double): Double = {
      if (lum.isNegInfinity || lum == NEGATIVE_INFINITE_2) NEGATIVE_INFINITE
      else {
        val r = Math.log10(lum)
        if (r.isNaN) lum
        else r
      }
    }
    //-------------------------------------------------------------------------
    val ki = keplerItem.item.asInstanceOf[ItemKepler]
    val dist = gas.getDistanceTo(ki.getRaDec(),observingDate)
    val raDecPM = gas.getRaDecWithPM(observingDate)

    bw.write(
   s"${keplerItem.id}$sep" +
     s"${keplerItem.raDec.x}$sep" +
     s"${keplerItem.raDec.y}$sep" +
     s"${ki.magnitude}$sep" +
     s"$dist$sep" +
     s"${dist * 3600}$sep" +
     s"${gas._id}$sep" +
     s"${gas.ra}$sep" +
     s"${gas.dec}$sep" +
     s"${gas.ref_epoch}$sep" +
     s"${gas.parallax}$sep" +
     s"${gas.parallax_error}$sep" +
     s"${gas.pmra}$sep" +
     s"${gas.pmra_error}$sep" +
     s"${gas.pmdec}$sep" +
     s"${gas.pmdec_error}$sep" +
     s"${raDecPM.x}$sep" +
     s"${raDecPM.y}$sep" +
     s"${gas.phot_g_mean_mag}$sep" +
     s"${gas.phot_bp_mean_mag}$sep" +
     s"${gas.phot_rp_mean_mag}$sep" +
     s"${gas.bp_g}$sep" +
     s"${gas.bp_rp}$sep")

     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.teff_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${getLog_10_Lum(physicalParameters.lum_flame)}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${getLog_10_Lum(physicalParameters.lum_flame_lower)}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${getLog_10_Lum(physicalParameters.lum_flame_upper)}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.radius_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.radius_flame}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.logg_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.mh_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.distance_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.azero_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.ag_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.abp_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.arp_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.ebpminrp_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.mg_gspphot}$sep")
     bw.write(if (physicalParameters == null) NEGATIVE_INFINITE_SEP else s"${physicalParameters.bc_flame}$sep")
  }
  //---------------------------------------------------------------------------
  def writeNoneMatch(bw: BufferedWriter
                     , keplerItem: QueryItem[_]) =
    bw.write(s"${keplerItem.id}$sep " +
      s"${keplerItem.raDec.x}$sep" +
      s"${keplerItem.raDec.y}$sep" +
      "\n")
  //---------------------------------------------------------------------------
}
//=============================================================================
import KeplerGaiaDR3._
case class KeplerGaiaDR3(csvFileName: String
                         , outputDir: String
                         , colDivider: String
                         , radiusNoPmMas: Double
                         , radiusMas: Double
                         , observingDate: String) {
  //---------------------------------------------------------------------------
  private val gaiaDR_3_AstrophysicalParametersDB = com.henosis.database.gaia.source.gaiaDR3.astrophysical.astrophysical_parameters.DB(MyConf(MyConf.c.getString("Database.gaia_dr_3_astrophysical_parameters")))
  private val querySeq = QueryItemKepler.loadQueryItemSeq(csvFileName, colDivider).asInstanceOf[Array[QueryItem[_]]]
  private val gasMap = DB.query(radiusNoPmMas
                        , radiusMas
                        , observingDate
                        , querySeq)
  //---------------------------------------------------------------------------
  writeOutputCsv(Path.resetDirectory(outputDir), querySeq, gasMap)
  //---------------------------------------------------------------------------
  private def writeMultipleOutput(oDir: String
                                , keplerItem: QueryItem[_]
                                , gasSeq:Array[Source]
                                , observingDate: String) = {
    val bw = new BufferedWriter(new FileWriter(new File(oDir + "kic_" + keplerItem.id + s"_gaia_${gasSeq.length}" + ".csv")))
    writHeader(bw)

    gasSeq.foreach { gas =>
      val physicalParameters = gaiaDR_3_AstrophysicalParametersDB.find(gas._id)
      writeUniqueMatch(bw,keplerItem,gas, physicalParameters, observingDate)
      bw.write("\n")
    }
    bw.close()
  }
  //---------------------------------------------------------------------------
  private def writeOutputCsv(oDir: String
                             , queryItemSeq: Array[QueryItem[_]]
                             , gasMap: scala.collection.concurrent.Map[String, Array[Source]]) = {

    val bwUnique = new BufferedWriter(new FileWriter(new File(oDir + "/unique_match.csv")))
    writHeader(bwUnique)

    val bwNone = new BufferedWriter(new FileWriter(new File(oDir + "/none_match.csv")))
    writHeader(bwNone)

    val oMultiple = oDir + "/multiple_match/"
    Path.ensureDirectoryExist(oMultiple)

    queryItemSeq.foreach { keplerItem =>
      val queryID = keplerItem.id
      if (gasMap.contains(queryID)) {
        val gasSeq = gasMap(queryID)
        gasSeq.length match {
          case 0 => writeNoneMatch(bwNone, keplerItem)
          case 1 =>
            val physicalParameters = gaiaDR_3_AstrophysicalParametersDB.find(gasSeq.head._id)
            writeUniqueMatch(bwUnique, keplerItem, gasSeq.head, physicalParameters, observingDate)
            bwUnique.write("\n")
          case _ => writeMultipleOutput(oMultiple, keplerItem, gasSeq, observingDate)
        }
      }
    }
    bwUnique.close()
    bwNone.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file KeplerGaiaDR_3.scala
//=============================================================================
