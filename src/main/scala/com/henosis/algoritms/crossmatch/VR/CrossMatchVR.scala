/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Mar/2024
 * Time:  11h:26m
 * Description: None
 */
package com.henosis.algoritms.crossmatch.VR
//=============================================================================
import com.common.DatabaseHub
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.database.vr.{VR, VR_DB, VR_Entry, VR_InitialEntry}
import com.common.database.mongoDB.spiceSPK.SpiceSPK_DB
import com.common.hardware.cpu.CPU
import com.common.image.focusType.ImageFocusTrait
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.Projections.include
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
case class CrossMatchVR(dbHub: DatabaseHub) extends MyLogger {
  //---------------------------------------------------------------------------
  private val debug = false
  //---------------------------------------------------------------------------
  private val vrDB = VR_DB()
  private val vrEntryMap = scala.collection.mutable.Map[String,ArrayBuffer[VR_Entry]]()
  //---------------------------------------------------------------------------
  def create(): Unit = {
    vrDB.dropCollection() //drop collection
    buildEntrySeq()
    buildDatabase()
  }
  //---------------------------------------------------------------------------
  private def getComposedName(id: String): Option[String] = {
    val spkID = SpiceSPK_DB.getSpkID(id)
    if (spkID.isEmpty) return None
    val doc = dbHub.horizons.getBySpkId(spkID.get).head
    Some(ImageFocusTrait.getComposedObjectName(doc("name").asString().getValue
                                         , spkID = spkID))
  }

  //---------------------------------------------------------------------------
  private def buildDatabase(): Unit = {
    val vrSeq = vrEntryMap.map { case (composedName,vrEntrySeq) =>
      VR(composedName,vrEntrySeq.toList)
    }.toSeq
    vrDB.collWithRegistry.insertMany(vrSeq).results()
  }
  //---------------------------------------------------------------------------
  private class ProcessVR_InitialEntry(seq: Array[VR_InitialEntry]
                                       , debug: Boolean) extends ParallelTask[VR_InitialEntry](
    seq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(vrInitialEntry: VR_InitialEntry): Unit = {

      val id = vrInitialEntry.id
      if (debug) info(s"'${vrInitialEntry.databaseName}' processing '$id' with reference: '${vrInitialEntry.reference}'")

      val composedName = getComposedName(id)
      if (composedName.isDefined) {
        val vrEntry = VR_Entry(
            vrInitialEntry.databaseName
          , vrInitialEntry.vr
          , vrInitialEntry.vrError
          , vrInitialEntry.reference)
        synchronized {
          if (!vrEntryMap.contains(composedName.get)) vrEntryMap(composedName.get) = ArrayBuffer()
          vrEntryMap(composedName.get) += vrEntry
        }
      }
      else warning(s"Can not find a valid Spice kernel for name '$id' at database '${vrInitialEntry.databaseName}'")
    }
    //-----------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def buildEntrySeq() = {
    buildEntrySeqLCDB()
    buildEntrySeqCandal2019()
    buildEntrySeqPeixinho()
    buildEntrySeqMboss()
    buildEntrySeqTnoAndCentaurColors()
  }
  //---------------------------------------------------------------------------
  private def buildEntrySeqLCDB() = {
    val docSeq = dbHub.lcdbMpcColorIndexDB.getAllDocuments(projectionFieldSeq = Seq("Number", "Name", "VR", "Reference"))
    val vrInitialEntreSeq = docSeq.map { doc =>
      val number = doc("Number").asInt32().getValue.toString
      val name = doc("Name").asString().getValue
      val vr = doc("VR").asDouble().getValue
      val vrError = 0d

      val databaseName = "lcdb_" + dbHub.lcdbMpcColorIndexDB.collectionName
      val reference = doc("Reference").asString().getValue

      val id = if (number == "0") name else number
      VR_InitialEntry(
        id
        , databaseName
        , vr
        , vrError
        , reference)
    }
    new ProcessVR_InitialEntry(vrInitialEntreSeq.toArray, debug)
  }
  //---------------------------------------------------------------------------
  private def buildEntrySeqCandal2019() = {
    val docSeq = dbHub.candal_2019_DB.getAllDocuments(projectionFieldSeq = Seq("_id", "Hv_Hr", "hError"))
    val vrInitialEntreSeq = docSeq.map { doc =>
      val id      = doc("_id").asString().getValue
      val vr      = doc("Hv_Hr").asDouble().getValue
      val vrError = doc("hError").asDouble().getValue

      val databaseName = dbHub.candal_2019_DB.collectionName
      val paperName = dbHub.candal_2019_DB.paperName

      VR_InitialEntry(
          id
        , databaseName
        , vr
        , vrError
        , reference = paperName)
    }
    new ProcessVR_InitialEntry(vrInitialEntreSeq.toArray, debug)
  }

  //---------------------------------------------------------------------------
  private def buildEntrySeqPeixinho() = {
    val docSeq = dbHub.peixhino_2015_DB.getAllDocuments(projectionFieldSeq = Seq("name", "V_R", "V_R_error", "references"))
    val vrInitialEntreSeq = docSeq.map { doc =>
      val id = doc("name").asString().getValue
      val vr = doc("V_R").asDouble().getValue
      val vrError = doc("V_R_error").asDouble().getValue
      val reference = doc("references").asString().getValue

      val databaseName = dbHub.peixhino_2015_DB.collectionName

      VR_InitialEntry(
        id
        , databaseName
        , vr
        , vrError
        , reference)
    }
    new ProcessVR_InitialEntry(vrInitialEntreSeq.toArray, debug)
  }
  //---------------------------------------------------------------------------
  private def buildEntrySeqMboss() = {
    val cometPattern =  """^\d+[CP]/.*$""".r
    val mpcID_pattern = """^\d+-.*+$""".r
    val docSeq = dbHub.mbossDB.getAllDocuments(projectionFieldSeq = Seq("_id", "V_R", "V_R_sigma"))

    val vrInitialEntreSeq = docSeq.flatMap { doc =>
      val id = doc("_id").asString().getValue
      val vr = doc("V_R").asDouble().getValue
      val vrError = doc("V_R_sigma").asDouble().getValue

      val databaseName = dbHub.mbossDB.collectionName
      val reference = "http://www.eso.org/~ohainaut/MBOSS/mbossTables.html"
      if (id.startsWith("C/") || id.startsWith("P/")) None //comets
      else {
        if (cometPattern.findFirstIn(id).isDefined) None
        else {
          var newID = if (id.endsWith("+A") || id.endsWith("+B")) id.dropRight(2) else id
          newID =  if (mpcID_pattern.findFirstIn(newID).isDefined) newID.split("-").head else newID
          Some(VR_InitialEntry(
              newID
            , databaseName
            , vr
            , vrError
            , reference = reference))
        }
      }
    }
    new ProcessVR_InitialEntry(vrInitialEntreSeq.toArray, debug)
  }
  //---------------------------------------------------------------------------
  private def buildEntrySeqTnoAndCentaurColors() = {
    val docSeq = dbHub.mpoDB.tnoColor.find(equal("colorID", "V-R"))
                                      .projection(include(Seq("astNumber", "astName", "provID", "color", "colorUncertainty", "reference"): _*)).results()

    val vrInitialEntreSeq = docSeq.map { doc =>
      val astNumber = doc("astNumber").asString().getValue
      val astName = doc("astName").asString().getValue
      val provID = doc("provID").asString().getValue  //provisional ID
      val vr = doc("color").asDouble().getValue
      val vrError = doc("colorUncertainty").asDouble().getValue
      val reference = doc("reference").asString().getValue
      val databaseName = "TNO and Centaur Colors V10.0"

      val id =
        if (!astNumber.isEmpty && astNumber != "0") astNumber
        else
          if (astName != "-") astName
          else  provID

      VR_InitialEntry(
        id
        , databaseName
        , vr
        , vrError
        , reference)
    }
    new ProcessVR_InitialEntry(vrInitialEntreSeq.toArray, debug)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CrossMatchVR.scala
//=============================================================================