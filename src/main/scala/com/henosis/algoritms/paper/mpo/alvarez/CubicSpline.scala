/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Sep/2021
 * Time:  19h:34m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo.alvarez
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import scala.collection.immutable.Range
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
case class CubicSpline(xSeq: Array[Double], ySeq: Array[Double], derivative: Array[Double]) extends MyLogger {

  private val N = xSeq.length
  private val A = ArrayBuffer.tabulate[Double](N)(_ => 0)
  private val B = ArrayBuffer.tabulate[Double](N)(_ => 1)
  private val C = ArrayBuffer.tabulate[Double](N)(_ => 0)
  private val R = ArrayBuffer.tabulate[Double](N)(_ => 0)
  private val gamma = ArrayBuffer.tabulate[Double](N)(_ => 0)
  private val deriv = ArrayBuffer.tabulate[Double](N)(_ => 0)

  R(0) = derivative(0)
  val KK = Range(1, N)
  for (i <- Range(1, N - 1)) {
    A(i) = 1 / (xSeq(i) - xSeq(i - 1))
    B(i) = 2 / (xSeq(i) - xSeq(i - 1)) + 2 / (xSeq(i + 1) - xSeq(i))
    C(i) = 1 / (xSeq(i + 1) - xSeq(i))
    R(i) += 3 * (ySeq(i) - ySeq(i - 1)) / Math.pow(xSeq(i) - xSeq(i - 1), 2)
    R(i) += 3 * (ySeq(i + 1) - ySeq(i)) / Math.pow(xSeq(i + 1) - xSeq(i), 2)
  }

  R(N - 1) = derivative(1)
  var beta = B(0)
  deriv(0) = R(0) / beta
  for (i <- Range(1, N)) {
    gamma(i) = C(i - 1) / beta
    beta = B(i) - A(i) * gamma(i)
    deriv(i) = (R(i) - A(i) * deriv(i - 1)) / beta
  }

  for (i <- Range(N - 2, 0, -1))
    deriv(i) = deriv(i) - gamma(i + 1) * deriv(i + 1)

  //---------------------------------------------------------------------------
  private def findPosition(x: Double): Option[Int] = {
    for (j <- Range(0, xSeq.length - 1))
      if (xSeq(j) <= x && xSeq(j + 1) > x) return Some(j)
    None
  }

  //---------------------------------------------------------------------------
  def evaluate(x: Double): Double = {
    val i = findPosition(x).getOrElse {
      error(s"Value $x not supported in the spline")
      return -1
    }
    val x1 = xSeq(i)
    val x2 = xSeq(i + 1)
    val y1 = ySeq(i)
    val y2 = ySeq(i + 1)
    val d1 = deriv(i)
    val d2 = deriv(i + 1)
    val xd = x2 - x1
    val yd = y2 - y1
    val a = d1 * xd - yd
    val b = -d2 * xd + yd
    val t = (x - x1) / xd
    val y = (1 - t) * y1 + t * y2 + t * (1 - t) * (a * (1 - t) + b * t)
    y
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CubicSpline.scala
//=============================================================================
