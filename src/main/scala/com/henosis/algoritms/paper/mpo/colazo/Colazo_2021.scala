/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Sep/2021
 * Time:  11h:34m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo.colazo
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.mpo.MPO_Common.PAPER_COLAZO_2021
import com.henosis.algoritms.paper.mpo.MPO_CrossMatch
import com.henosis.database.paper.mpo.colazo.Colazo_2021_DB_Importer
//=============================================================================
//=============================================================================
object Colazo_2021 {
  //---------------------------------------------------------------------------
  val paperName = "'Colazo_2021'"
  val referenceEpoch = "200-01-01T00:00:00"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Colazo_2021(dbHub: DatabaseHub
                        , outputCsv: String
                        , conf: MyConf) extends MPO_CrossMatch {
  //---------------------------------------------------------------------------
  val db =  dbHub.colazo_2021_DB
  val coll = db.getCollection
  val mpoType = PAPER_COLAZO_2021
  val csvHeader = Colazo_2021_DB_Importer.CSV_HEADER
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Colazo_2021.scala
//=============================================================================
