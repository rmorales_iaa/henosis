/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  00h:59m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo
//=============================================================================
import org.mongodb.scala.Document
//=============================================================================
//=============================================================================
object MPO_Common  {
  //---------------------------------------------------------------------------
  sealed trait PAPER_MPO_TYPE
  //---------------------------------------------------------------------------
  case object PAPER_POPESCU_2018     extends PAPER_MPO_TYPE
  case object PAPER_COLAZO_2021      extends PAPER_MPO_TYPE
  case object PAPER_MAHLKE_2021      extends PAPER_MPO_TYPE
  case object PAPER_OSZKIEWICZ_2011  extends PAPER_MPO_TYPE
  case object PAPER_VERES_2015       extends PAPER_MPO_TYPE
  case object PAPER_CARRY_2016       extends PAPER_MPO_TYPE
  case object PAPER_CANDAL_2019      extends PAPER_MPO_TYPE
  case object PAPER_PEIXHINO_2015      extends PAPER_MPO_TYPE
  //---------------------------------------------------------------------------
  def build(doc: Document, mpoType: PAPER_MPO_TYPE) : MPO_Common = {
    mpoType match {
      case PAPER_POPESCU_2018     => com.henosis.database.paper.mpo.popescu.MPO(doc)
      case PAPER_COLAZO_2021      => com.henosis.database.paper.mpo.colazo.MPO(doc)
      case PAPER_MAHLKE_2021      => com.henosis.database.paper.mpo.mahlke.MPO(doc)
      case PAPER_OSZKIEWICZ_2011  => com.henosis.database.paper.mpo.oszkiewicz.MPO(doc)
      case PAPER_VERES_2015       => com.henosis.database.paper.mpo.veres.MPO(doc)
      case PAPER_CARRY_2016       => com.henosis.database.paper.mpo.carry.MPO(doc)
      case PAPER_CANDAL_2019      => com.henosis.database.paper.mpo.candal.MPO(doc)
      case PAPER_PEIXHINO_2015    => com.henosis.database.paper.mpo.peixinho.MPO(doc)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait MPO_Common {
  //---------------------------------------------------------------------------
  var mpc_id: Int  = -1
  val name: Seq[String]
  //---------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t"): String
  //---------------------------------------------------------------------------
  def getInfo(paperName: String) = {
    s"\n\t$paperName MPO name     : ${name.mkString("'",",","'")}" +
    s"\n\t$paperName mpc_id            : $mpc_id"
  }
  //---------------------------------------------------------------------------
  def getSimpleName = name.mkString("'",",","'")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO_Common.scala
//=============================================================================
