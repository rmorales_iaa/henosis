/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Sep/2021
 * Time:  20h:22m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo.popescu
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.mpo.MPO_Common.PAPER_POPESCU_2018
import com.henosis.algoritms.paper.mpo.MPO_CrossMatch
import com.henosis.database.paper.mpo.popescu.Popescu_2018_DB_Importer
//=============================================================================
//=============================================================================
object Popescu_2018 {
  //---------------------------------------------------------------------------
  val paperName = "'Popescu_2018'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Popescu_2018(dbHub: DatabaseHub
                         , outputCsv: String
                         , conf: MyConf) extends MPO_CrossMatch {
  //---------------------------------------------------------------------------
  val db = dbHub.popescu_2018_DB
  val coll = db.getCollection
  val mpoType = PAPER_POPESCU_2018
  val csvHeader = Popescu_2018_DB_Importer.CSV_HEADER
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Popescu_2018.scala
//=============================================================================