/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Sep/2021
 * Time:  11h:34m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo.mahlke
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.mpo.MPO_Common.PAPER_MAHLKE_2021
import com.henosis.algoritms.paper.mpo.MPO_CrossMatch
import com.henosis.database.paper.mpo.mahlke.Mahlke_2021_DB_Importer
//=============================================================================
//=============================================================================
object  Mahlke_2021 {
  //---------------------------------------------------------------------------
  val paperName = "'Mahlke_2021'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class  Mahlke_2021(dbHub: DatabaseHub
                        , outputCsv: String
                        , conf: MyConf) extends MPO_CrossMatch {
  //---------------------------------------------------------------------------
  val db =  dbHub.mahlke_2021_DB
  val coll = db.getCollection
  val mpoType =  PAPER_MAHLKE_2021
  val csvHeader =  Mahlke_2021_DB_Importer.CSV_HEADER
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of filer.scala
//=============================================================================
