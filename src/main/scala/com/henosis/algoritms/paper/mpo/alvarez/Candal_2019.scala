/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Sep/2021
 * Time:  17h:17m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo.alvarez
//=============================================================================
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.mpo.MPO_Common.{PAPER_CANDAL_2019}
import com.henosis.algoritms.paper.mpo.MPO_CrossMatch
import com.henosis.database.paper.mpo.candal.MPO
//=============================================================================
//=============================================================================
object Candal_2019 {
  //---------------------------------------------------------------------------
  val paperName = "'Candal_2019'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Candal_2019(dbHub: DatabaseHub
                         , outputCsv: String
                         , conf: MyConf) extends MPO_CrossMatch {
  //---------------------------------------------------------------------------
  val db =  dbHub.candal_2019_DB
  val coll = db.getCollection
  val mpoType = PAPER_CANDAL_2019
  val csvHeader = MPO.getColNameSeq("")
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Candal_2019.scala
//=============================================================================
