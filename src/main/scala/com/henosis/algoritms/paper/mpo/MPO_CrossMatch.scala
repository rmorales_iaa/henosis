/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  01h:00m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.string.MyString
import com.common.util.time.Time
import com.henosis.algoritms.paper.mpo.MPO_Common.PAPER_MPO_TYPE
import com.henosis.database.mpc.MpcDB
import com.henosis.database.mpc.MpcDB.MPC_BASIC_INFO_COL_SEQ
import com.henosis.database.paper.mpo.MPO_PaperDB
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.atomic.AtomicInteger
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
trait MPO_CrossMatch extends MyLogger {
  //---------------------------------------------------------------------------
  val dbHub: DatabaseHub
  val db: MongoDB
  val coll: MongoCollection[Document]

  val mpoType: PAPER_MPO_TYPE
  val csvHeader: Seq[String]
  //---------------------------------------------------------------------------
  val outputCsv: String
  val conf: MyConf
  val sep: String = "\t"
  //---------------------------------------------------------------------------
  //stats
  val sourceWrittenCount = new AtomicInteger(0)
  val sourceFullMatchedCount = new AtomicInteger(0)
  val sourcePartialMatchedCount = new AtomicInteger(0)
  //-------------------------------------------------------------------------
  private def writeRow(bw: BufferedWriter
                       , mpo: MPO_Common
                       , mpcDoc: Option[Document]
                       , sdssMocDoc: Option[Document]
                       , twoMassMocDoc: Option[Document]
                       , tessMocDocSeq: Option[Seq[Document]]
                       , gaiaMocDocSeq: Option[Seq[Document]]
                       , paperDB: MPO_PaperDB) = {

    val matchInfoSeq = ArrayBuffer[String](paperDB.paperName)

    if (!mpcDoc.isEmpty)        matchInfoSeq += "'MPC'"
    if (!sdssMocDoc.isEmpty)    matchInfoSeq += "'SDSS MOC'"
    if (!twoMassMocDoc.isEmpty) matchInfoSeq += "'2Mass MOC'"
    if (!tessMocDocSeq.isEmpty) matchInfoSeq += "'Tess MOC'"
    if (!gaiaMocDocSeq.isEmpty) matchInfoSeq += "'GAIA 2 MOC'"

    if (matchInfoSeq.length == 6) {
      matchInfoSeq.clear()
      matchInfoSeq += "'ALL'"
    }

    val matchInfo = matchInfoSeq.mkString("(",";",")")

    if (matchInfoSeq.length > 6) {
      sourceFullMatchedCount.addAndGet(1)
      info(s"Writing the mpo: ${mpo.getSimpleName} fully matched: ${sourceFullMatchedCount.get}")
    }
    else {
      sourcePartialMatchedCount.addAndGet(1)
      info(s"Writing the mpo: ${mpo.getSimpleName} partially matched: $sourcePartialMatchedCount : " +matchInfo)
    }

    //order and match info
    val i = sourceWrittenCount.addAndGet(1)
    bw.write(i + sep)
    bw.write(matchInfo + sep)

    //paper source
    bw.write(mpo.getAsCsvRow(sep) + sep)

    //mpc
    if (mpcDoc.isDefined) MpcDB.writeCsvMocInfo(mpcDoc.get, bw, sep)
    else for (_ <- 1 to MPC_BASIC_INFO_COL_SEQ.length) bw.write("-1" + sep)

    //SDSS
    if (sdssMocDoc.isDefined) paperDB.writeCsvSdssMoc(sdssMocDoc.get, bw, sep)
    else for (_ <- 1 to paperDB.SDSS_MOC_BASIC_INFO_COL_SEQ.length) bw.write("-1" + sep)

    //2Mass
    if (twoMassMocDoc.isDefined) paperDB.writeCsv2MassMoc(twoMassMocDoc.get, bw, sep)
    else for (_ <- 1 to paperDB.TWO_MASS_MOC_BASIC_INFO_COL_SEQ.length) bw.write("-1" + sep)

    //Tess
    if (tessMocDocSeq.isDefined) paperDB.writeCsvTessMoc(tessMocDocSeq.get, bw, sep)
    else for (_ <- 1 to paperDB.TESS_MOC_BASIC_INFO_COL_SEQ.length) bw.write("-1" + sep)

    //gaia
    if (gaiaMocDocSeq.isDefined) paperDB.writeCsvGaia_2_Moc(gaiaMocDocSeq.get, bw, sep)
    else for (_ <- 1 to paperDB.GAIA_2_MOC_BASIC_INFO_COL_SEQ.length) bw.write("-1" + sep)

    //end of row
    bw.write("\n")
  }
  //---------------------------------------------------------------------------
  def reportStats(paperDB: MPO_PaperDB, totalDocCount: Long) = {
    //stats
    val fixedSize = 82
    val paperName = paperDB.paperName
    info(MyString.rightPadding(s"Total $paperName mpo's" ,fixedSize) + ": " + totalDocCount)
    info(MyString.rightPadding(s"Total $paperName mpo's full matched       " ,fixedSize) + ": " + sourceFullMatchedCount.get)
    info(MyString.rightPadding(s"Total $paperName mpo's partially matched   " ,fixedSize) + ": " + sourcePartialMatchedCount.get)
    paperDB.reportStats(fixedSize)
  }
  //---------------------------------------------------------------------------
  def crossMatch(paperDB: MPO_PaperDB) = {
    //---------------------------------------------------------------------------
    val mpoCount = coll.estimatedDocumentCount.results.head
    info(s"Calculating: ${paperDB.paperName} catalog cross match with 'MPC' 'Gaia2' with $mpoCount mpo's")
    val coreCount = conf.getInt("Configuration.coreCount")

    //info
    info(s"Using core count for processing: $coreCount")

    //create output file and write header
    val oFile = Path.getParentPath(outputCsv) + Path.getOnlyFilenameNoExtension(outputCsv) + "_" + Time.getTimeStampNoZone() + ".csv"
    val bw = new BufferedWriter(new FileWriter(new File(oFile)))
    val header = Seq("seq","matchInfo") ++
      csvHeader ++
      MpcDB.MPC_BASIC_INFO_CSV_HEADER ++
      paperDB.SDSS_MOC_BASIC_INFO_COL_SEQ ++
      paperDB.TWO_MASS_MOC_BASIC_INFO_COL_SEQ ++
      paperDB.TESS_MOC_BASIC_INFO_COL_SEQ ++
      paperDB.GAIA_2_MOC_BASIC_INFO_CSV_HEADER
    bw.write(header.mkString(sep) + "\n")
    //---------------------------------------------------------------------------
    class MyParallelTask(docSeq: Seq[Document]) extends ParallelTask[Document](
      docSeq
      , coreCount
      , isItemProcessingThreadSafe = true) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(doc: Document): Unit = {
        val mpo = MPO_Common.build(doc, mpoType)
        Try {
          val mpcDoc = paperDB.getMatchMpc(dbHub,mpo)
          val mpcID = if (mpcDoc.isEmpty) None else Some(mpcDoc.get("Number").asString().getValue.toInt)
          val gaiaSdssDoc = paperDB.getMatchSdss(dbHub,mpo,mpcID)
          val gaia2MassDoc = paperDB.getMatch2Mass(dbHub,mpo,mpcID)
          val gaiaTessDocSeq = paperDB.getMatchTess(dbHub,mpo,mpcID)
          val gaiaMocDocSeq = paperDB.getMatchGaia(dbHub,mpo,mpcID)
          synchronized(writeRow(bw, mpo, mpcDoc, gaiaSdssDoc, gaia2MassDoc, gaiaTessDocSeq, gaiaMocDocSeq, paperDB))
        }
        match {
          case Success(_) =>
          case Failure(ex) => error(s"Error processing ${paperDB.paperName} mpo: ${mpo.getSimpleName}." + ex.toString)
            error(ex.getStackTrace.mkString("\n"))
        }
      }
    }
    //-------------------------------------------------------------------------
    val docSeq = coll.find().results
    new MyParallelTask(docSeq)
    bw.close()
    reportStats(paperDB, docSeq.length)
    info(s"${paperDB.paperName} cross match file generated: '$oFile'")
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file MPO_CrossMatch.scala
//=============================================================================
