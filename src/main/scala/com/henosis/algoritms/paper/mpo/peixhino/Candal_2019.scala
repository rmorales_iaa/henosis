/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Sep/2021
 * Time:  17h:17m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo.peixhino

//=============================================================================
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.mpo.MPO_Common.{PAPER_PEIXHINO_2015}
import com.henosis.algoritms.paper.mpo.MPO_CrossMatch
import com.henosis.database.paper.mpo.candal.MPO
//=============================================================================
//=============================================================================
object Peixhino_2015 {
  //---------------------------------------------------------------------------
  val paperName = "'Peixhino_2015'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Peixhino_2015(dbHub: DatabaseHub
                         , outputCsv: String
                         , conf: MyConf) extends MPO_CrossMatch {
  //---------------------------------------------------------------------------
  val db =  dbHub.peixhino_2015_DB
  val coll = db.getCollection
  val mpoType = PAPER_PEIXHINO_2015
  val csvHeader = MPO.getColNameSeq("")
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Peixhino_2015.scala
//=============================================================================
