/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Sep/2021
 * Time:  15h:08m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo.alvarez
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.geometry.point.Point2D_Double
import com.common.jpl.horizons.Horizons
import com.common.logger.MyLogger
import com.henosis.algoritms.phaseCurve.{PhaseCurve, PhaseCurveMagnitudeSpaceG_1_2}
import com.henosis.database.sdsssMoc_4.SdssMoc_4_DB
import org.apache.commons.csv.{CSVFormat, CSVParser}
import org.apache.commons.math3.distribution.NormalDistribution
//=============================================================================
import org.mongodb.scala.Document
import org.mongodb.scala.model.Filters.{and, lt}
import org.mongodb.scala.model.Aggregates._
import org.mongodb.scala.MongoCollection
import org.mongodb.scala.model.Accumulators.{max, min, push, sum}
import org.mongodb.scala.model.Filters.notEqual
import scala.collection.JavaConverters.asScalaBufferConverter
import java.io.{BufferedReader, File, FileReader}
//=============================================================================
//=============================================================================
object Alvarez_2021 {
  //---------------------------------------------------------------------------
  val paperName = "'Alvarez_2021'"
  //---------------------------------------------------------------------------
  private final val COL_DETECTION_COUNT = "total_detection_count"
  private final val COL_PHASE_ANGLE     = "phase"
  private final val COL_DESIGNATION     = "designation"
  //---------------------------------------------------------------------------
  private val mpcInfoMap = scala.collection.mutable.Map[Int,String]()
  //---------------------------------------------------------------------------
  def loadData(fileName: String) = {
    val br = new BufferedReader(new FileReader(new File(fileName)))
    val inputFormat = CSVFormat
      .DEFAULT
      .withHeader()
      .withSkipHeaderRecord()
    (new CSVParser(br, inputFormat).getRecords.asScala map { r =>
      val split = r.get(0).split("\t")
      Point2D_Double(split(0).toDouble, split(1).toDouble)
    }).toArray
  }
  //---------------------------------------------------------------------------
  def test_1() = {

    val sampleCount = 10000
    val stdDev = 0.01
    //(phase angle, observed magnitude in the filter)
    val data = Array(
        Point2D_Double(6.72, 2.13)
      , Point2D_Double(4.27, 2.209)
      , Point2D_Double(24.07, 2.055)
      , Point2D_Double(4.9,  2.067)
      , Point2D_Double(1.63, 2.213))

    //error in the filter
    val filterError =  Array( 0.02
                            , 0.01
                            , 0.01
                            , 0.03
                            , 0.02)

    val dist = new NormalDistribution(0, stdDev)
    val splineSeq = PhaseCurve.getSplineSeq
    val r = (for (_ <- 0 until sampleCount) yield {
      val randomValue = dist.sample()
      val phaseCurve = PhaseCurveMagnitudeSpaceG_1_2.fit(data map { p => Point2D_Double(p.x, p.y + randomValue) }, splineSeq)
      val r = phaseCurve.getModelParameter()
      println(r.mkString("{",",","}"))
      r
    }).toArray

    println(r.length)
  }

  //---------------------------------------------------------------------------
  def simulate() = {

    val fileName = "/media/data/images/tools/Hg1g2/44_Nysa.dat"
   // val phaseCurve = PhaseCurveMagnitudeSpaceG_1_G_2.fit(loadData(fileName),PhaseCurve.getSplineSeq)
    val phaseCurve = PhaseCurveMagnitudeSpaceG_1_2.fit(loadData(fileName),PhaseCurve.getSplineSeq)
    
    val estimatedErrorSeq = PhaseCurve.estimateError(phaseCurve.getModelParameter, phaseCurve.getFitCovariance)

    println(estimatedErrorSeq)

  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Alvarez_2021._
case class Alvarez_2021(dbHub: DatabaseHub
                       , outputCsv: String
                       , conf: MyConf) extends MyLogger {
  //---------------------------------------------------------------------------
  private val collSdssMoc_4_DB = SdssMoc_4_DB(dbHub.mpoDB)
  private val collSdssMoc = collSdssMoc_4_DB.collection

  private val carry_2016_DB = dbHub.carry_2016_DB
  private val collCarry = carry_2016_DB.getCollection

  private val horizonsDB = Horizons().db

  private val magnitudeError  = conf.getDouble("Configuration.filter.magnitudeErrorLessThan")
  private val phaseAngleDifferentCount  = conf.getInt("Configuration.filter.phaseAngleDifferentGreaterOrEqualThan")
  private val phaseAngleRange  = conf.getDouble("Configuration.filter.phaseAngleRangeGreaterOrEqualThan")

  val coreCountCrossMatchingHorizons =  conf.getInt("Configuration.filter.coreCountCrossMatchingHorizons")
  //---------------------------------------------------------------------------
  private val SDSS_MOC_4_COL_LIST = Seq("_id",COL_DESIGNATION,COL_DETECTION_COUNT,COL_PHASE_ANGLE)
  //---------------------------------------------------------------------------
  private val sep = "\t"
  //---------------------------------------------------------------------------
  def findNameSeq(coll: MongoCollection[Document]) = {
    val docSeq = coll.aggregate(
      List(

        //filter by magnitude error
        `match`( and(
          notEqual(COL_DESIGNATION, "-")
          , lt("uErr",magnitudeError)
          , lt("gErr",magnitudeError)
          , lt("rErr",magnitudeError)
          , lt("iErr",magnitudeError)
          , lt("zErr",magnitudeError)
        ))

        //group by designation
       , group("$" + COL_DESIGNATION
          , max("max", "$" + COL_PHASE_ANGLE)
          , min("min", "$" + COL_PHASE_ANGLE)
          , sum("count",1)
          , push("phaseSeq",  "$" + COL_PHASE_ANGLE)
        )

      )).getResultDocumentSeq

    docSeq.flatMap { doc =>
      //get the distinct values of the phase seq
      val phaseSeq = doc("phaseSeq").asArray().getValues().asScala.toArray.map {t=> t.asDouble().getValue}.distinct

      //filter by the number of different phase angles
      if (phaseSeq.length >= phaseAngleDifferentCount) {
        val phaseRange = doc("max").asDouble().getValue - doc("min").asDouble().getValue
        if (phaseRange >= phaseAngleRange) Some(doc("_id").asString().getValue)
        else None
      }
      else None
    }
  }
  //---------------------------------------------------------------------------
  private def getMpcID(nameSeq: Seq[String]) = {
    /*horizonsDB.existAnyName(nameSeq.toArray.sorted,coreCountCrossMatchingHorizons) map { doc=>
      if(doc.isDefined)
        doc.get("_id").asInt64().getValue + sep + doc.get("name").asString().getValue
      else "None" + sep + "None"
    }*/ ""
  }
  //---------------------------------------------------------------------------
  def filterAndCrossMatch() = {
    info("Filtering databases: 'SDSS MOC4' and 'SVOMOC (Carry_2016)'")
    val nameSet_1 = findNameSeq(collSdssMoc).toSet
    val nameSet_2 = findNameSeq(collCarry).toSet

    val onlySet_1 = nameSet_1.diff(nameSet_2)
    val commonSet = nameSet_1.intersect(nameSet_2)
    val onlySet_2 = nameSet_1.diff(nameSet_1)

    println("only SDSS MOC 4 : " + onlySet_1.size )
    println("common          : " + commonSet.size)
    println("only SVOMOC     : " + onlySet_2.size)

    info("Matching filters")

    //cross match with horizons database
    //collSdssMoc_4_DB.saveFile(nameSet_1.toSeq,getMpcID(nameSet_1.toSeq),"output/sdss_moc_4.csv")
   // carry_2016_DB.saveFile(nameSet_2.toSeq, getMpcID(nameSet_2.toSeq) ,"output/svomoc_carry_2016.csv")
  }
  //---------------------------------------------------------------------------
  val docSeq = filterAndCrossMatch
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Alvarez_2021.scala
//=============================================================================
