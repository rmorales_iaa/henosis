/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  10/Sep/2021 
 * Time:  17h:17m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo.carry
//=============================================================================
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.mpo.MPO_Common.PAPER_CARRY_2016
import com.henosis.algoritms.paper.mpo.MPO_CrossMatch
import com.henosis.database.paper.mpo.carry.MPO
//=============================================================================
//=============================================================================
object Carry_2016 {
  //---------------------------------------------------------------------------
  val paperName = "'Carry_2016_DB'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Carry_2016(dbHub: DatabaseHub
                         , outputCsv: String
                         , conf: MyConf) extends MPO_CrossMatch {
  //---------------------------------------------------------------------------
  val db =  dbHub.carry_2016_DB
  val coll = db.getCollection
  val mpoType = PAPER_CARRY_2016
  val csvHeader = MPO.CSV_HEADER
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Carry_2016.scala
//=============================================================================
