/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Sep/2021
 * Time:  11h:34m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo.veres
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.mpo.MPO_Common.PAPER_VERES_2015
import com.henosis.algoritms.paper.mpo.MPO_CrossMatch
import com.henosis.database.paper.mpo.veres.Veres_2015_DB_Importer
//=============================================================================
object Veres_2015 {
  //---------------------------------------------------------------------------
  val paperName = "'Veres_2015'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Veres_2015(dbHub: DatabaseHub
                        , outputCsv: String
                        , conf: MyConf) extends MPO_CrossMatch {
  //---------------------------------------------------------------------------
  val db =  dbHub.veres_2015_DB
  val coll = db.getCollection
  val mpoType = PAPER_VERES_2015
  val csvHeader = Veres_2015_DB_Importer.CSV_HEADER
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Veres_2015.scala
//=============================================================================
