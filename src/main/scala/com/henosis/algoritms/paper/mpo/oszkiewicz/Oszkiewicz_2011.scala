/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Sep/2021
 * Time:  11h:34m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.mpo.oszkiewicz
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.mpo.MPO_Common.PAPER_OSZKIEWICZ_2011
import com.henosis.algoritms.paper.mpo.MPO_CrossMatch
import com.henosis.database.paper.mpo.oszkiewicz.Oszkiewicz_2011_DB_Importer
//=============================================================================
//=============================================================================
object Oszkiewicz_2011 {
  //---------------------------------------------------------------------------
  val paperName = "'Oszkiewicz_2011'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Oszkiewicz_2011(dbHub: DatabaseHub
                        , outputCsv: String
                        , conf: MyConf) extends MPO_CrossMatch {
  //---------------------------------------------------------------------------
  val db =  dbHub.oszkiewicz_2011_DB
  val coll = db.getCollection
  val mpoType = PAPER_OSZKIEWICZ_2011
  val csvHeader = Oszkiewicz_2011_DB_Importer.CSV_HEADER
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Oszkiewicz_2011.scala
//=============================================================================
