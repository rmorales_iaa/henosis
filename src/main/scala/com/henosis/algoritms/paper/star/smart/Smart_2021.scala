/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  29/Aug/2021
  * Time:  12h:50m
  * Description: None
  */
//=============================================================================
package com.henosis.algoritms.paper.star.smart
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.star.StarCommon.SMART_2021_STAR
import com.henosis.algoritms.paper.star.{StarCommon, StarCrossMatch}
import com.henosis.database.paper.star.StarPaperDB
import com.henosis.database.paper.star.smart.{Smart_2021_DB, Star}
//=============================================================================
import java.io.BufferedWriter
//=============================================================================
//=============================================================================
object Smart_2021 {
  //---------------------------------------------------------------------------
  private val gaia_dr_3_source_spg_csv       = com.henosis.database.gaia.source.gaiaDR3.performance_verification.synthetic_photometry_gspc.Source.getColNameSeq("GAIA_DR_3_SPG_")
  private val gaia_dr_3_source_spg_empty_row = ("null\t" * gaia_dr_3_source_spg_csv.size).dropRight(1)
  //---------------------------------------------------------------------------
}
//=============================================================================
import Smart_2021._
case class Smart_2021(dbHub: DatabaseHub
                      , outputCsv: String
                      , conf: MyConf) extends StarCrossMatch {
  //---------------------------------------------------------------------------
  val db            = dbHub.smart_2021_DB
  val coll          = db.getCollection
  val starType      = SMART_2021_STAR
  val starCsvHeader = Star.getColNameSeq(Smart_2021_DB.paperName)
  //---------------------------------------------------------------------------
  override def starCrossMatchHeaderCsv(bw: BufferedWriter) =
    bw.write(s"GAIA_EDR3_ID${sep}Hipparcos_id${sep}gaia_match_by_id" + sep +
      gaia_dr_3_source_spg_csv.mkString(sep)
      + "\n")
  //---------------------------------------------------------------------------
  override def starCrossMatchCsv(paperDB: StarPaperDB, star: StarCommon, bw: BufferedWriter) = {
    val crossMatchHipparcos    = paperDB.getMatchGaiaWithHipparcos(star, dbHub)

    if (crossMatchHipparcos.isDefined)
      synchronized{
        val gaiaDoc              = crossMatchHipparcos.get._1
        val hipparcosMatchSource = crossMatchHipparcos.get._2
        val gaiaDocMatchByID     = crossMatchHipparcos.get._3

        val gaiaID      = gaiaDoc("_id").asInt64().getValue
        val hipparcosID = hipparcosMatchSource.original_ext_source_id

        val gaia_DR_3_SPG = paperDB.getMatchSmartWithGaia_DR_3_SPG(gaiaID, dbHub)  //synthetic photometry gspc
        val gaia_DR_3_SPG_csvLine =
          if(gaia_DR_3_SPG.isDefined) gaia_DR_3_SPG.get.getAsCsvLine(sep)
          else gaia_dr_3_source_spg_empty_row

        bw.write(
          s"$gaiaID$sep$hipparcosID$sep$gaiaDocMatchByID" + sep +
          gaia_DR_3_SPG_csvLine +
          "\n")
      }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Smart_2021.scala
//=============================================================================
