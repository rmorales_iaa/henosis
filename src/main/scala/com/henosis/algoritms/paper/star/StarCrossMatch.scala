//=============================================================================
package com.henosis.algoritms.paper.star
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.database.postgreDB.gaia_dr_3.Source
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.string.MyString
import com.common.util.time.Time
import com.henosis.algoritms.paper.star.StarCommon.COMMON_STAR_TYPE
import Source._
import com.henosis.database.panStarrs.PanSTARRS_Source
import com.henosis.database.panStarrs.PanSTARRS_Source.{PANSTARRS_CSV_PHOTOMETRY_HEADER, PANSTARRS_PHOTOMETRY_COL_SEQ}
import com.henosis.database.paper.star.StarPaperDB
import com.henosis.database.twoMass.TwoMassSource
import com.henosis.database.twoMass.TwoMassSource.{TWO_MASS_COL_CSV_PHOTOMETRY_HEADER, TWO_MASS_COL_PHOTOMETRY_SEQ}
import org.mongodb.scala.{Document, MongoCollection}
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import java.util.concurrent.atomic.AtomicInteger
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
trait StarCrossMatch extends MyLogger {
  //---------------------------------------------------------------------------
  val dbHub: DatabaseHub
  val db: MongoDB
  val coll: MongoCollection[Document]

  val starType: COMMON_STAR_TYPE
  val starCsvHeader: Seq[String]
  //---------------------------------------------------------------------------
  val outputCsv: String
  val conf: MyConf
  val sep: String = "\t"
  //---------------------------------------------------------------------------
  //stats
  val sourceWrittenCount = new AtomicInteger(0)
  val sourceFullMatchedCount = new AtomicInteger(0)
  val sourcePartialMatchedCount = new AtomicInteger(0)

  //-------------------------------------------------------------------------
  private def writeRow(bw: BufferedWriter
                       , star: StarCommon
                       , twoMassDoc: Option[Document]
                       , gaiaDoc: Option[Document]
                       , panSTARRS_Doc: Option[Document]
                       , paperName: String) = {
    val matchInfoSeq = ArrayBuffer[String](paperName)

    if (!twoMassDoc.isEmpty)    matchInfoSeq += "'2Mass'"
    if (!gaiaDoc.isEmpty)       matchInfoSeq += "'GaiaDR3 ED3'"
    if (!panSTARRS_Doc.isEmpty) matchInfoSeq += "'PanSTARRS'"

    if (matchInfoSeq.length == 4) {
      matchInfoSeq.clear()
      matchInfoSeq += "'ALL'"
    }

    val matchInfo = matchInfoSeq.mkString("(", ";", ")")

    if (matchInfoSeq.length > 4) {
      sourceFullMatchedCount.addAndGet(1)
      info(s"Writing the star: '${star.name}' fully matched: ${sourceFullMatchedCount.get}")
    }
    else {
      sourcePartialMatchedCount.addAndGet(1)
      info(s"Writing the star: '${star.name}' partially matched: $sourcePartialMatchedCount : " + matchInfo)
    }

    //order and match info
    val i = sourceWrittenCount.addAndGet(1)
    bw.write(i + sep)
    bw.write(matchInfo + sep)

    //paper source
    bw.write(star.getAsCsvRow(sep) + sep)

    //gaia
    if (gaiaDoc.isDefined) Source.writePhotometricInfo(gaiaDoc.get, bw, sep)
    else for (_ <- 1 to GAIA_PHOTOMETRY_COL_SEQ.length) bw.write("-1" + sep)

    //two mass
    if (twoMassDoc.isDefined) TwoMassSource.writePhotometricInfo(twoMassDoc.get, bw, sep)
    else for (_ <- 1 to TWO_MASS_COL_PHOTOMETRY_SEQ.length) bw.write("-1" + sep)

    //PanSTARRS
    if (panSTARRS_Doc.isDefined)
      PanSTARRS_Source.writePhotometricInfo(panSTARRS_Doc.get, bw, sep)
    else for (_ <- 1 to PANSTARRS_PHOTOMETRY_COL_SEQ.length) bw.write("-1" + sep)

    //end of row
    bw.write("\n")
  }

  //---------------------------------------------------------------------------
  def reportStats(paperDB: StarPaperDB, totalDocCount: Long) = {
    //stats
    val fixedSize = 82
    val paperName = paperDB.paperName
    info(MyString.rightPadding(s"Total $paperName stars", fixedSize) + ": " + totalDocCount)
    info(MyString.rightPadding(s"Total $paperName stars full matched       ", fixedSize) + ": " + sourceFullMatchedCount.get)
    info(MyString.rightPadding(s"Total $paperName stars partially matched   ", fixedSize) + ": " + sourcePartialMatchedCount.get)

    paperDB.reportStats(fixedSize)
  }
  //---------------------------------------------------------------------------
  protected def starCrossMatchHeaderCsv(bw: BufferedWriter) = {
    val header = Seq("seq", "matchInfo") ++
      starCsvHeader ++
      GAIA_CSV_PHOTOMETRY_HEADER ++
      TWO_MASS_COL_CSV_PHOTOMETRY_HEADER ++
      PANSTARRS_CSV_PHOTOMETRY_HEADER
    bw.write(header.mkString(sep) + "\n")
  }
  //---------------------------------------------------------------------------
  protected def starCrossMatchCsv(paperDB: StarPaperDB, star: StarCommon, bw: BufferedWriter) = {
    val twoMassDoc = paperDB.getMatch2Mass(dbHub, star)
    val gaiaDoc = paperDB.getMatchGaiaWith2Mass(star, twoMassDoc, dbHub)
    val panSTARRS_Doc = paperDB.getBestMatchGaiaWithPanSTARSS(star, twoMassDoc, gaiaDoc, dbHub)
    synchronized(writeRow(bw, star, twoMassDoc, gaiaDoc, panSTARRS_Doc, paperDB.paperName))
  }
  //---------------------------------------------------------------------------
  def crossMatch(paperDB: StarPaperDB) = {
    //---------------------------------------------------------------------------
    val starCount = coll.estimatedDocumentCount.results.head
    info(s"Calculating: ${paperDB.paperName} catalog cross match with $starCount stars ")
    val coreCount = conf.getInt("Configuration.coreCount")

    //info
    info(s"Using core count for processing: $coreCount")
    //create output file and write header

    val oFile = Path.resetDirectory(s"output/${paperDB.paperName}") + Time.getTimeStampNoZone() + ".csv"
    val bw = new BufferedWriter(new FileWriter(new File(oFile)))
    starCrossMatchHeaderCsv(bw)
    //---------------------------------------------------------------------------
    class MyParallelTask(docSeq: Seq[Document]) extends ParallelTask[Document](
      docSeq
      , coreCount
      , isItemProcessingThreadSafe = true) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(doc: Document): Unit = {
        val star = StarCommon.build(doc, starType)
        Try {
          starCrossMatchCsv(paperDB,star, bw)
        }
        match {
          case Success(_) =>
          case Failure(ex) => error(s"Error processing ${paperDB.paperName} star: '${star.name}' ." + ex.toString)
            error(ex.getStackTrace.mkString("\n"))
        }
      }
    }
    //-------------------------------------------------------------------------
    //parse all stars and join the correspond star un gaia and 2Mass
    val docSeq = coll.find().results
    new MyParallelTask(docSeq)
    bw.close()
    reportStats(paperDB, docSeq.length)
    info(s"${paperDB.paperName} cross match file generated: '$oFile'")
  }
  //===========================================================================
}
//=============================================================================
//End of file StarCrossMatch.scala
//=============================================================================
