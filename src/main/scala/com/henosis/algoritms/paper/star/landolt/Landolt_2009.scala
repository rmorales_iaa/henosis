/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Aug/2021
 * Time:  11h:22m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.star.landolt
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.star.StarCommon.LANDOLT_2009_STAR
import com.henosis.algoritms.paper.star.StarCrossMatch
import com.henosis.database.paper.star.landolt.Landolt_2009_DB_Importer
//=============================================================================
//=============================================================================
object Landolt_2009 {
  //---------------------------------------------------------------------------
  val paperName = "'Landolt_2009'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Landolt_2009(dbHub: DatabaseHub
                        , outputCsv: String
                        , conf: MyConf) extends StarCrossMatch {
  //---------------------------------------------------------------------------
  val db =  dbHub.landolt_2009_DB
  val coll = db.getCollection

  val starType = LANDOLT_2009_STAR

  val starCsvHeader = Landolt_2009_DB_Importer.CSV_HEADER ++
    Seq("gaia_number_of_mates"
      , "gaia_number_of_neighbours"
      , "gaia_xm_flag"
      , "gaia_badMatchInfo"
      , "panSTARR_number_of_mates"
      , "panSTARR_number_of_neighbours"
      , "panSTARR_xm_flag"
      , "panSTARR_badMatchInfo")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Landolt_2009_Source.scala
//=============================================================================
