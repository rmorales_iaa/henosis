/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Aug/2021
 * Time:  19h:20m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.star

//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.geometry.point.Point2D_Double
import com.henosis.database.paper.star.landolt.{Landolt_2009_DB_Importer, Landolt_2013_DB_Importer}
import com.henosis.database.paper.star.smart.Smart_2021_DB_Importer
import com.henosis.database.paper.star.stauffer.Stauffer_2010_DB_Importer
import org.mongodb.scala.Document
//=============================================================================
//=============================================================================
object StarCommon {
  //---------------------------------------------------------------------------
  sealed trait COMMON_STAR_TYPE
  //---------------------------------------------------------------------------
  case object LANDOLT_2009_STAR    extends COMMON_STAR_TYPE
  case object LANDOLT_2013_STAR    extends COMMON_STAR_TYPE
  case object STAUFFER_2010_STAR   extends COMMON_STAR_TYPE
  case object SMART_2021_STAR      extends COMMON_STAR_TYPE
  //---------------------------------------------------------------------------
  def build(doc: Document, starType: COMMON_STAR_TYPE) : StarCommon = {
    starType match {
      case LANDOLT_2009_STAR  => Landolt_2009_DB_Importer(doc)
      case LANDOLT_2013_STAR  => Landolt_2013_DB_Importer(doc)
      case STAUFFER_2010_STAR => Stauffer_2010_DB_Importer(doc)
      case SMART_2021_STAR    => Smart_2021_DB_Importer(doc)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait StarCommon {
  //---------------------------------------------------------------------------
  val name: String
  val twoMassID: String
  val ra: Double
  val dec: Double

  var gaia_number_of_mates:             Int = -1
  var gaia_number_of_neighbours:        Int = -1
  var gaia_xm_flag:                     Int = -1
  var gaia_badMatchInfo:                String = "-1"

  var panSTARR_number_of_mates:         Int = -1
  var panSTARR_number_of_neighbours:    Int = -1
  var panSTARR_xm_flag:                 Int = -1
  var panSTARR_badMatchInfo:            String = "-1"

  var raDecPM = Point2D_Double.POINT_ZERO //proper motion
  //---------------------------------------------------------------------------
  def getRaDec = Point2D_Double(ra,dec)
  //---------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t"): String
  //---------------------------------------------------------------------------
  def getRaDecWithProperMotion(observingDate: String, referenceEpoch: String) =
    Conversion.properMotion(observingDate, Point2D_Double(ra,dec), referenceEpoch, raDecPM)
  //---------------------------------------------------------------------------
  def getInfo(paperName: String) = {
    val r = Conversion.DD_to_HMS(ra)
    val d = Conversion.DD_to_DMS(dec)
    s"\n\t$paperName Star name     : '$name'" +
    (if (twoMassID.isEmpty) "" else s"\n\t$paperName 2Mass ID      : ${twoMassID}") +
    s"\n\t$paperName (ra,dec)      : $ra $dec" +
    s"\n\t$paperName (ra,dec)      : ${r._1 + ":" + r._2 + ":" + r._3} ${d._1 + ":" + d._2 + ":" + d._3}"
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file StarCommon.scala
//=============================================================================
