/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Aug/2021
 * Time:  12h:50m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.paper.star.stauffer
//=============================================================================
import com.common.DatabaseHub
import com.common.configuration.MyConf
import com.henosis.algoritms.paper.star.StarCommon.STAUFFER_2010_STAR
import com.henosis.algoritms.paper.star.StarCrossMatch
import com.henosis.database.paper.star.stauffer.Stauffer_2010_DB_Importer
//=============================================================================
//=============================================================================
object Stauffer_2010 {
  //---------------------------------------------------------------------------
  val paperName = "'Stauffer_2010'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Stauffer_2010(dbHub: DatabaseHub
                         , outputCsv: String
                         , conf: MyConf) extends StarCrossMatch {
  //---------------------------------------------------------------------------
  val db =  dbHub.stauffer_2010_DB
  val coll = db.getCollection

  val starType = STAUFFER_2010_STAR

  val starCsvHeader = Stauffer_2010_DB_Importer.CSV_HEADER ++
    Seq("gaia_number_of_mates"
      , "gaia_number_of_neighbours"
      , "gaia_xm_flag"
      , "gaia_badMatchInfo"
      , "panSTARR_number_of_mates"
      , "panSTARR_number_of_neighbours"
      , "panSTARR_xm_flag"
      , "panSTARR_badMatchInfo")
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Stauffer_2010.scala
//=============================================================================
