/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  21/Jun/2022
  * Time:  19h:20m
  * Description: None
  */
//=============================================================================
package com.henosis.algoritms.gaia.gaia_dr_3.solarSystem
//=============================================================================
import com.common.configuration.MyConf
import com.common.constant.astronomy.AstronomicalUnit.{ASTRONOMICAL_UNIT_KM, LIGHT_SPEED_KM_S}
import com.common.database.mongoDB.Helpers._
import com.common.geometry.point.Point2D_Double
import com.common.hardware.cpu.CPU
import com.common.jpl.Spice
import com.common.jpl.Spice.{getEphemerisAndPhaseAngle, getHeliocentricRange}
import com.common.jpl.horizons.Horizons
import com.common.jpl.horizons.db.smallBody.SmallBodyDB
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.time.Time
import com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_observation.Source
//=============================================================================
import org.mongodb.scala.model.Filters.equal

import scala.util.{Failure, Success, Try}
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object Observation {
  //---------------------------------------------------------------------------
  private def sep = "\t"
  //---------------------------------------------------------------------------
  private def DEFAULT_OUTPUT_PATH = "output/gaia_dr_3/sso/"
  //---------------------------------------------------------------------------
  private def GAIA_KERNEL_PATH = "/home/rafa/proyecto/m2/input/spice/kernels/spk/spacecrafts/gaia_latest.bsp"
  //---------------------------------------------------------------------------
  private def JULIAN_DATE_2010_0 = 2455197.5D  //2010.0 JD
  //---------------------------------------------------------------------------
  private final val CSV_HEADER = com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_observation.Source.getColNameSeq() ++
    Array(
        "m2_jpl_lt(min)"
      , "m2_jpl_jd_corrected"
      , "m2_jpl_phase_angle(deg)"
      , "m2_jpl_heliocentric_range(r)(au)"
      , "m2_jpl_observer_range(delta)(au)"
      , "m2_jpl_ra(dec deg)"
      , "m2_jpl_dec(dec deg)"
      , "m2_offset_gaia_jpl_ra(mas)"
      , "m2_offset_gaia_jpl_dec(mas)"
      , "m2_spk_orbit_name")
  //---------------------------------------------------------------------------
  private def writeCsv(bw: BufferedWriter
                       , source: Source
                       , targetSpkID: Long
                       , observerSpkID: String
                       , version: String) = {

    val timeStamp = Time.fromJulian(source.epoch_utc +  JULIAN_DATE_2010_0)

    val (_, _, jplLtMin, jplPhaseAngle) = Spice.getEphemerisAndPhaseAngle (
        targetSpkID.toString
      , observerSpkID
      , timeStamp.format(Spice.ephemerisTimeFormatterWithMillis))

    val jplJD_Corrected =  Time.toJulian(timeStamp.minusNanos(Time.minuteToNanos(jplLtMin)))

    //get jpl estimated position
    val r = getEphemerisAndPhaseAngle(
        targetSpkID.toString
      , observerSpkID
      , timeStamp.toString
      , calculatePhaseAngle = false)
    val jplRadDec = Point2D_Double(r._1, r._2)

    //heliocentric and observer range
    val heliocentric_range = getHeliocentricRange(targetSpkID.toString, observerSpkID, timeStamp)
    val observerRange = (jplLtMin * 60 * LIGHT_SPEED_KM_S) / ASTRONOMICAL_UNIT_KM

    //offset between GAIA position and JPL position
    val offset =(Point2D_Double(source.ra,source.dec) - jplRadDec) * 3600 * 1000

    bw.write(source.getAsCsvLine(sep) + sep
      + jplLtMin + sep
      + jplJD_Corrected + sep
      + jplPhaseAngle + sep
      + heliocentric_range + sep
      + observerRange + sep
      + jplRadDec.x + sep
      + jplRadDec.y + sep
      + offset.x + sep
      + offset.y + sep
      + version
      + "\n")
  }
  //---------------------------------------------------------------------------
  private val mpcID_Seq = Path.getSortedFileList("/home/rafa/proyecto/m2/input/spice/kernels/spk/objects/",".bsp").map {
    s=> s.getName.split("_").head.toLong
  }
  //---------------------------------------------------------------------------
  class MyParallelTask(imageSeqID : Array[Long], obs: Observation) extends ParallelTask[Long](
      imageSeqID
      , threadCount = CPU.getCoreCount()
      , isItemProcessingThreadSafe = true) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(spkID: Long): Unit = {
      Try { obs.saveAsteroidObservationSeqBySpkID(spkID)}
      match {
        case Success(_) =>
        case Failure(ex) => error(s"Error processing spkID: '$spkID' . Continuing with the next one")
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.henosis.algoritms.gaia.gaia_dr_3.solarSystem.Observation._
case class Observation() extends  MyLogger {
  //---------------------------------------------------------------------------
  val db = com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_observation.DB(MyConf(MyConf.c.getString("Database.gaia_dr_3_sso_observation")))
  val horizons = Horizons()
  val smallBodyDB = SmallBodyDB()

  Path.resetDirectory(DEFAULT_OUTPUT_PATH)
  Spice.loadKernel(GAIA_KERNEL_PATH)

  new MyParallelTask(mpcID_Seq.toArray, this)

  Spice.unloadKernel(GAIA_KERNEL_PATH)
  //---------------------------------------------------------------------------
  private def saveAsteroidObservationSeqBySpkID(spkID: Long): Unit = {
    val mpcID   = horizons.getMpc_ID_BySpkID(spkID)
    if (mpcID != -1L) saveAsteroidObservationSeq(mpcID,spkID)
  }
  //---------------------------------------------------------------------------
  private def saveAsteroidObservationSeq(mpcID: Long, spkID: Long): Unit = {
    val name    = horizons.db.getBySpkId(spkID).head("name").asString().getValue.replaceAll("/","_")
    val version = smallBodyDB.getVersion(spkID)
    val spkKernelPath = ""

    info (s"Saving GAIA DR 3 Solar System observations of: '$name'")

    Path.ensureDirectoryExist(DEFAULT_OUTPUT_PATH)
    val csvFileName = DEFAULT_OUTPUT_PATH + mpcID + "_" + name + ".csv"
    val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))
    bw.write(CSV_HEADER.mkString(sep) + "\n")

    Spice.loadKernel(spkKernelPath)
    var count = 0
    db.collWithRegistry.find(equal(com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_observation.DB.COL_NAME_MPC_ID, mpcID))
      .results()
      .foreach {
        source=> writeCsv(bw, source, spkID, Spice.GAIA_CENTER_OBSERVER_ESA_SPK_ID_CODE, version)
        count +=1
      }
    Spice.unloadKernel(spkKernelPath)
    bw.close()
    info(s"Processed: $count observings for: '$name'")
    if (count == 0) MyFile.deleteFile(csvFileName)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Observation.scala
//=============================================================================
