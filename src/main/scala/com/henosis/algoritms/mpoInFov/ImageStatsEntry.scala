/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jul/2024
 * Time:  13h:51m
 * Description: None
 */
package com.henosis.algoritms.mpoInFov
//=============================================================================
import java.io.BufferedWriter
import java.time.LocalDateTime
//=============================================================================
//=============================================================================
case class ImageStatsEntry(imagePath: String
                           , timeStamp: LocalDateTime
                           , telescope: String
                           , filter: String
                           , var heliocentricRange: Double) {
  //-------------------------------------------------------------------------
  def write(bw: BufferedWriter
            , suffix: String
            , sep: String) = {
    bw.write(
      imagePath + sep +
        timeStamp.toString + sep +
        telescope + sep +
        filter + sep +
        heliocentricRange + suffix)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ImageStatsEntry.scala
//=============================================================================