/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jul/2024
 * Time:  13h:52m
 * Description: None
 */
package com.henosis.algoritms.mpoInFov
//=============================================================================
import java.io.BufferedWriter
//=============================================================================
//=============================================================================
private object MpoStatsEntry {
  //-------------------------------------------------------------------------
  def writeHeader(bw: BufferedWriter, sep: String) = {
    bw.write("mpo" + sep +
      "count" + sep +
      "spk_id" + sep +
      "mpc_id" + sep +
      "main_designation" + sep +
      "alternative_designation" + sep +
      "image_path_with_min_time" + sep +
      "image_min_time" + sep +
      "telescope_with_min_time" + sep +
      "filter_with_min_time" + sep +
      "heliocentric_range_with_min_time" + sep +
      "image_path_with_max_time" + sep +
      "image_max_time" + sep +
      "telescope_with_max_time" + sep +
      "filter_with_max_time" + sep +
      "heliocentric_range_with_max_time" + sep +
      "\n")
  }
  //-------------------------------------------------------------------------
}

//---------------------------------------------------------------------------
case class MpoStatsEntry(spkID: Long
                         , mpcID: Long
                         , var count: Long
                         , mainDesignation: String
                         , alternateDesignation: String
                         , var lastHeliocentricNotNan: Double
                         , var minImageStatsEntry: ImageStatsEntry
                         , var maxImageStatsEntry: ImageStatsEntry) {
  //-------------------------------------------------------------------------
  def getComposedName() = s"mpo_${spkID}_$mainDesignation"
  //-------------------------------------------------------------------------
  def write(bw: BufferedWriter, sep: String) = {
    bw.write(
      getComposedName() + sep +
      count + sep +
      spkID + sep +
      mpcID + sep +
      mainDesignation + sep +
      alternateDesignation + sep)
    minImageStatsEntry.write(bw, sep, sep)
    maxImageStatsEntry.write(bw, "\n", sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MpoStatsEntry.scala
//=============================================================================