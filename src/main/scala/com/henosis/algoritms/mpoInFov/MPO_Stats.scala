/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  10/Feb/2022
  * Time:  11h:18m
  * Description: Detects all mpo's present in a directory of images
  *              FOV = field of view
  */
//=============================================================================
package com.henosis.algoritms.mpoInFov
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.database.standadStar.LandoltDB
import com.common.database.mongoDB.spiceSPK.SpiceSPK_DB
import com.common.database.postgreDB.astrometry.{AstrometryDB, AstrometryEntry}
import com.common.geometry.point.Point2D_Double
import com.common.hardware.cpu.CPU
import com.common.image.focusType.ImageFocusMPO
import com.common.image.mpo.MpoLocation
import com.common.image.myImage.MyImage
import com.common.image.telescope.Telescope
import com.common.jpl.Spice
import com.common.jpl.Spice._
import com.common.jpl.horizons.Horizons
import com.common.logger.MyLogger
import com.common.math.MyMath
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.pattern.Pattern
import com.common.util.time.Time
import org.mongodb.scala.Document
//=============================================================================
import scalasql._
import java.io.{BufferedWriter, File, FileWriter}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.collection.mutable.ArrayBuffer
import scala.jdk.CollectionConverters.{CollectionHasAsScala, ConcurrentMapHasAsScala}
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object MPO_Stats {
  //---------------------------------------------------------------------------
  private final val DEFAULT_TMP_DIRECTORY = "output/tmp_spk_fov"
  //---------------------------------------------------------------------------
  private final val coreCount = CPU.getCoreCount()
  //---------------------------------------------------------------------------
  private val sep = ","
  //---------------------------------------------------------------------------
  private final val IGNORED_ADDITIONAL_STANDARD_STAR_SEQ = //Not included in LandoltDB
    Seq(
      "landolt"
      , "land"
      , "HAT-P"
      , "star"
      , "67P"
      , "HAT-P-23"
      , "GJ1253"
      , "WASP"
      , "COROT"
      , "CVSO30"
      , "GJ4276"
      , "GJ644"
      , "GJ1002"
      , "RU1491"
      , "PG0918"
      , "GJ83"
      , "GJ3622"
      , "SN2021"
      , "TOI2457"
      , "NGC"
      , "TOI"
      , "BLLac"
      , "GJ4276"
      , "GD 246"
      , "G97"
      , "estrella"
      , "trappist"
      , "GD71"
      , "zenith"
    )
  //---------------------------------------------------------------------------
  private val STANDARD_START_PG_4_PATTERN = ".*pg\\d{4}.*".r
  private val STANDARD_START_SA_2_PATTERN = ".*sa\\d{2}.*".r

  private val STANDARD_START_PG_2_PATTERN = "pg\\d{2}.*".r
  private val STANDARD_START_PG_3_PATTERN = "pg\\d{3}.*".r
  private val STANDARD_START_GSC_4_PATTERN = "gsc\\d{4}.*".r
  private val STANDARD_START_GSC_2_PATTERN = "gsc\\d{4}.*".r
  private val STANDARD_START_SA_4_PATTERN = "sa\\d{2}.*".r
  private val STANDARD_START_M_1_PATTERN = "m\\d{1}.*".r
  private val STANDARD_START_M_2_PATTERN = "m\\d{2}.*".r
  private val STANDARD_START_M_3_PATTERN = "m\\d{3}.*".r
  private val STANDARD_START_M_4_PATTERN = "m\\d{4}.*".r

  private val STANDARD_START_OBJECT_PATTERN_SEQ = List(
    STANDARD_START_PG_2_PATTERN
    , STANDARD_START_PG_3_PATTERN
    , STANDARD_START_PG_4_PATTERN
    , STANDARD_START_SA_2_PATTERN
    , STANDARD_START_GSC_4_PATTERN
    , STANDARD_START_GSC_2_PATTERN
    , STANDARD_START_SA_4_PATTERN
    , STANDARD_START_M_1_PATTERN
    , STANDARD_START_M_2_PATTERN
    , STANDARD_START_M_3_PATTERN
    , STANDARD_START_M_4_PATTERN
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.henosis.algoritms.mpoInFov.MPO_Stats._
case class MPO_Stats(inDirSeq: Option[List[String]]) extends MyLogger {
  //---------------------------------------------------------------------------
  Spice.init(verbose = false)  //Init SPICE

  private val fitsExtension = MyConf.c.getStringSeq("Common.fitsFileExtension")

  //output dir
  private val outputStatsDir = Path.resetDirectory(s"output/stats/${Time.getISO_DateLocalTimeStampWindowsCompatible()}")

  //databases
  private val horizonsDB = Horizons().db
  private val astrometryDB = AstrometryDB()
  private val landoltDB: LandoltDB = LandoltDB()

  //storage
  private val spkAtImageMap      = new java.util.concurrent.ConcurrentHashMap[String, ArrayBuffer[Long]]()
  private val mpoStatsMap        = new java.util.concurrent.ConcurrentHashMap[Long, MpoStatsEntry]()
  private val telescopeStatsMap  = new java.util.concurrent.ConcurrentHashMap[String, TelescopeStatsEntry]()
  private val filterStatsMap     = new java.util.concurrent.ConcurrentHashMap[String, FilterStatsEntry]()
  private val imageFovWithMpoSeq = if (inDirSeq.isDefined) Seq[AstrometryEntry[Sc]]()
                                   else getValidImageSeq()

  if (inDirSeq.isDefined) locateInBlindMode(inDirSeq.get)
  else locateUsingStoredKernel()

  info("Writing MPO's stats")
  writeMpoStats()

  //close databases
  horizonsDB.close()

  astrometryDB.close()
  landoltDB.close()

  Spice.close() //close SPICE
  astrometryDB.close()
  Path.deleteDirectory(DEFAULT_TMP_DIRECTORY)

  //---------------------------------------------------------------------------
  private def locateInBlindMode(inSeq: List[String]) = {
    //-------------------------------------------------------------------------
    class MyParallelTask(imageNameSeq : Array[String])
      extends ParallelTask[String](
        imageNameSeq
        , coreCount
        , isItemProcessingThreadSafe = true
        , message = Some(" processing SPICE SPK in blind mode")) {
      //----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String): Unit = {
        MpoLocation.locate(imageName, mpo = None, printReport = false, initSpiceLibrary = false).foreach { imageFocus =>
          addSpkAtImageMap(imageName,imageFocus.spiceSpk.spkID)
          updateMpoStats(imageName,imageFocus.spiceSpk.spkID)
        }
        //----------------------------------------------------------------------
      }
      //-------------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    inSeq.foreach { in =>
      if (!Path.directoryExist(in)) error(s"Directory '$in' does not exists")
      else
        new MyParallelTask(Path.getSortedFileList(in, fitsExtension).map(_.getAbsolutePath).toArray)
    }
  }
  //---------------------------------------------------------------------------
  private def locateUsingStoredKernel() = {

    //get latest SPICE SPK
    val spiceSpkMap = SpiceSPK_DB.getLastestSpk(Path.resetDirectory(DEFAULT_TMP_DIRECTORY))

    //process all SPICE SPKs
    var currentSpiceSpk = 1
    val totalSpiceSpk = spiceSpkMap.size
    spiceSpkMap.toSeq.sortWith(_._1 < _._1)
      .foreach { case (spkID,spkPath)=>
        info(s"Processing SPICE SPK:'$spkPath $currentSpiceSpk/$totalSpiceSpk'")

        loadKernel(spkPath)                       //load object kernel

        ProcessFovSeq(spkID)

        unloadKernel(spkPath)                    //unload object kernel
        currentSpiceSpk += 1
      }

    info("Processing SPICE kernel at Image")

    new ProcessSpkAtImageMap(spkAtImageMap.asScala.toArray)
  }
  //---------------------------------------------------------------------------
  private def addSpkAtImageMap(fileName: String, spkID: Long) = {
    val arr = if (spkAtImageMap.contains(fileName)) spkAtImageMap.get(fileName)
              else ArrayBuffer[Long]()
    arr += spkID
    spkAtImageMap.put(fileName,arr)
  }
  //---------------------------------------------------------------------------
  private def ProcessFovSeq(spkID: Long) = {
    //-------------------------------------------------------------------------
    class MyParallelTask(fovPosSeq : Array[Array[Int]])
      extends ParallelTask[Array[Int]](
        fovPosSeq
        , coreCount
        , isItemProcessingThreadSafe = true
        , verbose = false
        , message = Some(" processing SPICE SPK")) {
      //----------------------------------------------------------------------
      def userProcessSingleItem(posSeq: Array[Int]): Unit = {
        posSeq.foreach { pos =>
          val astrometryEntry = imageFovWithMpoSeq(pos)

          val (timeStamp, telescope, _, _, _, exposureTime, _, _, _) = MyImage.getMetadaFromImageName(astrometryEntry.fileName)

          //get observing time mid point
          val midPointTime = LocalDateTime.parse(timeStamp.toString, DateTimeFormatter.ISO_LOCAL_DATE_TIME).plusSeconds(exposureTime.toInt/2)
          val spiceMinPointTime = if (timeStamp.toString.length == Spice.defaultEphemeridesTimeFormatterSize) midPointTime.format(ephemerisTimeFormatter)
                                  else midPointTime.format(ephemerisTimeFormatterWithMillis)
          //check if mpo is in the FOV of the image
          val r = getEphemerisAndPhaseAngle(
            spkID.toString
            , Telescope.getSpkId(telescope)
            , spiceMinPointTime
            , calculatePhaseAngle = false)
          val raDec = Point2D_Double(r._1, r._2)

          if(raDec.x >= astrometryEntry.raMin  &&  raDec.x <= astrometryEntry.raMax &&
             raDec.y >= astrometryEntry.decMin &&  raDec.y <= astrometryEntry.decMax)
            addSpkAtImageMap(astrometryEntry.fileName,spkID)
        }
      }
      //----------------------------------------------------------------------
    }
    //------------------------------------------------------------------------
    val seq = Array.tabulate(imageFovWithMpoSeq.length)(n=>n)
    val partitionSeq = MyMath.getPartitionInt(0,seq.length-1,coreCount)
    new MyParallelTask(partitionSeq)
  }
  //--------------------------------------------------------------------------
  private def findSpkID_InHorizons(mpoSpkId: String
                                   , imageName:String): Seq[Document] = {
    var r = horizonsDB.getBySpkId(mpoSpkId.toLong)
    if (!r.isEmpty) return r
    r = horizonsDB.getBySpkId(("20" + mpoSpkId.drop(1)).toLong)
    if (!r.isEmpty) return r
    error(s"Image: '$imageName' can not find in horizons the spk id: '$mpoSpkId'")
    Seq()
  }
  //--------------------------------------------------------------------------
  class ProcessSpkAtImageMap(seq: Array[(String, ArrayBuffer[Long])])
    extends ParallelTask[(String, ArrayBuffer[Long])](
    seq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 500
    , message = Some("SPK at image")) {
    //-----------------------------------------------------------------------
    def userProcessSingleItem(t: (String, ArrayBuffer[Long])) =
      Try {
       val imageName = t._1
       val mpoSpkIdSeq = t._2
        mpoSpkIdSeq.foreach { mpoSpkId =>
          updateMpoStats(imageName, mpoSpkId)
        }
      }
      match {
        case Success(_) =>
        case Failure(_) =>
      }
    //-----------------------------------------------------------------------..
  }
  //---------------------------------------------------------------------------
  private def writeMpoAtImage() = {
    val outputAtImageCsv = s"$outputStatsDir/mpo_at_image.csv"
    val bwAtImage = new BufferedWriter(new FileWriter(new File(outputAtImageCsv)))

    bwAtImage.write("image_name" + sep
      + "list_of_mpos" + sep
      + "count" + "\n")

    spkAtImageMap.asScala.foreach { case (imageName,spkIdSeq)=>
      val composedNameSeq = spkIdSeq.flatMap { skpID =>
        val r = mpoStatsMap.get(skpID)
        if (r == null) None
        else Some(r.getComposedName())

      }.mkString("{", ";", "}")

      bwAtImage.write(imageName + sep +
        composedNameSeq + sep +
        spkIdSeq.size.toString +
        "\n")
    }
    bwAtImage.close()
  }
  //---------------------------------------------------------------------------
  private def writeMpoStats() = {

    writeMpoAtImage()

    val outputImageNoMpoCsv = s"$outputStatsDir/image_with_no_mpo.csv"
    val outputUniqueNameCsv = s"$outputStatsDir/mpo_unique.csv"
    val outputStatsCsv      = s"$outputStatsDir/mpo_stats.csv"
    val telescopeStatsCsv   = s"$outputStatsDir/telescope_stats.csv"
    val filterStatsCsv      = s"$outputStatsDir/filter_stats.csv"

    //unique mpo name
    val bwUnique = new BufferedWriter(new FileWriter(new File(outputUniqueNameCsv)))
    val mpoStatsScalaMap = mpoStatsMap.asScala
    mpoStatsScalaMap.values.foreach(s => bwUnique.write(s.getComposedName() + "\n"))

    //images with no mpo
    val bwImageNoMop = new BufferedWriter(new FileWriter(new File(outputImageNoMpoCsv)))
    val uniqueNameSet = spkAtImageMap.keySet()
    bwImageNoMop.write("image_name\n")
    imageFovWithMpoSeq.foreach { fov =>
      if (!uniqueNameSet.contains(fov.fileName))
        bwImageNoMop.write(s"${fov.fileName}\n")
    }

    //mpo stats
    val bwMpoStats = new BufferedWriter(new FileWriter(new File(outputStatsCsv)))
    MpoStatsEntry.writeHeader(bwMpoStats, sep)
    mpoStatsScalaMap.foreach { case (_,mpoStatsEntry) =>
      mpoStatsEntry.write(bwMpoStats,sep)
    }

    //telescope stats
    val bwTelescopeStats = new BufferedWriter(new FileWriter(new File(telescopeStatsCsv)))
    TelescopeStatsEntry.writeHeader(bwTelescopeStats, sep)
    telescopeStatsMap.asScala.foreach { case (_,statsEntry) =>
      statsEntry.write(bwTelescopeStats,sep)
    }

    //filter stats
    val bwFilterStats = new BufferedWriter(new FileWriter(new File(filterStatsCsv)))
    FilterStatsEntry.writeHeader(bwFilterStats, sep)
    filterStatsMap.asScala.foreach { case (_,statsEntry) =>
      statsEntry.write(bwFilterStats,sep)
    }

    //close open objects
    bwImageNoMop.close()
    bwUnique.close()
    bwMpoStats.close()
    bwTelescopeStats.close()
    bwFilterStats.close()
  }
  //---------------------------------------------------------------------------
  private def getHeliocentricRange(spkId: Long
                                   , timeStamp: LocalDateTime
                                   , telescope: String) = {
    val observerSpkID = Telescope.getSpkId(telescope)
    var r = Double.NaN
    Try {
      val focus = ImageFocusMPO.build(spkId.toString).get
      val spkPath = focus.getSpkFilePath()
      Spice.loadKernel(focus.getSpkFilePath(), verbose = false)
      r = Spice.getHeliocentricRange(
        spkId.toString
        , observerSpkID
        , timeStamp)
      Spice.unloadKernel(spkPath)
    }
    match {
      case Success(_) =>
        r
      case Failure(_) =>
        Double.NaN
    }
  }
  //---------------------------------------------------------------------------
  private def updateMpoStats(mpoSpkId: Long
                             , mpcID: Long
                             , mpoMainDesignation: String
                             , mpoAlternateDesignation: String
                             , imageStatsEntry: ImageStatsEntry) = {
    if (mpoStatsMap.get(mpoSpkId) == null)
      mpoStatsMap.put(mpoSpkId,
        MpoStatsEntry(mpoSpkId
          , mpcID
          , 1L
          , mpoMainDesignation
          , mpoAlternateDesignation
          , lastHeliocentricNotNan = Double.NaN
          , imageStatsEntry
          , imageStatsEntry))
    else {
      val mpoStatsEntry = mpoStatsMap.get(mpoSpkId)

      //update count
      mpoStatsEntry.count = mpoStatsEntry.count + 1

      //update heliocentric range if it is nan
      if (mpoStatsEntry.minImageStatsEntry.heliocentricRange.isNaN)
        mpoStatsEntry.minImageStatsEntry.heliocentricRange = mpoStatsEntry.lastHeliocentricNotNan
      else
        mpoStatsEntry.lastHeliocentricNotNan = mpoStatsEntry.minImageStatsEntry.heliocentricRange

      if (mpoStatsEntry.maxImageStatsEntry.heliocentricRange.isNaN)
        mpoStatsEntry.maxImageStatsEntry.heliocentricRange = mpoStatsEntry.lastHeliocentricNotNan
      else
        mpoStatsEntry.lastHeliocentricNotNan = mpoStatsEntry.maxImageStatsEntry.heliocentricRange

      //update the min and max image stats
      if (imageStatsEntry.timeStamp.isBefore(mpoStatsEntry.minImageStatsEntry.timeStamp))
        mpoStatsEntry.minImageStatsEntry = imageStatsEntry
      if (imageStatsEntry.timeStamp.isAfter(mpoStatsEntry.maxImageStatsEntry.timeStamp))
        mpoStatsEntry.maxImageStatsEntry = imageStatsEntry
    }
  }

  //---------------------------------------------------------------------------
  private def updateTelescopeStats(imageStatsEntry: ImageStatsEntry) = {
    val map = telescopeStatsMap
    val item = imageStatsEntry.telescope
    if (map.get(item) == null)
      map.put(item,
        TelescopeStatsEntry(
            item
          , count = 1L
          , imageStatsEntry
          , imageStatsEntry))
    else {
      val mpoStatsEntry = map.get(item)

      //update count
      mpoStatsEntry.count = mpoStatsEntry.count + 1

      //update the min and max image stats
      if (imageStatsEntry.timeStamp.isBefore(mpoStatsEntry.minImageStatsEntry.timeStamp))
        mpoStatsEntry.minImageStatsEntry = imageStatsEntry
      if (imageStatsEntry.timeStamp.isAfter(mpoStatsEntry.maxImageStatsEntry.timeStamp))
        mpoStatsEntry.maxImageStatsEntry = imageStatsEntry
    }
  }
  //---------------------------------------------------------------------------
  private def updateFilterStats(imageStatsEntry: ImageStatsEntry) = {
    val map = filterStatsMap
    val item = imageStatsEntry.filter
    if (map.get(item) == null)
      map.put(item,
        FilterStatsEntry(
          item
          , count = 1L
          , imageStatsEntry
          , imageStatsEntry))
    else {
      val mpoStatsEntry = map.get(item)

      //update count
      mpoStatsEntry.count = mpoStatsEntry.count + 1

      //update the min and max image stats
      if (imageStatsEntry.timeStamp.isBefore(mpoStatsEntry.minImageStatsEntry.timeStamp))
        mpoStatsEntry.minImageStatsEntry = imageStatsEntry
      if (imageStatsEntry.timeStamp.isAfter(mpoStatsEntry.maxImageStatsEntry.timeStamp))
        mpoStatsEntry.maxImageStatsEntry = imageStatsEntry
    }
  }
  //---------------------------------------------------------------------------
  private def updateMpoStats(imageName: String
                              , mpoSpkId: Long): Unit = {
    val r = findSpkID_InHorizons(mpoSpkId.toString, imageName)

    if (r.isEmpty)
      error(s"Image: '$imageName' can not find in horizons the spk id: '$mpoSpkId'")
    else {
      val doc = r.head
      val mpcID = doc("_id").asInt64().getValue
      val mpoMainDesignation = doc("name").asString().getValue
      val mpoAlternateDesignation =
        doc("alternateName").asArray().getValues().asScala.toArray.map { t => t.asString().getValue }.mkString("(", ";", ")")

      val (timeStamp, telescope, filter, _, _, _, _, _, _) = MyImage.getMetadaFromImageName(imageName)

      val imageStats = ImageStatsEntry(
        imageName
        , timeStamp
        , telescope
        , filter
        , getHeliocentricRange(mpoSpkId,  timeStamp, telescope))

      //update stats
      updateMpoStats(
          mpoSpkId
        , mpcID
        , mpoMainDesignation
        , mpoAlternateDesignation
        , imageStats)

      updateTelescopeStats(imageStats)
      updateFilterStats(imageStats)
    }
  }
  //---------------------------------------------------------------------------
  private def getInvalidFovReason(astrometryEntry: AstrometryEntry[Sc]): Option[String] = {

    val (_, _, _, _, _, _, _, _, objectName) = MyImage.getMetadaFromImageName(astrometryEntry.fileName)
    val imageNameLowerCase = astrometryEntry.fileName.toLowerCase
    val objectNameLowerCase = objectName.toLowerCase

    IGNORED_ADDITIONAL_STANDARD_STAR_SEQ.foreach { ignoredName =>
      val ignoredNameLowCase = ignoredName.toLowerCase

      if (imageNameLowerCase.contains(ignoredNameLowCase)) return Some("image name match ignored list")
      if (objectNameLowerCase.contains(ignoredNameLowCase)) return Some("object name match ignored list")
    }

    if (Pattern.parsePattern(imageNameLowerCase, List(STANDARD_START_PG_4_PATTERN), reportErrorMessage = false)) return Some("image name match pattern 'PGxxx'")
    if (Pattern.parsePattern(imageNameLowerCase, List(STANDARD_START_SA_2_PATTERN), reportErrorMessage = false)) return Some("image name match pattern 'SAxxx'")

    if (Pattern.parsePattern(objectNameLowerCase, STANDARD_START_OBJECT_PATTERN_SEQ, reportErrorMessage = false)) return Some("object name match pattern standard star or galaxy")

    val rName = landoltDB.getStandardStar(astrometryEntry.fileName, astrometryEntry.raMin, astrometryEntry.raMax: Double, astrometryEntry.decMin, astrometryEntry.decMax)
    if (rName.isDefined) return Some(s"image name matches standard star:${rName.get}")

    val rObjectName = landoltDB.getStandardStar(objectNameLowerCase, astrometryEntry.raMin, astrometryEntry.raMax: Double, astrometryEntry.decMin, astrometryEntry.decMax)
    if (rObjectName.isDefined) return Some(s"object name matches standard star:${rObjectName.get}")

    None
  }
  //---------------------------------------------------------------------------
  private def getValidImageSeq() = {
    //-------------------------------------------------------------------------
    val validImageResult = new java.util.concurrent.ConcurrentLinkedQueue[AstrometryEntry[Sc]]()
    val invalidImageResult = new java.util.concurrent.ConcurrentLinkedQueue[(AstrometryEntry[Sc],String)]()
    //-------------------------------------------------------------------------
    class MyParallelTask(fovPosSeq: Array[AstrometryEntry[Sc]])
      extends ParallelTask[AstrometryEntry[Sc]](
        fovPosSeq
        , coreCount
        , isItemProcessingThreadSafe = true
        , verbose = false) {
      //----------------------------------------------------------------------
      def userProcessSingleItem(astrometryEntry: AstrometryEntry[Sc]): Unit = {
        val invalidReason = getInvalidFovReason(astrometryEntry)
        if (invalidReason.isEmpty) validImageResult.add(astrometryEntry)
        else invalidImageResult.add((astrometryEntry,invalidReason.get))
      }
      //----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    info("Getting valid images ")
    val astrometryEntrySeq = astrometryDB.getAllAstrometryEntrySeq()

    info(s"Starting image count :'${astrometryEntrySeq.length}'")
    new MyParallelTask(astrometryEntrySeq)

    val inValidSeq = invalidImageResult.asScala.toSeq
    val validSeq = validImageResult.asScala.toSeq
    info(s" Valid image count   :'${validSeq.length}'")
    info(s" Invalid image count :'${inValidSeq.length}'")

    //save invalid image
    val invalidCsv = s"$outputStatsDir/invalid.csv"
    info(s"Saving Standard stars and galaxies images at:'$invalidCsv'")
    val bw = new BufferedWriter(new FileWriter(new File(invalidCsv)))
    bw.write(s"invalid_reason${sep}image_name${sep}object_name${sep}fov_min_ra${sep}fov_max_ra${sep}fov_min_dec${sep}fov_max_dec" + "\n")
    inValidSeq.foreach { case (astrometryEntry,invalidReason) =>
      val (_, _, _, _, _, _, _, _, objectName) = MyImage.getMetadaFromImageName(astrometryEntry.fileName)
      bw.write(
      invalidReason + sep +
        astrometryEntry.fileName + sep +
        objectName + sep +
        astrometryEntry.raMin + sep +
        astrometryEntry.raMax + sep +
        astrometryEntry.decMin + sep +
        astrometryEntry.decMax + sep +
      "\n")
    }
    bw.close()

    validSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MOP_Stats.scala
//=============================================================================
