/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  26/Jul/2024
 * Time:  13h:52m
 * Description: None
 */
package com.henosis.algoritms.mpoInFov

//=============================================================================
import java.io.BufferedWriter
//=============================================================================
//=============================================================================
private object FilterStatsEntry {
  //-------------------------------------------------------------------------
  def writeHeader(bw: BufferedWriter, sep: String) = {
    bw.write("filter" + sep +
      "count" + sep +
      "image_path_with_min_time" + sep +
      "image_path_with_max_time" + sep +
      "\n")
  }
  //-------------------------------------------------------------------------
}

//---------------------------------------------------------------------------
case class FilterStatsEntry(name: String
                            , var count: Long
                            , var minImageStatsEntry: ImageStatsEntry
                            , var maxImageStatsEntry: ImageStatsEntry) {
  //-------------------------------------------------------------------------
  def write(bw: BufferedWriter, sep: String) = {
    bw.write(
      name + sep +
        count + sep +
        minImageStatsEntry.imagePath + sep +
        maxImageStatsEntry.imagePath + sep +
        "\n")
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file FilterStatsEntry.scala
//=============================================================================