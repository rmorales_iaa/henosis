/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Sep/2021
 * Time:  11h:24m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.phaseCurve
//=============================================================================
import com.common.math.regression.spline.Spline
import com.henosis.algoritms.phaseCurve.PhaseCurve._
//=============================================================================
//=============================================================================
case class PhaseCurveFluxSpace(f_1: BasisFunction //phase angle basis function 1
                             , f_2: BasisFunction //phase angle basis function 2
                             , f_3: BasisFunction //phase angle basis function 2
                              ) extends PhaseCurve {
  //--------------------------------------------------------------------------
  val H: Double = -1 //no meaning in this flux space
  //--------------------------------------------------------------------------
  def this(a_1: Double
           , a_2: Double
           , f_1: Spline
           , f_2: Spline
           , f_3: Spline) =
    this(BasisFunction(a_1,f_1)
      , BasisFunction(a_2,f_2)
      , BasisFunction(1 - a_1 - a_2,f_3))
  //--------------------------------------------------------------------------
  def reducedMagnitude(a: Double
                       , w_1: Double = 1
                       , w_2: Double = 1
                       , w_3: Double = 1) =
    f_1(a,w_1) + f_2(a,w_2) + f_3(a,w_3)
  //--------------------------------------------------------------------------
  def getModelParameter() = Array(f_1.curveShaper, f_2.curveShaper)
  //--------------------------------------------------------------------------
  def H_MagnitudeSpace() = fluxToMagnitude(f_1.curveShaper + f_2.curveShaper + f_3.curveShaper)
  //---------------------------------------------------------------------------
  def G_1_MagnitudeSpace() = f_1.curveShaper / (f_1.curveShaper + f_2.curveShaper + f_3.curveShaper)
  //---------------------------------------------------------------------------
  def G_2_MagnitudeSpace() = f_2.curveShaper / (f_1.curveShaper + f_2.curveShaper + f_3.curveShaper)
  //--------------------------------------------------------------------------
}
//=============================================================================
//End of file PhaseCurveFluxSpace.scala
//=============================================================================