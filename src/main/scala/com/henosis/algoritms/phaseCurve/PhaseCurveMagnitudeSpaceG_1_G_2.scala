/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Sep/2021
 * Time:  11h:15m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.phaseCurve
//=============================================================================
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.math.regression.spline.Spline
import com.henosis.algoritms.phaseCurve.PhaseCurve._
import org.apache.commons.math3.linear.{Array2DRowRealMatrix, MatrixUtils}
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression
//=============================================================================
object PhaseCurveMagnitudeSpaceG_1_G_2 {
  //---------------------------------------------------------------------------
  def fit(_dataSeq: Array[Point2D_Double] //(phase_angle, observed magnitude)
                , splineSeq: Array[Spline]
                , convertX_ToRadians : Boolean = true
                , weightsSeq: Option[Array[Double]] = None
                , defaultStdDevError: Double = 0.03) = {
    val dataSeq = _dataSeq map { p=> Point2D_Double(if (convertX_ToRadians) Math.toRadians(p.x) else p.x, Math.pow(10,-0.4 * p.y)) }
    val errorSeq = if (weightsSeq.isDefined) weightsSeq.get else Array.tabulate[Double](dataSeq.length)( _ => defaultStdDevError)
    val sigmaSeq = errorSeq.zipWithIndex map { case (e,i)=>  dataSeq(i).y * (Math.pow(10, 0.4 * e) - 1) }

    //apply the spline on the x values
    val m = dataSeq.zipWithIndex map { case (p,i) =>
      splineSeq map { spline => spline.evaluate(p.x, 1/sigmaSeq(i)) }
    }
    //calculate the regression on the data
    //https://commons.apache.org/proper/commons-math/javadocs/api-3.6/org/apache/commons/math3/stat/regression/OLSMultipleLinearRegression.html
    val initSeqY = errorSeq map { e =>1 / (Math.pow(10, 0.4 * e) - 1) }
    val regression = new  OLSMultipleLinearRegression()
    regression.setNoIntercept(true)
    regression.newSampleData(initSeqY,m)
    val regressionParameters = regression.estimateRegressionParameters()
    val mRealM = new Array2DRowRealMatrix(m)
    val covariance = MatrixUtils.inverse(mRealM.transpose().multiply(mRealM)).getData
    val r = a1a2a3_to_HG1G2(regressionParameters)
    val phaseCurve = new PhaseCurveMagnitudeSpaceG_1_G_2(
        r(0)
      , r(1)
      , r(2)
      , splineSeq(0)
      , splineSeq(1)
      , splineSeq(2))
    phaseCurve.setFitCovariance(covariance)
    phaseCurve
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class PhaseCurveMagnitudeSpaceG_1_G_2(H: Double //absolute magnitude
                                           , f_1: BasisFunction //phase angle function 1
                                           , f_2: BasisFunction //phase angle function 2
                                           , f_3: BasisFunction //phase angle function 2
                       ) extends PhaseCurveMagnitudeSpace {
  //--------------------------------------------------------------------------
  //magnitude as a function of phase angle: Penttilä,2016, Planetary ans Space Science 123, 115-125
  def reducedMagnitude(a: Double
                       , w_1: Double = 1  //weight 1
                       , w_2: Double = 1  //weight 2
                       , w_3: Double = 1  //weight 3
                      ) =
    H - fluxToMagnitude(f_1(a,w_1) +
                        f_2(a,w_2) +
                        f_3(a,w_3))
  //--------------------------------------------------------------------------
  def this(H: Double
           , a_1: Double
           , a_2: Double
           , f_1: Spline
           , f_2: Spline
           , f_3: Spline) =
    this(H
      , BasisFunction(a_1 ,f_1)
      , BasisFunction(a_2, f_2)
      , BasisFunction(1 - a_1 - a_2, f_3))
  //--------------------------------------------------------------------------
  def getModelParameter() = Array(H, f_1.curveShaper, f_2.curveShaper)
  //--------------------------------------------------------------------------
}
//=============================================================================
//End of file PhaseCurveMagnitudeSpaceG_1_G_2.scala
//=============================================================================
