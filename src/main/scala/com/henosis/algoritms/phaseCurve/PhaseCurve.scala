/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Sep/2021
 * Time:  11h:55m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.phaseCurve
//=============================================================================
import com.common.logger.MyLogger
import com.common.math.regression.spline.{PiecewiseFunction, Spline}
import com.common.stat.StatDescriptive
import com.henosis.algoritms.paper.mpo.alvarez.CubicSpline
import org.apache.commons.math3.distribution.MultivariateNormalDistribution
import org.apache.commons.math3.stat.descriptive.rank.Percentile
//=============================================================================
//=============================================================================
object PhaseCurve extends MyLogger {
  //---------------------------------------------------------------------------
  type ConversionFunction = Array[Double] => Array[Double]
  //---------------------------------------------------------------------------
  def fluxToMagnitude(f:Double) = -2.5 * Math.log10(f)
  //---------------------------------------------------------------------------
  def magnitudeToFlux(m:Double) = Math.pow(10, -0.4 * m)
  //---------------------------------------------------------------------------
  def errorInFluxSpace(m: Double,stdDevErrorMagnitudeSpace: Double) =
    magnitudeToFlux(m) * (Math.pow(10, 0.4 * stdDevErrorMagnitudeSpace) - 1)
  //---------------------------------------------------------------------------
  private def errorStat(dataSeq: Array[Double]
                        , covariance: Array[Array[Double]]
                        , convertTo: ConversionFunction
                        , sampleCount: Int
                        , percentileValueSeq: Array[Double]) = {

    val distribution = new MultivariateNormalDistribution(dataSeq, covariance)
    val sample = (for(_<-0 until sampleCount) yield convertTo(distribution.sample())).toArray

    val sampleSplit = (for(i<-0 until dataSeq.length) yield {
      sample map (_ (i))
    }).toArray

    val sampleStatSeq = sampleSplit map { sample=>
      val p = new Percentile()
      p.setData(sample)
      (StatDescriptive.getWithDouble(sample), p)
    }

    val percentileDataSeq = sampleStatSeq map (_._2)
    val calculatedPercentileSeq = percentileValueSeq map { pValue=> percentileDataSeq map (p=> p.evaluate(pValue)) }

    Array (sampleStatSeq map (_._1.average)
         , sampleStatSeq map (_._1.median)) ++calculatedPercentileSeq
  }
  //---------------------------------------------------------------------------
  def estimateError(dataSeq: Array[Double]
                    , covariance: Array[Array[Double]]
                    , sampleCount: Int = 100000
                    , percentileSeq: Array[Double] = Array(0.13499, 2.27501, 15.8655, 84.1345, 97.725, 99.865)) = {
    var convertFrom: ConversionFunction = null
    var convertTo: ConversionFunction = null

    dataSeq.length match {
      case 3 => convertFrom = HG1G2_to_a1a2a3
        convertTo = a1a2a3_to_HG1G2
      case 2 =>
        convertFrom = HG12_to_a1a2
        convertTo = a1a2_to_HG12
      case _ => error("Expected a input data length with size 2 or 3")
    }

    errorStat(convertFrom(dataSeq), covariance, convertTo, sampleCount, percentileSeq)
  }
  //---------------------------------------------------------------------------
  def a1a2a3_to_HG1G2(data: Array[Double]) = {
    val x = data.sum
    Array(
      fluxToMagnitude(x)
      , data(0) / x
      , data(1) / x
    )
  }
  //---------------------------------------------------------------------------
  def HG1G2_to_a1a2a3(data: Array[Double]) = {
    val t =  Math.pow(10,-0.4 * data(0))
    Array(
      t * data(1)
      , t *data(2)
      , t * (1 - data(1) - data(2))
    )
  }
  //---------------------------------------------------------------------------
  def a1a2_to_HG12(data: Array[Double]) =
    Array(
      fluxToMagnitude(data(0))
      , data(1) / data(0)
    )
  //---------------------------------------------------------------------------
  def HG12_to_a1a2(data: Array[Double]) = {
    val t = Math.exp(-0.9210340371976184 * data(0))
    Array(
      t
      , t * data(1)
    )
  }

  //---------------------------------------------------------------------------
  def getSplineSeq() = {

    //Penttilä,2016, Planetary and Space Science 123, 115-125
    val radians_0 = Math.toRadians(0)
    val radians_7 = Math.toRadians(7)
    val radians_30 = Math.toRadians(30)
    val radians_150 = Math.toRadians(150)

    //first basis function, linear and spline. Table A1: columns 'x', 'y1' and 'd1'
    def f1(x: Double) = 1.0 - 1.90985931710274 * x
    val cubicSpline_1 = CubicSpline(Array(7.5, 30, 60, 90, 120, 150) map (Math.toRadians(_))
      , Array(7.5e-1, 3.3486016e-1, 1.3410560e-1, 5.1104756e-2, 2.1465687e-2, 3.6396989e-3)
      , Array(-1.9098593, -9.1328612e-2))
    val spline_1 = Spline(PiecewiseFunction(radians_0, radians_7, f1))
    spline_1 += PiecewiseFunction(radians_7, radians_150, cubicSpline_1.evaluate)

    //second basis function, linear and spline. Table A1: columns 'x', 'y2' and 'd2'
    def f2(x: Double) = 1.0 - 0.572957795130823 * x
    val cubicSpline_2 = CubicSpline(Array(7.5, 30, 60, 90, 120, 150) map (Math.toRadians(_))
      , Array(9.25e-1, 6.2884169e-1, 3.1755495e-1, 1.2716367e-1, 2.2373903e-2, 1.6505689e-4)
      , Array(-5.7295780e-1, -8.6573138e-8))
    val spline_2 = Spline(PiecewiseFunction(radians_0, radians_7, f2))
    spline_2 += PiecewiseFunction(radians_7, radians_150, cubicSpline_2.evaluate)

    //third basis function, spline and constant zero. Table A2: columns 'x', 'y3' and 'd3'
    def f3(x: Double) = 0
    val cubicSpline_3 = CubicSpline(Array(0, 0.3, 1, 2, 4, 8, 12, 20, 30) map (Math.toRadians(_))
      , Array(1, 8.3381185e-1, 5.7735424e-1, 4.2144772e-1, 2.3174230e-1 ,1.0348178e-1 ,6.1733473e-2 ,1.6107006e-2 ,0)
      , Array(-1.0630097e-1, 0.0))
    val spline_3 = Spline(PiecewiseFunction(radians_0, radians_30, cubicSpline_3.evaluate))
    spline_3 += PiecewiseFunction(radians_30, radians_150, f3)

    Array(spline_1,spline_2,spline_3)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
trait PhaseCurve {
  //-------------------------------------------------------------------------
  val f_1: BasisFunction   //phase angle function 1
  val f_2: BasisFunction   //phase angle function 2
  val f_3: BasisFunction   //phase angle function 2
  //-------------------------------------------------------------------------
  //magnitude as a function of phase angle: Penttilä,2016, Planetary ans Space Science 123, 115-125
  def reducedMagnitude(a: Double
                       , w_1: Double = 1
                       , w_2: Double = 1
                       , w_3: Double = 1) : Double
  //-------------------------------------------------------------------------
  def getModelParameter: Array[Double]
  //-------------------------------------------------------------------------
  def getCurveShaperSeq = Array(f_1.curveShaper, f_2.curveShaper, f_3.curveShaper)
  //-------------------------------------------------------------------------
}
//=============================================================================
trait PhaseCurveMagnitudeSpace extends PhaseCurve {
  //-------------------------------------------------------------------------
  val H: Double            //absolute magnitude
  protected var fitCovariance = Array[Array[Double]]()
  //-------------------------------------------------------------------------
  def setFitCovariance(c: Array[Array[Double]]) = fitCovariance = c
  //-------------------------------------------------------------------------
  def getFitCovariance()  = fitCovariance
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file PhaseCurve.scala
//=============================================================================
