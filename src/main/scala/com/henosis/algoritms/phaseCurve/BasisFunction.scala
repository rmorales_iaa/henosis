/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Sep/2021
 * Time:  12h:25m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.phaseCurve
//=============================================================================
import com.common.math.regression.spline.{PiecewiseFunction, Spline}
//=============================================================================
object BasisFunction {
  //---------------------------------------------------------------------------
  type SimpleConversionFunction = Double => Double
  //---------------------------------------------------------------------------
  private final def zeroFunction(a: Double) = 0d
  //---------------------------------------------------------------------------
  final val basisFunctionZero = BasisFunction(0, Spline(PiecewiseFunction(0,0,zeroFunction)))
  //---------------------------------------------------------------------------
}
//=============================================================================
case class BasisFunction(curveShaper: Double, f: Spline) {
  //---------------------------------------------------------------------------
  def apply(a: Double, weight: Double = 1) = curveShaper * f.evaluate(a,weight)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BasisFunction.scala
//=============================================================================
