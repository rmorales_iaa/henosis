/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  24/Sep/2021
 * Time:  11h:15m
 * Description: None
 */
//=============================================================================
package com.henosis.algoritms.phaseCurve
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.common.math.regression.spline.Spline
import com.henosis.algoritms.phaseCurve.BasisFunction.{basisFunctionZero}
import com.henosis.algoritms.phaseCurve.PhaseCurve._
//=============================================================================
import org.apache.commons.math3.linear.{Array2DRowRealMatrix, MatrixUtils}
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object PhaseCurveMagnitudeSpaceG_1_2 {
  //---------------------------------------------------------------------------
  def fit(_dataSeq: Array[Point2D_Double]  //(phase_angle, observed magnitude)
               , splineSeq: Array[Spline]
               , parameterSeq: Array[Array[Double]] = Array(Array(0.7527, 0.06164, -0.9612, 0.6270)
                                                          , Array(0.9529, 0.02162, -0.6125, 0.5572))
               , convertX_ToRadians : Boolean = true
               , weightsSeq: Option[Array[Double]] = None
               , defaultStdDevError: Double = 0.03) = {
    val dataSeq = _dataSeq map { p=> Point2D_Double(if (convertX_ToRadians) Math.toRadians(p.x) else p.x, Math.pow(10,-0.4 * p.y)) }
    val errorSeq = if (weightsSeq.isDefined) weightsSeq.get else Array.tabulate[Double](dataSeq.length)( _ => defaultStdDevError)
    val sigmaSeq = errorSeq.zipWithIndex map { case (e,i)=>  dataSeq(i).y * (Math.pow(10, 0.4 * e) - 1) }

    var minResidual = 1e12
    var bestRegressionParameterSeq = Array[Double]()
    var covariance = Array[Array[Double]]()
    parameterSeq foreach { paramSeq =>
      val b1 = paramSeq(0)
      val b0 = paramSeq(1)
      val g1 = paramSeq(2)
      val g0 = paramSeq(3)
      val m_1= ArrayBuffer.tabulate[ArrayBuffer[Double]](dataSeq.length)(_ => ArrayBuffer(0,0))
      dataSeq.zipWithIndex.foreach { case (p,i)=>
        val Phi = splineSeq map { spline => spline.evaluate(p.x) }
        val Gamma1 = b0*Phi(0) + g0*Phi(1) + Phi(2) - b0*Phi(2) - g0*Phi(2)
        val Gamma2 = b1*Phi(0) + g1*Phi(1) - (b1+g1)*Phi(2)
        m_1(i)(0) = Gamma1 / sigmaSeq(i)
        m_1(i)(1) = Gamma2 / sigmaSeq(i)
      }

      //calculate the regression
      //https://commons.apache.org/proper/commons-math/javadocs/api-3.6/org/apache/commons/math3/stat/regression/OLSMultipleLinearRegression.html
      val initSeqY = errorSeq map { e =>1 / (Math.pow(10, 0.4 * e) - 1) }
      val m = m_1.map(_.toArray).toArray
      val mRealM = new Array2DRowRealMatrix(m)
      covariance = MatrixUtils.inverse(mRealM.transpose().multiply(mRealM)).getData

      val regression = new OLSMultipleLinearRegression()
      regression.setNoIntercept(true)
      regression.newSampleData(initSeqY,m)
      val regressionParameters = regression.estimateRegressionParameters()
      val residual = regression.calculateResidualSumOfSquares
      if (residual < minResidual) {
        minResidual  = residual
        bestRegressionParameterSeq = regressionParameters
      }
    }
    val r = a1a2_to_HG12(bestRegressionParameterSeq)
    val phaseCurve = new PhaseCurveMagnitudeSpaceG_1_2(
        r(0)
      , r(1)
      , splineSeq(0)
      , splineSeq(1))
    phaseCurve.setFitCovariance(covariance)
    phaseCurve
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class PhaseCurveMagnitudeSpaceG_1_2(H: Double //absolute magnitude
                                         , G_12: Double //curve shape
                                         , f_1: BasisFunction //phase angle function 1
                                         , f_2: BasisFunction //phase angle function 2
                        ) extends PhaseCurveMagnitudeSpace {
  //--------------------------------------------------------------------------
  val f_3  = basisFunctionZero
  val G_1 = 0.84293649 * G_12
  val G_2 = 0.53513350 * (1 - G_12)
  //--------------------------------------------------------------------------
  //magnitude as a function of phase angle: Penttilä,2016, Planetary and Space Science 123, 115-125
  def reducedMagnitude(a: Double
                       , w_1: Double = 1
                       , w_2: Double = 1
                       , w_3: Double = 1) =
    H - fluxToMagnitude(f_1(a,w_1) + f_2(a,w_2))
  //--------------------------------------------------------------------------
  def this(H: Double
           , G_12: Double
           , f_1: Spline
           , f_2: Spline) =
    this(H
      , G_12
      , BasisFunction(0.84293649 * G_12, f_1)
      , BasisFunction(0.53513350 * (1 - G_12), f_2))
  //--------------------------------------------------------------------------
  def getModelParameter() = Array(H, G_12)
  //--------------------------------------------------------------------------
}
//=============================================================================
//End of file PhaseCurveMagnitudeSpaceG_1_2.scala
//=============================================================================
