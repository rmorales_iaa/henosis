/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Jul/2021
 * Time:  10h:31m
 * Description: Import data from TESS dr1 to mongo
 * https://archive.konkoly.hu/pub/tssys/dr1/
 */
//=============================================================================
package com.henosis.database.tessToMongo
//=============================================================================
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import com.common.util.path.Path
import com.henosis.database.MPO_DB
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import org.mongodb.scala.bson.{BsonDouble, BsonInt32, BsonString, BsonValue}
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
//=============================================================================
//=============================================================================
object TessToMongo extends MyLogger {
  //---------------------------------------------------------------------------
  final val COL_NAME_MPC_ID       = "mpc_id"
  final val COL_NAME_MAGNITUDE    = "tessMag"
  //---------------------------------------------------------------------------
  final val AsteroidCsvHeader = Seq(
      "tess_mpcID"
    , "tess_jd"
    , "tess_xPosCCD"
    , "tess_yPosCCD"
    , "tessMag"
    , "tess_uncertMag"
    , "tess_background"
    , "tess_rmsBackground"
    , "tess_unused"
    , "tess_flags"
    , "tess_tess_dist"
    , "tess_sun_dist"
    , "tess_tess_obj_sun_phase_angle")
  //---------------------------------------------------------------------------
  private final val LIGHT_CURVE_FILE_EXTENSION = ".lc"
  //---------------------------------------------------------------------------
  //                             1        2         3         4         5     6                  7            8               9        10      11          12         13
  private val headerSeq = Array(COL_NAME_MPC_ID,"tess_jd","xPosCCD","yPosCCD",COL_NAME_MAGNITUDE,"uncertMag","background","rmsBackground","unused","flags","tess_dist","sun_dist","tess_obj_sun_phase_angle")
  private val typeSeq   = Array("string","double" ,"double" ,"double" ,"double" ,"double"   ,"double"    ,"double"       ,"double","int"  ,"double"   ,"double"  ,"double")
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, db: MongoDB, coll: MongoCollection[Document]) = {
    info(s"Processing TESS file: '$fileName'")
    val docSeq = Source.fromFile(fileName).getLines.map { line =>
      val row = ArrayBuffer[(String,BsonValue)]()
      val seq = line.split(" ").filter( _.length > 0)
      seq.zipWithIndex.map { case (s,k)=>
        val colName = headerSeq(k)
        val colType = typeSeq(k)
        colType match {
          case "double"  =>
            if(s.isEmpty) row += (colName ->  new BsonDouble(-1))
            else row += (colName -> new BsonDouble(s.toDouble))
          case "int"    =>
            if(s.isEmpty) row += (colName -> new BsonInt32(-1))
            else row += (colName -> new BsonInt32(s.toInt))
          case "string"    =>
            if(s.isEmpty) row += (colName -> new BsonString("-1"))
            else row += (colName -> new BsonString(s))
        }
      }
      Document(row)
    }
    //insert all doc sequence
    db.insertBulk(docSeq.toArray, coll.namespace.getCollectionName, dropCollection = false)
  }
  //---------------------------------------------------------------------------
  def importData(directory: String, db: MPO_DB) = {
    val coll = db.collTessMoc
    db.dropCollection(coll)
    Path.getFileListWithExtension(directory,LIGHT_CURVE_FILE_EXTENSION).foreach{ f=>
      processFile(f.getAbsolutePath, db, coll)
    }
    info("Creating indexes")
    db.createIndex(COL_NAME_MPC_ID, coll, _unique = false)
    db.createIndex("tess_jd", coll, _unique = false)
  }
  //---------------------------------------------------------------------------
  def apply(doc :Document) = TessAsteroid(
    doc(COL_NAME_MPC_ID).asString().getValue.toInt
    , doc("tess_jd").asDouble().getValue
    , doc("xPosCCD").asDouble().getValue
    , doc("yPosCCD").asDouble().getValue
    , doc(COL_NAME_MAGNITUDE).asDouble().getValue
    , doc("uncertMag").asDouble().getValue
    , doc("background").asDouble().getValue
    , doc("rmsBackground").asDouble().getValue
    , doc("unused").asDouble().getValue
    , doc("flags").asInt32().getValue
    , doc("tess_dist").asDouble().getValue
    , doc("sun_dist").asDouble().getValue
    , doc("tess_obj_sun_phase_angle").asDouble().getValue
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class TessAsteroid(mpcID: Int
                        , tess_jd: Double
                        , xPosCCD: Double
                        , yPosCCD: Double
                        , tessMag: Double
                        , uncertMag : Double
                        , background : Double
                        , rmsBackground : Double
                        , unused : Double
                        , flags : Int
                        , tess_dist : Double
                        , sun_dist : Double
                        , tess_obj_sun_phase_angle : Double) {
  //---------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    mpcID.toString + sep +
      tess_jd.toString + sep +
      xPosCCD.toString + sep +
      yPosCCD.toString + sep +
      tessMag.toString + sep +
      uncertMag.toString + sep +
      background.toString + sep +
      rmsBackground.toString + sep +
      unused.toString + sep +
      flags.toString + sep +
      tess_dist.toString + sep +
      sun_dist.toString + sep +
      tess_obj_sun_phase_angle.toString
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TessToMongo.scala
//=============================================================================
