/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  02/Jun/2022
  * Time:  11h:40m
  * Description: List if MPC observatories codes of MPC
  * https://www.minorplanetcenter.net/iau/lists/ObsCodesF.html
  */
//=============================================================================
package com.henosis.database.observatoriesCodes
//=============================================================================
import com.common.configuration.MyConf
import com.common.constant.astronomy.AstronomicalUnit.pointToEllipse
import com.common.constant.astronomy.Earth.{EARTH_MAJOR_AXIS_M, EARTH_MINOR_AXIS_M}
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.observatories.{ObservatoriesDB, Observatory}
import com.common.hardware.cpu.CPU
import com.common.image.telescope.Telescope
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.{DatabaseImporter, LineEntry}
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import scala.sys.process._
import scala.util.{Failure, Success, Try}
import java.io.{FileInputStream}
import ujson.Obj
import scala.collection.mutable
//=============================================================================
//=============================================================================
case class ObservatoriesImporter(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = ObservatoriesDB(conf)
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[Observatory] =  mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  private var jsonFile = ""
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    mongoDB.createIndex("_id", mongoDB.getCollection, _unique = false)
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    Try {
      val obsSeq = ujson.read(new FileInputStream(jsonFile))
        .value
        .asInstanceOf[upickle.core.LinkedHashMap[String, Obj]]
        .map { case (key, s) =>
          val seq = s.value

          val name = seq.get("Name").get.toString().replaceAll("\"","")
          val longitude = seq.get("Longitude").getOrElse("NaN").toString().toDouble
          val cosPhi = seq.get("cos").getOrElse("NaN").toString().toDouble
          val sinPhi = seq.get("sin").getOrElse("NaN").toString().toDouble
          val (_latitude,_altitude) = pointToEllipse(1D
                                                  , EARTH_MINOR_AXIS_M / EARTH_MAJOR_AXIS_M
                                                  , cosPhi
                                                  , sinPhi)
          val latitude = math.toDegrees(_latitude)
          val altitude = _altitude * EARTH_MINOR_AXIS_M

          val rectCoordSeq = {
             if (latitude.isNaN){
               warning(s"Observatory:'$name' has a NaN latitude, so summing (-1d,-1d,-1d) as rectangular coordinates")
               (-1d,-1d,-1d)
             }
             else  Telescope.getRectangularCoordinateSeq(longitude, latitude, altitude)
          }
          Observatory(key
                    , name
                    , latitude
                    , longitude
                    , altitude
                    , cosPhi
                    , sinPhi
                    , rectCoordSeq._1
                    , rectCoordSeq._2
                    , rectCoordSeq._3
                    )
        }
      warning(s"Adding to database:'${mongoDB.database.name}.${mongoDB.collectionName}' '${obsSeq.size}' observatories")
      collWithRegistry.insertMany(obsSeq.toSeq).results()
    }
    match {
      case Success(_) => info(s"Success processing json file: '$jsonFile'")
      case Failure(ex) => error(s"Error processing json file: '$jsonFile'" + ex.toString)
    }
    info("Creating indexes")
    createIndexes
    true
  }
  //---------------------------------------------------------------------------
  private def downloadAndUncompress() = {
    Path.ensureDirectoryExist(iDir)
    val url = conf.getString("ObservatoriesCodes.url")

    val fileName = Path.getOnlyFilename(url)
    val fileNameNoExtension = Path.getOnlyFilenameNoExtension(url)
    jsonFile = s"$iDir/$fileNameNoExtension"
    MyFile.deleteFileIfExist(jsonFile)

    val compressedFile = s"$iDir/$fileName"
    val commandDownload= s"wget -O $compressedFile $url"
    commandDownload.!

    val commandUncompress = s"gzip -d $compressedFile"
    commandUncompress.!
  }
  //---------------------------------------------------------------------------
  override def run(dropCollection: Boolean = true, fileExtension: String = ""): Unit = {
    downloadAndUncompress
    if (dropCollection) mongoDB.dropCollection()
    processFile("", mongoDB.getCollection)
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ObservatoriesImporter.scala
//=============================================================================
