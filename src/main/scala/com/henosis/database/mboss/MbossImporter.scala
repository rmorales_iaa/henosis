/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  02/Jun/2022
  * Time:  11h:40m
  * Description: None
  */
//=============================================================================
package com.henosis.database.mboss
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.hardware.cpu.CPU
import com.common.{DatabaseImporter, LineEntry}
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import scala.io.Source
//=============================================================================
//=============================================================================
object MbossImporter {
  //---------------------------------------------------------------------------
  private def loadSeqFromCsv(csvFileName: String) = {
    //-------------------------------------------------------------------------
    var index = -1
    //-------------------------------------------------------------------------
    def getNextIndex() = {
      index += 1
      index
    }
    //---------------------------------------------------------------------------
    def getDoubleOrDefault(s: String) = {
      val v = s.trim
      if (v.isEmpty || v == "------") Double.NaN
      else v.toDouble
    }
    //---------------------------------------------------------------------------
    val bufferedSource = Source.fromFile(csvFileName)
    var continue = true
    val r = bufferedSource.getLines.flatMap { _line =>
      if (_line.startsWith("#EOF"))
        continue = false
      if (_line.isEmpty || _line.startsWith("#") || !continue) None
      else {
        index = -1
        val _lineSeq = _line
          .trim
          .replaceAll("/", " ")
          .replaceAll(" +", ";")
          .split(";")
        val lineSeq =
          if (_line.take(8).contains("/"))
            Array(_lineSeq(0) + "/" + _lineSeq(1)) ++ _lineSeq.drop(2)  //rebuild comet name
          else  {
            if (_lineSeq(0) == "Nereid" ||
                _lineSeq(0) == "Phoebe" ||
                _lineSeq(0) == "Sycorax"
            )
              Array(_lineSeq(0),"NONE") ++ _lineSeq.drop(1)  // has no class
            else _lineSeq
          }

        Some(MbossSource(
            lineSeq(getNextIndex()).trim //Name
          , lineSeq(getNextIndex()).trim  //Class

          , lineSeq(getNextIndex()).trim.toInt //Epo
          , lineSeq(getNextIndex()).trim.toInt //Mult

          , getDoubleOrDefault(lineSeq(getNextIndex())) //M_11_plus
          , getDoubleOrDefault(lineSeq(getNextIndex()))  //M11_minus_sigma

          , getDoubleOrDefault(lineSeq(getNextIndex())) //Grt_plus
          , getDoubleOrDefault(lineSeq(getNextIndex())) //Grt_minus_sigma

          , getDoubleOrDefault(lineSeq(getNextIndex())) //B_V
          , getDoubleOrDefault(lineSeq(getNextIndex())) //sigma
          , getDoubleOrDefault(lineSeq(getNextIndex())) //disp

          , getDoubleOrDefault(lineSeq(getNextIndex())) //V_R
          , getDoubleOrDefault(lineSeq(getNextIndex())) //sigma
          , getDoubleOrDefault(lineSeq(getNextIndex())) //disp

          , getDoubleOrDefault(lineSeq(getNextIndex())) //R_I
          , getDoubleOrDefault(lineSeq(getNextIndex())) //sigma
          , getDoubleOrDefault(lineSeq(getNextIndex())) //disp

          , getDoubleOrDefault(lineSeq(getNextIndex())) //V_J
          , getDoubleOrDefault(lineSeq(getNextIndex())) //sigma
          , getDoubleOrDefault(lineSeq(getNextIndex())) //disp

          , getDoubleOrDefault(lineSeq(getNextIndex())) //J_H
          , getDoubleOrDefault(lineSeq(getNextIndex())) //sigma
          , getDoubleOrDefault(lineSeq(getNextIndex())) //disp

          , getDoubleOrDefault(lineSeq(getNextIndex())) //H_K
          , getDoubleOrDefault(lineSeq(getNextIndex())) //sigma
          , getDoubleOrDefault(lineSeq(getNextIndex())) //disp
        ))
      }
    }
    r.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MbossImporter(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = MbossDB(conf)
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[MbossSource] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    val sourceSeq = MbossImporter.loadSeqFromCsv(path)
    collWithRegistry.insertMany(sourceSeq).results()
    true
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true, fileExtension = ".txt")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MbossImporter.scala
//=============================================================================
