/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/Mar/2024
 * Time:  17h:25m
 * Description: None
 */
package com.henosis.database.mboss
//=============================================================================
//=============================================================================
import com.common.logger.MyLogger
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import scala.language.existentials
//=============================================================================
//=============================================================================
object MbossSource extends MyLogger {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[MbossSource]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MbossSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[MbossSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MbossSource(_id: String
                       , Class: String

                       , Epo: Int
                       , Mult: Int

                       , M_11_plus: Double
                       , M11_minus_sigma: Double

                       , Grt_plus: Double
                       , Grt_minus_sigma: Double

                       , B_V: Double
                       , B_V_sigma: Double
                       , B_V_disp: Double

                       , V_R: Double
                       , V_R_sigma: Double
                       , V_R_disp: Double

                       , R_I: Double
                       , R_I_sigma: Double
                       , R_I_disp: Double

                       , V_J: Double
                       , V_J_sigma: Double
                       , V_J_disp: Double

                       , J_H: Double
                       , J_H_sigma: Double
                       , J_H_disp: Double

                       , H_K: Double
                       , H_K_sigma: Double
                       , H_K_disp: Double
                      )
//=============================================================================
//End of file MbossSource.scala
//=============================================================================