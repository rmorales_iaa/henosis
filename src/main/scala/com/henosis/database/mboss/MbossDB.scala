/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  25/Mar/2024
 * Time:  17h:25m
 * Description: None
 */
package com.henosis.database.mboss
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.MongoDB
import com.common.database.mongoDB.MongoDB.getProjectionColList
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
//=============================================================================
object MbossDB {
  //---------------------------------------------------------------------------
  private val COL_NAME_V_R = "V_R"
  //---------------------------------------------------------------------------
  def apply(conf: MyConf =  MyConf(MyConf.c.getString("Database.mboss"))): MbossDB =
    MbossDB(conf.getString("Mboss.databaseName")
      , conf.getString("Mboss.collectionName"))
  //---------------------------------------------------------------------------
}
//=============================================================================
import MbossDB._
case class MbossDB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(MbossSource.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  //---------------------------------------------------------------------------
  connected = true
  //-------------------------------------------------------------------------
  def findByName(name: String, colList: Seq[String]): Seq[Document] = {
    collection.find(equal("Name", name))
      .projection(getProjectionColList(colList))
      .getResultDocumentSeq
  }
  //-------------------------------------------------------------------------
  private def getV_R(docSeq: Seq[Document]
                     , duplicateErrorMessage: String
                     , reportMultipleMatch: Boolean): Double = {
    if (docSeq.isEmpty) return Double.NaN
    if (docSeq.size > 1) {
      if (reportMultipleMatch) warning(duplicateErrorMessage)
      return Double.NaN
    }
    docSeq.head(COL_NAME_V_R).asDouble().getValue
  }
  //-------------------------------------------------------------------------
  def getV_R(name: String, mpcID: String = "", reportMultipleMatch: Boolean): Double = {
    if (!mpcID.isEmpty) {
      val pattern = s"$mpcID-.*"
      getV_R(findByPattern("Name", pattern, Seq(COL_NAME_V_R))
        , s"The object with mpcID: '$mpcID' has multiple entries in database: '$databaseName'"
        , reportMultipleMatch)
    }
    else {
      val pattern = s".*$name.*"
      getV_R(findByPattern("Name", pattern, Seq(COL_NAME_V_R))
        , s"The object with name: '$name' has multiple entries in database: '$databaseName'"
        , reportMultipleMatch)
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MbossDB.scala
//=============================================================================