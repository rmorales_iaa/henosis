/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  02/Jun/2022
  * Time:  11h:40m
  * Description: Hipparcos database
 *  https://heasarc.gsfc.nasa.gov/W3Browse/all/hipparcos.html
  */
//=============================================================================
package com.henosis.database.hipparcos
//=============================================================================
import com.common.{DatabaseImporter, LineEntry}
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.hardware.cpu.CPU
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
//=============================================================================
//=============================================================================
case class HipparcosImporter(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = HipparcosDB(conf)
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[HipparcosSource] =  mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    mongoDB.createIndexes(Array("HIP"))
    mongoDB.createIndexes(Array("RAdeg_ICRS","DEdeg_ICRS"),_unique = false)
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    val sourceSeq = HipparcosSource.loadSeqFromCsv(path)
    collWithRegistry.insertMany(sourceSeq.toArray).results()
    true
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file HipparcosImporter.scala
//=============================================================================
