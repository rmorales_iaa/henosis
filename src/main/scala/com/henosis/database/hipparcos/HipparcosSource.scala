/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:59m
 * Description: https://heasarc.gsfc.nasa.gov/W3Browse/all/hipparcos.html
 */
//=============================================================================
package com.henosis.database.hipparcos
//=============================================================================
import com.common.csv.CsvFile._
import com.common.logger.MyLogger
//=============================================================================
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import scala.language.existentials
//=============================================================================
//=============================================================================
object HipparcosSource extends MyLogger {
  //---------------------------------------------------------------------------
  //http://mongodb.github.io/mongo-scala-driver/2.3/getting-started/quick-tour-case-classes/
  //https://jannikarndt.de/blog/2017/08/writing_case_classes_to_mongodb_in_scala/
  val codecRegistry = fromRegistries(fromProviders(classOf[HipparcosSource]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[HipparcosSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[HipparcosSource].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  final val HIPPARCOS_COL_CSV_PHOTOMETRY_HEADER = getColNameSeq("hipparcos_")
  //---------------------------------------------------------------------------
  def loadSeqFromCsv(csvFileName: String) = {

    val CSV_HEADER = Array("recno","HIP","Proxy","RAhms","DEdms","Vmag","VarFlag","r_Vmag","RAICRS","DEICRS","AstroRef","Plx","pmRA","pmDE","e_RAICRS","e_DEICRS","e_Plx","e_pmRA","e_pmDE","DE:RA","Plx:RA","Plx:DE","pmRA:RA","pmRA:DE","pmRA:Plx","pmDE:RA","pmDE:DE","pmDE:Plx","pmDE:pmRA","F1","F2","BTmag","e_BTmag","VTmag","e_VTmag","m_BTmag","B-V","e_B-V","r_B-V","V-I","e_V-I","r_V-I","CombMag","Hpmag","e_Hpmag","Hpscat","o_Hpmag","m_Hpmag","Hpmax","HPmin","Period","HvarType","moreVar","morePhoto","CCDM","n_CCDM","Nsys","Ncomp","MultFlag","Source","Qual","m_HIP","theta","rho","e_rho","dHp","e_dHp","Survey","Chart","Notes", "HD","BD","CoD","CPD","(V-I)red","SpType","r_SpType","_RA_icrs","_DE_icrs")
    val csvInputFormat = CSVFormat.DEFAULT
      .withDelimiter(',')
      .withHeader(CSV_HEADER:_*)
      .withSkipHeaderRecord()
      .withIgnoreEmptyLines(true)

    val br = new BufferedReader(new FileReader(new File(csvFileName)))
    new CSVParser(br, csvInputFormat).getRecords.asScala.map { r =>

      HipparcosSource(
        getIntOrDefault(r.get("HIP"))
      , r.get("Proxy").head
      , r.get("RAhms")
      , r.get("DEdms")
      , getDoubleOrDefault(r.get("Vmag"))
      , getByteOrDefault(r.get("VarFlag"))
      , r.get("r_Vmag").head
      , r.get("RAICRS")    //RAdeg_1991
      , r.get("DEICRS")    //DEdeg_1991
      , getDoubleOrDefault(r.get("_RA_icrs"))  //RAdeg_ICRS
      , getDoubleOrDefault(r.get("_DE_icrs"))  //DEdeg_ICRS
      , r.get("AstroRef").head
      , getDoubleOrDefault(r.get("Plx"))
      , getDoubleOrDefault(r.get("pmRA"))
      , getDoubleOrDefault(r.get("pmDE"))
      , getDoubleOrDefault(r.get("e_RAICRS"))
      , getDoubleOrDefault(r.get("e_DEICRS"))
      , getDoubleOrDefault(r.get("e_Plx"))
      , getDoubleOrDefault(r.get("e_pmRA"))
      , getDoubleOrDefault(r.get("e_pmDE"))
      , getDoubleOrDefault(r.get("DE:RA"))
      , getDoubleOrDefault(r.get("Plx:RA"))
      , getDoubleOrDefault(r.get("Plx:DE"))
      , getDoubleOrDefault(r.get("pmRA:RA"))
      , getDoubleOrDefault(r.get("pmRA:DE"))
      , getDoubleOrDefault(r.get("pmRA:Plx"))
      , getDoubleOrDefault( r.get("pmDE:RA"))
      , getDoubleOrDefault(r.get("pmDE:DE"))
      , getDoubleOrDefault(r.get("pmDE:Plx"))
      , getDoubleOrDefault(r.get("pmDE:pmRA"))
      , getDoubleOrDefault(r.get("F1"))
      , getDoubleOrDefault(r.get("F2"))
      , getDoubleOrDefault(r.get("BTmag"))
      , getDoubleOrDefault(r.get("e_BTmag"))
      , getDoubleOrDefault(r.get("VTmag"))
      , getDoubleOrDefault(r.get("e_VTmag"))
      , r.get("m_BTmag").head
      , getDoubleOrDefault(r.get("B-V"))
      , getDoubleOrDefault(r.get("e_B-V"))
      , r.get("r_B-V").head
      , getDoubleOrDefault(r.get("V-I"))
      , getDoubleOrDefault(r.get("e_V-I"))
      , r.get("r_V-I").head
      , r.get("CombMag").head
      , getDoubleOrDefault(r.get("Hpmag"))
      , getDoubleOrDefault(r.get("e_Hpmag"))
      , getDoubleOrDefault(r.get("Hpscat"))
      , getDoubleOrDefault(r.get("o_Hpmag"))
      , r.get("m_Hpmag").head
      , getDoubleOrDefault(r.get("Hpmax"))
      , getDoubleOrDefault(r.get("HPmin"))
      , getDoubleOrDefault(r.get("Period"))
      , r.get("HvarType").head
      , getIntOrDefault(r.get("moreVar"))
      , r.get("morePhoto").head
      , r.get("CCDM")
      , r.get("n_CCDM").head
      , getIntOrDefault(r.get("Nsys"))
      , getIntOrDefault(r.get("Ncomp"))
      , r.get("MultFlag").head
      , r.get("Source").head
      , r.get("Qual").head
      , r.get("m_HIP")
      , getIntOrDefault(r.get("theta"))
      , getDoubleOrDefault(r.get("rho"))
      , getDoubleOrDefault(r.get("e_rho"))
      , getDoubleOrDefault(r.get("dHp"))
      , getDoubleOrDefault(r.get("e_dHp"))
      , r.get("Survey").head
      , r.get("Chart").head
      , r.get("Notes").head
      , getIntOrDefault(r.get("HD"))
      , r.get("BD")
      , r.get("CoD")
      , r.get("CPD")
      , getDoubleOrDefault(r.get("(V-I)red"))
      , r.get("SpType")
      , r.get("r_SpType")
      )
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class HipparcosSource(  HIP:   Int           //HIP_Number          /Identifier (HIP number)
                            , Proxy    : Char      //Prox_10asec        /Proximity flag
                            , RAhms    : String    //RA                 /RA in h m s, ICRS (J1991.25)
                            , DEdms    : String    //Dec                /Dec in deg ' ", ICRS (J1991.25)
                            , Vmag     : Double    //Vmag               /Magnitude in Johnson V
                            , VarFlag  : Byte      //Var_Flag           /Coarse variability flag
                            , r_Vmag   : Char      //Vmag_Source        /Source of magnitude
                            , RAdeg_1991: String   //RA_Deg             /RA in degrees (ICRS, Epoch-J1991.25)
                            , DEdeg_1991: String   //Dec_Deg            /Dec in degrees (ICRS, Epoch-J1991.25)
                            , RAdeg_ICRS: Double   //RA_Deg             /RA in degrees (ICRS, Epoch-J2000)
                            , DEdeg_ICRS: Double   //Dec_Deg            /Dec in degrees (ICRS, Epoch-J2000)
                            , AstroRef : Char      //Astrom_Ref_Dbl     /Reference flag for astrometry
                            , Plx      : Double    //Parallax           /Trigonometric parallax
                            , pmRA     : Double    //pm_RA              /Proper motion in RA
                            , pmDE     : Double    //pm_Dec             /Proper motion in Dec
                            , e_RAdeg  : Double    //RA_Error           /Standard error in RA*cos(Dec_Deg) (ICRS, Epoch-J1991.25)
                            , e_DEdeg  : Double    //Dec_Error          /Standard error in Dec_Deg  (ICRS, Epoch-J1991.25)
                            , e_Plx    : Double    //Parallax_Error     /Standard error in Parallax
                            , e_pmRA   : Double    //pm_RA_Error        /Standard error in pmRA
                            , e_pmDE   : Double    //pm_Dec_Error       /Standard error in pmDE
                            , DE_RA    : Double    //Crl_Dec_RA         /(DE over RA)xCos(delta)
                            , Plx_RA   : Double    //Crl_Plx_RA         /(Plx over RA)xCos(delta)
                            , Plx_DE   : Double    //Crl_Plx_Dec        /(Plx over DE)
                            , pmRA_RA  : Double    //Crl_pmRA_RA        /(pmRA over RA)xCos(delta)
                            , pmRA_DE  : Double    //Crl_pmRA_Dec       /(pmRA over DE)
                            , pmRA_Plx : Double    //Crl_pmRA_Plx       /(pmRA over Plx)
                            , pmDE_RA  : Double    //Crl_pmDec_RA       /(pmDE over RA)xCos(delta)
                            , pmDE_DE  : Double    //Crl_pmDec_Dec      /(pmDE over DE)
                            , pmDE_Plx : Double    //Crl_pmDec_Plx      /(pmDE over Plx)
                            , pmDE_pmRA: Double    //Crl_pmDec_pmRA     /(pmDE over pmRA)
                            , F1       : Double    //Reject_Percent     /Percentage of rejected data
                            , F2       : Double    //Quality_Fit        /Goodness-of-fit parameter
                            , BTmag    : Double    //BT_Mag             /Mean BT magnitude
                            , e_BTmag  : Double    //BT_Mag_Error       /Standard error on BTmag
                            , VTmag    : Double    //VT_Mag             /Mean VT magnitude
                            , e_VTmag  : Double    //VT_Mag_Error       /Standard error on VTmag
                            , m_BTmag  : Char      //BT_Mag_Ref_Dbl     /Reference flag for BT and VTmag
                            , B_V      : Double    //BV_Color           /Johnson BV colour
                            , e_B_V    : Double    //BV_Color_Error     /Standard error on BV
                            , r_B_V    : Char      //BV_Mag_Source      /Source of BV from Ground or Tycho
                            , V_I      : Double    //VI_Color           /Colour index in Cousins' system
                            , e_V_I    : Double    //VI_Color_Error     /Standard error on VI
                            , r_V_I    : Char      //VI_Color_Source    /Source of VI
                            , CombMag  : Char      //Mag_Ref_Dbl        /Flag for combined Vmag, BV, VI
                            , Hpmag    : Double    //Hip_Mag            /Median magnitude in Hipparcos system
                            , e_Hpmag  : Double    //Hip_Mag_Error      /Standard error on Hpmag
                            , Hpscat   : Double    //Scat_Hip_Mag       /Scatter of Hpmag
                            , o_Hpmag  : Double    //N_Obs_Hip_Mag      /Number of observations for Hpmag
                            , m_Hpmag  : Char      //Hip_Mag_Ref_Dbl    /Reference flag for Hpmag
                            , Hpmax    : Double    //Hip_Mag_Max        /Hpmag at maximum (5th percentile)
                            , HPmin    : Double    //Hip_Mag_Min        /Hpmag at minimum (95th percentile)
                            , Period   : Double    //Var_Period         /Variability period (days)
                            , HvarType : Char      //Hip_Var_Type       /Variability type
                            , moreVar  : Int       //Var_Data_Annex     /Additional data about variability
                            , morePhoto: Char      //Var_Curv_Annex     /Light curve Annex
                            , CCDM     : String    //CCDM_Id            /CCDM identifier
                            , n_CCDM   : Char      //CCDM_History       /Historical status flag
                            , Nsys     : Int       //CCDM_N_Entries     /Number of entries with same CCDM
                            , Ncomp    : Int       //CCDM_N_Comp        /Number of components in this entry
                            , MultFlag : Char      //Dbl_Mult_Annex     /Double and or Multiple Systems flag
                            , Source   : Char      //Astrom_Mult_Source /Astrometric source flag
                            , Qual     : Char      //Dbl_Soln_Qual      /Solution quality flag
                            , m_HIP    : String    //Dbl_Ref_ID         /Component identifiers
                            , theta    : Int       //Dbl_Theta          /Position angle between components
                            , rho      : Double    //Dbl_Rho            /Angular separation of components
                            , e_rho    : Double    //Rho_Error          /Standard error of rho
                            , dHp      : Double    //Diff_Hip_Mag       /Magnitude difference of components
                            , e_dHp    : Double    //dHip_Mag_Error     /Standard error in dHp
                            , Survey   : Char      //Survey_Star        /Flag indicating a Survey Star
                            , Chart    : Char      //ID_Chart           /Identification Chart
                            , Notes    : Char      //Notes              /Existence of notes
                            , HD       : Int       //HD_Id              /HD number <III 135>
                            , BD       : String    //BD_Id              /Bonner DM <I 119>, <I 122>
                            , CoD      : String    //CoD_Id             /Cordoba Durchmusterung (DM) <I 114>
                            , CPD      : String    //CPD_Id             /Cape Photographic DM <I 108>
                            , V_I_red  : Double    //VI_Color_Reduct    /VI used for reductions
                            , SpType   : String    //Spect_Type         /Spectral type
                            , r_SpType : String    //Spect_Type_Source  /Source of spectral type
                          )
//=============================================================================
//=============================================================================
//End of file Observatory.scala
//=============================================================================
