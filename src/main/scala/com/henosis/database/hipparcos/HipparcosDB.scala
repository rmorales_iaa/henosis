/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Aug/2021
 * Time:  19h:04m
 * Description: Hipparcos database
 *  https://heasarc.gsfc.nasa.gov/W3Browse/all/hipparcos.html
 */
//=============================================================================
package com.henosis.database.hipparcos
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.MongoDB
import com.common.database.mongoDB.MongoDB.getProjectionColList
import org.mongodb.scala.model.Filters.{and, equal, gte, lte}
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object HipparcosDB {
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): HipparcosDB =
    HipparcosDB(conf.getString("Hipparcos.databaseName")
              , conf.getString("Hipparcos.collectionName"))
  //---------------------------------------------------------------------------
}
//=============================================================================
case class HipparcosDB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(HipparcosSource.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  //---------------------------------------------------------------------------
  connected = true
  //-------------------------------------------------------------------------
  def findByDesignation(designation: String, colList: Seq[String]) : Seq[Document] = {
    collection.find( equal("designation", designation))
      .projection(getProjectionColList(colList))
      .getResultDocumentSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file HipparcosDB.scala
//=============================================================================
