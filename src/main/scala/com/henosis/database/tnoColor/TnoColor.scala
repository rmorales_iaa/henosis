/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Mar/2024
 * Time:  13h:58m
 * Description: None
 */
package com.henosis.database.tnoColor
//=============================================================================
import com.common.database.mongoDB.fortran.FortranColumn
import com.common.logger.MyLogger
//=============================================================================
import org.mongodb.scala.bson.BsonValue
import scala.collection.mutable.ArrayBuffer
import org.mongodb.scala.{Document}
//=============================================================================
object TnoColor extends MyLogger {
  //---------------------------------------------------------------------------
  final val COL_NAME_AST_NUMBER        = "astNumber"
  final val COL_NAME_AST_NAME          = "astName"
  final val COL_NAME_PROV_ID           = "provID"
  final val COL_NAME_NOTE_CODE         = "noteCode"
  final val COL_NAME_DYN_CLASS         = "dynClass"
  final val COL_NAME_COLOR_ID          = "colorID"
  final val COL_NAME_COLOR             = "color"
  final val COL_NAME_COLOR_UNCERTAINTY = "colorUncertainty"
  final val COL_NAME_OBS_DATE          = "obsDate"
  final val COL_NAME_REFERENCE         = "reference"
  //---------------------------------------------------------------------------
  private val columnDataColor = Array(
    FortranColumn(COL_NAME_AST_NUMBER, "A6")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_AST_NAME, "A17")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_PROV_ID, "A11")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_NOTE_CODE, "A2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_DYN_CLASS, "A7")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_COLOR_ID, "A3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_COLOR, "F7.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_COLOR_UNCERTAINTY, "F7.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_OBS_DATE, "A10")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_REFERENCE, "A23")
  )
  //---------------------------------------------------------------------------
  private val columnDataColorMultiRun = Array(
    FortranColumn(COL_NAME_AST_NUMBER, "A6")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_AST_NAME, "A17")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_PROV_ID, "A11")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_NOTE_CODE, "A2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_DYN_CLASS, "A7")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_COLOR_ID, "A3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_COLOR, "F7.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_COLOR_UNCERTAINTY, "F7.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn(COL_NAME_REFERENCE, "A23")
  )
  //---------------------------------------------------------------------------
  val rowByteSizeColor = (columnDataColor map (_.getCharSize)).sum
  val rowByteSizeColorMultiRun = (columnDataColorMultiRun map (_.getCharSize)).sum
  //-------------------------------------------------------------------------
  def apply(doc: Document, isMultiRun: Boolean) = new TnoColor(
    doc(COL_NAME_AST_NUMBER).asInt32().getValue
    , doc(COL_NAME_AST_NAME).asString().getValue
    , doc(COL_NAME_PROV_ID).asString().getValue
    , doc(COL_NAME_NOTE_CODE).asString().getValue
    , doc(COL_NAME_DYN_CLASS).asString().getValue
    , doc(COL_NAME_COLOR_ID).asString().getValue
    , doc(COL_NAME_COLOR).asDouble().getValue
    , doc(COL_NAME_COLOR_UNCERTAINTY).asDouble().getValue
    , if (isMultiRun) "multiRun" else doc(COL_NAME_OBS_DATE).asString().getValue
    , doc(COL_NAME_REFERENCE).asString().getValue
  )
  //-------------------------------------------------------------------------
  def apply(line: String, isMultiRun: Boolean, debug: Boolean = false): Option[Document] = {
    val row = ArrayBuffer[(String, BsonValue)]()
    var offset: Int = 0
    val columnData = if (isMultiRun) columnDataColorMultiRun else columnDataColor
    columnData.foreach { t =>
      val token = line.substring(offset, offset + t.charSize)
      offset += t.charSize
      if (t.name == "divider") {
        if (token != (" " * token.length)) {
          error(s"Error parsing column '${t.name}' the divider is not ' '. Current value:'$token'")
          error(line)
          return None
        }
      }
      else if (t.name != "end_of_column") {
        if (debug) println(s"${t.name} -> '$token' (${t.charSize}:${token.length})")
        t.addColumnToRow(token.trim, row)
      }
    }
    Some(Document(row))
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
case class TnoColor(astNumber: Int
                     , astName: String
                     , provID: String
                     , noteCode: String
                     , dynClass: String
                     , colorID: String
                     , color: Double
                     , colorUncertainty: Double
                     , obsDate: String
                     , reference: String
                    )
//=============================================================================
//End of file LcdbColorIndexDB.scala
//=============================================================================