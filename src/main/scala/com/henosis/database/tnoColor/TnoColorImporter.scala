/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Mar/2024
 * Time:  13h:35m
 * Description: None
 */
package com.henosis.database.tnoColor
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import com.henosis.database.tnoColor.TnoColor._
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
//=============================================================================
object TnoColorImporter extends MyLogger {
  //---------------------------------------------------------------------------
  private def processFile(fileName: String
                          , db: MongoDB
                          , coll: MongoCollection[Document]
                          , isMultiRun: Boolean
                         ): Boolean = {
    info(s"Processing 'TNO and Centaur Colors V10.0. EAR-A-COMPIL-3-TNO-CEN-COLOR-V10.0. NASA Planetary Data System, 2014' file: '$fileName'")

    val starBuffer = ArrayBuffer[Document]()
    val maxBufferedDocuments  = 10 //to minimize the write operations in mongo

    //read the star info and store it
    val bs = Source.fromFile(fileName)
    var i = 0
    for (line <- bs.getLines) {
      i = i+1
      val doc = TnoColor(line.padTo(if (isMultiRun) rowByteSizeColorMultiRun else rowByteSizeColor,' '), isMultiRun)
      if (doc.isEmpty) {
        bs.close
        return false
      }
      else {
        //store the star in the buffer and store the star sequence if is needed
        starBuffer += doc.get
        if (starBuffer.length > maxBufferedDocuments) {
          coll.insertMany(starBuffer.toArray).results()
          starBuffer.clear()
        }
      }
    }
    bs.close()

    //store the remain buffered stars
    if (starBuffer.length > 0) {
      coll.insertMany(starBuffer.toArray).results()
      starBuffer.clear()
    }

    info("Creating indexes")
    db.createIndex(COL_NAME_AST_NUMBER,coll,_unique = false)

    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB
                 , collection: MongoCollection[Document]
                 , filename: String
                 , isMultiRun: Boolean
                 , dropCollection: Boolean = true) = {
    if (dropCollection) db.dropCollection(collection)
    processFile(filename, db, collection, isMultiRun)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TnoColorImporter.scala
//=============================================================================