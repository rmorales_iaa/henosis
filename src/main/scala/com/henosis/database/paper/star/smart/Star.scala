/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:32m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.star.smart
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.henosis.algoritms.paper.star.StarCommon
//=============================================================================
object Star {
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String = "") =
    classOf[Star].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Star(name: String  = ""
                , fName: String = ""
                , oName: String = ""
                , ra: Double = -1d
                , dec: Double = -1d
                , l_pmra: String = ""
                , pmra: Double = -1d
                , l_pmdec: String = ""
                , pmdec: Double = -1d
                , twoMassID: String = ""
                , j_mag: Double = -1d
                , h_mag: Double = -1d
                , ks_mag: Double = -1d
                , comments: String = ""
                , source: String = ""
                , note: String = "") extends StarCommon {
  //-------------------------------------------------------------------------
  raDecPM = Point2D_Double(pmra, pmdec)
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = { name + sep}
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file EphmerisJPL.scala
//=============================================================================
