/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Jul/2021
 * Time:  20h:04mc
 * Description: Paper: Closest stars to the Sun within 50pc catalog (Torres 2019)
 * https://cdsarc.cds.unistra.fr/viz-bin/cat/J/A+A/629/A139
 * Date:  21/Jul/2021
 */
//=============================================================================
package com.henosis.database.paper.star.torres
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import com.common.database.mongoDB.fortran.FortranColumn
//=============================================================================
import org.mongodb.scala.Document
import org.mongodb.scala.bson.BsonValue
import scala.collection.mutable.ArrayBuffer
import org.mongodb.scala.MongoCollection
import scala.io.Source
//=============================================================================
object Torres_2019_DB_Importer extends MyLogger {
  //---------------------------------------------------------------------------
  final val COL_SOURCE_ID        = "TwoMassSource"
  final val COL_NAME_RA          = "RAdeg"
  final val COL_NAME_DEC         = "DEdeg"
  //---------------------------------------------------------------------------
  //table 2 and table 5 are joined: all columns of table 2 and columns starting in column 'UCAC2' (inclusive)
  private val columnData = Array(
      FortranColumn("Seq", "I5")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("SolID", "A19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("DR2Name", "A28")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("TwoMassSource", "I19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("Epoch", "F6.1")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("RAdeg", "F22.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("e_RAdeg", "F20.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("DEdeg", "F22.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("e_DEdeg", "F20.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("Plx", "F20.15")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("e_Plx", "F20.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("RPlx", "F22.16")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("pmRA", "F25.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("e_pmRA", "F20.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("pmDE", "F26.20")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("e_pmDE", "F20.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("RADEcor", "E23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("RAPlxcor", "E23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("RApmRAcor", "E23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("RApmDEcor", "E23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("DEPlxcor", "E23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("DEpmRAcor", "E23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("DEpmDEcor", "E23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("PlxpmRAcor", "E23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("PlxpmDEcor", "E23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("pmRApmDEcor", "E23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("gofAL", "F25.20")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("Dup", "A5")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("Gmag", "F19.16")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("BPmag", "F19.16")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("RPmag", "F19.16")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("BP-RP", "E22.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("BP-G", "E22.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("G-RP", "E22.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("RV", "F24.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("e_RV", "F20.17")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("o_RV", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("Tefftemp", "F6.1")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("loggtemp", "F3.1")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("[Fe/H]temp", "F5.2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("Var", "A13")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("GLON", "F23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("GLAT", "F23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("ELON", "F23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("ELAT", "F23.19")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("Teff", "F18.13")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("b_Teff", "F18.13")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("B_Teff", "F18.13")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("AG", "F20.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("b_E(BP-RP)", "F20.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("Lum", "F22.18")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("rh", "F19.16")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("tph", "A23")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("dph", "F20.17")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("vtot", "F20.16")
  )
  //---------------------------------------------------------------------------
  private val rowByteSize = (columnData map (_.getCharSize)).sum
  //---------------------------------------------------------------------------
  final val CSV_HEADER = Seq(

    //table 2
    "Torres_2019_name"
    , "Torres_2019_ra"
    , "Torres_2019_dec"
    , "Torres_2019_V"
    , "Torres_2019_B-V"
    , "Torres_2019_U-B"
    , "Torres_2019_V-R"
    , "Torres_2019_R-I"
    , "Torres_2019_V-I"
    , "Torres_2019_observings"
    , "Torres_2019_nights"
    , "Torres_2019_V_mean"
    , "Torres_2019_B-V_mean"
    , "Torres_2019_U-B_mean"
    , "Torres_2019_V-R_mean"
    , "Torres_2019_R-I_mean"
    , "Torres_2019_V-I_mean"

    //table5
    , "Torres_2019_uca2"
    , "Torres_2019_twoMass"
    , "Torres_2019_pmra"
    , "Torres_2019_pmdec"
    , "Torres_2019_pmRef"
  )
  //---------------------------------------------------------------------------
  case class Star(name : String
                  , ra : Double
                  , dec : Double
                  , V : Double
                  , B_V : Double
                  , U_B : Double
                  , V_R: Double
                  , R_I: Double
                  , V_I: Double
                  , observings: Int
                  , nights: Int
                  , V_mean: Double
                  , B_V_mean: Double
                  , U_B_mean: Double
                  , V_R_mean: Double
                  , R_I_mean: Double
                  , V_I_mean: Double
                  , uca2: String
                  , twoMass: String
                  , pmra: Double
                  , pmdec: Double
                  , pmRef: Int) {
    //-------------------------------------------------------------------------
    def getAsCsvRow(sep: String = "\t") = {

      //table 2
      name + sep +
        ra.toString + sep +
        dec.toString + sep +
        V.toString + sep +
        B_V.toString + sep +
        U_B.toString + sep +
        V_R.toString + sep +
        R_I.toString + sep +
        V_I.toString + sep +
        observings.toString + sep +
        nights.toString + sep +
        V_mean.toString + sep +
        B_V_mean.toString + sep +
        U_B_mean.toString + sep +
        V_R_mean.toString + sep +
        R_I_mean.toString + sep +
        V_I_mean.toString + sep +
        uca2 + sep +
        twoMass + sep +
        pmra.toString + sep +
        pmdec.toString + sep +
        pmRef
    }
    //-------------------------------------------------------------------------
    def this(doc: Document) = this(

      //table 2
      doc("").asString().getValue
      , doc(COL_NAME_RA).asDouble().getValue
      , doc(COL_NAME_DEC).asDouble().getValue
      , doc("V").asDouble().getValue
      , doc("B-V").asDouble().getValue
      , doc("U-B").asDouble().getValue
      , doc("V-R").asDouble().getValue
      , doc("R-I").asDouble().getValue
      , doc("V-I").asDouble().getValue
      , doc("observings").asInt32().getValue
      , doc("nights").asInt32().getValue
      , doc("V_mean").asDouble().getValue
      , doc("B-V_mean").asDouble().getValue
      , doc("U-B_mean").asDouble().getValue
      , doc("V-R_mean").asDouble().getValue
      , doc("R-I_mean").asDouble().getValue
      , doc("V-I_mean").asDouble().getValue

      //table 5
      , doc("UCAC2").asString().getValue
      , doc("2MASS").asString().getValue
      , doc("pmra").asDouble().getValue
      , doc("pmdec").asDouble().getValue
      , doc("pmRef").asInt32().getValue
    )
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def buildDocument(row: ArrayBuffer[(String, BsonValue)]) = Some(Document(row))
  //---------------------------------------------------------------------------
  private def readStarInfo(line: String,i: Int, debug: Boolean = false) : Option[Document] = {
    val row = ArrayBuffer[(String, BsonValue)]()
    var offset: Int = 0
    columnData.foreach { t =>
      val token = line.substring(offset,offset + t.charSize)
      offset += t.charSize
      if (t.name == "divider") {
        if (token != (" " * token.length)) {
          error(s"MPO: $i. Error parsing column '${t.name}' the divider is not ' '. Current value:'$token'")
          error(line)
          return None
        }
      }
      else
        if (t.name != "end_of_column") {
          if (debug) println(s"${t.name} -> '$token' (${t.charSize}:${token.length})")
          t.addColumnToRow(token.trim, row)
        }
    }
    buildDocument(row)
  }
  //---------------------------------------------------------------------------
  private def table_3_processFile(fileName: String, db: MongoDB, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Torres 2019' paper table3. File: '$fileName'")

    val starBuffer = ArrayBuffer[Document]()
    val maxBufferedAsteroid  = 10 //to minimize the write operations in mongo

    //read the mpo info and store it
    val bs = Source.fromFile(fileName)
    var i = 0
    for (line <- bs.getLines) {
      i = i+1
      val doc = readStarInfo(line.padTo(rowByteSize,' '),i)
      if (doc.isEmpty) {
        bs.close
        return false
      }
      else {
        //store the mpo in the buffer and store the mpo sequence if is needed
        info(s"Added star: '${doc.get(COL_SOURCE_ID).asInt64().getValue}'")
        starBuffer += doc.get
        if (starBuffer.length > maxBufferedAsteroid) {
          coll.insertMany(starBuffer.toArray).results()
          starBuffer.clear()
        }
      }
    }
    bs.close()

    //store the remain buffered stars
    if (starBuffer.length > 0) {
      coll.insertMany(starBuffer.toArray).results()
      starBuffer.clear()
    }

    info("Creating indexes")
    db.createIndex(COL_SOURCE_ID, coll)
    db.createIndex(COL_NAME_RA, coll, _unique = false)
    db.createIndex(COL_NAME_DEC, coll, _unique = false)
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, table_3_Filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    table_3_processFile(table_3_Filename, db, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Torres_2019_DB_Importer.scala
//=============================================================================
