/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Jul/2021
 * Time:  20h:04m
 * Description: Paper: Gliese catalog star/2MASS cross identifications : J/PASP/122/885
 * https://cdsarc.cds.unistra.fr/viz-bin/cat/J/PASP/122/885
 */
//=============================================================================
package com.henosis.database.paper.star.stauffer
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import com.common.database.mongoDB.fortran.FortranColumn
//=============================================================================
import org.mongodb.scala.bson.BsonValue
import org.mongodb.scala.{Document, MongoCollection}
import scala.collection.mutable.ArrayBuffer
import org.mongodb.scala.bson.BsonDouble
import scala.io.Source
//=============================================================================
object Stauffer_2010_DB_Importer extends  MyLogger {
  //---------------------------------------------------------------------------
  final val COL_NAME_STAR_NAME   = "name"
  final val COL_NAME_RA          = "ra"
  final val COL_NAME_DEC         = "dec"
  //---------------------------------------------------------------------------
  private val columnData = Array(
      FortranColumn("name", "A21")
    , FortranColumn("f_name", "A1")
    , FortranColumn("oname", "A13")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("ra_h", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("ra_m", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("ra_s", "F5.2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("dec_d", "A3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("dec_m", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("dec_s", "F4.1")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("l_pmra", "A1")
    , FortranColumn("pmra", "F6.3")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("l_pmdec", "A1")
    , FortranColumn("pmdec", "F6.3")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("2MASS", "A17")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("j_mag", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("h_mag", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("ks_mag", "F6.3")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("comments", "A7")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("source", "A43")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("note",  "A20")
  )
  //---------------------------------------------------------------------------
  private val rowByteSize = (columnData map (_.getCharSize)).sum
  //---------------------------------------------------------------------------
  final val CSV_HEADER = {
    val r = columnData.zipWithIndex.flatMap { case (t, i) =>
      if (t.name == "divider") None //remove dividers
      else if (i == 4) Some("stauffer_2010_ra")
      else if (i == 6) Some("stauffer_2010_dec")
      else if (i >= 4 && i <= 14) None //remove  ra,dec expressed as sequence of fields
      else Some("stauffer_2010_" + t.name)
    }
    r
  }

  //-------------------------------------------------------------------------
  def apply(doc: Document) = Star(
    doc(COL_NAME_STAR_NAME).asString().getValue
    , doc("f_name").asString().getValue
    , doc("oname").asString().getValue
    , doc(COL_NAME_RA).asDouble().getValue
    , doc(COL_NAME_DEC).asDouble().getValue
    , doc("l_pmra").asString().getValue
    , doc("pmra").asDouble().getValue
    , doc("l_pmdec").asString().getValue
    , doc("pmdec").asDouble().getValue
    , doc("2MASS").asString().getValue.drop(1)  //remove the starting 'J'
    , doc("j_mag").asDouble().getValue
    , doc("h_mag").asDouble().getValue
    , doc("ks_mag").asDouble().getValue
    , doc("comments").asString().getValue
    , doc("source").asString().getValue
    , doc("note").asString().getValue
  )
  //---------------------------------------------------------------------------
  private def buildDocument(row: ArrayBuffer[(String, BsonValue)]) = {

    //build ra,dec in decimal format
    val raString = row(3)._2.asInt32.getValue.toString + ":" +
                   row(4)._2.asInt32.getValue.toString + ":" +
                   row(5)._2.asDouble.getValue.toString


    val decString = row(6)._2.asString.getValue + ":" +
                    row(7)._2.asInt32.getValue.toString + ":" +
                    row(8)._2.asDouble.getValue.toString

    val ra  = Conversion.HMS_to_DD(raString)
    val dec = Conversion.DMS_to_DD(decString)

    row.remove(3,6)
    row.insert(2,COL_NAME_RA-> new BsonDouble(ra))
    row.insert(3,COL_NAME_DEC-> new BsonDouble(dec))
    Some(Document(row))
  }
  //---------------------------------------------------------------------------
  private def readStarInfo(line: String,i: Int, debug: Boolean = false) : Option[Document] = {
    val row = ArrayBuffer[(String, BsonValue)]()
    var offset: Int = 0
    columnData.foreach { t =>
      val token = line.substring(offset,offset + t.charSize)
      offset += t.charSize
      if (t.name == "divider") {
        if (token != (" " * token.length)) {
          error(s"Star: '$i'. Error parsing column '${t.name}' the divider is not ' '. Current value:'$token'")
          error(line)
          return None
        }
      }
      else
        if (t.name != "end_of_column") {
          if (debug) println(s"${t.name} -> '$token' (${t.charSize}:${token.length})")
          t.addColumnToRow(token.trim, row)
        }
    }
    buildDocument(row)
  }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, db: MongoDB, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Stauffer 2010' paper table 1 File: '$fileName'")

    val starBuffer = ArrayBuffer[Document]()
    val maxBufferedDocuments  = 10 //to minimize the write operations in mongo

    //read the star info and store it
    val bs = Source.fromFile(fileName)
    var i = 0
    for (line <- bs.getLines) {
      i = i+1
      val doc = readStarInfo(line.padTo(rowByteSize,' '),i)
      if (doc.isEmpty) {
        bs.close
        return false
      }
      else {
        //store the star in the buffer and store the star sequence if is needed
        info(s"Added star: '${doc.get("name").asString.getValue.trim}'")
        starBuffer += doc.get
        if (starBuffer.length > maxBufferedDocuments) {
          coll.insertMany(starBuffer.toArray).results()
          starBuffer.clear()
        }
      }
    }
    bs.close()

    //store the remain buffered stars
    if (starBuffer.length > 0) {
      coll.insertMany(starBuffer.toArray).results()
      starBuffer.clear()
    }

    info("Creating indexes")
    db.createIndex(COL_NAME_STAR_NAME, coll)
    db.createIndex(COL_NAME_RA, coll, _unique = false)
    db.createIndex(COL_NAME_DEC, coll, _unique = false)

    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, db, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Stauffer_2010_DB_Importer.scala
//=============================================================================
