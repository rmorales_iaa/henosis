/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Jul/2021
 * Time:  20h:04m
 * Description: Paper: UBVRI standards around celestial equator (landolt, 2009)
 * http://cdsarc.u-strasbg.fr/viz-bin/cat/J/AJ/137/4186
 */
//=============================================================================
package com.henosis.database.paper.star.landolt
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.database.mongoDB.fortran.FortranColumn
import com.common.logger.MyLogger
import org.mongodb.scala.bson.BsonDouble
//=============================================================================
import org.mongodb.scala.Document
import org.mongodb.scala.bson.BsonValue
import scala.collection.mutable.ArrayBuffer
import org.mongodb.scala.MongoCollection
import scala.io.Source
//=============================================================================
object Landolt_2009_DB_Importer extends  MyLogger {
  //---------------------------------------------------------------------------
  final val COL_NAME_STAR_NAME   = "name"
  final val COL_NAME_RA          = "ra"
  final val COL_NAME_DEC         = "dec"
  //---------------------------------------------------------------------------
  //table 2 and table 5 are joined: all columns of table 2 and columns starting in column 'UCAC2' (inclusive)
  private val columnData = Array(
      FortranColumn("name", "A11")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("ra_h", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("ra_m", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("ra_s", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("dec_d", "A3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("dec_m", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("dec_s", "F5.2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("V", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("B-V", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("U-B", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("V-R", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("R-I", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("V-I", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("observings", "I3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("nights", "I3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("V_mean", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("B-V_mean", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("U-B_mean", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("V-R_mean", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("R-I_mean", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("V-I_mean", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("UCAC2", "A8")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("2MASS", "A17")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("pmra", "F8.2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("pmdec", "F8.2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("pmRef", "I1")
  )
  //-------------------------------------------------------------------------
  def apply(doc: Document) = Star_2009 (
    //table 2
    doc(COL_NAME_STAR_NAME).asString().getValue
    , doc(COL_NAME_RA).asDouble().getValue
    , doc(COL_NAME_DEC).asDouble().getValue
    , doc("V").asDouble().getValue
    , doc("B-V").asDouble().getValue
    , doc("U-B").asDouble().getValue
    , doc("V-R").asDouble().getValue
    , doc("R-I").asDouble().getValue
    , doc("V-I").asDouble().getValue
    , doc("observings").asInt32().getValue
    , doc("nights").asInt32().getValue
    , doc("V_mean").asDouble().getValue
    , doc("B-V_mean").asDouble().getValue
    , doc("U-B_mean").asDouble().getValue
    , doc("V-R_mean").asDouble().getValue
    , doc("R-I_mean").asDouble().getValue
    , doc("V-I_mean").asDouble().getValue

    //table 5
    , doc("UCAC2").asString().getValue
    , doc("2MASS").asString().getValue.drop(1) //remove the starting 'J'
    , doc("pmra").asDouble().getValue
    , doc("pmdec").asDouble().getValue
    , doc("pmRef").asInt32().getValue
  )
  //---------------------------------------------------------------------------
  private val rowByteSize = (columnData map (_.getCharSize)).sum
  //---------------------------------------------------------------------------
  final val CSV_HEADER = {
    columnData.zipWithIndex.flatMap { case (t,i)=>
      if (t.name == "divider") None   //remove dividers
      else
        if (i == 2) Some("landolt_2009_ra")
        else
          if(i == 4) Some("landolt_2009_dec")
          else
            if (i >= 1 && i<= 12) None //remove  ra,dec expressed as sequence of fields
            else Some("landolt_2009_" + t.name)
    }
  }
  //---------------------------------------------------------------------------
  private def buildDocument(row: ArrayBuffer[(String, BsonValue)]) = {

    //build ra,dec in decimal format
    val raString = row(1)._2.asInt32.getValue.toString + ":" +
                   row(2)._2.asInt32.getValue.toString + ":" +
                   row(3)._2.asDouble.getValue.toString


    val decString = row(4)._2.asString.getValue + ":" +
                    row(5)._2.asInt32.getValue.toString + ":" +
                    row(6)._2.asDouble.getValue.toString

    val ra  = Conversion.HMS_to_DD(raString)
    val dec = Conversion.DMS_to_DD(decString)

    row.remove(1,6)
    row.insert(1,COL_NAME_RA-> new BsonDouble(ra))
    row.insert(2,COL_NAME_DEC-> new BsonDouble(dec))
    Some(Document(row))
  }
  //---------------------------------------------------------------------------
  private def readStarInfo(line: String,i: Int, debug: Boolean = false) : Option[Document] = {
    val row = ArrayBuffer[(String, BsonValue)]()
    var offset: Int = 0
    columnData.foreach { t =>
      val token = line.substring(offset,offset + t.charSize)
      offset += t.charSize
      if (t.name == "divider") {
        if (token != (" " * token.length)) {
          error(s"Star: $i. Error parsing column '${t.name}' the divider is not ' '. Current value:'$token'")
          error(line)
          return None
        }
      }
      else
        if (t.name != "end_of_column") {
          if (debug) println(s"${t.name} -> '$token' (${t.charSize}:${token.length})")
          t.addColumnToRow(token.trim, row)
        }
    }
    buildDocument(row)
  }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, db: MongoDB, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Landolt 2009' paper table 2 and table 5. File: '$fileName'")

    val starBuffer = ArrayBuffer[Document]()
    val maxBufferedDocuments  = 10 //to minimize the write operations in mongo

    //read the star info and store it
    val bs = Source.fromFile(fileName)
    var i = 0
    for (line <- bs.getLines) {
      i = i+1
      val doc = readStarInfo(line.padTo(rowByteSize,' '),i)
      if (doc.isEmpty) {
        bs.close
        return false
      }
      else {
        //store the star in the buffer and store the star sequence if is needed
        info(s"Added star: '${doc.get("name").asString.getValue.trim}'")
        starBuffer += doc.get
        if (starBuffer.length > maxBufferedDocuments) {
          coll.insertMany(starBuffer.toArray).results()
          starBuffer.clear()
        }
      }
    }
    bs.close()

    //store the remain buffered stars
    if (starBuffer.length > 0) {
      coll.insertMany(starBuffer.toArray).results()
      starBuffer.clear()
    }

    info("Creating indexes")
    db.createIndex(COL_NAME_STAR_NAME, coll)
    db.createIndex(COL_NAME_RA, coll, _unique = false)
    db.createIndex(COL_NAME_DEC, coll, _unique = false)

    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, db, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Landolt_2009_DB_Importer.scala
//=============================================================================
