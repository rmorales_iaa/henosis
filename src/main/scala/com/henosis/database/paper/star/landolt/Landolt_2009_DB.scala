/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Sep/2021
 * Time:  09h:56m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.star.landolt
//=============================================================================
//=============================================================================
import com.henosis.algoritms.paper.star.landolt.Landolt_2009
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.star.StarPaperDB
import org.bson.codecs.configuration.CodecRegistry
//=============================================================================
//=============================================================================
object Landolt_2009_DB {
  //---------------------------------------------------------------------------
  def apply(): Landolt_2009_DB =
    Landolt_2009_DB(PaperDB.DATABASE_NAME
      , PaperDB.COLLECTION_PAPER_LANDOLT_2009
      , Landolt_2009.paperName
      , Landolt_2009.referenceEpoch)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Landolt_2009_DB( databaseName: String
                            , collectionName: String
                            , paperName: String
                            , paperReferenceEpoch: String
                            , codecRegistry: CodecRegistry = null)
  extends StarPaperDB
//=============================================================================
//End of file Landolt_2009_DB.scala
//=============================================================================
