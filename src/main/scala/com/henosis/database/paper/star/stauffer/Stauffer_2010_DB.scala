/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Sep/2021
 * Time:  09h:56m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.star.stauffer
//=============================================================================
import com.henosis.algoritms.paper.star.stauffer.Stauffer_2010
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.star.StarPaperDB
import org.bson.codecs.configuration.CodecRegistry
//=============================================================================
//=============================================================================
object Stauffer_2010_DB {
  //---------------------------------------------------------------------------
  def apply(): Stauffer_2010_DB =
    Stauffer_2010_DB(PaperDB.DATABASE_NAME
      , PaperDB.COLLECTION_PAPER_STAUFFER_2010
      , Stauffer_2010.paperName
      , Stauffer_2010.referenceEpoch)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Stauffer_2010_DB( databaseName: String
                            , collectionName: String
                            , paperName: String
                            , paperReferenceEpoch: String
                            , codecRegistry: CodecRegistry = null)
  extends StarPaperDB
//=============================================================================
//End of file EphemerisJPL_DB.scala
//=============================================================================
