/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:32m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.star.stauffer
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.henosis.algoritms.paper.star.StarCommon
//=============================================================================
object Star {
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[Star].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Star(name: String
                , fName: String
                , oName: String
                , ra: Double
                , dec: Double
                , l_pmra: String
                , pmra: Double
                , l_pmdec: String
                , pmdec: Double
                , twoMassID: String
                , j_mag: Double
                , h_mag: Double
                , ks_mag: Double
                , comments: String
                , source: String
                , note: String) extends StarCommon {
  //-------------------------------------------------------------------------
  raDecPM = Point2D_Double(pmra, pmdec)
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    name + sep +
      fName + sep +
      oName + sep +
      ra.toString + sep +
      dec.toString + sep +
      l_pmra + sep +
      pmra.toString + sep +
      l_pmdec + sep +
      pmdec.toString + sep +
      twoMassID + sep +
      j_mag.toString + sep +
      h_mag.toString + sep +
      ks_mag.toString + sep +
      comments + sep +
      source + sep +
      note + sep +
      gaia_number_of_mates + sep +
      gaia_number_of_neighbours + sep +
      gaia_xm_flag + sep +
      gaia_badMatchInfo + sep +
      panSTARR_number_of_mates + sep +
      panSTARR_number_of_neighbours + sep +
      panSTARR_xm_flag + sep +
      panSTARR_badMatchInfo
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file EphmerisJPL.scala
//=============================================================================
