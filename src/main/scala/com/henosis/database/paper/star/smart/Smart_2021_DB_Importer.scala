/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  21/Jul/2021
  * Time:  20h:04m
  * Description: Paper: GaiaDR3 Early Data Release 3: The GaiaDR3 Catalogue of Nearby Stars
  * https://doi.org/10.1051/0004-6361/202039498
  * https://cdsarc.cds.unistra.fr/viz-bin/cat/J/A+A/649/A6
  */
//=============================================================================
package com.henosis.database.paper.star.smart
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import com.common.database.mongoDB.fortran.FortranColumn
//=============================================================================
import org.mongodb.scala.bson.BsonValue
import org.mongodb.scala.{Document, MongoCollection}
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
//=============================================================================
object Smart_2021_DB_Importer extends  MyLogger {
  //---------------------------------------------------------------------------
  final val COL_NAME_RA          = "RAdeg"
  final val COL_NAME_DEC         = "DEdeg"
  //---------------------------------------------------------------------------
  //https://cdsarc.cds.unistra.fr/ftp/J/A+A/649/A6/ReadMe
  private val columnData = Array(
    FortranColumn("divider_0", "A2")  //divider
    , FortranColumn("_id", "I19")  // original name: 'GaiaEDR3'
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("RAdeg", "F14")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_RAdeg", "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("DEdeg", "F14")
    , FortranColumn("divider_4", "A1")  //divider
    , FortranColumn("e_DEdeg", "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Plx", "F9")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_Plx", "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("pmRA", "F9")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_pmRA", "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("pmDE", "F9")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_pmDE", "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Gmag", "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("RFG", "F9")
    , FortranColumn("divider", "A1")  //divider

    , FortranColumn("BPmag", "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("RFBP", "F9")
    , FortranColumn("divider", "A1")  //divider

    , FortranColumn("RPmag", "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("RFRP", "F9")
    , FortranColumn("divider", "A1")  //divider

    , FortranColumn("E(BP/RP)", "F8")
    , FortranColumn("divider", "A1")  //divider

    , FortranColumn("RUWE", "F5")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("IPDfmp", "I3")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("RV", "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_RV", "F8")
    , FortranColumn("divider", "A2")  //divider
    , FortranColumn("r_RV", "A19")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("f_RV", "A1")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("GCNSprob", "F5")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("WDprob", "F5")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Dist1", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Dist16", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Dist50", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Dist84", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("xcoord50", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("xcoord16", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("xcoord84", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("ycoord50", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("ycoord16", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("ycoord84", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("zcoord50", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("zcoord16", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("zcoord84", "F12")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Uvel50",  "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Uvel16",  "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Uvel84",  "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Vvel50",  "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Vvel16",  "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Vvel84",  "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Wvel50",  "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Wvel16",  "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Wvel84",  "F8")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("GUNN",  "A20")
    , FortranColumn("divider", "A1")  //divider

    , FortranColumn("r_GUNN",  "A20")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("gmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_gmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("rmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_rmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("imag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_imag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("zmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_zmag",  "F7")
    , FortranColumn("divider", "A4")  //divider
    , FortranColumn("2MASS",  "A17")
    , FortranColumn("divider", "A1")  //divider

    , FortranColumn("Jmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_Jmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Hmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_Hmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("Ksmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_Ksmag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("WISE",  "A20")
    , FortranColumn("divider", "A1")  //divider

    , FortranColumn("W1mag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_W1mag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("W2mag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_W2mag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("W3mag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_W3mag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("W4mag",  "F7")
    , FortranColumn("divider", "A1")  //divider
    , FortranColumn("e_W4mag",  "F7")
  )
  //---------------------------------------------------------------------------
  private val rowByteSize = (columnData map (_.getCharSize)).sum
  //-------------------------------------------------------------------------
  def apply(doc: Document) = Star(
    name = doc("_id").asInt64().getValue.toString
    , ra = doc("RAdeg").asDouble().getValue
    , dec = doc("DEdeg").asDouble().getValue
    , pmra =doc("e_pmRA").asDouble().getValue
    , pmdec =  doc("e_pmDE").asDouble().getValue
  )
  //---------------------------------------------------------------------------
  private def buildDocument(row: ArrayBuffer[(String, BsonValue)]) =
    Some(Document(row))
  //---------------------------------------------------------------------------
  private def readStarInfo(line: String,i: Int, debug: Boolean = false) : Option[Document] = {
    val row = ArrayBuffer[(String, BsonValue)]()
    var offset: Int = 0
    columnData.zipWithIndex.foreach { case (t,k) =>
      val token = line.substring(offset,offset + t.charSize)
      offset += t.charSize

      if (debug) println(t.name+"->" + "'"+token+"'")
      if (t.name.startsWith("divider")) {
        if (token != (" " * token.length)) {
          error(s"Error parsing star: '$i' token: $k '${t.name}' offset: $offset The divider is not '${" " * token.length}'. Current value:'$token'")
          error(line)
          return None
        }
      }
      else t.addColumnToRow(token.trim, row)
    }
    buildDocument(row)
  }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, db: MongoDB, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing '${Smart_2021_DB.paperName}' paper table 1c File: '$fileName'")

    val starBuffer = ArrayBuffer[Document]()
    val maxBufferedDocuments  = 1000 //to minimize the write operations in mongo

    //read the star info and store it
    val bs = Source.fromFile(fileName)
    var i = 0
    for (line <- bs.getLines) {
      i = i+1
      val doc = readStarInfo(line.padTo(rowByteSize,' '),i)
      if (doc.isEmpty) {
        bs.close
        return false
      }
      else {
        //store the star in the buffer and store the star sequence if is needed
        info(s"Added star $i: '${doc.get("_id").asInt64().getValue}'")
        starBuffer += doc.get
        if (starBuffer.length > maxBufferedDocuments) {
          coll.insertMany(starBuffer.toArray).results()
          starBuffer.clear()
        }
      }
    }
    bs.close()

    //store the remain buffered stars
    if (starBuffer.length > 0) {
      coll.insertMany(starBuffer.toArray).results()
      starBuffer.clear()
    }

    info("Creating indexes")
    db.createIndex(COL_NAME_RA, coll, _unique = false)
    db.createIndex(COL_NAME_DEC, coll, _unique = false)
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, db, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Smart_2021_DB_Importer.scala
//=============================================================================
