/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:29m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.star.landolt
//=============================================================================
import com.common.geometry.point.Point2D_Double
import com.henosis.algoritms.paper.star.StarCommon
//=============================================================================
//=============================================================================
object Star_2009 {
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[Star_2009].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Star_2009(name : String
                     , ra : Double
                     , dec : Double
                     , V : Double
                     , B_V : Double
                     , U_B : Double
                     , V_R: Double
                     , R_I: Double
                     , V_I: Double
                     , observings: Int
                     , nights: Int
                     , V_mean: Double
                     , B_V_mean: Double
                     , U_B_mean: Double
                     , V_R_mean: Double
                     , R_I_mean: Double
                     , V_I_mean: Double
                     , uca2: String
                     , twoMassID: String
                     , pmra: Double
                     , pmdec: Double
                     , pmRef: Int) extends StarCommon {
  //-------------------------------------------------------------------------
  raDecPM = Point2D_Double(pmra, pmdec)
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    name + sep +
      ra.toString + sep +
      dec.toString + sep +
      V.toString + sep +
      B_V.toString + sep +
      U_B.toString + sep +
      V_R.toString + sep +
      R_I.toString + sep +
      V_I.toString + sep +
      observings.toString + sep +
      nights.toString + sep +
      V_mean.toString + sep +
      B_V_mean.toString + sep +
      U_B_mean.toString + sep +
      V_R_mean.toString + sep +
      R_I_mean.toString + sep +
      V_I_mean.toString + sep +
      uca2 + sep +
      twoMassID + sep +
      pmra.toString + sep +
      pmdec.toString + sep +
      pmRef  + sep +
      gaia_number_of_mates + sep +
      gaia_number_of_neighbours + sep +
      gaia_xm_flag + sep +
      gaia_badMatchInfo + sep +
      panSTARR_number_of_mates + sep +
      panSTARR_number_of_neighbours + sep +
      panSTARR_xm_flag + sep +
      panSTARR_badMatchInfo
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file Star_2009.scala
//=============================================================================
