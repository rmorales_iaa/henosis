/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:31m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.star.landolt
//=============================================================================
import com.henosis.algoritms.paper.star.StarCommon
//=============================================================================
object Star_2013 {
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[Star_2013].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Star_2013(name : String
                     , ra : Double
                     , dec : Double
                     , V : Double
                     , B_V : Double
                     , U_B : Double
                     , V_R: Double
                     , R_I: Double
                     , V_I: Double
                     , observings: Int
                     , nights: Int
                     , V_error: Double
                     , B_V_error: Double
                     , U_B_error: Double
                     , V_R_error: Double
                     , R_I_error: Double
                     , V_I_error: Double
                     , catalog: String
                     , twoMassID: String
                     , pmra: Double
                     , pmdec: Double
               ) extends StarCommon {
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    name + sep +
      ra.toString + sep +
      dec.toString + sep +
      V.toString + sep +
      B_V.toString + sep +
      U_B.toString + sep +
      V_R.toString + sep +
      R_I.toString + sep +
      V_I.toString + sep +
      observings.toString + sep +
      nights.toString + sep +
      V_error.toString + sep +
      B_V_error.toString + sep +
      U_B_error.toString + sep +
      V_R_error.toString + sep +
      R_I_error.toString + sep +
      V_I_error.toString + sep +
      catalog + sep +
      twoMassID + sep +
      pmra + sep +
      pmdec + sep +
      gaia_number_of_mates + sep +
      gaia_number_of_neighbours + sep +
      gaia_xm_flag + sep +
      gaia_badMatchInfo + sep +
      panSTARR_number_of_mates + sep +
      panSTARR_number_of_neighbours + sep +
      panSTARR_xm_flag + sep +
      panSTARR_badMatchInfo
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file Star_2013.scala
//=============================================================================
