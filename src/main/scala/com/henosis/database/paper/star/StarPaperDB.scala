package com.henosis.database.paper.star
//=============================================================================
import com.common.DatabaseHub
import com.common.coordinate.conversion.Conversion
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.postgreDB.gaia_dr_3.Source
import com.common.database.postgreDB.gaia_dr_3.Source.{GAIA_PHOTOMETRY_COL_SEQ, getProjectionColList}
import com.common.geometry.point.Point2D_Double
import com.common.util.string.MyString
import com.henosis.algoritms.paper.star.StarCommon
import com.henosis.database.gaia.crossMatch.hipparcos.GaiaHipparcosCrossMatchSource
import com.henosis.database.gaia.crossMatch.panSTARRS.GaiaPanSTARRS_CrossMatchSource
import com.henosis.database.gaia.crossMatch.twoMass.Gaia_2MassCrossMatchSource
import com.henosis.database.gaia.source.gaiaDR3.gaia_source.DB._
import com.henosis.database.gaia.source.gaiaDR3.gaia_source.DB
import com.henosis.database.panStarrs.PanSTARRS_DB.PANSTARRS_REFERENCE_EPOCH
import com.henosis.database.panStarrs.PanSTARRS_Source
import com.henosis.database.paper.PaperDB
import com.henosis.database.twoMass.TwoMassSource
import com.henosis.database.twoMass.TwoMassSource.TWO_MASS_COL_PHOTOMETRY_SEQ
import org.mongodb.scala.MongoCollection
import org.mongodb.scala.model.Filters.equal
//=============================================================================
import org.mongodb.scala.Document
import java.util.concurrent.atomic.AtomicInteger
//=============================================================================
//=============================================================================
trait StarPaperDB extends PaperDB {
  //---------------------------------------------------------------------------
  val paperReferenceEpoch: String
  //---------------------------------------------------------------------------
  private val paperBadMatchTwoMassCount   = new AtomicInteger(0)
  private val twoMassBadMatchGaiaCount    = new AtomicInteger(0)
  private val hipparcosBadMatchGaiaCount  = new AtomicInteger(0)
  private val gaiaBadMatchPanSTARRS_Count = new AtomicInteger(0)
  //---------------------------------------------------------------------------
  private def paperBadMatchTwoMass(star: StarCommon): Unit = {
    paperBadMatchTwoMassCount.addAndGet(1)
    warning(s"Bad match between $paperName and '2Mass' :" +
      star.getInfo(paperName) +
      s"\n\tCount: $paperBadMatchTwoMassCount")
  }
  //---------------------------------------------------------------------------
  private def twoMassBadMatchGaia(star: StarCommon
                                  , twoMassDoc: Option[Document]
                                  , gaiaCrossMatchDocSeq: Seq[Document]): Unit = {
    twoMassBadMatchGaiaCount.addAndGet(1)
    warning(s"Bad match between '2Mass' and 'GaiaDR3 EDR3' :" +
      star.getInfo(paperName) +
      (if (twoMassDoc.isEmpty) s"\n\t'GAIA 2Mass cross match' EMPTY!!!" else TwoMassSource.getInfo(twoMassDoc.get)) +
      Gaia_2MassCrossMatchSource.getInfo(gaiaCrossMatchDocSeq) +
      s"\n\tCount: $twoMassBadMatchGaiaCount")
  }
  //---------------------------------------------------------------------------
  private def hipparcosBadMatchGaiaEDR_3(star: StarCommon
                                         , gaiaDoc: Option[Document]
                                         , gaiaCrossMatchDocSeq: Seq[Document]): Unit = {
    hipparcosBadMatchGaiaCount.addAndGet(1)
    warning(s"Bad match between 'GaiaDR3 EDR3' and 'Hipparcos' :" +
      s"\n\tpaper     : '$paperName'" +
      s"\n\tpaper star:  '${star.name}'" +
      (if (gaiaDoc.isEmpty) s"\n\t'GaiaDR3 EDR3' source is empty!!!" else s"\n\t'GaiaDR3 EDR3' source: '${gaiaDoc.get("_id").asInt64().getValue}''") +
      GaiaHipparcosCrossMatchSource.getInfo(gaiaCrossMatchDocSeq) +
      s"\n\tError count: $hipparcosBadMatchGaiaCount")
  }
  //---------------------------------------------------------------------------
  private def gaiaBadMatchPanSTARRS(star: StarCommon
                                    , twoMassDoc: Option[Document]
                                    , gaiaDoc: Option[Document]
                                    , panSTARRS_CrossMatchDocSeq: Seq[Document]): Unit = {
    gaiaBadMatchPanSTARRS_Count.addAndGet(1)
    warning(s"Bad match between 'GaiaDR3 EDR3' and 'PanSTARRS' :" +
      star.getInfo(paperName) +
      (if (twoMassDoc.isEmpty) s"\n\t'GAIA EDR3 2Mass cross match' EMPTY!!!" else TwoMassSource.getInfo(twoMassDoc.get)) +
      (if (gaiaDoc.isEmpty) ""
      else Source.getInfo(gaiaDoc.get)) +
        GaiaPanSTARRS_CrossMatchSource.getInfo(panSTARRS_CrossMatchDocSeq) +
        s"\n\tCount: $gaiaBadMatchPanSTARRS_Count")
  }

  //---------------------------------------------------------------------------
  private def matchGaiaByPosition(dbHub: DatabaseHub, star: StarCommon) = {
    val gaiaRaDecPM = star.getRaDecWithProperMotion(observingDate = GAIA_REFERENCE_EPOCH, paperReferenceEpoch)

    dbHub.gaiaEDR_3_DB.findSourceSeqInRectangle(
        gaiaRaDecPM.x - DB.GAIA_QUERY_BY_POSITION_RA_PRECISION
      , gaiaRaDecPM.x + GAIA_QUERY_BY_POSITION_RA_PRECISION
      , gaiaRaDecPM.y - GAIA_QUERY_BY_POSITION_DEC_PRECISION
      , gaiaRaDecPM.y + GAIA_QUERY_BY_POSITION_DEC_PRECISION
      , colList = Array("_id"))
  }

  //-------------------------------------------------------------------------
  def getPhotometricDoc(gaiaID : Long, coll: MongoCollection[Document]) =
    Some(coll
      .find(equal("_id", gaiaID))
      .projection(getProjectionColList(GAIA_PHOTOMETRY_COL_SEQ))
      .getResultDocumentSeq.head)
  //---------------------------------------------------------------------------
  def getMatchGaiaByPosition(dbHub: DatabaseHub, star: StarCommon) = {
    val docSeq = matchGaiaByPosition(dbHub, star)
    docSeq.length match {
      case 0 => None
      case 1 =>
        val gaiaDoc = getPhotometricDoc(docSeq.head("_id").asInt64.getValue, dbHub.gaiaEDR_3_DB.getCollection).head
        val gaiaRaDecPM = Point2D_Double(gaiaDoc("pmra").asDouble.getValue, gaiaDoc("pmdec").asDouble.getValue)
        val gaiaPrediction = Conversion.properMotion(paperReferenceEpoch, star.getRaDec, GAIA_REFERENCE_EPOCH, gaiaRaDecPM)
        val distance = star.getRaDec.getDistance(gaiaPrediction)
        warning(s"Source '${star.name}' solved by position in 'GaiaDR3 EDR 3' " +
          s"\n  GaiaDR3 position prediction with proper motion: $gaiaPrediction" +
          s"\n  Source distance between $paperName and GaiaDR3 position prediction: $distance " +
          star.getInfo(paperName) +
          Source.getInfo(gaiaDoc))
        Some(gaiaDoc)
      case _ =>
        docSeq.zipWithIndex.map { case (doc, i) =>
          val photoMetricDoc = getPhotometricDoc(doc("_id").asInt64().getValue, dbHub.gaiaEDR_3_DB.getCollection).get
          s"\n\t'------ GaiaDR3 source candidate: $i ---------" +
            Source.getInfo(photoMetricDoc) +
            s"\n\t'------------------------------------------"
        }.mkString
        None
    }
  }
  //---------------------------------------------------------------------------
  def getPanSTARRS_ByPosition(dbHub: DatabaseHub, star: StarCommon) = {
    val startRaDecPM = star.getRaDecWithProperMotion(observingDate = PANSTARRS_REFERENCE_EPOCH, paperReferenceEpoch)
    val docSeq = dbHub.panSTARRS_DB.downloadAndAdd(star.name, startRaDecPM)
    docSeq.length match {
      case 0 => None
      case 1 =>
        val sourceDoc = PanSTARRS_Source.getPhotometricDoc(docSeq.head("objID").asInt64.getValue, dbHub.panSTARRS_DB.getCollection).head
        val distance = star.getRaDec.getDistance(Point2D_Double(sourceDoc("raMean").asDouble.getValue, sourceDoc("decMean").asDouble.getValue))
        warning(s"Source '${star.name}' solved by position in 'PanSTARRS'" +
          s"\n  Warning, PanSTARRS does not provide proper motion data, so assuming it as none." +
          s"\n  Source distance between $paperName and PanSTARSS position(no proper motion): $distance " +
          star.getInfo(paperName) +
          PanSTARRS_Source.getInfo(sourceDoc))
        Some(sourceDoc)
      case _ =>
        docSeq.zipWithIndex.map { case (doc, i) =>
          val photoMetricDoc = PanSTARRS_Source.getPhotometricDoc(doc("objID").asInt64().getValue, dbHub.panSTARRS_DB.getCollection).get
          s"\n\t'------ PanSTARRS source candidate: $i -------" +
            PanSTARRS_Source.getInfo(photoMetricDoc) +
            s"\n\t'---------------------------------------------"
        }.mkString
        None
    }
  }

  //-------------------------------------------------------------------------
  def getPanSTARRS_Doc(dbHub: DatabaseHub, id: Long): Option[Document] = {
    val existSource = dbHub.panSTARRS_DB.existSource(id)
    if (!existSource) {
      if (!dbHub.panSTARRS_DB.downloadAndAdd(id)) None
      else getPanSTARRS_Doc(dbHub,id)
    }
    else PanSTARRS_Source.getPhotometricDoc(id, dbHub.panSTARRS_DB.getCollection)
  }
  //---------------------------------------------------------------------------
  def getMatch2Mass(dbHub: DatabaseHub, star: StarCommon): Option[Document] = {
    val docSeq = dbHub.twoMassDB.findByDesignation(star.twoMassID, TWO_MASS_COL_PHOTOMETRY_SEQ)
    if (docSeq.isEmpty || docSeq.length != 1) {
      paperBadMatchTwoMass(star)
      None
    }
    else Some(docSeq.head)
  }
  //---------------------------------------------------------------------------
  def getMatchHipparcos(dbHub: DatabaseHub, star: StarCommon): Option[Document] = {
    val docSeq = dbHub.hipparcosDB.findByDesignation(star.twoMassID, TWO_MASS_COL_PHOTOMETRY_SEQ)
    if (docSeq.isEmpty || docSeq.length != 1) {
      paperBadMatchTwoMass(star)
      None
    }
    else Some(docSeq.head)
  }
  //---------------------------------------------------------------------------
  def getMatchGaiaWith2Mass(star: StarCommon
                            , twoMassDoc: Option[Document]
                            , dbHub: DatabaseHub): Option[Document] = {
    //-------------------------------------------------------------------------
    def updateBadMatchInfo(gaiaCrossMatchDocSeq: Seq[Document]): Unit = {
      val s = gaiaCrossMatchDocSeq map { case doc =>
        val gaiaID = doc("source_id").asInt64.getValue
        val gaiaDoc = getPhotometricDoc(doc("source_id").asInt64.getValue, dbHub.gaiaEDR_3_DB.getCollection).get
        val gMag = gaiaDoc("phot_g_mean_mag").asDouble.getValue
        gaiaID.toString + s"G:$gMag"
      }
      star.gaia_badMatchInfo = s.mkString("(", ";", ")")
    }
    //-------------------------------------------------------------------------
    if (twoMassDoc.isEmpty) return getMatchGaiaByPosition(dbHub,star)
    val gaiaCrossMatchDocSeq = dbHub.gaiaEDR_3_CrossMatchTwoMassDB.matchSource(star.twoMassID)
    gaiaCrossMatchDocSeq.length match {
      case 0 =>
        twoMassBadMatchGaia(star, twoMassDoc, gaiaCrossMatchDocSeq)
        None
      case 1 =>
        val source = Gaia_2MassCrossMatchSource(gaiaCrossMatchDocSeq.head)
        star.gaia_number_of_mates = source.number_of_mates
        star.gaia_number_of_neighbours = source.number_of_neighbours
        star.gaia_xm_flag = source.xm_flag
        if (source.number_of_mates == 0 && source.number_of_neighbours == 1)
          getPhotometricDoc(source.source_id, dbHub.gaiaEDR_3_DB.getCollection)
        else {
          updateBadMatchInfo(gaiaCrossMatchDocSeq)
          twoMassBadMatchGaia(star, twoMassDoc, gaiaCrossMatchDocSeq)
          None
        }
      case _ =>
        updateBadMatchInfo(gaiaCrossMatchDocSeq)
        twoMassBadMatchGaia(star, twoMassDoc, gaiaCrossMatchDocSeq)
        None
    }
  }

  //---------------------------------------------------------------------------
  //(GaiaID,Hipparos,gaiaDocMatchByID)
  def getMatchGaiaWithHipparcos(star: StarCommon
                                , dbHub: DatabaseHub): Option[(Document,GaiaHipparcosCrossMatchSource,Boolean)] = {
    //-------------------------------------------------------------------------
    val verbose = false
    var gaiaDocMatchByID = true
    var gaiaDoc = dbHub.gaiaEDR_3_DB.tryToFind(star.name.toLong)
    if (!gaiaDoc.isDefined) {
      gaiaDocMatchByID = false
      gaiaDoc = getMatchGaiaByPosition(dbHub,star)
      if (!gaiaDoc.isDefined){
        if (verbose) hipparcosBadMatchGaiaEDR_3(star, None, Seq[Document]())
        return None
      }
    }
    val gaiaCrossMatchDocSeq = dbHub.gaiaEDR_3_CrossMatchHipparcosDB.matchSource(star.name.toLong)
    gaiaCrossMatchDocSeq.length match {
      case 0 =>
        if (verbose) hipparcosBadMatchGaiaEDR_3(star, gaiaDoc, gaiaCrossMatchDocSeq)
        None
      case 1 =>
        val matchedSource = GaiaHipparcosCrossMatchSource(gaiaCrossMatchDocSeq.head)
        if (matchedSource.number_of_neighbours == 1) Some((gaiaDoc.get,matchedSource,gaiaDocMatchByID))
        else {
          if (verbose) hipparcosBadMatchGaiaEDR_3(star, gaiaDoc, gaiaCrossMatchDocSeq)
          None
        }
      case _ =>
        if (verbose) hipparcosBadMatchGaiaEDR_3(star, gaiaDoc, gaiaCrossMatchDocSeq)
        None
    }
  }

  //---------------------------------------------------------------------------
  //synthetic photometry gspc
  def getMatchSmartWithGaia_DR_3_SPG(gaiaID: Long
                                   , dbHub: DatabaseHub): Option[com.henosis.database.gaia.source.gaiaDR3.performance_verification.synthetic_photometry_gspc.Source]
  = {
    //-------------------------------------------------------------------------
    val r = dbHub.gaiaDR_3_DB_spgDB.collWithRegistry.find(equal("source_id",gaiaID)).results
    if (r.isEmpty) None
    else Some(r.head)
  }
  //---------------------------------------------------------------------------
  def getBestMatchGaiaWithPanSTARSS(star: StarCommon
                                    , twoMassDoc: Option[Document]
                                    , gaiaDoc: Option[Document]
                                    , dbHub: DatabaseHub): Option[Document] = {
    //-------------------------------------------------------------------------
    def updateBadMatchInfo(panSTARRS_CrossMatchDocSeq: Seq[Document]): Unit = {
      val s = panSTARRS_CrossMatchDocSeq map { case doc =>
        val panSTARR_ID = doc("source_id").asInt64.getValue
        panSTARR_ID.toString
      }
      star.panSTARR_badMatchInfo = s.mkString("(", ";", ")")
    }
    //-------------------------------------------------------------------------
    if (gaiaDoc.isEmpty) return getPanSTARRS_ByPosition(dbHub, star)
    val gaiaID = gaiaDoc.get("_id").asInt64().getValue
    val panSTARRS_CrossMatchDocSeq = dbHub.gaiaEDR_3_CrossMatchPanStarrDB.matchSource(gaiaID)
    panSTARRS_CrossMatchDocSeq.length match {
      case 0 =>
        val r = getPanSTARRS_ByPosition(dbHub, star)
        if (r.isEmpty) {
          gaiaBadMatchPanSTARRS(star, twoMassDoc, gaiaDoc, panSTARRS_CrossMatchDocSeq)
          None
        }
        else r
      case 1 =>
        val source = GaiaPanSTARRS_CrossMatchSource(panSTARRS_CrossMatchDocSeq.head)
        star.panSTARR_number_of_mates = source.number_of_mates
        star.panSTARR_number_of_neighbours = source.number_of_neighbours
        star.panSTARR_xm_flag = source.xm_flag
        if (source.number_of_mates == 0 && source.number_of_neighbours == 1) {
          val r = getPanSTARRS_Doc(dbHub, panSTARRS_CrossMatchDocSeq.head("original_ext_source_id").asInt64.getValue)
          if (!r.isDefined) {
            updateBadMatchInfo(panSTARRS_CrossMatchDocSeq)
            gaiaBadMatchPanSTARRS(star, twoMassDoc, gaiaDoc, panSTARRS_CrossMatchDocSeq)
          }
          r
        }
        else {
          updateBadMatchInfo(panSTARRS_CrossMatchDocSeq)
          gaiaBadMatchPanSTARRS(star, twoMassDoc, gaiaDoc, panSTARRS_CrossMatchDocSeq)
          None
        }
      case _ =>
        updateBadMatchInfo(panSTARRS_CrossMatchDocSeq)
        gaiaBadMatchPanSTARRS(star, twoMassDoc, gaiaDoc, panSTARRS_CrossMatchDocSeq)
        None
    }
  }
  //---------------------------------------------------------------------------
  def reportStats(fixedSize: Int) = {
    info("\t" + MyString.rightPadding(s"Total stars not matched between $paperName and '2Mass' : ", fixedSize) + paperBadMatchTwoMassCount.get)
    info("\t" + MyString.rightPadding(s"Total stars not matched between '2Mass' and 'GaiaDR3 EDR3'     : ", fixedSize) + twoMassBadMatchGaiaCount.get)
    info("\t" + MyString.rightPadding(s"Total stars not matched between 'GaiaDR3 EDR3' and 'PanSTARRS' : ", fixedSize) + gaiaBadMatchPanSTARRS_Count.get)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file StarPaperDB.scala
//=============================================================================
