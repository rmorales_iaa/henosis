/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Sep/2021
 * Time:  09h:56m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.star.landolt
//=============================================================================
import com.henosis.algoritms.paper.star.landolt.Landolt_2013
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.star.StarPaperDB
import org.bson.codecs.configuration.CodecRegistry
//=============================================================================
//=============================================================================
object Landolt_2013_DB {
  //---------------------------------------------------------------------------
  def apply(): Landolt_2013_DB =
    Landolt_2013_DB(PaperDB.DATABASE_NAME
      , PaperDB.COLLECTION_PAPER_LANDOLT_2013
      , Landolt_2013.paperName
      , Landolt_2013.referenceEpoch)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Landolt_2013_DB( databaseName: String
                             , collectionName: String
                             , paperName: String
                             , paperReferenceEpoch: String
                             , codecRegistry: CodecRegistry = null)
  extends StarPaperDB
//=============================================================================
//End of file Landolt_2013_DB.scala
//=============================================================================
