/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/Sep/2021
 * Time:  09h:56m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.star.smart
//=============================================================================
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.star.StarPaperDB
import org.bson.codecs.configuration.CodecRegistry
//=============================================================================
//=============================================================================
object Smart_2021_DB {
  //---------------------------------------------------------------------------
  val paperName = "smart_2021"
  val referenceEpoch = "2016-01-01T00:00:00"
  //---------------------------------------------------------------------------
  def apply(): Smart_2021_DB =
    Smart_2021_DB(PaperDB.DATABASE_NAME, PaperDB.COLLECTION_PAPER_SMART_2021)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Smart_2021_DB(databaseName: String
                          , collectionName: String
                          , codecRegistry: CodecRegistry = null) extends StarPaperDB {
  //---------------------------------------------------------------------------
  val paperName = Smart_2021_DB.paperName
  val paperReferenceEpoch= Smart_2021_DB.referenceEpoch
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Smart_2021_DB.scala
//=============================================================================
