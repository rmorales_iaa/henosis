/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Jul/2021
 * Time:  20h:04m
 * Description: Paper: UBVRI standard stars at +50{deg} declination : J/AJ/146/131
 * https://cdsarc.cds.unistra.fr/viz-bin/cat/J/AJ/146/131
 */
//=============================================================================
package com.henosis.database.paper.star.landolt
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import com.common.database.mongoDB.fortran.FortranColumn
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import org.mongodb.scala.bson.BsonValue
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import org.mongodb.scala.bson.BsonDouble
//=============================================================================
object Landolt_2013_DB_Importer extends MyLogger {
  //---------------------------------------------------------------------------
  final val COL_NAME_STAR_NAME   = "name"
  final val COL_NAME_RA          = "ra"
  final val COL_NAME_DEC         = "dec"
  final val COL_NAME_CATALOG_ID  = "catalog_id"
  //---------------------------------------------------------------------------
  private val columnData = Array(
      FortranColumn("name", "A12")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("ra_h", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("ra_m", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("ra_s", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("dec_d", "A3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("dec_m", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("dec_s", "F5.2")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("V", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("B-V", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("U-B", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("V-R", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("R-I", "F6.3")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("V-I", "F6.3")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("observings", "I2")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("nights", "I2")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("V_error", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("B-V_error", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("U-B_error", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("V-R_error", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("R-I_error", "F6.4")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("V-I_error", "F6.4")
    , FortranColumn("divider", "A2") //divider

    , FortranColumn("catalog", "A5")
    , FortranColumn("divider", "A1") //divider
    , FortranColumn("catalog_id", "A19")
    , FortranColumn("divider", "A1") //divider

    , FortranColumn("pmra", "F10.3") //divider
    , FortranColumn("pmdec", "F7.3") //divider
  )
  //---------------------------------------------------------------------------
  private val rowByteSize = (columnData map (_.getCharSize)).sum
  //---------------------------------------------------------------------------
  final val CSV_HEADER = {
    columnData.zipWithIndex.flatMap { case (t,i)=>
      if (t.name == "divider") None   //remove dividers
      else
        if (i == 2) Some("landolt_2013_ra")
        else
          if(i == 4) Some("landolt_2013_dec")
          else
            if (i >= 1 && i<= 12) None //remove  ra,dec expressed as sequence of fields
            else Some("landolt_2013_" + t.name)
    }
  }
  //-------------------------------------------------------------------------
  def apply(doc: Document) = Star_2013(
    doc(COL_NAME_STAR_NAME).asString().getValue
    , doc(COL_NAME_RA).asDouble().getValue
    , doc(COL_NAME_DEC).asDouble().getValue
    , doc("V").asDouble().getValue
    , doc("B-V").asDouble().getValue
    , doc("U-B").asDouble().getValue
    , doc("V-R").asDouble().getValue
    , doc("R-I").asDouble().getValue
    , doc("V-I").asDouble().getValue
    , doc("observings").asInt32().getValue
    , doc("nights").asInt32().getValue
    , doc("V_error").asDouble().getValue
    , doc("B-V_error").asDouble().getValue
    , doc("U-B_error").asDouble().getValue
    , doc("V-R_error").asDouble().getValue
    , doc("R-I_error").asDouble().getValue
    , doc("V-I_error").asDouble().getValue
    , doc("catalog").asString().getValue
    , doc("catalog_id").asString().getValue.drop(1) //remove the starting 'J'
    , doc("pmra").asDouble().getValue
    , doc("pmdec").asDouble().getValue
  )
  //---------------------------------------------------------------------------
  private def buildDocument(row: ArrayBuffer[(String, BsonValue)]) = {

    //build ra,dec in decimal format
    val raString = row(1)._2.asInt32.getValue.toString + ":" +
                   row(2)._2.asInt32.getValue.toString + ":" +
                   row(3)._2.asDouble.getValue.toString


    val decString = row(4)._2.asString.getValue + ":" +
                    row(5)._2.asInt32.getValue.toString + ":" +
                    row(6)._2.asDouble.getValue.toString

    val ra  = Conversion.HMS_to_DD(raString)
    val dec = Conversion.DMS_to_DD(decString)

    row.remove(1,6)
    row.insert(1,COL_NAME_RA-> new BsonDouble(ra))
    row.insert(2,COL_NAME_DEC-> new BsonDouble(dec))
    Some(Document(row))
  }
  //---------------------------------------------------------------------------
  private def readStarInfo(line: String,i: Int, debug: Boolean = false) : Option[Document] = {
    val row = ArrayBuffer[(String, BsonValue)]()
    var offset: Int = 0
    columnData.foreach { t =>
      val token = line.substring(offset,offset + t.charSize)
      offset += t.charSize
      if (t.name == "divider") {
        if (token != (" " * token.length)) {
          error(s"Star: '$i'. Error parsing column '${t.name}' the divider is not ' '. Current value:'$token'")
          error(line)
          return None
        }
      }
      else
        if (t.name != "end_of_column") {
          if (debug) println(s"${t.name} -> '$token' (${t.charSize}:${token.length})")
          t.addColumnToRow(token.trim, row)
        }
    }
    buildDocument(row)
  }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, db: MongoDB, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Landolt 2013' paper: tables 2,3 and 5 sim. File: '$fileName'")

    val starBuffer = ArrayBuffer[Document]()
    val maxBufferedDocuments  = 10 //to minimize the write operations in mongo

    //read the star info and store it
    val bs = Source.fromFile(fileName)
    var i = 0
    for (line <- bs.getLines) {
      i = i+1
      val doc = readStarInfo(line.padTo(rowByteSize,' '),i)
      if (doc.isEmpty) {
        bs.close
        return false
      }
      else {
        //store the star in the buffer and store the star sequence if is needed
        info(s"Added star: '${doc.get("name").asString.getValue.trim}'")
        starBuffer += doc.get
        if (starBuffer.length > maxBufferedDocuments) {
          coll.insertMany(starBuffer.toArray).results()
          starBuffer.clear()
        }
      }
    }
    bs.close()

    //store the remain buffered stars
    if (starBuffer.length > 0) {
      coll.insertMany(starBuffer.toArray).results()
      starBuffer.clear()
    }

    info("Creating indexes")
    db.createIndex(COL_NAME_STAR_NAME, coll)
    db.createIndex(COL_NAME_RA, coll, _unique = false)
    db.createIndex(COL_NAME_DEC, coll, _unique = false)
    db.createIndex(COL_NAME_CATALOG_ID, coll)

    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, db, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Landolt_2013_DB_Importer.scala
//=============================================================================
