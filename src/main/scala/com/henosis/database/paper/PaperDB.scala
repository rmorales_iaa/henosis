/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  01h:31m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper
//=============================================================================
import com.common.database.mongoDB.MongoDB
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
//=============================================================================
object PaperDB {
  //---------------------------------------------------------------------------
  final val DATABASE_NAME = "paper"
  //---------------------------------------------------------------------------
  final val COLLECTION_PAPER_PEIXHINO_2015   = "peixhino_2015"
  final val COLLECTION_PAPER_CANDAL_2019     = "candal_2019"
  final val COLLECTION_PAPER_COLAZO_2021     = "colazo_2021"
  final val COLLECTION_PAPER_CARRY_2016      = "carry_2016"
  final val COLLECTION_PAPER_LANDOLT_2009    = "landolt_2009_table_2_5c"
  final val COLLECTION_PAPER_LANDOLT_2013    = "landolt_2013_table_2_3_5"
  final val COLLECTION_PAPER_MAHLKE_2021     = "mahlke_2021"
  final val COLLECTION_PAPER_OSZKIEWICZ_2021 = "oszkiewicz_2011"
  final val COLLECTION_PAPER_POPESCU_2018    = "popescu_2018"
  final val COLLECTION_PAPER_STAUFFER_2010   = "stauffer_2010_table_1"
  final val COLLECTION_PAPER_SMART_2021      = "smart_2021_table_1_c"
  final val COLLECTION_PAPER_VERES_2015      = "veres_2015"
  //---------------------------------------------------------------------------
}
//=============================================================================
trait PaperDB extends MongoDB {
  //---------------------------------------------------------------------------
  val databaseName: String
  val collectionName: String
  val paperName: String
  val codecRegistry: CodecRegistry
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initDatabase()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
  private def initDatabase()= {
    if (codecRegistry != null) initWithCodeRegister(codecRegistry)
    else init()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PaperDB.scala
//=============================================================================
