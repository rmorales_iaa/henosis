package com.henosis.database.paper.mpo
//=============================================================================
import com.common.DatabaseHub
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.MongoDB.getProjectionColList
import com.common.util.string.MyString
import com.henosis.algoritms.paper.mpo.MPO_Common
import com.henosis.database.mpc.MpcDB
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.mpo.popescu.MPO
//=============================================================================
import org.mongodb.scala.Document
import java.util.concurrent.atomic.AtomicInteger
import org.mongodb.scala.model.Filters.equal
import java.io.BufferedWriter
//=============================================================================
trait MPO_PaperDB extends PaperDB {
  //---------------------------------------------------------------------------
  private val paperBadMatchMpcCount  = new AtomicInteger(0)
  private val mpcBadMatchSdssCount   = new AtomicInteger(0)
  private val mpcBadMatch_2MassCount = new AtomicInteger(0)
  private val mpcBadMatchTessCount   = new AtomicInteger(0)
  private val mpcBadMatchGaiaCount   = new AtomicInteger(0)
  //--------------------------------------------------------------------------
  final val SDSS_MOC_BASIC_INFO_COL_SEQ = Seq(
      "U_MAG"
    , "U_ERR"
    , "G_MAG"
    , "G_ERR"
    , "R_MAG"
    , "R_ERR"
    , "I_MAG"
    , "I_ERR"
    , "Z_MAG"
    , "Z_ERR"
    , "A_MAG"
    , "A_ERR"
    , "V_MAG"
    , "B_MAG"
    , "PHASE"
    , "H"
    , "G"
  )
  final val SDDS_MOC_BASIC_INFO_CSV_HEADER = SDSS_MOC_BASIC_INFO_COL_SEQ map ("sdss_moc_" + _)
  //--------------------------------------------------------------------------
  final val TWO_MASS_MOC_BASIC_INFO_COL_SEQ = Seq(
      "J_MAG"
    , "J_MAG_SIG"
    , "H_MAG"
    , "H_MAG_SIG"
    , "KS_MAG"
    , "KS_MAG_SIG"
    , "V_MAG"
    , "PHASE"
    , "Q"
  )
  final val TWO_MASS_MOC_BASIC_INFO_CSV_HEADER = TWO_MASS_MOC_BASIC_INFO_COL_SEQ map ("2mass_moc_" + _)
  //--------------------------------------------------------------------------
  final val TESS_MOC_BASIC_INFO_COL_SEQ = Seq(
      "tessMag"
    , "uncertMag"
    , "tess_dist"
    , "sun_dist"
    , "tess_obj_sun_phase_angle")
  final val TESS_MOC_BASIC_INFO_CSV_HEADER = TESS_MOC_BASIC_INFO_COL_SEQ map ("tess_moc_" + _)
  //--------------------------------------------------------------------------
  final val GAIA_2_MOC_BASIC_INFO_COL_SEQ  = Seq(
    "number_mp"
    , "epoch_utc"
    , "g_mag"
    , "g_flux"
    , "g_flux_error")
  final val GAIA_2_MOC_BASIC_INFO_CSV_HEADER = GAIA_2_MOC_BASIC_INFO_COL_SEQ map ("gaia_2_moc_" + _)
  //---------------------------------------------------------------------------
  private def paperBadMatchMpc(mpo: MPO_Common): Unit = {
    paperBadMatchMpcCount.addAndGet(1)
    warning(s"Bad match between $paperName and 'MPC' :" +
      mpo.getInfo(paperName) +
      s"\n\tCount: $paperBadMatchMpcCount")
  }
  //---------------------------------------------------------------------------
  private def mpcBadMatchSdss(mpo: MPO_Common): Unit = {
    mpcBadMatchSdssCount.addAndGet(1)
    warning(s"Bad match between 'MPC' and 'SDSS MOC' :" +
      mpo.getInfo(paperName) +
      s"\n\tCount: $mpcBadMatchSdssCount")
  }
  //---------------------------------------------------------------------------
  private def mpcBadMatchTwoMass(mpo: MPO_Common): Unit = {
    mpcBadMatch_2MassCount.addAndGet(1)
    warning(s"Bad match between 'MPC' and '2Mass MOC' :" +
      mpo.getInfo(paperName) +
      s"\n\tCount: $mpcBadMatch_2MassCount")
  }
  //---------------------------------------------------------------------------
  private def mpcBadMatchTess(mpo: MPO_Common): Unit = {
    mpcBadMatchTessCount.addAndGet(1)
    warning(s"Bad match between 'MPC' and 'Tess MOC' :" +
      mpo.getInfo(paperName) +
      s"\n\tCount: $mpcBadMatchTessCount")
  }
  //---------------------------------------------------------------------------
  private def mpcBadMatchGaia(mpo: MPO_Common): Unit = {
    mpcBadMatchGaiaCount.addAndGet(1)
    warning(s"Bad match between 'MPC' and 'GAIA 2 MOC' :" +
      mpo.getInfo(paperName) +
      s"\n\tCount: $mpcBadMatchGaiaCount")
  }
  //---------------------------------------------------------------------------
  def getMatchMpc(dbHub: DatabaseHub, mpo: MPO_Common): Option[Document] = {
    val source = mpo.asInstanceOf[MPO]
    val mpcDB = dbHub.mpcDB
    source.name.foreach{ s=>
      val r = mpcDB.getByName(s, verbose = false)
      if (r.isDefined) return Some(r.head)
      else {
          val r = mpcDB.collection
            .find(equal("Number", s))
            .projection(getProjectionColList(MpcDB.MPC_BASIC_INFO_COL_SEQ))
            .results()
          if (r.length == 1) return Some(r.head)
      }
    }
    paperBadMatchMpc(mpo)
    None
  }
  //---------------------------------------------------------------------------
  def getMatchSdss(dbHub: DatabaseHub, mpo: MPO_Common, mpcID: Option[Int]): Option[Document] = {
    if (mpcID.isEmpty) return None

    val r = dbHub.mpoDB.collSdssMoc_4
      .find(equal("AST_NUMBER", mpcID.get))
      .projection(getProjectionColList(SDSS_MOC_BASIC_INFO_COL_SEQ))
      .results()
    if (r.length == 1)
      Some(r.head)
    else {
      mpcBadMatchSdss(mpo)
      None
    }
  }
  //---------------------------------------------------------------------------
  def getMatch2Mass(dbHub: DatabaseHub, mpo: MPO_Common, mpcID: Option[Int]): Option[Document] = {
    if (mpcID.isEmpty) return None
    val r = dbHub.mpoDB.coll2MassMoc
      .find(equal("designation", mpcID.get))
      .projection(getProjectionColList(TWO_MASS_MOC_BASIC_INFO_COL_SEQ))
      .results()
    if (r.length == 1)
      Some(r.head)
    else {
      mpcBadMatchTwoMass(mpo)
      None
    }
  }
  //---------------------------------------------------------------------------
  def getMatchTess(dbHub: DatabaseHub, mpo: MPO_Common, mpcID: Option[Int]): Option[Seq[Document]] = {
    if (mpcID.isEmpty) return None
    val r = dbHub.mpoDB.collTessMoc
      .find(equal("mpc_id", mpcID.get))
      .projection(getProjectionColList(TESS_MOC_BASIC_INFO_COL_SEQ))
      .results()
    if (r.length > 0)
      Some(r)
    else {
      mpcBadMatchTess(mpo)
      None
    }
  }
  //---------------------------------------------------------------------------
  def getMatchGaia(dbHub: DatabaseHub, mpo: MPO_Common, mpcID: Option[Int]): Option[Seq[Document]] = {
    if (mpcID.isEmpty) return None
    val r = dbHub.mpoDB.collGaiaMoc
      .find(equal("number_mp", mpcID.get))
      .projection(getProjectionColList(GAIA_2_MOC_BASIC_INFO_COL_SEQ))
      .results()
    if (r.length > 0) Some(r)
    else {
      mpcBadMatchGaia(mpo)
      None
    }
  }
  //--------------------------------------------------------------------------
  def writeCsvSdssMoc(doc: Document, bw: BufferedWriter, sep: String = "\t") = {
    bw.write(doc("U_MAG").asDouble.getValue + sep)
    bw.write(doc("U_ERR").asDouble.getValue + sep)
    bw.write(doc("G_MAG").asDouble.getValue + sep)
    bw.write(doc("G_ERR").asDouble.getValue + sep)
    bw.write(doc("R_MAG").asDouble.getValue + sep)
    bw.write(doc("R_ERR").asDouble.getValue + sep)
    bw.write(doc("I_MAG").asDouble.getValue + sep)
    bw.write(doc("I_ERR").asDouble.getValue + sep)
    bw.write(doc("Z_MAG").asDouble.getValue + sep)
    bw.write(doc("Z_ERR").asDouble.getValue + sep)
    bw.write(doc("A_MAG").asDouble.getValue + sep)
    bw.write(doc("A_ERR").asDouble.getValue + sep)
    bw.write(doc("V_MAG").asDouble.getValue + sep)
    bw.write(doc("B_MAG").asDouble.getValue + sep)
    bw.write(doc("PHASE").asDouble.getValue + sep)
    bw.write(doc("H").asDouble.getValue + sep)
    bw.write(doc("G").asDouble.getValue + sep)
  }
  //--------------------------------------------------------------------------
  def writeCsv2MassMoc(doc: Document, bw: BufferedWriter, sep: String = "\t") = {
    bw.write(doc("J_MAG").asDouble.getValue + sep)
    bw.write(doc("J_MAG_SIG").asDouble.getValue + sep)
    bw.write(doc("H_MAG").asDouble.getValue + sep)
    bw.write(doc("H_MAG_SIG").asDouble.getValue + sep)
    bw.write(doc("KS_MAG").asDouble.getValue + sep)
    bw.write(doc("KS_MAG_SIG").asDouble.getValue + sep)
    bw.write(doc("V_MAG").asDouble.getValue + sep)
    bw.write(doc("PHASE").asDouble.getValue + sep)
    bw.write(doc("Q").asDouble.getValue + sep)
  }
  //--------------------------------------------------------------------------
  def writeCsvTessMoc(docSeq: Seq[Document], bw: BufferedWriter, sep: String = "\t") = {
    val r = docSeq map { doc =>
      (doc("tessMag").asDouble.getValue
        , doc("uncertMag").asDouble.getValue
        , doc("tess_dist").asDouble.getValue
        , doc("sun_dist").asDouble.getValue
        , doc("tess_obj_sun_phase_angle").asDouble.getValue
      )
    }
    bw.write((r map (t=> t._1.toString)).mkString("{",",","}") + sep)
    bw.write((r map (t=> t._2.toString)).mkString("{",",","}") + sep)
    bw.write((r map (t=> t._3.toString)).mkString("{",",","}") + sep)
    bw.write((r map (t=> t._4.toString)).mkString("{",",","}") + sep)
    bw.write((r map (t=> t._5.toString)).mkString("{",",","}") + sep)
  }
  //--------------------------------------------------------------------------
  def writeCsvGaia_2_Moc(docSeq: Seq[Document], bw: BufferedWriter, sep: String = "\t") = {
    val r = docSeq map { doc =>
      (doc("number_mp").asInt32.getValue
        , doc("epoch_utc").asDouble.getValue
        , doc("g_mag").asDouble.getValue
        , doc("g_flux").asDouble.getValue
        , doc("g_flux_error").asDouble.getValue
      )
    }
    bw.write((r map (t=> t._1.toString)).mkString("{",",","}") + sep)
    bw.write((r map (t=> t._2.toString)).mkString("{",",","}") + sep)
    bw.write((r map (t=> t._3.toString)).mkString("{",",","}") + sep)
    bw.write((r map (t=> t._4.toString)).mkString("{",",","}") + sep)
    bw.write((r map (t=> t._5.toString)).mkString("{",",","}") + sep)
  }
  //---------------------------------------------------------------------------
  def reportStats(fixedSize: Int) = {
    info("\t" + MyString.rightPadding(s"Total mpos not matched between $paperName and 'MPC': ", fixedSize) + paperBadMatchMpcCount.get)
    info("\t" + MyString.rightPadding(s"Total mpos not matched between 'MPC' and 'SDSS MOC'    : ", fixedSize) + mpcBadMatchSdssCount.get)
    info("\t" + MyString.rightPadding(s"Total mpos not matched between 'MPC' and '2Mass MOC'   : ", fixedSize) + mpcBadMatch_2MassCount.get)
    info("\t" + MyString.rightPadding(s"Total mpos not matched between 'MPC' and 'Tess MOC'    : ", fixedSize) + mpcBadMatchTessCount.get)
    info("\t" + MyString.rightPadding(s"Total mpos not matched between 'MPC' and 'GaiaDR3 2 MOC'  : ", fixedSize) + mpcBadMatchGaiaCount.get)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file MPO_PaperDB.scala
//=============================================================================

