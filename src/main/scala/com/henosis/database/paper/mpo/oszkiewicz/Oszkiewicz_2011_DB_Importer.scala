/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  06/Sep/2021
 * Time:  19h:08m
 * Description: https://wiki.helsinki.fi/display/PSR/Asteroid+absolute+magnitude+and+slope
 */
//=============================================================================
package com.henosis.database.paper.mpo.oszkiewicz
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
import scala.language.postfixOps
//=============================================================================
//=============================================================================
object Oszkiewicz_2011_DB_Importer extends MyLogger {
  //---------------------------------------------------------------------------
  final val CSV_HEADER =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = "oszkiewicz_2011_" + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'oszkiewicz 2011' paper table 1 File: '$fileName'")
    //-------------------------------------------------------------------------
    def getDoubleOrMinusOne(s: String) = if(s.isEmpty) -1d else s.replace(",",".")toDouble
    //-------------------------------------------------------------------------
    val br = new BufferedReader(new FileReader(new File(fileName)))
    val inputFormat = CSVFormat.DEFAULT
      .withDelimiter(' ')

    //get the doc sequence
    val docSeq = new CSVParser(br, inputFormat).getRecords.asScala map { r =>
      info(s"Parsing mpo: '${r.get(0)}' ")
      Document(
        "designation"                 -> r.get(0)
        , "h_g_h"                     -> getDoubleOrMinusOne(r.get(1))
        , "h_g_g"                     -> getDoubleOrMinusOne(r.get(2))
        , "h_g_h_right_error"         -> getDoubleOrMinusOne(r.get(3))
        , "h_g_h_left_error"          -> getDoubleOrMinusOne(r.get(4))
        , "h_g_g_right_error"         -> getDoubleOrMinusOne(r.get(5))
        , "h_g_g_left_error"          -> getDoubleOrMinusOne(r.get(6))
        , "h_g_rms"                   -> getDoubleOrMinusOne(r.get(7))
        , "h_g_flag"                  -> getDoubleOrMinusOne(r.get(8))
        , "h_g1_g2_h"                 -> getDoubleOrMinusOne(r.get(9))
        , "h_g1_g2_g1"                -> getDoubleOrMinusOne(r.get(10))
        , "h_g1_g2_g2"                -> getDoubleOrMinusOne(r.get(11))
        , "h_g1_g2_h_right_error"     -> getDoubleOrMinusOne(r.get(12))
        , "h_g1_g2_h_left_error"      -> getDoubleOrMinusOne(r.get(13))
        , "h_g1_g2_g1_right_error"    -> getDoubleOrMinusOne(r.get(14))
        , "h_g1_g2_g1_left_error"     -> getDoubleOrMinusOne(r.get(15))
        , "h_g1_g2_g2_right_error"    -> getDoubleOrMinusOne(r.get(16))
        , "h_g1_g2_g2_left_error"     -> getDoubleOrMinusOne(r.get(17))
        , "h_g1_g2_rms"               -> getDoubleOrMinusOne(r.get(18))
        , "h_g1_g2_flag"              -> getDoubleOrMinusOne(r.get(19))
        , "h_g12_h"                   -> getDoubleOrMinusOne(r.get(20))
        , "h_g12_g12"                 -> getDoubleOrMinusOne(r.get(21))
        , "h_g12_h_right_error"       -> getDoubleOrMinusOne(r.get(22))
        , "h_g12_h_left_error"        -> getDoubleOrMinusOne(r.get(23))
        , "h_g12_g12_right_error"     -> getDoubleOrMinusOne(r.get(24))
        , "h_g12_g12_left_error"      -> getDoubleOrMinusOne(r.get(25))
        , "h_g12_rms"                 -> getDoubleOrMinusOne(r.get(26))
        , "h_g12_flag"                -> r.get(27).toInt
      )
    }
    br.close
    coll.insertMany(docSeq.toArray).results()
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Oszkiewicz_2011_DB_Importer.scala
//=============================================================================
