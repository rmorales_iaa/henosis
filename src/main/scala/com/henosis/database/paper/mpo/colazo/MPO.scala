/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:27m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.colazo
//=============================================================================
//=============================================================================
import com.henosis.algoritms.paper.mpo.MPO_Common
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import org.mongodb.scala.Document
//=============================================================================
object MPO {
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }

  //-------------------------------------------------------------------------
  def apply(doc: Document): MPO =  MPO(
    doc("_id").asInt32().getValue
    , doc("H").asDouble().getValue
    , doc("hError").asDouble().getValue
    , doc("G").asDouble().getValue
    , doc("gError").asDouble().getValue
    , doc("obs").asInt32().getValue
    , doc("tax").asString().getValue
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MPO(id : Int
               , H : Double
               , hError: Double
               , G : Double
               , gError : Double
               , obs : Int
               , tax : String
               , name : Seq[String] = Seq()) extends MPO_Common {
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    val seq = ArrayBuffer[String]()
    seq ++= productIterator.mkString(sep).split(sep)
    seq.insert(1, name.mkString("|"))  //fix equivalent designation
    seq.mkString(sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO.scala
//=============================================================================
