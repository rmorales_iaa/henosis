/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  06/Sep/2021
 * Time:  19h:03m
 * Description: https://ui.adsabs.harvard.edu/abs/2015Icar..261...34V/abstract
 * https://www.sciencedirect.com/science/article/pii/S0019103515003516
 */
//=============================================================================
package com.henosis.database.paper.mpo.veres
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
//=============================================================================
//=============================================================================
object Veres_2015_DB_Importer extends MyLogger {
  //---------------------------------------------------------------------------
  final val CSV_HEADER =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = "veres_2015_" + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Veres 2015' paper table 1 File: '$fileName'")
    //-------------------------------------------------------------------------
    def getDoubleOrMinusOne(s: String) = if(s.isEmpty) -1d else s.toDouble
    //-------------------------------------------------------------------------
    val br = new BufferedReader(new FileReader(new File(fileName)))
    val inputFormat = CSVFormat.DEFAULT
      .withHeader()
      .withSkipHeaderRecord()
    //get the doc sequence
    val docSeq = new CSVParser(br, inputFormat).getRecords.asScala map { r =>
      Document(
          "name"         -> r.get(0)
        , "className"    -> r.get(1)
        , "N"            -> getDoubleOrMinusOne(r.get(2))
        , "delta_alpha"  -> getDoubleOrMinusOne(r.get(3))
        , "H_Bowell_i"   -> getDoubleOrMinusOne(r.get(4))
        , "H_Bowell"     -> getDoubleOrMinusOne(r.get(5))
        , "H_Bowell_unc" -> getDoubleOrMinusOne(r.get(6))
        , "H_Bowell_err" -> getDoubleOrMinusOne(r.get(7))
        , "H_Mui_i"      -> getDoubleOrMinusOne(r.get(8))
        , "H_Mui"        -> getDoubleOrMinusOne(r.get(9))
        , "H_Mui_unc"    -> getDoubleOrMinusOne(r.get(10))
        , "H_Mui_err"    -> getDoubleOrMinusOne(r.get(11))
        , "G_Bowell"     -> getDoubleOrMinusOne(r.get(12))
        , "G_Bowell_unc" -> getDoubleOrMinusOne(r.get(13))
        , "G_Bowell_err" -> getDoubleOrMinusOne(r.get(14))
        , "G_Mui"        -> getDoubleOrMinusOne(r.get(15))
        , "G_Mui_unc"    -> getDoubleOrMinusOne(r.get(16))
        , "G_Mui_err"    -> getDoubleOrMinusOne(r.get(17))
      )
    }
    br.close
    coll.insertMany(docSeq.toArray).results()
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Veres_2015_DB_Importer.scala
//=============================================================================
