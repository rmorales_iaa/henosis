/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  17/Mar/2024
 * Time:  12h:15m
 * Description: None
 */
package com.henosis.database.paper.mpo.candal
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import org.mongodb.scala.{Document, MongoCollection}
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
//=============================================================================
object Candal_2019_DB_Importer extends MyLogger {
  //---------------------------------------------------------------------------
  final val CSV_HEADER =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = "candal_2019_" + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Candal 2019' file: '$fileName'")
    //-------------------------------------------------------------------------
    def getDoubleOrMinusOne(s: String) = {
      if (s == "nan") scala.Double.NaN
      else
        if (s == "inf") Double.PositiveInfinity
        else
          if (s == "-inf") Double.NegativeInfinity
          else
            if (s.isEmpty) -1d else s.toDouble
    }
    //-------------------------------------------------------------------------
    val br = new BufferedReader(new FileReader(new File(fileName)))
    val inputFormat = CSVFormat.DEFAULT
      .withCommentMarker('#')
    //get the doc sequence
    val docSeq = new CSVParser(br, inputFormat).getRecords.asScala map { r =>
      val lineSeq = r.get(0)
        .trim
        .replaceAll(" +", " ")
        .replaceAll("\t","")
        .split(" ")
        .filter(!_.isEmpty)
      Document(
        "_id"             -> lineSeq(0)
        , "Hv_Hr"         -> getDoubleOrMinusOne(lineSeq(1))
        , "hError"        -> getDoubleOrMinusOne(lineSeq(2))
        , "delta_beta"    -> getDoubleOrMinusOne(lineSeq(3))
        , "error"         -> getDoubleOrMinusOne(lineSeq(4))
        , "point_V"       -> lineSeq(5).toInt
        , "point_R"       -> lineSeq(6).toInt
        , "delta_mag"     -> getDoubleOrMinusOne(lineSeq(7))
        , "flag"          -> lineSeq(8).toInt
        , "delta_mag_literature"-> getDoubleOrMinusOne(lineSeq(9))
        , "delta_mag_est"   -> getDoubleOrMinusOne(lineSeq(10))
        , "alpha_min"       -> getDoubleOrMinusOne(lineSeq(11))
        , "alpha_max"       -> getDoubleOrMinusOne(lineSeq(12))
      )
    }

    br.close
    coll.insertMany(docSeq.toArray).results()
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Peixhino_2015_DB_Importer.scala
//=============================================================================