/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Sep/2021
 * Time:  17h:19m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.carry
//=============================================================================
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.henosis.algoritms.paper.mpo.carry.Carry_2016
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.mpo.MPO_PaperDB
import com.henosis.database.paper.mpo.carry.MPO.CSV_HEADER
//=============================================================================
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.model.Filters.equal
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object Carry_2016_DB {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[MPO]), DEFAULT_CODEC_REGISTRY )
  //---------------------------------------------------------------------------
  def apply(): Carry_2016_DB =
    Carry_2016_DB(PaperDB.DATABASE_NAME
      , PaperDB.COLLECTION_PAPER_CARRY_2016
      , Carry_2016.paperName
      , Carry_2016.referenceEpoch
      , codecRegistry)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Carry_2016_DB(databaseName: String
                          , collectionName: String
                          , paperName: String
                          , paperReferenceEpoch: String
                          , codecRegistry: CodecRegistry)
  extends MPO_PaperDB {

  //---------------------------------------------------------------------------
  def getDocSeq(nameSeq: Seq[String]) = {
    nameSeq map { name =>
      collection.find(equal("designation", name)).results
    }
  }
  //---------------------------------------------------------------------------
  def saveFile(nameSeq: Seq[String], mpcInfo: Seq[String], outputCsv: String, sep: String = "\t") = {
    val docSeq = getDocSeq(nameSeq)
    val bw = new BufferedWriter(new FileWriter(new File(outputCsv)))
    bw.write("seq" + sep + CSV_HEADER.mkString(sep) + sep + s"carry_2016_mpc_id${sep}carry_2016_mpc_name" + "\n")
    var seqCount: Long = 0
    docSeq.zipWithIndex.foreach { case (seq,i) =>
      seq foreach { doc =>
        seqCount += 1
        val mpo = MPO(doc)
        bw.write(seqCount.toString + sep)
        bw.write(mpo.getAsCsvRow(sep) + sep)
        bw.write(mpcInfo(i) + sep)
        bw.write("\n")
      }
    }
    bw.flush()
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Carry_2016.scala
//=============================================================================
