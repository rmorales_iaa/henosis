/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:27m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.popescu
//=============================================================================
import com.henosis.algoritms.paper.mpo.MPO_Common
import org.mongodb.scala.Document
//=============================================================================
import scala.collection.mutable.ArrayBuffer

//=============================================================================
object MPO {
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //-------------------------------------------------------------------------
  def apply(doc: Document): MPO =  MPO(
    doc("_id").asString().getValue
    , doc("equivalentDesignation").asString().getValue.split('|')
    , doc("className").asString().getValue
    , doc("ymJ").asDouble().getValue
    , doc("ymJerr").asDouble().getValue
    , doc("ymJt_min").asDouble().getValue
    , doc("jmK").asDouble().getValue
    , doc("jmKerr").asDouble().getValue
    , doc("jmKt_min").asDouble().getValue
    , doc("hmK").asDouble().getValue
    , doc("hmKerr").asDouble().getValue
    , doc("hmKt_min").asDouble().getValue
    , doc("taxClassKNN").asString().getValue
    , doc("knnMCProb").asDouble().getValue
    , doc("taxClassProb").asString().getValue
    , doc("probProb").asDouble().getValue
    , doc("taxClassFin").asString().getValue
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MPO(id : String
               , name : Seq[String]
               , clasName: String
               , ymj : Double
               , ymJerr : Double
               , ymJt_min : Double
               , jmK : Double
               , jmKerr : Double
               , jmKt_min: Double
               , hmK: Double
               , hmKerr: Double
               , hmKt_min: Double
               , taxClassKNN: String
               , knnMCProb: Double
               , taxClassProb: String
               , probProb: Double
               , taxClassFin: String) extends MPO_Common {
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    val seq = ArrayBuffer[String]()
    seq ++= productIterator.mkString(sep).split(sep)
    seq.insert(1, name.mkString("|"))  //fix equivalent designation
    seq.mkString(sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO.scala
//=============================================================================
