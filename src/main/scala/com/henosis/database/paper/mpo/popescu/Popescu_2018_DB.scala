/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Sep/2021
 * Time:  20h:26m
 * Description: https://github.com/marcelpopescu/MOVIS-Taxonomy/blob/master/Files/MOVIS-CTax.csv
 * https://www.sciencedirect.com/science/article/abs/pii/S003206331730452X
 */
//=============================================================================
package com.henosis.database.paper.mpo.popescu
//=============================================================================
import com.henosis.algoritms.paper.mpo.popescu.Popescu_2018
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.mpo.MPO_PaperDB
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
//=============================================================================
//=============================================================================
object Popescu_2018_DB {
  //---------------------------------------------------------------------------
  //http://mongodb.github.io/mongo-scala-driver/2.3/getting-started/quick-tour-case-classes/
  //https://jannikarndt.de/blog/2017/08/writing_case_classes_to_mongodb_in_scala/
  val codecRegistry = fromRegistries(fromProviders(classOf[MPO]), DEFAULT_CODEC_REGISTRY )
  //---------------------------------------------------------------------------
  def apply(): Popescu_2018_DB =
    Popescu_2018_DB(PaperDB.DATABASE_NAME
                    , PaperDB.COLLECTION_PAPER_POPESCU_2018
                    , Popescu_2018.paperName
                    , Popescu_2018.referenceEpoch
                    , codecRegistry)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Popescu_2018_DB( databaseName: String
                           , collectionName: String
                           , paperName: String
                           , paperReferenceEpoch: String
                           , codecRegistry: CodecRegistry)
  extends MPO_PaperDB
//=============================================================================
//End of file Popescu_2018_DB.scala
//=============================================================================
