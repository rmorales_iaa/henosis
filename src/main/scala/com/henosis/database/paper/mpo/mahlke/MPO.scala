/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:27m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.mahlke
//=============================================================================
//=============================================================================
import com.henosis.algoritms.paper.mpo.MPO_Common
//=============================================================================
import scala.collection.mutable.ArrayBuffer
import org.mongodb.scala.Document
//=============================================================================
object MPO {
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //-------------------------------------------------------------------------
  def apply(doc: Document): MPO =  MPO(
    doc("number").asString().getValue
    , Seq(doc("name").asString().getValue)
    , doc("band").asString().getValue
    , doc("className").asString().getValue
    , doc("scheme").asString().getValue
    , doc("ref_tax").asString().getValue
    , doc("ap").asDouble().getValue
    , doc("ep").asDouble().getValue
    , doc("ip").asDouble().getValue
    , doc("N").asInt32().getValue
    , doc("phMin").asDouble().getValue
    , doc("phMax").asDouble().getValue
    , doc("h").asDouble().getValue
    , doc("g1").asDouble().getValue
    , doc("g2").asDouble().getValue
    , doc("rms").asDouble().getValue
    , doc("h12").asDouble().getValue
    , doc("g12").asDouble().getValue
    , doc("rms12").asDouble().getValue
    , doc("h_up").asDouble().getValue
    , doc("h_low").asDouble().getValue
    , doc("g1_up").asDouble().getValue
    , doc("g1_low").asDouble().getValue
    , doc("g2_up").asDouble().getValue
    , doc("g2_low").asDouble().getValue
    , doc("h12_up").asDouble().getValue
    , doc("h12_low").asDouble().getValue
    , doc("g12_up").asDouble().getValue
    , doc("g12_loq").asDouble().getValue
    , doc("albedo").asDouble().getValue
    , doc("err_albedo").asDouble().getValue
    , doc("ref_albedo").asString().getValue
    , doc("family_number").asString().getValue
    , doc("family_name").asString().getValue
    , doc("family_status").asString().getValue
  )
  //---------------------------------------------------------------------------
}

//=============================================================================
case class MPO(number: String
               , name: Seq[String]
               , band: String
               , className: String
               , scheme: String
               , ref_tax: String
               , ap: Double
               , ep: Double
               , ip: Double
               , N: Int
               , phMin: Double
               , phMax: Double
               , h: Double
               , g1: Double
               , g2: Double
               , rms: Double
               , h12: Double
               , g12: Double
               , rms12: Double
               , h_up: Double
               , h_low: Double
               , g1_up: Double
               , g1_low: Double
               , g2_up: Double
               , g2_low: Double
               , h12_up: Double
               , h12_low: Double
               , g12_up: Double
               , g12_loq: Double
               , albedo: Double
               , err_albedo: Double
               , ref_albedo: String
               , family_number: String
               , family_name: String
               , family_status: String) extends MPO_Common {
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    val seq = ArrayBuffer[String]()
    seq ++= productIterator.mkString(sep).split(sep)
    seq.insert(1, name.mkString("|")) //fix equivalent designation
    seq.mkString(sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO.scala
//=============================================================================
