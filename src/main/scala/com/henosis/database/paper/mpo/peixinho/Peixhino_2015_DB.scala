/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Sep/2019
 * Time:  11h:16m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.peixinho
//=============================================================================
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.mpo.MPO_PaperDB
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
//=============================================================================
//=============================================================================
object Peixhino_2015_DB {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[MPO]), DEFAULT_CODEC_REGISTRY )
  //---------------------------------------------------------------------------
  def apply(): Peixhino_2015_DB =
    Peixhino_2015_DB(PaperDB.DATABASE_NAME
      , PaperDB.COLLECTION_PAPER_PEIXHINO_2015
      , MPO.paperName
      , MPO.referenceEpoch
      , codecRegistry)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Peixhino_2015_DB(databaseName: String
                            , collectionName: String
                            , paperName: String
                            , paperReferenceEpoch: String
                            , codecRegistry: CodecRegistry)
  extends MPO_PaperDB
//=============================================================================
//End of file Peixhino_2015_DB.scala
//=============================================================================
