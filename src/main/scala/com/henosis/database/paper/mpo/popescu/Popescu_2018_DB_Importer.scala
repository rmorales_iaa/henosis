/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Sep/2021
 * Time:  20h:27m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.popescu
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
//=============================================================================
object Popescu_2018_DB_Importer extends MyLogger {
  //---------------------------------------------------------------------------
  final val CSV_HEADER =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = "popescu_2018_" + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Popescu 2018' paper table 1 File: '$fileName'")
    //-------------------------------------------------------------------------
    def getDoubleOrMinusOne(s: String) = if(s.isEmpty) -1d else s.toDouble
    //-------------------------------------------------------------------------
    val br = new BufferedReader(new FileReader(new File(fileName)))
    val inputFormat = CSVFormat.DEFAULT
      .withHeader()
      .withSkipHeaderRecord()
    //get the doc sequence
    val docSeq = new CSVParser(br, inputFormat).getRecords.asScala map { r =>
      Document(
        "_id" -> r.get(0)
        , "equivalentDesignation" -> r.get(1)
        , "className"             -> r.get(2)
        , "ymJ"                   -> getDoubleOrMinusOne(r.get(3))
        , "ymJerr"                -> getDoubleOrMinusOne(r.get(4))
        , "ymJt_min"              -> getDoubleOrMinusOne(r.get(5))
        , "jmK"                   -> getDoubleOrMinusOne(r.get(6))
        , "jmKerr"                -> getDoubleOrMinusOne(r.get(7))
        , "jmKt_min"              -> getDoubleOrMinusOne(r.get(8))
        , "hmK"                   -> getDoubleOrMinusOne(r.get(9))
        , "hmKerr"                -> getDoubleOrMinusOne(r.get(10))
        , "hmKt_min"              -> getDoubleOrMinusOne(r.get(11))
        , "taxClassKNN"           -> r.get(12)
        , "knnMCProb"             -> getDoubleOrMinusOne(r.get(13))
        , "taxClassProb"          -> r.get(14)
        , "probProb"              -> getDoubleOrMinusOne(r.get(15))
        , "taxClassFin"           -> r.get(16)
      )
    }
    br.close
    coll.insertMany(docSeq.toArray).results()
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Popescu_2018_DB_Importer.scala
//=============================================================================
