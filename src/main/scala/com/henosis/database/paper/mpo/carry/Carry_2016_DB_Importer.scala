/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Sep/2021
 * Time:  17h:22m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.carry
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
//=============================================================================
//=============================================================================
object Carry_2016_DB_Importer  extends MyLogger {
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Carry 2016' paper table 1 File: '$fileName'")
    //-------------------------------------------------------------------------
    def getDoubleOrMinusOne(s: String) = {
      if (s == "nan") scala.Double.NaN
      else
        if (s == "inf") Double.PositiveInfinity
        else
          if (s == "-inf") Double.NegativeInfinity
          else
            if (s.isEmpty) -1d else s.toDouble
    }
    //-------------------------------------------------------------------------
    val br = new BufferedReader(new FileReader(new File(fileName)))
    val inputFormat = CSVFormat.DEFAULT
      .withHeader()
      .withSkipHeaderRecord()
    //get the doc sequence
    var lineCount = 0
    val docSeq = new CSVParser(br, inputFormat).getRecords.asScala map { r =>
      lineCount += 1
      info(s"Processing line: $lineCount")

      Document(
          "ra"             -> r.get(0).toDouble
        , "dec"            -> r.get(1).toDouble
        , "dis"            -> r.get(2).toDouble
        , "moID"           -> r.get(3)
        , "run"            -> r.get(4).toInt
        , "col"            -> r.get(5).toInt
        , "field"          -> r.get(6).toInt
        , "object"         -> r.get(7).toInt
        , "rowc"           -> r.get(8).toDouble
        , "colc"           -> r.get(9).toDouble
        , "mjd"            -> r.get(10).toDouble
        , "lambda"         -> r.get(11).toDouble
        , "beta"           -> r.get(12).toDouble
        , "phi"            -> r.get(13).toDouble
        , "vMu"            -> r.get(14).toDouble
        , "vMu_error"      -> r.get(15).toDouble
        , "vNu"            -> r.get(16).toDouble
        , "vNu_error"      -> r.get(17).toDouble
        , "vLambda"        -> r.get(18).toDouble
        , "vBeta"          -> r.get(19).toDouble
        , "u"              -> r.get(20).toDouble
        , "uErr"           -> r.get(21).toDouble
        , "g"              -> r.get(22).toDouble
        , "gErr"           -> r.get(23).toDouble
        , "r"              -> r.get(24).toDouble
        , "rErr"           -> r.get(25).toDouble
        , "i"              -> r.get(26).toDouble
        , "iErr"           -> r.get(27).toDouble
        , "z"              -> r.get(28).toDouble
        , "zErr"           -> r.get(29).toDouble
        , "a"              -> r.get(30).toDouble
        , "aErr"           -> r.get(31).toDouble
        , "V"              -> r.get(32).toDouble
        , "B"              -> r.get(33).toDouble
        , "numeration"     -> r.get(34).toInt
        , "designation"    -> r.get(35)
        , "source"         -> r.get(36)
        , "detection_counter"        -> r.get(37).toInt
        , "total_detection_count"    -> r.get(38).toInt
        , "flags"          -> r.get(39).toInt
        , "computed_ra"    -> r.get(40).toDouble
        , "computed_dec"   -> r.get(41).toDouble
        , "computed_app_mag"   -> r.get(42).toDouble
        , "r"              -> r.get(43).toDouble
        , "geocentric"     -> r.get(44).toDouble
        , "phase"          -> r.get(45).toDouble
        , "catalogID"      -> r.get(46)
        , "H"              -> r.get(47).toDouble
        , "G"              -> r.get(48).toDouble
        , "arc"            -> r.get(49).toDouble
        , "epoch"          -> r.get(50).toDouble
        , "a"              -> r.get(51).toDouble
        , "e"              -> r.get(52).toDouble
        , "i"              -> r.get(53).toDouble
        , "lon_asc_node"     -> r.get(54).toDouble
        , "arg_of_perihelion" -> r.get(55).toDouble
        , "M"          -> r.get(56).toDouble
        , "proper_elements_catalog_ID"         -> r.get(57)
        , "a_"         -> r.get(58).toDouble
        , "e_"         -> r.get(59).toDouble
        , "sin(i_)"      -> r.get(60).toDouble
        , "flag_1"         -> (r.get(61) == "1")
        , "flag_2"         -> (r.get(62) == "1")
        , "flag_3"         -> (r.get(63) == "1")
        , "flag_4"         -> (r.get(64) == "1")
        , "flag_5"         -> (r.get(65) == "1")
        , "flag_6"         -> (r.get(66) == "1")
        , "flag_7"         -> (r.get(67) == "1")
        , "flag_8"         -> (r.get(68) == "1")
        , "flag_9"         -> (r.get(69) == "1")
        , "flag_10"        -> (r.get(70) == "1")
        , "flag_11"        -> (r.get(71) == "1")
        , "flag_12"        -> (r.get(72) == "1")
        , "flag_13"        -> (r.get(73) == "1")
        , "flag_14"        -> (r.get(74) == "1")
        , "flag_15"        -> (r.get(75) == "1")
        , "flag_16"        -> (r.get(76) == "1")
        , "flag_17"        -> (r.get(77) == "1")
        , "flag_18"        -> (r.get(78) == "1")
        , "flag_19"        -> (r.get(79) == "1")
        , "flag_20"        -> (r.get(80) == "1")
        , "flag_21"        -> (r.get(81) == "1")
        , "flag_22"        -> (r.get(82) == "1")
        , "flag_23"        -> (r.get(83) == "1")
        , "flag_24"        -> (r.get(84) == "1")
        , "flag_25"        -> (r.get(85) == "1")
        , "flag_26"        -> (r.get(86) == "1")
        , "flag_27"        -> (r.get(87) == "1")
        , "flag_28"        -> (r.get(88) == "1")
        , "flag_29"        -> (r.get(89) == "1")
        , "flag_30"        -> (r.get(90) == "1")
        , "flag_31"        -> (r.get(91) == "1")
        , "flag_32"        -> (r.get(92) == "1")
        , "flag_33"        -> (r.get(93) == "1")
        , "flag_34"        -> (r.get(94) == "1")
        , "flag_35"        -> (r.get(95) == "1")
        , "flag_36"        -> (r.get(96) == "1")
        , "flag_37"        -> (r.get(97) == "1")
        , "flag_38"        -> (r.get(98) == "1")
        , "flag_39"        -> (r.get(99) == "1")
        , "flag_40"        -> (r.get(100) == "1")
        , "flag_41"        -> (r.get(101) == "1")
        , "flag_42"        -> (r.get(102) == "1")
        , "flag_43"        -> (r.get(103) == "1")
        , "flag_44"        -> (r.get(104) == "1")
        , "flag_45"        -> (r.get(105) == "1")
        , "flag_46"        -> (r.get(106) == "1")
        , "flag_47"        -> (r.get(107) == "1")
        , "flag_48"        -> (r.get(108) == "1")
        , "flag_49"        -> (r.get(109) == "1")
        , "flag_50"        -> (r.get(110) == "1")
        , "flag_51"        -> (r.get(111) == "1")
        , "flag_52"        -> (r.get(112) == "1")
        , "flag_53"        -> (r.get(113) == "1")
        , "flag_54"        -> (r.get(114) == "1")
        , "flag_55"        -> (r.get(115) == "1")
        , "flag_56"        -> (r.get(116) == "1")
        , "flag_57"        -> (r.get(117) == "1")
        , "flag_58"        -> (r.get(118) == "1")
        , "flag_59"        -> (r.get(119) == "1")
        , "flag_60"        -> (r.get(120) == "1")
        , "flag_61"        -> (r.get(121) == "1")
        , "flag_62"        -> (r.get(122) == "1")
        , "flag_63"        -> (r.get(123) == "1")
        , "flag_64"        -> (r.get(124) == "1")
      )
    }
    br.close
    coll.insertMany(docSeq.toArray).results()
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, coll)
    db.createIndex("designation", _unique = false)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Carry_2016_DB_Importer.scala
//=============================================================================
