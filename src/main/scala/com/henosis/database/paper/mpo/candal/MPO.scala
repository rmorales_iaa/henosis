/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:27m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.candal
//=============================================================================
import com.henosis.algoritms.paper.mpo.MPO_Common
//=============================================================================
import org.mongodb.scala.Document
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object MPO {
   //---------------------------------------------------------------------------
  val paperName = "'Candal_2019'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }

  //-------------------------------------------------------------------------
  def apply(doc: Document): MPO =  MPO(
      doc("_id").asString().getValue
    , doc("Hv_Hr").asDouble().getValue
    , doc("hError").asDouble().getValue
    , doc("delta_beta").asDouble().getValue
    , doc("error").asDouble().getValue
    , doc("point_V").asInt32().getValue
    , doc("point_R").asInt32().getValue
    , doc("delta_mag").asDouble().getValue
    , doc("flag").asInt32().getValue
    , doc("delta_mag_literature").asDouble().getValue
    , doc("delta_mag_est").asDouble().getValue
    , doc("alpha_min").asDouble().getValue
    , doc("alpha_max").asDouble().getValue
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MPO(id : String           //1
               , Hv_Hr : Double      //2
               , hError: Double      //3
               , delta_beta : Double //4
               , error : Double      //5
               , point_V : Int       //6
               , point_R : Int       //7
               , delta_mag: Double   //8
               , flag: Int           //9
               , delta_mag_literature: Double
               , delta_mag_est: Double
               , alpha_min: Double   //10
               , alpha_max: Double   //11
               , name : Seq[String] = Seq()) extends MPO_Common {
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    val seq = ArrayBuffer[String]()
    seq ++= productIterator.mkString(sep).split(sep)
    seq.insert(1, name.mkString("|"))  //fix equivalent designation
    seq.mkString(sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO.scala
//=============================================================================
