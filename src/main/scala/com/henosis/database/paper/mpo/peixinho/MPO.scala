/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:27m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.peixinho

//=============================================================================
import com.henosis.algoritms.paper.mpo.MPO_Common
//=============================================================================
import org.mongodb.scala.Document

import scala.collection.mutable.ArrayBuffer
//=============================================================================
object MPO {
   //---------------------------------------------------------------------------
  val paperName = "'Peixinho_2015'"
  val referenceEpoch = "2000-01-01T00:00:00"
  //---------------------------------------------------------------------------
  final val referenceSeq = Seq(
      "Luu & Jewitt (1996)"
    , "Boehnhardt et al. (2001)"
    , "Tegler & Romanishin (2000)"
    , "Gil-Hutton & Licandro (2001)"
    , "Boehnhardt et al. (2002)"
    , "Trujillo & Brown (2002)"
    , "Jewitt & Luu (2001)"
    , "Benecchi et al. (2011)"
    , "Green et al. (1997)"
    , "Barucci et al. (2000)"
    , "Doressoundiram et al. (2002)"
    , "Jewitt & Luu (1998)"
    , "Tegler & Romanishin (2003)"
    , "Delsanti et al. (2001)"
    , "Santos-Sanz et al. (2009)"
    , "Snodgrass et al. (2010)"
    , "Doressoundiram et al. (2001)"
    , "Sheppard (2012)"
    , "Peixinho et al. (2004)"
    , "Tegler et al. (2003)"
    , "Gulbis et al. (2006)"
    , "Doressoundiram et al. (2005b)"
    , "Doressoundiram et al. (2007)"
    , "Sheppard (2010)"
    , "Tegler et al. http://www.physics.nau.edu/∼tegler/research/survey.htm"
    , "Fornasier et al. (2004)"
    , "Perna et al. (2010)"
    , "Rabinowitz et al. (2008)"
    , "Romanishin et al. (2010)"
    , "Lazzaro et al. (1997)"
    , "Bauer et al. (2003)"
    , "Romon-Martin et al.(2003)"
    , "Romanishin et al. (1997)"
    , "Romon-Martin et al. (2002)"
    , "Rabinowitz et al. (2007)"
    , "Peixinho et al. (2001)"
    , "Tegler & Romanishin (1997)"
    , "Barucci et al. (1999)"
    , "Hainaut et al. (2000)"
    , "Boehnhardt et al. (2004)"
    , "Barucci et al. (2002)"
    , "Ferrin et al. (2001)"
    , "Alvarez-Candal et al.(2010)"
    , "Dotto et al. (2003)"
    , "DeMeo et al. (2010)"
    , "Doressoundiram et al. (2005a)"
    , "de Bergh et al. (2005)"
    , "Rabinowitz et al. (2006)"
    , "Benecchi et al. (2009)"
    , "Peixinho et al. (2012)"
  )
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //-------------------------------------------------------------------------
  def apply(doc: Document): MPO =  MPO(
      doc("_id").asString().getValue
    , doc("mpcID").asInt32().getValue
    , doc("className").asString().getValue
    , doc("Hr").asDouble().getValue
    , doc("Hr_error").asDouble().getValue
    , doc("Grt").asDouble().getValue
    , doc("Grt_error").asDouble().getValue
    , doc("B_V").asDouble().getValue
    , doc("B_V_error").asDouble().getValue
    , doc("V_R").asDouble().getValue
    , doc("V_R_error").asDouble().getValue
    , doc("R_I").asDouble().getValue
    , doc("R_I_error").asDouble().getValue
    , doc("V_I").asDouble().getValue
    , doc("V_I_error").asDouble().getValue
    , doc("B_I").asDouble().getValue
    , doc("B_I_error").asDouble().getValue
    , doc("B_R").asDouble().getValue
    , doc("B_R_error").asDouble().getValue
    , doc("q").asDouble().getValue
    , doc("Q").asDouble().getValue
    , doc("i").asDouble().getValue
    , doc("e").asDouble().getValue
    , doc("a").asDouble().getValue
    , doc("vc").asDouble().getValue
    , doc("psi").asDouble().getValue
    , doc("epsilon").asDouble().getValue
    , doc("Tn").asDouble().getValue
    , doc("Tj").asDouble().getValue
    , doc("references").asString().getValue
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MPO(id : String          //1
               , mpcID : Int        //2
               , className: String  //3
               , Hr: Double         //4
               , Hr_error: Double
               , Grt: Double        //5
               , Grt_error: Double
               , B_V: Double        //6
               , B_V_error: Double
               , V_R: Double        //7
               , V_R_error:Double
               , R_I: Double        //8
               , R_I_error: Double
               , V_I: Double        //9
               , V_I_error: Double
               , B_I: Double        //10
               , B_I_error: Double
               , B_R: Double        //11
               , B_R_error: Double
               , q: Double          //12
               , Q: Double          //13
               , i: Double          //14
               , e: Double          //15
               , a: Double          //16
               , vc: Double         //17  //collisional velocity
               , psi: Double        //18  //object encounter velocity
               , epsilon: Double    //19  //orbital excitation
               , Tn: Double         //20  //Tisserand parameter relative to Neptune
               , Tj: Double         //21  //Tisserand parameter relative to Jupiter
               , references: String //22
               , name : Seq[String] = Seq()) extends MPO_Common {
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    val seq = ArrayBuffer[String]()
    seq ++= productIterator.mkString(sep).split(sep)
    seq.insert(1, name.mkString("|"))  //fix equivalent designation
    seq.mkString(sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO.scala
//=============================================================================
