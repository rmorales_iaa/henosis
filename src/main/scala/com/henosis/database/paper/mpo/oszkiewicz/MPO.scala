/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:27m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.oszkiewicz
//=============================================================================
import com.henosis.algoritms.paper.mpo.MPO_Common
import org.mongodb.scala.Document
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object MPO {
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def apply(doc: Document): MPO =  MPO(
    Seq(doc("designation").asString().getValue)
    , doc("h_g_h").asDouble().getValue
    , doc("h_g_g").asDouble().getValue
    , doc("h_g_h_right_error").asDouble().getValue
    , doc("h_g_h_left_error").asDouble().getValue
    , doc("h_g_g_right_error").asDouble().getValue
    , doc("h_g_g_left_error").asDouble().getValue
    , doc("h_g_rms").asDouble().getValue
    , doc("h_g_flag").asDouble().getValue
    , doc("h_g1_g2_h").asDouble().getValue
    , doc("h_g1_g2_g1").asDouble().getValue
    , doc("h_g1_g2_g2").asDouble().getValue
    , doc("h_g1_g2_h_right_error").asDouble().getValue
    , doc("h_g1_g2_h_left_error").asDouble().getValue
    , doc("h_g1_g2_g1_right_error").asDouble().getValue
    , doc("h_g1_g2_g1_left_error").asDouble().getValue
    , doc("h_g1_g2_g2_right_error").asDouble().getValue
    , doc("h_g1_g2_g2_left_error").asDouble().getValue
    , doc("h_g1_g2_rms").asDouble().getValue
    , doc("h_g1_g2_flag").asDouble().getValue
    , doc("h_g12_h").asDouble().getValue
    , doc("h_g12_g12").asDouble().getValue
    , doc("h_g12_h_right_error").asDouble().getValue
    , doc("h_g12_h_left_error").asDouble().getValue
    , doc("h_g12_g12_right_error").asDouble().getValue
    , doc("h_g12_g12_left_error").asDouble().getValue
    , doc("h_g12_rms").asDouble().getValue
    , doc("h_g12_flag").asDouble().getValue
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MPO(name: Seq[String]
               , h_g_h: Double
               , h_g_g: Double
               , h_g_h_right_error: Double
               , h_g_h_left_error: Double
               , h_g_g_right_error: Double
               , h_g_g_left_error: Double
               , h_g_rms: Double
               , h_g_flag: Double
               , h_g1_g2_h: Double
               , h_g1_g2_g1: Double
               , h_g1_g2_g2: Double
               , h_g1_g2_h_right_error: Double
               , h_g1_g2_h_left_error: Double
               , h_g1_g2_g1_right_error: Double
               , h_g1_g2_g1_left_error: Double
               , h_g1_g2_g2_right_error: Double
               , h_g1_g2_g2_left_error: Double
               , h_g1_g2_rms: Double
               , h_g1_g2_flag: Double
               , h_g12_h: Double
               , h_g12_g12: Double
               , h_g12_h_right_error: Double
               , h_g12_h_left_error: Double
               , h_g12_g12_right_error: Double
               , h_g12_g12_left_error: Double
               , h_g12_rms: Double
               , h_g12_flag: Double) extends MPO_Common {
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    val seq = ArrayBuffer[String]()
    seq ++= productIterator.mkString(sep).split(sep)
    seq.insert(1, name.mkString("|"))  //fix equivalent designation
    seq.mkString(sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO.scala
//=============================================================================
