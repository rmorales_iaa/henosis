/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Sep/2021
 * Time:  11h:16m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.colazo
//=============================================================================
import com.henosis.algoritms.paper.mpo.colazo.Colazo_2021
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.mpo.MPO_PaperDB
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
//=============================================================================
//=============================================================================
object Colazo_2021_DB {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[MPO]), DEFAULT_CODEC_REGISTRY )
  //---------------------------------------------------------------------------
  def apply(): Colazo_2021_DB =
    Colazo_2021_DB(PaperDB.DATABASE_NAME
      , PaperDB.COLLECTION_PAPER_COLAZO_2021
      , Colazo_2021.paperName
      , Colazo_2021.referenceEpoch
      , codecRegistry)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Colazo_2021_DB(databaseName: String
                            , collectionName: String
                            , paperName: String
                            , paperReferenceEpoch: String
                            , codecRegistry: CodecRegistry)
  extends MPO_PaperDB
//=============================================================================
//End of file Peixhino_2015_DB.scala
//=============================================================================
