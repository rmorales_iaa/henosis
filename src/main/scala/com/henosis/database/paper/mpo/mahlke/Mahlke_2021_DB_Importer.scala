/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  06/Sep/2021
 * Time:  19h:08m
 * Description:  https://arxiv.org/abs/2009.05129
 *               http://cdsarc.u-strasbg.fr/viz-bin/cat/J/A+A/649/A98
 */
//=============================================================================
package com.henosis.database.paper.mpo.mahlke
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import scala.io.Source
//=============================================================================
//=============================================================================
object Mahlke_2021_DB_Importer extends MyLogger {
  //---------------------------------------------------------------------------
  final val CSV_HEADER =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = "mahlke_2021_" + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Mahlke 2021' paper table 1 File: '$fileName'")
    //-------------------------------------------------------------------------
    def getDoubleOrMinusOne(s: String) = {
      if(s.isEmpty || s.startsWith("--")) -1d
      else s.toDouble
    }
    //-------------------------------------------------------------------------
    //get the doc sequence
    val bufferedSource = Source.fromFile(fileName)
    var i = 0
    val docSeq = bufferedSource.getLines().toArray.map { line=>
      i += 1
      val r = line.trim.replaceAll(" +", " ").split(" ")
      info(s"Parsing mpo $i: name: '${r(0)}' with: ${r.length} tokens")

      var o = 0
      val name =
        if (r.length == 36) {
        o = 1
        r(1) + " " + r(2)
      }
      else r(2)

      Document(
        "number"             -> r(0)
        , "name"             -> name
        , "band"             -> r(2 + o)
        , "className"        -> r(3 + o)
        , "scheme"           -> r(4 + o)
        , "ref_tax"          -> r(5 + o)
        , "ap"               -> getDoubleOrMinusOne(r(6 + o))
        , "ep"               -> getDoubleOrMinusOne(r(7 + o))
        , "ip"               -> getDoubleOrMinusOne(r(8 + o))
        , "N"                -> r(9 + o).toInt
        , "phMin"            -> getDoubleOrMinusOne(r(11 + o))
        , "phMax"            -> getDoubleOrMinusOne(r(11 + o))
        , "h"                -> getDoubleOrMinusOne(r(12 + o))
        , "g1"               -> getDoubleOrMinusOne(r(13 + o))
        , "g2"               -> getDoubleOrMinusOne(r(14 + o))
        , "rms"              -> getDoubleOrMinusOne(r(15 + o))
        , "h12"              -> getDoubleOrMinusOne(r(16 + o))
        , "g12"              -> getDoubleOrMinusOne(r(17 + o))
        , "rms12"            -> getDoubleOrMinusOne(r(18 + o))
        , "h_up"             -> getDoubleOrMinusOne(r(19 + o))
        , "h_low"            -> getDoubleOrMinusOne(r(20 + o))
        , "g1_up"            -> getDoubleOrMinusOne(r(21 + o))
        , "g1_low"           -> getDoubleOrMinusOne(r(22 + o))
        , "g2_up"            -> getDoubleOrMinusOne(r(23 + o))
        , "g2_low"           -> getDoubleOrMinusOne(r(24 + o))
        , "h12_up"           -> getDoubleOrMinusOne(r(25 + o))
        , "h12_low"          -> getDoubleOrMinusOne(r(26 + o))
        , "g12_up"           -> getDoubleOrMinusOne(r(27 + o))
        , "g12_low"          -> getDoubleOrMinusOne(r(28 + o))
        , "albedo"           -> getDoubleOrMinusOne(r(29 + o))
        , "err_albedo"       -> getDoubleOrMinusOne(r(30) + o)
        , "ref_albedo"       -> r(31 + o)
        , "family_number"    -> r(32 + o)
        , "family_name"      -> r(33 + o)
        , "family_status"    -> r(34 + o)
      )
    }
    bufferedSource.close
    coll.insertMany(docSeq.toSeq).results()
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Mahlke_2021_DB_Importer.scala
//=============================================================================
