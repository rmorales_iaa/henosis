/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  06/Sep/2021
 * Time:  19h:08m
 * Description: https://arxiv.org/abs/2103.06850
 */
//=============================================================================
package com.henosis.database.paper.mpo.colazo
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
//=============================================================================
//=============================================================================
object Colazo_2021_DB_Importer extends MyLogger {
  //---------------------------------------------------------------------------
  final val CSV_HEADER =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = "colazo_2021_" + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Colazo 2021' paper table 1 File: '$fileName'")
    //-------------------------------------------------------------------------
    def getDoubleOrMinusOne(s: String) = {
      if (s == "nan") scala.Double.NaN
      else
        if (s == "inf") Double.PositiveInfinity
        else
          if (s == "-inf") Double.NegativeInfinity
          else
            if (s.isEmpty) -1d else s.toDouble
    }
    //-------------------------------------------------------------------------
    val br = new BufferedReader(new FileReader(new File(fileName)))
    val inputFormat = CSVFormat.DEFAULT
      .withHeader()
      .withSkipHeaderRecord()
    //get the doc sequence
    val docSeq = new CSVParser(br, inputFormat).getRecords.asScala map { r =>
      Document(
        "_id" -> r.get(0).toInt
        , "H"               -> getDoubleOrMinusOne(r.get(1))
        , "hError"          -> getDoubleOrMinusOne(r.get(2))
        , "G"               -> getDoubleOrMinusOne(r.get(3))
        , "gError"          -> getDoubleOrMinusOne(r.get(4))
        , "obs"             -> r.get(5).toInt
        , "tax"             -> r.get(6)
      )
    }
    br.close
    coll.insertMany(docSeq.toArray).results()
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, coll)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Colazo_2021_DB_Importer.scala
//=============================================================================
