/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Sep/2021
 * Time:  11h:17m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.veres
//=============================================================================
import com.henosis.algoritms.paper.mpo.veres.Veres_2015
import com.henosis.database.MPO
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.mpo.MPO_PaperDB
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
//=============================================================================
//=============================================================================
object Veres_2015_DB {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[MPO]), DEFAULT_CODEC_REGISTRY )
  //---------------------------------------------------------------------------
  def apply(): Veres_2015_DB =
    Veres_2015_DB(PaperDB.DATABASE_NAME
      , PaperDB.COLLECTION_PAPER_VERES_2015
      , Veres_2015.paperName
      , Veres_2015.referenceEpoch
      , codecRegistry)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Veres_2015_DB(databaseName: String
                            , collectionName: String
                            , paperName: String
                            , paperReferenceEpoch: String
                            , codecRegistry: CodecRegistry)
  extends MPO_PaperDB
//=============================================================================
//End of file Veres_2015_DB.scala
//=============================================================================
