/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Sep/2021
 * Time:  21h:37m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.veres
//=============================================================================
import com.henosis.algoritms.paper.mpo.MPO_Common
//=============================================================================
import org.mongodb.scala.Document
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object MPO  {
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //-------------------------------------------------------------------------
  def apply(doc: Document) : MPO =  MPO(
    Seq(doc("name").asString().getValue)
    , doc("className").asString().getValue
    , doc("N").asDouble().getValue
    , doc("delta_alpha").asDouble().getValue
    , doc("H_Bowell_i").asDouble().getValue
    , doc("H_Bowell").asDouble().getValue
    , doc("H_Bowell_unc").asDouble().getValue
    , doc("H_Bowell_err").asDouble().getValue
    , doc("H_Mui_i").asDouble().getValue
    , doc("H_Mui").asDouble().getValue
    , doc("H_Mui_unc").asDouble().getValue
    , doc("H_Mui_err").asDouble().getValue
    , doc("G_Bowell").asDouble().getValue
    , doc("G_Bowell_unc").asDouble().getValue
    , doc("G_Bowell_err").asDouble().getValue
    , doc("G_Mui").asDouble().getValue
    , doc("G_Mui_unc").asDouble().getValue
    , doc("G_Mui_err").asDouble().getValue
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MPO(name: Seq[String]
               , className: String
               , N : Double
               , delta_alpha : Double
               , H_Bowell_i : Double
               , H_Bowell : Double
               , H_Bowell_unc : Double
               , H_Bowell_err: Double
               , H_Mui_i: Double
               , H_Mui: Double
               , H_Mui_unc: Double
               , H_Mui_err: Double
               , G_Bowell: Double
               , G_Bowell_unc: Double
               , G_Bowell_err: Double
               , G_Mui: Double
               , G_Mui_unc: Double
               , G_Mui_err: Double
                   ) extends MPO_Common {
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    val seq = ArrayBuffer[String]()
    seq ++= productIterator.mkString(sep).split(sep)
    seq.insert(1, name.mkString("|"))  //fix equivalent designation
    seq.mkString(sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO.scala
//=============================================================================
