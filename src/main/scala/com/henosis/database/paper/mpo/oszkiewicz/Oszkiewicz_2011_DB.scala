/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Sep/2021
 * Time:  11h:17m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.oszkiewicz
//=============================================================================
import com.henosis.algoritms.paper.mpo.oszkiewicz.Oszkiewicz_2011
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.mpo.MPO_PaperDB
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
//=============================================================================
//=============================================================================
object Oszkiewicz_2011_DB {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[MPO]), DEFAULT_CODEC_REGISTRY )
  //---------------------------------------------------------------------------
  def apply(): Oszkiewicz_2011_DB =
    Oszkiewicz_2011_DB(PaperDB.DATABASE_NAME
      , PaperDB.COLLECTION_PAPER_OSZKIEWICZ_2021
      , Oszkiewicz_2011.paperName
      , Oszkiewicz_2011.referenceEpoch
      , codecRegistry)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Oszkiewicz_2011_DB( databaseName: String
                               , collectionName: String
                               , paperName: String
                               , paperReferenceEpoch: String
                               , codecRegistry: CodecRegistry)
  extends MPO_PaperDB
//=============================================================================
//End of file Oszkiewicz_2011_DB.scala
//=============================================================================
