/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  14h:27m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.carry
//=============================================================================
import com.henosis.algoritms.paper.mpo.MPO_Common
import org.mongodb.scala.Document
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object MPO {
  //---------------------------------------------------------------------------
  final val CSV_HEADER =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = "carry_2016_" + f.getName
      f.setAccessible(false)
      res
    }.take(58) ++ Seq("carry_2016_flag")
  //---------------------------------------------------------------------------
  def apply(doc: Document): MPO =  MPO(
      doc("ra").asDouble().getValue
    , doc("dec").asDouble().getValue
    , doc("dis").asDouble().getValue
    , doc("moID").asString().getValue
    , doc("run").asInt32().getValue
    , doc("col").asInt32().getValue
    , doc("field").asInt32().getValue
    , doc("object").asInt32().getValue
    , doc("rowc").asDouble().getValue
    , doc("colc").asDouble().getValue
    , doc("mjd").asDouble().getValue
    , doc("lambda").asDouble().getValue
    , doc("beta").asDouble().getValue
    , doc("phi").asDouble().getValue
    , doc("vMu").asDouble().getValue
    , doc("vMu_error").asDouble().getValue
    , doc("vNu").asDouble().getValue
    , doc("vNu_error").asDouble().getValue
    , doc("vLambda").asDouble().getValue
    , doc("vBeta").asDouble().getValue
    , doc("u").asDouble().getValue
    , doc("uErr").asDouble().getValue
    , doc("g").asDouble().getValue
    , doc("gErr").asDouble().getValue
    , doc("r").asDouble().getValue
    , doc("rErr").asDouble().getValue
    , doc("i").asDouble().getValue
    , doc("iErr").asDouble().getValue
    , doc("z").asDouble().getValue
    , doc("zErr").asDouble().getValue
    , doc("a").asDouble().getValue
    , doc("aErr").asDouble().getValue
    , doc("V").asDouble().getValue
    , doc("B").asDouble().getValue
    , doc("numeration").asInt32().getValue
    , Seq(doc("designation").asString().getValue)
    , doc("source").asString().getValue
    , doc("detection_counter").asInt32().getValue
    , doc("total_detection_count").asInt32().getValue
    , doc("flags").asInt32().getValue
    , doc("computed_ra").asDouble().getValue
    , doc("computed_dec").asDouble().getValue
    , doc("computed_app_mag").asDouble().getValue
    , doc("geocentric").asDouble().getValue
    , doc("phase").asDouble().getValue
    , doc("catalogID").asString().getValue
    , doc("H").asDouble().getValue
    , doc("G").asDouble().getValue
    , doc("arc").asDouble().getValue
    , doc("epoch").asDouble().getValue
    , doc("e").asDouble().getValue
    , doc("lon_asc_node").asDouble().getValue
    , doc("arg_of_perihelion").asDouble().getValue
    , doc("M").asDouble().getValue
    , doc("proper_elements_catalog_ID").asString().getValue
    , doc("a_").asDouble().getValue
    , doc("e_").asDouble().getValue
    , doc("sin(i_)").asDouble().getValue
    , doc("flag_1").asBoolean().getValue
    , doc("flag_2").asBoolean().getValue
    , doc("flag_3").asBoolean().getValue
    , doc("flag_4").asBoolean().getValue
    , doc("flag_5").asBoolean().getValue
    , doc("flag_6").asBoolean().getValue
    , doc("flag_7").asBoolean().getValue
    , doc("flag_8").asBoolean().getValue
    , doc("flag_9").asBoolean().getValue
    , doc("flag_10").asBoolean().getValue
    , doc("flag_11").asBoolean().getValue
    , doc("flag_12").asBoolean().getValue
    , doc("flag_13").asBoolean().getValue
    , doc("flag_14").asBoolean().getValue
    , doc("flag_15").asBoolean().getValue
    , doc("flag_16").asBoolean().getValue
    , doc("flag_17").asBoolean().getValue
    , doc("flag_18").asBoolean().getValue
    , doc("flag_19").asBoolean().getValue
    , doc("flag_20").asBoolean().getValue
    , doc("flag_21").asBoolean().getValue
    , doc("flag_22").asBoolean().getValue
    , doc("flag_23").asBoolean().getValue
    , doc("flag_24").asBoolean().getValue
    , doc("flag_25").asBoolean().getValue
    , doc("flag_26").asBoolean().getValue
    , doc("flag_27").asBoolean().getValue
    , doc("flag_28").asBoolean().getValue
    , doc("flag_29").asBoolean().getValue
    , doc("flag_30").asBoolean().getValue
    , doc("flag_31").asBoolean().getValue
    , doc("flag_32").asBoolean().getValue
    , doc("flag_33").asBoolean().getValue
    , doc("flag_34").asBoolean().getValue
    , doc("flag_35").asBoolean().getValue
    , doc("flag_36").asBoolean().getValue
    , doc("flag_37").asBoolean().getValue
    , doc("flag_38").asBoolean().getValue
    , doc("flag_39").asBoolean().getValue
    , doc("flag_40").asBoolean().getValue
    , doc("flag_41").asBoolean().getValue
    , doc("flag_42").asBoolean().getValue
    , doc("flag_43").asBoolean().getValue
    , doc("flag_44").asBoolean().getValue
    , doc("flag_45").asBoolean().getValue
    , doc("flag_46").asBoolean().getValue
    , doc("flag_47").asBoolean().getValue
    , doc("flag_48").asBoolean().getValue
    , doc("flag_49").asBoolean().getValue
    , doc("flag_50").asBoolean().getValue
    , doc("flag_51").asBoolean().getValue
    , doc("flag_52").asBoolean().getValue
    , doc("flag_53").asBoolean().getValue
    , doc("flag_54").asBoolean().getValue
    , doc("flag_55").asBoolean().getValue
    , doc("flag_56").asBoolean().getValue
    , doc("flag_57").asBoolean().getValue
    , doc("flag_58").asBoolean().getValue
    , doc("flag_59").asBoolean().getValue
    , doc("flag_60").asBoolean().getValue
    , doc("flag_61").asBoolean().getValue
    , doc("flag_62").asBoolean().getValue
    , doc("flag_63").asBoolean().getValue
    , doc("flag_64").asBoolean().getValue
    )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MPO(ra: Double
               , dec: Double
               , dis: Double
               , moID: String
               , run: Int
               , col: Int
               , field: Int
               , objectID: Int
               , rowc: Double
               , colc: Double
               , mjd: Double
               , lambda: Double
               , beta: Double
               , phi: Double
               , vMu: Double
               , vMu_err: Double
               , vNu: Double
               , vNu_err: Double
               , vLambda: Double
               , vBeta: Double
               , u: Double
               , uErr: Double
               , g: Double
               , gErr: Double
               , r: Double
               , rErr: Double
               , i: Double
               , iErr: Double
               , z: Double
               , zErr: Double
               , a: Double
               , aErr: Double
               , V: Double
               , B: Double
               , number: Int
               , name: Seq[String] = Seq()
               , source: String
               , detection_counter: Int
               , total_detection_count: Int
               , flags: Int
               , computed_ra: Double
               , computed_dec: Double
               , computed_app_mag: Double
               , geocentric: Double
               , phase: Double
               , catalogID: String
               , H: Double
               , G: Double
               , arc: Double
               , epoch: Double
               , e: Double
               , lon_asc_node: Double
               , arg_of_perihelion: Double
               , M: Double
               , proper_elements_catalog_ID: String
               , aprime: Double
               , eprime: Double
               , siniprime: Double
               , flag_1: Boolean
               , flag_2: Boolean
               , flag_3: Boolean
               , flag_4: Boolean
               , flag_5: Boolean
               , flag_6: Boolean
               , flag_7: Boolean
               , flag_8: Boolean
               , flag_9: Boolean
               , flag_10: Boolean
               , flag_11: Boolean
               , flag_12: Boolean
               , flag_13: Boolean
               , flag_14: Boolean
               , flag_15: Boolean
               , flag_16: Boolean
               , flag_17: Boolean
               , flag_18: Boolean
               , flag_19: Boolean
               , flag_20: Boolean
               , flag_21: Boolean
               , flag_22: Boolean
               , flag_23: Boolean
               , flag_24: Boolean
               , flag_25: Boolean
               , flag_26: Boolean
               , flag_27: Boolean
               , flag_28: Boolean
               , flag_29: Boolean
               , flag_30: Boolean
               , flag_31: Boolean
               , flag_32: Boolean
               , flag_33: Boolean
               , flag_34: Boolean
               , flag_35: Boolean
               , flag_36: Boolean
               , flag_37: Boolean
               , flag_38: Boolean
               , flag_39: Boolean
               , flag_40: Boolean
               , flag_41: Boolean
               , flag_42: Boolean
               , flag_43: Boolean
               , flag_44: Boolean
               , flag_45: Boolean
               , flag_46: Boolean
               , flag_47: Boolean
               , flag_48: Boolean
               , flag_49: Boolean
               , flag_50: Boolean
               , flag_51: Boolean
               , flag_52: Boolean
               , flag_53: Boolean
               , flag_54: Boolean
               , flag_55: Boolean
               , flag_56: Boolean
               , flag_57: Boolean
               , flag_58: Boolean
               , flag_59: Boolean
               , flag_60: Boolean
               , flag_61: Boolean
               , flag_62: Boolean
               , flag_63: Boolean
               , flag_64: Boolean
                   ) extends MPO_Common {
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    val seq = ArrayBuffer[String]()

    val flags = productIterator.drop(58)
      .mkString("{",",","}")
      .replaceAll("false", "0")
      .replaceAll("true", "1")
    seq ++= productIterator.take(58).mkString(sep).split(sep)
    seq += flags
    seq(35) = name.mkString(sep)
    seq.mkString(sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO_ColorIndex.scala
//=============================================================================
