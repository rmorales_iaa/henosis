/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/Sep/2021
 * Time:  11h:17m
 * Description: None
 */
//=============================================================================
package com.henosis.database.paper.mpo.mahlke
//=============================================================================
import com.henosis.algoritms.paper.mpo.mahlke.Mahlke_2021
import com.henosis.database.paper.PaperDB
import com.henosis.database.paper.mpo.MPO_PaperDB
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
//=============================================================================
//=============================================================================
object Mahlke_2021_DB {
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[MPO]), DEFAULT_CODEC_REGISTRY )
  //---------------------------------------------------------------------------
  def apply(): Mahlke_2021_DB =
    Mahlke_2021_DB(PaperDB.DATABASE_NAME
      , PaperDB.COLLECTION_PAPER_MAHLKE_2021
      , Mahlke_2021.paperName
      , Mahlke_2021.referenceEpoch
      , codecRegistry)
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Mahlke_2021_DB( databaseName: String
                            , collectionName: String
                            , paperName: String
                            , paperReferenceEpoch: String
                            , codecRegistry: CodecRegistry)
  extends MPO_PaperDB

//=============================================================================
//End of file Mahlke_2021_DB.scala
//=============================================================================
