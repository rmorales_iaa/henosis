/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  17/Mar/2024
 * Time:  12h:15m
 * Description: None
 */
package com.henosis.database.paper.mpo.peixinho

//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import scala.util.matching.Regex
import org.mongodb.scala.{Document, MongoCollection}
import scala.io.Source
//=============================================================================
object Peixhino_2015_DB_Importer extends MyLogger {
  //---------------------------------------------------------------------------
  final val CSV_HEADER =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = "Peixhino_2015_" + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'Candal 2019' file: '$fileName'")
    //-------------------------------------------------------------------------
    def getDoubleOrMinusOne(s: String) = {
      if (s == "nan") scala.Double.NaN
      else
        if (s == "inf") Double.PositiveInfinity
        else
          if (s == "-inf") Double.NegativeInfinity
          else
            if (s.isEmpty) -1d else s.toDouble
    }

    //-------------------------------------------------------------------------
    def getValueAndError(s: String) = {
      if (s == "—" || s == "——") (Double.NaN,Double.NaN)
      else {
        val seq = s
          .replaceAll("-","-")
          .split("±")
        (seq(0).trim.toDouble
          , seq(1).trim.toDouble)
      }
    }
    //-------------------------------------------------------------------------
    def deleteSubScript(str: String): String = {
      val pattern = """(\d{4})(\p{Alpha}+)(\d+)""".r
      val t = str.replaceAll(" +", "")
      t match {
        case pattern(year, name, _*) => year + name
        case _ => str
      }
    }
    //-------------------------------------------------------------------------
    val br = Source.fromFile(fileName)
    val docSeq = (for (line <- br.getLines) yield {
      if (line.startsWith("#")) None
      else {
        val lineSeq = line
          .trim
          .replaceAll(" +", " ")
          .split(",")
          .filter(s=> !s.isEmpty && s != "\"")

        val findReferenceIndex =  {
          val pos = lineSeq.zipWithIndex.filter(t=> t._1.startsWith("\""))
          if (pos.isEmpty) {
            if (line.startsWith("(")) 21
            else 20
          }
          else pos.head._2
        }
        val (id, _mpcID, offset) =
          if (findReferenceIndex == 21) {
            (lineSeq(0)
              .replace("(", "")
              .replace(")", "")
              .trim
              , lineSeq(1).replaceAll(" ","")
              , 1)
          }
          else (lineSeq(0).replaceAll(" ",""), "-1", 0)

        val referenceSeq = lineSeq.drop(findReferenceIndex)
          .mkString(",")
          .replaceAll(" +", " ")
          .replaceAll(",+", ",")
          .replaceAll("\"", "")
          .split(",")
          .map(ref=> MPO.referenceSeq(ref.trim.toInt - 1))
          .mkString("{",";","}")


        val (vHr,vHr_error)   = getValueAndError(lineSeq(2+offset))
        val (vGrt,vGrt_error) = getValueAndError(lineSeq(3+offset))
        val (vB_V,vB_V_error) = getValueAndError(lineSeq(4+offset))
        val (vV_R,vV_R_error) = getValueAndError(lineSeq(5+offset))
        val (vR_I,vR_I_error) = getValueAndError(lineSeq(6+offset))
        val (vV_I,vV_I_error) = getValueAndError(lineSeq(7+offset))
        val (vB_I,vB_I_error) = getValueAndError(lineSeq(8+offset))
        val (vB_R,vB_R_error) = getValueAndError(lineSeq(9+offset))

        Some(Document(
            "name" -> id
          , "mpcID" -> _mpcID
          , "className" -> lineSeq(1+offset)
          , "Hr" -> vHr
          , "Hr_error" -> vHr_error
          , "Grt" -> vGrt
          , "Grt_error" -> vGrt_error
          , "B_V" -> vB_V
          , "B_V_error" -> vB_V_error
          , "V_R" -> vV_R
          , "V_R_error" -> vV_R_error
          , "R_I" -> vR_I
          , "R_I_error" -> vR_I_error
          , "V_I" -> vV_I
          , "V_I_error" -> vV_I_error
          , "B_I" -> vB_I
          , "B_I_error" -> vB_I_error
          , "B_R" -> vB_R
          , "B_R_error" -> vB_R_error
          , "q" -> getDoubleOrMinusOne(lineSeq(10+offset))
          , "Q" -> getDoubleOrMinusOne(lineSeq(11+offset))
          , "i" -> getDoubleOrMinusOne(lineSeq(12+offset))
          , "e" -> getDoubleOrMinusOne(lineSeq(13+offset))
          , "a" -> getDoubleOrMinusOne(lineSeq(14+offset))
          , "vc" -> getDoubleOrMinusOne(lineSeq(15+offset))
          , "psi" -> getDoubleOrMinusOne(lineSeq(16+offset))
          , "epsilon" -> getDoubleOrMinusOne(lineSeq(17+offset))
          , "Tn" -> getDoubleOrMinusOne(lineSeq(18+offset))
          , "Tj" -> getDoubleOrMinusOne(lineSeq(19+offset))
          , "references" -> referenceSeq
        ))
      }}).flatten.toArray

    br.close
    coll.insertMany(docSeq).results()
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MongoDB, filename: String) = {
    val coll = db.getCollection
    db.dropCollection(coll)
    processFile(filename, coll)
    db.createIndex("name", _unique = false)
    db.createIndex("mpcID", _unique = false)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Peixhino_2015_DB_Importer.scala
//=============================================================================