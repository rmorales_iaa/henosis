/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Jul/2021
 * Time:  10h:31m
 * Description: Import data from astorb (MPO Orbital Elements Database) to mongo
 * https://asteroid.lowell.edu/main/astorb/
 */
//=============================================================================
package com.henosis.database.astorb
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import com.common.util.util.Util
import com.henosis.database.MPO_DB
import com.common.database.mongoDB.fortran.FortranColumn
//=============================================================================
import org.mongodb.scala.bson.BsonValue
import org.mongodb.scala.{Document, MongoCollection}
import java.io.{BufferedInputStream, FileInputStream}
import java.nio.charset.StandardCharsets
import scala.collection.mutable.ArrayBuffer
import java.io.File
//=============================================================================
//=============================================================================
object AstorbImporter extends MyLogger {
  //---------------------------------------------------------------------------
  private val asteroidColumnData = Array(                                                  //meaning    item
                                  FortranColumn("asteroid_number","A6")                    //01         0
                                , FortranColumn("divider", "1x")                           //divider    1
                                , FortranColumn("name", "A18")                             //02         2
                                , FortranColumn("divider", "1x")                           //divider    3
                                , FortranColumn("orbit_computer", "A15")                   //03         4
                                , FortranColumn("divider", "1x")                           //divider    5
                                , FortranColumn("H","A5")                                  //04         6
                                , FortranColumn("divider", "1x")                           //divider    7
                                , FortranColumn("G","F5.2")                                //05         8
                                , FortranColumn("divider", "1x")                           //divider    9
                                , FortranColumn("b-v","A4")                                //06        10
                                , FortranColumn("divider", "1x")                           //divider   11
                                , FortranColumn("iras_diameter","A5")                      //07        12
                                , FortranColumn("divider", "1x")                           //divider   13
                                , FortranColumn("iras_taxonomic","A4")                     //08        14
                                , FortranColumn("divider", "1x")                           //divider   15
                                , FortranColumn("integer_codes","A24")                     //09        16
                                , FortranColumn("divider", "1x")                           //divider   17
                                , FortranColumn("orbital_arc_days","I5")                   //10        18
                                , FortranColumn("observing_count", "I5")                   //11        19
                                , FortranColumn("divider", "1x")                           //divider   20
                                , FortranColumn("epoch_of_osculation","I8")                //12        21
                                , FortranColumn("divider", "1x")                           //divider   22
                                , FortranColumn("mean_anomaly","F10.6")                    //13        23
                                , FortranColumn("divider", "1x")                           //divider   24
                                , FortranColumn("argument_of_perihelion","F10.6")          //14        25
                                , FortranColumn("divider", "1x")                           //divider   26
                                , FortranColumn("longitude_of_ascending_node","F10.6")     //15        27
                                , FortranColumn("inclination","F10.8")                     //16        28
                                , FortranColumn("divider", "1x")                           //divider   29
                                , FortranColumn("eccentricity","F10.8")                    //17        30
                                , FortranColumn("semi_major_axis","F13.8")                 //18        31
                                , FortranColumn("divider", "1x")                           //divider   32
                                , FortranColumn("orbit_computation_date","I8")             //19        33
                                , FortranColumn("divider", "1x")                           //divider   34
                                , FortranColumn("ephemeris_uncertainty","F7.2")            //20        35
                                , FortranColumn("divider", "1x")                           //divider   36
                                , FortranColumn("rate_of_change_of_CEU","F8.2")            //21        37
                                , FortranColumn("divider", "1x")                           //divider   38
                                , FortranColumn("date_of_CEU","F8.2")                      //22        39
                                , FortranColumn("divider", "1x")                           //divider   40
                                , FortranColumn("PEU","A16")                               //23        41
                                , FortranColumn("divider", "1x")                           //divider   42
                                , FortranColumn("greatest_PEU_CEU","A16")                  //24        43
                                , FortranColumn("divider", "1x")                           //divider   44
                                , FortranColumn("greatest_PEU_next_PEU","A16")             //25        45
                                , FortranColumn("end_of_column","A1")                      //divider   46
  )
  //---------------------------------------------------------------------------
  private val rowByteSize = (asteroidColumnData map(_.getCharSize)).sum
  //---------------------------------------------------------------------------
  private def readAsteroidInfo(bis: BufferedInputStream,i: Int) : Option[Document] = {
    val row = ArrayBuffer[(String, BsonValue)]()
    asteroidColumnData.zipWithIndex.foreach { case (t,item) =>
      val columnBytes = Array.fill[Byte](t.charSize)(' ')
      bis.read(columnBytes)
      val columnValue = new String(columnBytes, StandardCharsets.UTF_8)

      if (t.name == "_id") {
        if (!Util.isInteger(columnValue.trim)) {
          error(s"MPO: $i item: $item. Error parsing column '${t.name}' the '_id' is not a valid integer. Current value:'$columnValue'")
          error(new String(columnBytes, StandardCharsets.UTF_8))
          return None
        }
        val v = columnValue.trim.toInt
        if (v <= 0) {
          error(s"MPO: $i item: $item. Error parsing column '${t.name}' the '_id' is not a positive integer. Current value:'$columnValue'")
          error(new String(columnBytes, StandardCharsets.UTF_8))
          return None
        }
      }
      if (t.name == "divider") {
        if (columnValue != " ") {
          error(s"MPO: $i item: $item. Error parsing column '${t.name}' the divider is not ' '. Current value:'$columnValue'")
          error(new String(columnBytes, StandardCharsets.UTF_8))
          return None
        }
      }
      else
        if (t.name != "end_of_column") t.addColumnToRow(columnValue.trim, row)
    }
    Some(Document(row))
  }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, db: MongoDB, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing Astorb file: '$fileName'")
    val f = new File(fileName);

    //check the file consistency
    val asteroidCount = f.length  / rowByteSize.toDouble
    val totalAsteroidCount =  asteroidCount.toInt
    val partialRecord = asteroidCount - totalAsteroidCount
    if (partialRecord != 0)
      return error(s"The number of asteroids stored in the file is: $asteroidCount. So there are partial mpo info and the file will not be processed ")
    info(s"Processing: $totalAsteroidCount asteroids stored in the input file. Each mpo has: $rowByteSize bytes size")

    val asteroidBuffer = ArrayBuffer[Document]()
    val maxBufferedAsteroid  = 64 * 1024 //to minimize the write operations in mongo

    //read the mpo info and store it
    val bis = new BufferedInputStream(new FileInputStream(fileName))
    for( i <-0 until totalAsteroidCount){
      val doc = readAsteroidInfo(bis,i)
      if (doc.isEmpty) {
        bis.close
        return false
      }
      else {
        //store the mpo in the buffer and store the mpo sequence if is needed
        asteroidBuffer += doc.get
        if (asteroidBuffer.length > maxBufferedAsteroid) {
          coll.insertMany(asteroidBuffer.toArray).results()
          asteroidBuffer.clear()
        }
      }
    }
    bis.close()

    //store the remain buffered asteroids
    if (asteroidBuffer.length > 0) {
      coll.insertMany(asteroidBuffer.toArray).results()
      asteroidBuffer.clear()
    }

    info("Creating indexes")
    db.createIndex("asteroid_number", coll, _unique = false) //due to unnumbered mpo
    db.createIndex("name", coll)
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MPO_DB) = {
    val coll = db.collAstorb
    db.dropCollection(coll)
    val updateScript = MyConf.c.getString("AsteroidDatabases.astorb.updateScript")
    val r = Util.runShellCommandAndGetBinaryOutput_2(updateScript)
    val inputFile = new String(r, StandardCharsets.UTF_8).split("\n").last
    processFile(inputFile, db, coll)
    new File(inputFile).delete()
    info(s"File: '$inputFile' deleted")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AstorbImporter.scala
//=============================================================================
