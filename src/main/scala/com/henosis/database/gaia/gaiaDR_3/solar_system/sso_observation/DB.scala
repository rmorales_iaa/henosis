/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:05m
 * Description: None
 */
//=============================================================================
package com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_observation
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers._
import com.common.database.mongoDB.MongoDB
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
//=============================================================================
object DB {
  //---------------------------------------------------------------------------
  final val codecRegistry = fromRegistries(fromProviders(classOf[Source]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  final val COL_NAME_DENOMINATION = "denomination"
  final val COL_NAME_MPC_ID        = "number_mp"
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_observation.DB._
case class DB(conf: MyConf) extends MongoDB {
  //---------------------------------------------------------------------------
  val databaseName   = conf.getString("Database.name")
  val collectionName = conf.getString("Database.source.collectionName")
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(DB.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[Source] = this.database.getCollection(this.collectionName)
  connected = true
  //---------------------------------------------------------------------------
  def getSource(name: String) =
    collWithRegistry.find(equal(COL_NAME_DENOMINATION, name)).getResultDocumentSeq.head
  //---------------------------------------------------------------------------
  def getSourceByMpcID(mpcID: Int) =
    collWithRegistry.find(equal(COL_NAME_MPC_ID, mpcID)).getResultDocumentSeq.head
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DB.scala
//=============================================================================
