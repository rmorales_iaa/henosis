/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  17/Jun/2022
 * Time:  13h:22m
 * Description: None
 */
//=============================================================================
package com.henosis.database.gaia.source.gaiaDR3.gaia_source
//=============================================================================
import com.common.DatabaseImporter.CSV_EXTENSION_FILE
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.postgreDB.PostgreScalaDB
import com.common.database.postgreDB.gaia_dr_3.Source.info
import com.common.database.postgreDB.gaia_dr_3.{Source, SourceMinimal}
import com.common.hardware.cpu.CPU
import com.common.util.path.Path
import com.common.{DatabaseImporter, LineEntry}
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}

import java.io.{BufferedReader, File, FileReader}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object  DB_Importer {
  //---------------------------------------------------------------------------
  def processFileMongo(path: String
                       , coll: MongoCollection[Source]
                       , maxBufferedDocument:  Int): Boolean = {

    val name = Path.getOnlyFilename(path)
    info(s"Processing file: '$name'")
    val br = new BufferedReader(new FileReader(new File(path)))
    val sourceBuffer = ArrayBuffer[Source]()
    var line = br.readLine()
    line = br.readLine()  //avoid header
    while(line != null &&  line.length()!=0 ) {
      if (!line.isEmpty && !line.startsWith("#") && !line.startsWith("solution_id")) {
        val source = Source.getDocument(line)
        sourceBuffer += source
        if (sourceBuffer.length > maxBufferedDocument) {
          coll.insertMany(sourceBuffer.toArray).results()
          sourceBuffer.clear()
        }
      }
      line = br.readLine()
    }
    br.close()

    //store the remain buffered docs
    if (sourceBuffer.length > 0) {
      coll.insertMany(sourceBuffer.toArray).results()
      sourceBuffer.clear()
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class DB_Importer(conf: MyConf
                       , inputDir: String
                       , usePostGre: Boolean) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = DB(conf)
  val maxBufferedDocument = conf.getInt("Database.source.maxBufferedDocument")
  val colNameSeq = Array[LineEntry]() //not used
  val lineDivider: String = ","
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[Source] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  //postgre
  private val postgreDB = PostgreScalaDB(MyConf.c.getString("Database.postgre.url")
                                         , conf.getString("Database.name"))
  //---------------------------------------------------------------------------
  override def createIndexes() = {

    info("Creating indexes")
    if (usePostGre) {
      postgreDB.createIndex(
          SourceMinimal.tableName
        , Array(SourceMinimal.RA_COL_NAME,SourceMinimal.DEC_COL_NAME))

      postgreDB.createIndex(
        SourceMinimal.tableName
        , Array(SourceMinimal.PARALLAX_COL_NAME)
        , unique = false)
    }
    else
      info("Please create indexes manually using a local connection to the database:\nuse GAIA_DR_3\ndb.gaia_source.createIndex( { \"ra\": 1, \"dec\": 1 } , {unique: true})")
  }
  //---------------------------------------------------------------------------
  private def dropPostgreTableSeq() =  {
    postgreDB.executeSQL_script(conf.getString("Database.postgre.source.minimalTable_SQL"))
    postgreDB.executeSQL_script(conf.getString("Database.postgre.source.reducedTable_SQL"))
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String
                                     , coll: MongoCollection[Document]): Boolean = {
    if (usePostGre)
      SourceMinimal.processFile(
        postgreDB
        , path
        , maxBufferedDocument
        )
    else
      DB_Importer.processFileMongo(path, collWithRegistry, maxBufferedDocument)
  }
  //---------------------------------------------------------------------------
  override def run(dropCollection : Boolean = true
                   , fileExtension: String = CSV_EXTENSION_FILE) : Unit = {
    if (usePostGre) {
      if (dropCollection) dropPostgreTableSeq
      val pathSeq = Path.getSortedFileList(iDir,fileExtension) map (_.getAbsolutePath)
      new MyParallelTask(pathSeq, null)

      info(s"Total documents inserted: ${postgreDB.rowCount(SourceMinimal.tableName)}")
      postgreDB.close
    }
    else
      super.run(dropCollection, fileExtension)
  }
  //---------------------------------------------------------------------------
  run()
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file DB_Importer.scala
//=============================================================================
