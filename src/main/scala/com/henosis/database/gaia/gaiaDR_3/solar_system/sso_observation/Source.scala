/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:08m
 * Description: https://gea.esac.esa.int/archive/documentation/GDR3/Gaia_archive/chap_datamodel/sec_dm_solar_system_object_tables/ssec_dm_sso_source.html
 */
//=============================================================================
package com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_observation
//=============================================================================
import com.common.logger.MyLogger
import com.common.csv.CsvFile._
//=============================================================================
import scala.language.existentials
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Source extends MyLogger {
  //---------------------------------------------------------------------------
  def getIntArray(r: Array[String], index: Int) = {
    var i = index
    val result = ArrayBuffer[Int]()
    var continue = true
    while (continue) {
      val s = r(i)
      if (s.startsWith("\"[")) result += s.drop(2).toInt
      else {
        if (s.endsWith("]\"")) {
          result += s.dropRight(2).toInt
          continue = false
        }
        else  result += s.toInt
      }
      i += 1
    }
    result.toArray
  }
  //---------------------------------------------------------------------------
  def getDocument(line: String) = {
    val r = line.split(",")
    var index = -1
    //-------------------------------------------------------------------------
    def getNewIndex = {
      index += 1
      index
    }
    //-------------------------------------------------------------------------
    def getArray(r: Array[String]) = {
      val result = getIntArray(r, index+1)
      index += result.length
      result.mkString("[",",","]")
    }
    //-------------------------------------------------------------------------
    Source(
        getLongOrDefault(r(getNewIndex))    //solution_id
      , getLongOrDefault(r(getNewIndex))    //source_id
      , r(getNewIndex).replaceAll("\"","")                       //denomination
      , getLongOrDefault(r(getNewIndex))    //transit_id
      , getLongOrDefault(r(getNewIndex))    //observation_id
      , getLongOrDefault(r(getNewIndex))    //number_mp
      , getDoubleOrDefault(r(getNewIndex))  //epoch
      , getDoubleOrDefault(r(getNewIndex))  //epoch_err
      , getDoubleOrDefault(r(getNewIndex))  //epoch_utc
      , getDoubleOrDefault(r(getNewIndex))  //ra
      , getDoubleOrDefault(r(getNewIndex))  //dec
      , getDoubleOrDefault(r(getNewIndex))  //ra_error_systematic
      , getDoubleOrDefault(r(getNewIndex))  //dec_error_systematic
      , getDoubleOrDefault(r(getNewIndex))  //ra_dec_correlation_systematic
      , getDoubleOrDefault(r(getNewIndex))  //ra_error_random
      , getDoubleOrDefault(r(getNewIndex))  //dec_error_random
      , getDoubleOrDefault(r(getNewIndex))  //ra_dec_correlation_random
      , getDoubleOrDefault(r(getNewIndex))  //g_mag
      , getDoubleOrDefault(r(getNewIndex))  //g_flux
      , getDoubleOrDefault(r(getNewIndex))  //g_flux_error
      , getDoubleOrDefault(r(getNewIndex))  //x_gaia
      , getDoubleOrDefault(r(getNewIndex))  //y_gaia
      , getDoubleOrDefault(r(getNewIndex))  //z_gaia
      , getDoubleOrDefault(r(getNewIndex))  //      vx_gaia
      , getDoubleOrDefault(r(getNewIndex))  //vy_gaia
      , getDoubleOrDefault(r(getNewIndex))  //vz_gaia
      , getDoubleOrDefault(r(getNewIndex))  //x_gaia_geocentric
      , getDoubleOrDefault(r(getNewIndex))  //y_gaia_geocentric
      , getDoubleOrDefault(r(getNewIndex))  //z_gaia_geocentric
      , getDoubleOrDefault(r(getNewIndex))  //vx_gaia_geocentric
      , getDoubleOrDefault(r(getNewIndex))  //vy_gaia_geocentric
      , getDoubleOrDefault(r(getNewIndex))  //vz_gaia_geocentric
      , getDoubleOrDefault(r(getNewIndex))  //position_angle_scan
      , getArray(r)                         //astrometric_outcome_ccd
      , getIntOrDefault(r(getNewIndex))     // astrometric_outcome_transit
     )
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String="") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }

  //---------------------------------------------------------------------------
}
//=============================================================================
case class Source(solution_id                        : Long
                  , _id                        : Long
                  , denomination                     : String
                  , transit_id                       : Long
                  , observation_id                   : Long
                  , number_mp                        : Long
                  , epoch                            : Double
                  , epoch_err                        : Double
                  , epoch_utc                        : Double
                  , ra                               : Double
                  , dec                              : Double
                  , ra_error_systematic              : Double
                  , dec_error_systematic             : Double
                  , ra_dec_correlation_systematic    : Double
                  , ra_error_random                  : Double
                  , dec_error_random                 : Double
                  , ra_dec_correlation_random        : Double
                  , g_mag                            : Double
                  , g_flux                           : Double
                  , g_flux_error                     : Double
                  , x_gaia                           : Double
                  , y_gaia                           : Double
                  , z_gaia                           : Double
                  , vx_gaia                          : Double
                  , vy_gaia                          : Double
                  , vz_gaia                          : Double
                  , x_gaia_geocentric                : Double
                  , y_gaia_geocentric                : Double
                  , z_gaia_geocentric                : Double
                  , vx_gaia_geocentric               : Double
                  , vy_gaia_geocentric               : Double
                  , vz_gaia_geocentric               : Double
                  , position_angle_scan              : Double
                  , astrometric_outcome_ccd          : String
                  , astrometric_outcome_transit      : Int
                 ) {

  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String) =
    this.getClass.getDeclaredFields.map { f=>
      f.setAccessible(true)
      val v = f.get(this).toString
      f.setAccessible(false)
      v
    }.mkString(sep)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Source.scala
//=============================================================================
