/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:08m
 * Description: https://gea.esac.esa.int/archive/documentation/GDR3/Gaia_archive/chap_datamodel/sec_dm_solar_system_object_tables/ssec_dm_sso_source.html
 */
//=============================================================================
package com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_source
//=============================================================================
import com.common.logger.MyLogger
import com.common.csv.CsvFile._
import scala.language.existentials
//=============================================================================
//=============================================================================
object Source extends MyLogger {
  //---------------------------------------------------------------------------
  def getDocument(line: String) = {
    val r = line.split(",")
    var index = -1
    //-------------------------------------------------------------------------
    def getNewIndex = {
      index += 1
      index
    }
    //-------------------------------------------------------------------------
    Source(
      getLongOrDefault(r(getNewIndex))    //solution_id
      , getLongOrDefault(r(getNewIndex))  //source_id
      , getIntOrDefault(r(getNewIndex))   //num_of_obs
      , getLongOrDefault(r(getNewIndex))  //number_mp
      , r(getNewIndex).replaceAll("\"","")                    //denomination
      , getIntOrDefault(r(getNewIndex))   //num_of_spectra
     )
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String="") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Source(solution_id:                      Long
                  , _id:                            Long //original name: 'source_id'
                  , num_of_obs:                     Int
                  , number_mp:                      Long
                  , denomination:                   String
                  , num_of_spectra:                 Int
                 )
//=============================================================================
//=============================================================================
//End of file Source.scala
//=============================================================================
