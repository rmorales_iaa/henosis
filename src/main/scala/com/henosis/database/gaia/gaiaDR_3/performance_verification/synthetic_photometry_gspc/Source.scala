/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:08m
 * Description: https://gea.esac.esa.int/archive/documentation/GDR3/Gaia_archive/chap_datamodel/sec_dm_solar_system_object_tables/ssec_dm_sso_source.html
 */
//=============================================================================
package com.henosis.database.gaia.source.gaiaDR3.performance_verification.synthetic_photometry_gspc
//=============================================================================
import com.common.logger.MyLogger
import com.common.csv.CsvFile._
//=============================================================================
import scala.language.existentials
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Source extends MyLogger {
  //---------------------------------------------------------------------------
  def getIntArray(r: Array[String], index: Int) = {
    var i = index
    val result = ArrayBuffer[Int]()
    var continue = true
    while (continue) {
      val s = r(i)
      if (s.startsWith("\"[")) result += s.drop(2).toInt
      else {
        if (s.endsWith("]\"")) {
          result += s.dropRight(2).toInt
          continue = false
        }
        else  result += s.toInt
      }
      i += 1
    }
    result.toArray
  }
  //---------------------------------------------------------------------------
  def getDocument(line: String) = {
    val r = line.split(",")
    var index = -1
    //-------------------------------------------------------------------------
    def getNewIndex = {
      index += 1
      index
    }
    //-------------------------------------------------------------------------
    Source(
        getLongOrDefault(r(getNewIndex))  // source_id
      , getFloatOrDefault(r(getNewIndex)) // c_star
      , getFloatOrDefault(r(getNewIndex)) // u_jkc_flux
      , getFloatOrDefault(r(getNewIndex)) // u_jkc_flux_error
      , getFloatOrDefault(r(getNewIndex)) // u_jkc_mag
      , getByteOrDefault(r(getNewIndex))  // u_jkc_flag
      , getFloatOrDefault(r(getNewIndex)) // b_jkc_flux
      , getFloatOrDefault(r(getNewIndex)) // b_jkc_flux_error
      , getFloatOrDefault(r(getNewIndex)) // b_jkc_mag
      , getByteOrDefault(r(getNewIndex))  // b_jkc_flag
      , getFloatOrDefault(r(getNewIndex)) // v_jkc_flux
      , getFloatOrDefault(r(getNewIndex)) // v_jkc_flux_error
      , getFloatOrDefault(r(getNewIndex)) // v_jkc_mag
      , getByteOrDefault(r(getNewIndex))  // v_jkc_flag
      , getFloatOrDefault(r(getNewIndex)) // r_jkc_flux
      , getFloatOrDefault(r(getNewIndex)) // r_jkc_flux_error
      , getFloatOrDefault(r(getNewIndex)) // r_jkc_mag
      , getByteOrDefault(r(getNewIndex))  // r_jkc_flag
      , getFloatOrDefault(r(getNewIndex)) // i_jkc_flux
      , getFloatOrDefault(r(getNewIndex)) // i_jkc_flux_error
      , getFloatOrDefault(r(getNewIndex)) // i_jkc_mag
      , getByteOrDefault(r(getNewIndex))  // i_jkc_flag
      , getFloatOrDefault(r(getNewIndex)) // u_sdss_flux
      , getFloatOrDefault(r(getNewIndex)) // u_sdss_flux_error
      , getFloatOrDefault(r(getNewIndex)) // u_sdss_mag
      , getByteOrDefault(r(getNewIndex))  // u_sdss_flag
      , getFloatOrDefault(r(getNewIndex)) // g_sdss_flux
      , getFloatOrDefault(r(getNewIndex)) // g_sdss_flux_error
      , getFloatOrDefault(r(getNewIndex)) // g_sdss_mag
      , getByteOrDefault(r(getNewIndex))  // g_sdss_flag
      , getFloatOrDefault(r(getNewIndex)) // r_sdss_flux
      , getFloatOrDefault(r(getNewIndex)) // r_sdss_flux_error
      , getFloatOrDefault(r(getNewIndex)) // r_sdss_mag
      , getByteOrDefault(r(getNewIndex))  // r_sdss_flag
      , getFloatOrDefault(r(getNewIndex)) // i_sdss_flux
      , getFloatOrDefault(r(getNewIndex)) // i_sdss_flux_error
      , getFloatOrDefault(r(getNewIndex)) // i_sdss_mag
      , getByteOrDefault(r(getNewIndex))  // i_sdss_flag
      , getFloatOrDefault(r(getNewIndex)) // z_sdss_flux
      , getFloatOrDefault(r(getNewIndex)) // z_sdss_flux_error
      , getFloatOrDefault(r(getNewIndex)) // z_sdss_mag
      , getByteOrDefault(r(getNewIndex))  // z_sdss_flag
      , getFloatOrDefault(r(getNewIndex)) // y_ps1_flux
      , getFloatOrDefault(r(getNewIndex)) // y_ps1_flux_error
      , getFloatOrDefault(r(getNewIndex)) // y_ps1_mag
      , getByteOrDefault(r(getNewIndex))  // y_ps1_flag
      , getFloatOrDefault(r(getNewIndex)) // f606w_acswfc_flux
      , getFloatOrDefault(r(getNewIndex)) // f606w_acswfc_flux_error
      , getFloatOrDefault(r(getNewIndex)) // f606w_acswfc_mag
      , getByteOrDefault(r(getNewIndex))  // f606w_acswfc_flag
      , getFloatOrDefault(r(getNewIndex)) // f814w_acswfc_flux
      , getFloatOrDefault(r(getNewIndex)) // f814w_acswfc_flux_error
      , getFloatOrDefault(r(getNewIndex)) // f814w_acswfc_mag
      , getByteOrDefault(r(getNewIndex))  // f814w_acswfc_flag
    )
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String="") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Source(source_id                 : Long
                  , c_star                  : Float
                  , u_jkc_flux              : Float
                  , u_jkc_flux_error        : Float
                  , u_jkc_mag               : Float
                  , u_jkc_flag              : Byte
                  , b_jkc_flux              : Float
                  , b_jkc_flux_error        : Float
                  , b_jkc_mag               : Float
                  , b_jkc_flag              : Byte
                  , v_jkc_flux              : Float
                  , v_jkc_flux_error        : Float
                  , v_jkc_mag               : Float
                  , v_jkc_flag              : Byte
                  , r_jkc_flux              : Float
                  , r_jkc_flux_error        : Float
                  , r_jkc_mag               : Float
                  , r_jkc_flag              : Byte
                  , i_jkc_flux              : Float
                  , i_jkc_flux_error        : Float
                  , i_jkc_mag               : Float
                  , i_jkc_flag              : Byte
                  , u_sdss_flux             : Float
                  , u_sdss_flux_error       : Float
                  , u_sdss_mag              : Float
                  , u_sdss_flag             : Byte
                  , g_sdss_flux             : Float
                  , g_sdss_flux_error       : Float
                  , g_sdss_mag              : Float
                  , g_sdss_flag             : Byte
                  , r_sdss_flux             : Float
                  , r_sdss_flux_error       : Float
                  , r_sdss_mag              : Float
                  , r_sdss_flag             : Byte
                  , i_sdss_flux             : Float
                  , i_sdss_flux_error       : Float
                  , i_sdss_mag              : Float
                  , i_sdss_flag             : Byte
                  , z_sdss_flux             : Float
                  , z_sdss_flux_error       : Float
                  , z_sdss_mag              : Float
                  , z_sdss_flag             : Byte
                  , y_ps1_flux              : Float
                  , y_ps1_flux_error        : Float
                  , y_ps1_mag               : Float
                  , y_ps1_flag              : Byte
                  , f606w_acswfc_flux       : Float
                  , f606w_acswfc_flux_error : Float
                  , f606w_acswfc_mag        : Float
                  , f606w_acswfc_flag       : Byte
                  , f814w_acswfc_flux       : Float
                  , f814w_acswfc_flux_error : Float
                  , f814w_acswfc_mag        : Float
                  , f814w_acswfc_flag       : Byte
                 ) {
  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String) =
    this.getClass.getDeclaredFields.map { f=>
      f.setAccessible(true)
      val v = f.get(this).toString
      f.setAccessible(false)
      v
    }.mkString(sep)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Source.scala
//=============================================================================
