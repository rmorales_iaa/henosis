/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:08m
 * Description: https://gea.esac.esa.int/archive/documentation/GDR3/Gaia_archive/chap_datamodel/sec_dm_solar_system_object_tables/ssec_dm_sso_source.html
 */
//=============================================================================
package com.henosis.database.gaia.source.gaiaDR3.solar_system.sso_reflectance_spectrum
//=============================================================================
import com.common.logger.MyLogger
import com.common.csv.CsvFile._
//=============================================================================
import scala.language.existentials
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object Source extends MyLogger {
  //---------------------------------------------------------------------------
  def getIntArray(r: Array[String], index: Int) = {
    var i = index
    val result = ArrayBuffer[Int]()
    var continue = true
    while (continue) {
      val s = r(i)
      if (s.startsWith("\"[")) result += s.drop(2).toInt
      else {
        if (s.endsWith("]\"")) {
          result += s.dropRight(2).toInt
          continue = false
        }
        else  result += s.toInt
      }
      i += 1
    }
    result.toArray
  }
  //---------------------------------------------------------------------------
  def getDocument(line: String) = {
    val r = line.split(",")
    var index = -1
    //-------------------------------------------------------------------------
    def getNewIndex = {
      index += 1
      index
    }
    //-------------------------------------------------------------------------
    Source(
        getLongOrDefault(r(getNewIndex))  // source_id
      , getLongOrDefault(r(getNewIndex))  //  solution_id
      , getLongOrDefault(r(getNewIndex))  //  number_mp
      , r(getNewIndex).replaceAll("\"","")                       //denomination
      , getShortOrDefault(r(getNewIndex))  //  nb_samples
      , getIntOrDefault(r(getNewIndex))   //  num_of_spectra
      , getFloatOrDefault(r(getNewIndex)) //  reflectance_spectrum
      , getFloatOrDefault(r(getNewIndex)) //  reflectance_spectrum_err
      , getFloatOrDefault(r(getNewIndex)) //  wavelength
      , getByteOrDefault(r(getNewIndex))  //  reflectance_spectrum_flag
    )
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String="") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Source(source_id                          : Long
                  , solution_id                      : Long
                  , number_mp                        : Long
                  , denomination                     : String
                  , nb_samples                       : Short
                  , num_of_spectra                   : Int
                  , reflectance_spectrum             : Float
                  , reflectance_spectrum_err         : Float
                  , wavelength                       : Float
                  , reflectance_spectrum_flag        : Byte
                 )
//=============================================================================
//=============================================================================
//End of file Source.scala
//=============================================================================
