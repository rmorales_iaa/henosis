/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:05m
 * Description: None
 */
//=============================================================================
package com.henosis.database.gaia.source.gaiaDR3.gaia_source
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.database.postgreDB.gaia_dr_3.Source
import com.common.geometry.point.Point2D_Double
import com.common.hardware.cpu.CPU
import com.common.util.parallelTask.ParallelTask
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}

import java.util.concurrent.ConcurrentHashMap
import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object DB {
  //---------------------------------------------------------------------------
  final val POSTGRE_MINIMAL_TABLE_NAME = "public.gaia_source_minimal"
  final val POSTGRE_REDUCED_TABLE_NAME = "public.gaia_source_reduced"

  //---------------------------------------------------------------------------
  final val GAIA_REFERENCE_EPOCH = "2016-01-01T00:00:00"
  //---------------------------------------------------------------------------
  //Matching between paper catalog and GAIA3
  //decimal degree ra: 0.00417 => 1 arcosec (hms)
  //decimal degree dec: 0.000278 => 1 arcosec (dms)
  final val GAIA_QUERY_BY_POSITION_RA_PRECISION = 0.00417d
  final val GAIA_QUERY_BY_POSITION_DEC_PRECISION = GAIA_QUERY_BY_POSITION_RA_PRECISION / 15
  //---------------------------------------------------------------------------
  final val codecRegistry = fromRegistries(fromProviders(classOf[Source]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  private val gaiaDB = DB(MyConf(MyConf.c.getString("Database.gaia_dr_3")))
  //---------------------------------------------------------------------------
  trait QueryItem[T] {
    //---------------------------------------------------------------------------
    val id: String
    val raDec: Point2D_Double
    val item: T
    //---------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private class MyParallelTask(radiusNoPmMas: Double
                               , radiusMas: Double
                               , observingDate: String
                               , seq: Array[QueryItem[_]]
                               , map: ConcurrentHashMap[String, Array[Source]])
    extends ParallelTask[QueryItem[_]](seq
      , threadCount = CPU.getCoreCount()
      , isItemProcessingThreadSafe = true) {
    //-----------------------------------------------------------------------
    def userProcessSingleItem(item: QueryItem[_]): Unit = {
      Try {
        val id = item.id
        val ra = item.raDec.x
        val dec = item.raDec.y

        val gasSeq = gaiaDB.query(
            ra - radiusNoPmMas
          , ra + radiusNoPmMas
          , dec - radiusNoPmMas
          , dec + radiusNoPmMas).results().toArray

        val finalGasSeq = gasSeq.flatMap { gas =>
          val dist = gas.getDistanceTo(Point2D_Double(ra,dec),observingDate)
          if (dist < radiusMas) Some(gas)
          else None
        }
        map.put(id,finalGasSeq)
      }
      match {
        case Success(_) =>
        case Failure(ex) => error(s"Error processing the (ra,dec): '(${item.raDec.x},${item.raDec.y})'" + ex.toString)
      }
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def query(radiusNoPmMas: Double
            , radiusMas: Double
            , observingDate: String
            , seq: Array[QueryItem[_]])= {
    val map = new ConcurrentHashMap[String, Array[Source]]()
    new MyParallelTask(
        radiusNoPmMas
      , radiusMas
      , observingDate
      , seq
      , map)
    map.asScala
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class DB(conf: MyConf) extends MongoDB {
  //---------------------------------------------------------------------------
  val databaseName   = conf.getString("Database.name")
  val collectionName = conf.getString("Database.source.collectionName")
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(DB.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[Source] = database.getCollection(collectionName)
  connected = true
  //-------------------------------------------------------------------------
  def query(minRa: Double
            , maxRa: Double
            , minDec: Double
            , maxDec: Double) =
    collWithRegistry.find(and(
        gte("ra", minRa)
      , lte("ra", maxRa)
      , gte("dec", minDec)
      , lte("dec", maxDec)
    ))
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DB.scala
//=============================================================================

