/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:08m
 * Description: https://gea.esac.esa.int/archive/documentation/GDR3/Gaia_archive/chap_datamodel/sec_dm_solar_system_object_tables/ssec_dm_sso_source.html
 */
//=============================================================================
package com.henosis.database.gaia.source.gaiaDR3.astrophysical.astrophysical_parameters
//=============================================================================
import com.common.csv.CsvFile._
import com.common.logger.MyLogger
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
//=============================================================================
import scala.language.existentials
//=============================================================================
//=============================================================================
object Source extends MyLogger {
  //---------------------------------------------------------------------------
  type GaiaDR_3_Astrophysical_Parameters = Source
  //---------------------------------------------------------------------------
  val codecRegistry = fromRegistries(fromProviders(classOf[Source]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  def getDocument(line: String) = {
    val r = line.split(",")
    var index = -1
    //-------------------------------------------------------------------------
    def getNewIndex(offset: Int = 1) = {
      index += offset
      index
    }
    //-------------------------------------------------------------------------
    Source(
      //    getLongOrDefault(r(getNewIndex())) //solution_id
         getLongOrDefault(r(getNewIndex(2))) //source_id
        /*, getFloatOrDefault(r(getNewIndex())) //classprob_dsc_combmod_quasar
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_combmod_galaxy
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_combmod_star
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_combmod_whitedwarf
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_combmod_binarystar
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_specmod_quasar
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_specmod_galaxy
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_specmod_star
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_specmod_whitedwarf
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_specmod_binarystar
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_allosmod_quasar
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_allosmod_galaxy
        , getFloatOrDefault(r(getNewIndex())) //classprob_dsc_allosmod_star*/
        , getFloatOrDefault(r(getNewIndex(13 + 1))) //teff_gspphot
      //  , getFloatOrDefault(r(getNewIndex())) //teff_gspphot_lower
      //  , getFloatOrDefault(r(getNewIndex())) //teff_gspphot_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //logg_gspphot
      //  , getFloatOrDefault(r(getNewIndex())) //logg_gspphot_lower
      //  , getFloatOrDefault(r(getNewIndex())) //logg_gspphot_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //mh_gspphot
      //  , getFloatOrDefault(r(getNewIndex())) //mh_gspphot_lower
      //  , getFloatOrDefault(r(getNewIndex())) //mh_gspphot_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //distance_gspphot
       // , getFloatOrDefault(r(getNewIndex())) //distance_gspphot_lower
       // , getFloatOrDefault(r(getNewIndex())) //distance_gspphot_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //azero_gspphot
       // , getFloatOrDefault(r(getNewIndex())) //azero_gspphot_lower
      //  , getFloatOrDefault(r(getNewIndex())) //azero_gspphot_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //ag_gspphot
       // , getFloatOrDefault(r(getNewIndex())) //ag_gspphot_lower
       // , getFloatOrDefault(r(getNewIndex())) //ag_gspphot_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //abp_gspphot
       // , getFloatOrDefault(r(getNewIndex())) //abp_gspphot_lower
       // , getFloatOrDefault(r(getNewIndex())) //abp_gspphot_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //arp_gspphot
      //  , getFloatOrDefault(r(getNewIndex())) //arp_gspphot_lower
      //  , getFloatOrDefault(r(getNewIndex())) //arp_gspphot_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //ebpminrp_gspphot
      //  , getFloatOrDefault(r(getNewIndex())) //ebpminrp_gspphot_lower
      //  , getFloatOrDefault(r(getNewIndex())) //ebpminrp_gspphot_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //mg_gspphot
      //  , getFloatOrDefault(r(getNewIndex())) //mg_gspphot_lower
      //  , getFloatOrDefault(r(getNewIndex())) //mg_gspphot_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //radius_gspphot
      /*  , getFloatOrDefault(r(getNewIndex())) //radius_gspphot_lower
        , getFloatOrDefault(r(getNewIndex())) //radius_gspphot_upper
        , getFloatOrDefault(r(getNewIndex())) //logposterior_gspphot
        , getFloatOrDefault(r(getNewIndex())) //mcmcaccept_gspphot
        , r(getNewIndex) //libname_gspphot
        , getFloatOrDefault(r(getNewIndex())) //teff_gspspec
        , getFloatOrDefault(r(getNewIndex())) //teff_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //teff_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //logg_gspspec
        , getFloatOrDefault(r(getNewIndex())) //logg_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //logg_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //mh_gspspec
        , getFloatOrDefault(r(getNewIndex())) //mh_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //mh_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //alphafe_gspspec
        , getFloatOrDefault(r(getNewIndex())) //alphafe_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //alphafe_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //fem_gspspec
        , getFloatOrDefault(r(getNewIndex())) //fem_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //fem_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //fem_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //fem_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //sife_gspspec
        , getFloatOrDefault(r(getNewIndex())) //sife_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //sife_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //sife_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //sife_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //cafe_gspspec
        , getFloatOrDefault(r(getNewIndex())) //cafe_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //cafe_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //cafe_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //cafe_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //tife_gspspec
        , getFloatOrDefault(r(getNewIndex())) //tife_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //tife_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //tife_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //tife_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //mgfe_gspspec
        , getFloatOrDefault(r(getNewIndex())) //mgfe_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //mgfe_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //mgfe_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //mgfe_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //ndfe_gspspec
        , getFloatOrDefault(r(getNewIndex())) //ndfe_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //ndfe_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //ndfe_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //ndfe_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //feiim_gspspec
        , getFloatOrDefault(r(getNewIndex())) //feiim_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //feiim_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //feiim_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //feiim_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //sfe_gspspec
        , getFloatOrDefault(r(getNewIndex())) //sfe_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //sfe_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //sfe_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //sfe_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //zrfe_gspspec
        , getFloatOrDefault(r(getNewIndex())) //zrfe_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //zrfegspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //zrfe_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //zrfe_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //nfe_gspspec
        , getFloatOrDefault(r(getNewIndex())) //nfe_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //nfe_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //nfe_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //nfe_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //crfe_gspspec
        , getFloatOrDefault(r(getNewIndex())) //crfe_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //crfe_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //crfe_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //crfe_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //cefe_gspspec
        , getFloatOrDefault(r(getNewIndex())) //cefe_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //cefe_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //cefe_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //cefe_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //nife_gspspec
        , getFloatOrDefault(r(getNewIndex())) //nife_gspspec_lower
        , getFloatOrDefault(r(getNewIndex())) //nife_gspspec_upper
        , getFloatOrDefault(r(getNewIndex())) //nife_gspspec_nlines
        , getFloatOrDefault(r(getNewIndex())) //nife_gspspec_linescatter
        , getFloatOrDefault(r(getNewIndex())) //cn0ew_gspspec
        , getFloatOrDefault(r(getNewIndex())) //cn0ew_gspspec_uncertainty
        , getFloatOrDefault(r(getNewIndex())) //cn0_gspspec_centralline
        , getFloatOrDefault(r(getNewIndex())) //cn0_gspspec_width
        , getFloatOrDefault(r(getNewIndex())) //dib_gspspec_lambda
        , getFloatOrDefault(r(getNewIndex())) //dib_gspspec_lambda_uncertainty
        , getFloatOrDefault(r(getNewIndex())) //dibew_gspspec
        , getFloatOrDefault(r(getNewIndex())) //dibew_gspspec_uncertainty
        , getFloatOrDefault(r(getNewIndex())) //dibewnoise_gspspec_uncertainty
        , getFloatOrDefault(r(getNewIndex())) //dibp0_gspspec
        , getFloatOrDefault(r(getNewIndex())) //dibp2_gspspec
        , getFloatOrDefault(r(getNewIndex())) //dibp2_gspspec_uncertainty
        , getIntOrDefault(r(getNewIndex())) //dibqf_gspspec
        , r(getNewIndex) //flags_gspspec
        , getFloatOrDefault(r(getNewIndex())) //logchisq_gspspec
        , getFloatOrDefault(r(getNewIndex())) //ew_espels_halpha
        , getFloatOrDefault(r(getNewIndex())) //ew_espels_halpha_uncertainty
        , r(getNewIndex) //ew_espels_halpha_flag
        , getFloatOrDefault(r(getNewIndex())) //ew_espels_halpha_model
        , r(getNewIndex) //classlabel_espels
        , r(getNewIndex) //classlabel_espels_flag
        , getFloatOrDefault(r(getNewIndex())) //classprob_espels_wcstar
        , getFloatOrDefault(r(getNewIndex())) //classprob_espels_wnstar
        , getFloatOrDefault(r(getNewIndex())) //classprob_espels_bestar
        , getFloatOrDefault(r(getNewIndex())) //classprob_espels_ttauristar
        , getFloatOrDefault(r(getNewIndex())) //classprob_espels_herbigstar
        , getFloatOrDefault(r(getNewIndex())) //classprob_espels_dmestar
        , getFloatOrDefault(r(getNewIndex())) //classprob_espels_pne
        , getFloatOrDefault(r(getNewIndex())) //azero_esphs
        , getFloatOrDefault(r(getNewIndex())) //azero_esphs_uncertainty
        , getFloatOrDefault(r(getNewIndex())) //ag_esphs
        , getFloatOrDefault(r(getNewIndex())) //ag_esphs_uncertainty
        , getFloatOrDefault(r(getNewIndex())) //ebpminrp_esphs
        , getFloatOrDefault(r(getNewIndex())) //ebpminrp_esphs_uncertainty
        , getFloatOrDefault(r(getNewIndex())) //teff_esphs
        , getFloatOrDefault(r(getNewIndex())) //teff_esphs_uncertainty
        , getFloatOrDefault(r(getNewIndex())) //logg_esphs
        , getFloatOrDefault(r(getNewIndex())) //logg_esphs_uncertainty
        , getFloatOrDefault(r(getNewIndex())) //vsini_esphs
        , getFloatOrDefault(r(getNewIndex())) //vsini_esphs_uncertainty
        , r(getNewIndex) //flags_esphs
        , r(getNewIndex) //spectraltype_esphs
        , getFloatOrDefault(r(getNewIndex())) //activityindex_espcs
        , getFloatOrDefault(r(getNewIndex())) //activityindex_espcs_uncertainty
        , r(getNewIndex) //activityindex_espcs_input
        , getFloatOrDefault(r(getNewIndex())) //teff_espucd
        , getFloatOrDefault(r(getNewIndex())) //teff_espucd_uncertainty
        , r(getNewIndex) //flags_espucd*/
        , getFloatOrDefault(r(getNewIndex(130 + 1))) //radius_flame
        //, getFloatOrDefault(r(getNewIndex())) //radius_flame_lower
      //, getFloatOrDefault(r(getNewIndex())) //radius_flame_upper
        , getFloatOrDefault(r(getNewIndex(2 + 1))) //lum_flame
        , getFloatOrDefault(r(getNewIndex(1))) //lum_flame_lower
        , getFloatOrDefault(r(getNewIndex(1))) //lum_flame_upper
        , getFloatOrDefault(r(getNewIndex(1))) //mass_flame
       /* , getFloatOrDefault(r(getNewIndex())) //mass_flame_lower
        , getFloatOrDefault(r(getNewIndex())) //mass_flame_upper
        , getFloatOrDefault(r(getNewIndex())) //age_flame
        , getFloatOrDefault(r(getNewIndex())) //age_flame_lower
        , getFloatOrDefault(r(getNewIndex())) //age_flame_upper
        , r(getNewIndex) //flags_flame
        , getIntOrDefault(r(getNewIndex())) //evolstage_flame
        , getFloatOrDefault(r(getNewIndex())) //gravredshift_flame
        , getFloatOrDefault(r(getNewIndex())) //gravredshift_flame_lower
        , getFloatOrDefault(r(getNewIndex())) //gravredshift_flame_upper*/
        , getFloatOrDefault(r(getNewIndex(10 + 1))) //bc_flame
      /*  , getFloatOrDefault(r(getNewIndex())) //mh_msc
        , getFloatOrDefault(r(getNewIndex())) //mh_msc_upper
        , getFloatOrDefault(r(getNewIndex())) //mh_msc_lower
        , getFloatOrDefault(r(getNewIndex())) //azero_msc
        , getFloatOrDefault(r(getNewIndex())) //azero_msc_upper
        , getFloatOrDefault(r(getNewIndex())) //azero_msc_lower
        , getFloatOrDefault(r(getNewIndex())) //distance_msc
        , getFloatOrDefault(r(getNewIndex())) //distance_msc_upper
        , getFloatOrDefault(r(getNewIndex())) //distance_msc_lower
        , getFloatOrDefault(r(getNewIndex())) //teff_msc1
        , getFloatOrDefault(r(getNewIndex())) //teff_msc1_upper
        , getFloatOrDefault(r(getNewIndex())) //teff_msc1_lower
        , getFloatOrDefault(r(getNewIndex())) //teff_msc2
        , getFloatOrDefault(r(getNewIndex())) //teff_msc2_upper
        , getFloatOrDefault(r(getNewIndex())) //teff_msc2_lower
        , getFloatOrDefault(r(getNewIndex())) //logg_msc1
        , getFloatOrDefault(r(getNewIndex())) //logg_msc1_upper
        , getFloatOrDefault(r(getNewIndex())) //logg_msc2
        , getFloatOrDefault(r(getNewIndex())) //logg_msc2_upper
        , getFloatOrDefault(r(getNewIndex())) //logg_msc2_lower
        , getFloatOrDefault(r(getNewIndex())) //ag_msc
        , getFloatOrDefault(r(getNewIndex())) //ag_msc_upper
        , getFloatOrDefault(r(getNewIndex())) //ag_msc_lower
        , getFloatOrDefault(r(getNewIndex())) //logposterior_msc
        , getFloatOrDefault(r(getNewIndex())) //mcmcaccept_msc
        , getFloatOrDefault(r(getNewIndex())) //mcmcdrift_msc
        , r(getNewIndex) //flags_msc
        , getLongOrDefault(r(getNewIndex())) //neuron_oa_id
        , getFloatOrDefault(r(getNewIndex())) //neuron_oa_dist
        , getFloatOrDefault(r(getNewIndex())) //neuron_oa_dist_percentile_rank
        , r(getNewIndex) //flags_oa: String*/
    )
  }
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String = "") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }

  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[Source].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Source(//solution_id: Long
                  _id: Long
                  /* , classprob_dsc_combmod_quasar: Float
                   , classprob_dsc_combmod_galaxy: Float
                   , classprob_dsc_combmod_star: Float
                   , classprob_dsc_combmod_whitedwarf: Float
                   , classprob_dsc_combmod_binarystar: Float
                   , classprob_dsc_specmod_quasar: Float
                   , classprob_dsc_specmod_galaxy: Float
                   , classprob_dsc_specmod_star: Float
                   , classprob_dsc_specmod_whitedwarf: Float
                   , classprob_dsc_specmod_binarystar: Float
                   , classprob_dsc_allosmod_quasar: Float
                   , classprob_dsc_allosmod_galaxy: Float
                   , classprob_dsc_allosmod_star: Float*/
                  , teff_gspphot: Float
                  //, teff_gspphot_lower: Float
                  //, teff_gspphot_upper: Float
                  , logg_gspphot: Float
                  //, logg_gspphot_lower: Float
                  //, logg_gspphot_upper: Float
                  , mh_gspphot: Float
                  //, mh_gspphot_lower: Float
                  //, mh_gspphot_upper: Float
                  , distance_gspphot: Float
                  //, distance_gspphot_lower: Float
                  //, distance_gspphot_upper: Float
                  , azero_gspphot: Float
                  //, azero_gspphot_lower: Float
                  //, azero_gspphot_upper: Float
                  , ag_gspphot: Float
                  //, ag_gspphot_lower: Float
                  //, ag_gspphot_upper: Float
                  , abp_gspphot: Float
                  //, abp_gspphot_lower: Float
                  //, abp_gspphot_upper: Float
                  , arp_gspphot: Float
                  //, arp_gspphot_lower: Float
                  //, arp_gspphot_upper: Float
                  , ebpminrp_gspphot: Float
                  //, ebpminrp_gspphot_lower: Float
                  //, ebpminrp_gspphot_upper: Float
                  , mg_gspphot: Float
                  //, mg_gspphot_lower: Float
                  //, mg_gspphot_upper: Float
                  , radius_gspphot: Float
                  //, radius_gspphot_lower: Float
                  //, radius_gspphot_upper: Float
                  /*, logposterior_gspphot: Float
                  , mcmcaccept_gspphot: Float
                  , libname_gspphot: String
                  , teff_gspspec: Float
                  , teff_gspspec_lower: Float
                  , teff_gspspec_upper: Float
                  , logg_gspspec: Float
                  , logg_gspspec_lower: Float
                  , logg_gspspec_upper: Float
                  , mh_gspspec: Float
                  , mh_gspspec_lower: Float
                  , mh_gspspec_upper: Float
                  , alphafe_gspspec: Float
                  , alphafe_gspspec_lower: Float
                  , alphafe_gspspec_upper: Float
                  , fem_gspspec: Float
                  , fem_gspspec_lower: Float
                  , fem_gspspec_upper: Float
                  , fem_gspspec_nlines: Float
                  , fem_gspspec_linescatter: Float
                  , sife_gspspec: Float
                  , sife_gspspec_lower: Float
                  , sife_gspspec_upper: Float
                  , sife_gspspec_nlines: Float
                  , sife_gspspec_linescatter: Float
                  , cafe_gspspec: Float
                  , cafe_gspspec_lower: Float
                  , cafe_gspspec_upper: Float
                  , cafe_gspspec_nlines: Float
                  , cafe_gspspec_linescatter: Float
                  , tife_gspspec: Float
                  , tife_gspspec_lower: Float
                  , tife_gspspec_upper: Float
                  , tife_gspspec_nlines: Float
                  , tife_gspspec_linescatter: Float
                  , mgfe_gspspec: Float
                  , mgfe_gspspec_lower: Float
                  , mgfe_gspspec_upper: Float
                  , mgfe_gspspec_nlines: Float
                  , mgfe_gspspec_linescatter: Float
                  , ndfe_gspspec: Float
                  , ndfe_gspspec_lower: Float
                  , ndfe_gspspec_upper: Float
                  , ndfe_gspspec_nlines: Float
                  , ndfe_gspspec_linescatter: Float
                  , feiim_gspspec: Float
                  , feiim_gspspec_lower: Float
                  , feiim_gspspec_upper: Float
                  , feiim_gspspec_nlines: Float
                  , feiim_gspspec_linescatter: Float
                  , sfe_gspspec: Float
                  , sfe_gspspec_lower: Float
                  , sfe_gspspec_upper: Float
                  , sfe_gspspec_nlines: Float
                  , sfe_gspspec_linescatter: Float
                  , zrfe_gspspec: Float
                  , zrfe_gspspec_lower: Float
                  , zrfegspspec_upper: Float
                  , zrfe_gspspec_nlines: Float
                  , zrfe_gspspec_linescatter: Float
                  , nfe_gspspec: Float
                  , nfe_gspspec_lower: Float
                  , nfe_gspspec_upper: Float
                  , nfe_gspspec_nlines: Float
                  , nfe_gspspec_linescatter: Float
                  , crfe_gspspec: Float
                  , crfe_gspspec_lower: Float
                  , crfe_gspspec_upper: Float
                  , crfe_gspspec_nlines: Float
                  , crfe_gspspec_linescatter: Float
                  , cefe_gspspec: Float
                  , cefe_gspspec_lower: Float
                  , cefe_gspspec_upper: Float
                  , cefe_gspspec_nlines: Float
                  , cefe_gspspec_linescatter: Float
                  , nife_gspspec: Float
                  , nife_gspspec_lower: Float
                  , nife_gspspec_upper: Float
                  , nife_gspspec_nlines: Float
                  , nife_gspspec_linescatter: Float
                  , cn0ew_gspspec: Float
                  , cn0ew_gspspec_uncertainty: Float
                  , cn0_gspspec_centralline: Float
                  , cn0_gspspec_width: Float
                  , dib_gspspec_lambda: Float
                  , dib_gspspec_lambda_uncertainty: Float
                  , dibew_gspspec: Float
                  , dibew_gspspec_uncertainty: Float
                  , dibewnoise_gspspec_uncertainty: Float
                  , dibp0_gspspec: Float
                  , dibp2_gspspec: Float
                  , dibp2_gspspec_uncertainty: Float
                  , dibqf_gspspec: Int
                  , flags_gspspec: String
                  , logchisq_gspspec: Float
                  , ew_espels_halpha: Float
                  , ew_espels_halpha_uncertainty: Float
                  , ew_espels_halpha_flag: String
                  , ew_espels_halpha_model: Float
                  , classlabel_espels: String
                  , classlabel_espels_flag: String
                  , classprob_espels_wcstar: Float
                  , classprob_espels_wnstar: Float
                  , classprob_espels_bestar: Float
                  , classprob_espels_ttauristar: Float
                  , classprob_espels_herbigstar: Float
                  , classprob_espels_dmestar: Float
                  , classprob_espels_pne: Float
                  , azero_esphs: Float
                  , azero_esphs_uncertainty: Float
                  , ag_esphs: Float
                  , ag_esphs_uncertainty: Float
                  , ebpminrp_esphs: Float
                  , ebpminrp_esphs_uncertainty: Float
                  , teff_esphs: Float
                  , teff_esphs_uncertainty: Float
                  , logg_esphs: Float
                  , logg_esphs_uncertainty: Float
                  , vsini_esphs: Float
                  , vsini_esphs_uncertainty: Float
                  , flags_esphs: String
                  , spectraltype_esphs: String
                  , activityindex_espcs: Float
                  , activityindex_espcs_uncertainty: Float
                  , activityindex_espcs_input: String
                  , teff_espucd: Float
                  , teff_espucd_uncertainty: Float
                  , flags_espucd: String*/
                  , radius_flame: Float
                  //, radius_flame_lower: Float
                  //, radius_flame_upper: Float
                  , lum_flame: Float
                  , lum_flame_lower: Float
                  , lum_flame_upper: Float
                  , mass_flame: Float
                  /*, mass_flame_lower: Float
                  , mass_flame_upper: Float
                  , age_flame: Float
                  , age_flame_lower: Float
                  , age_flame_upper: Float
                  , flags_flame: String
                  , evolstage_flame: Int
                  , gravredshift_flame: Float
                  , gravredshift_flame_lower: Float
                  , gravredshift_flame_upper: Float*/
                  , bc_flame: Float
                  /*, mh_msc: Float
                  , mh_msc_upper: Float
                  , mh_msc_lower: Float
                  , azero_msc: Float
                  , azero_msc_upper: Float
                  , azero_msc_lower: Float
                  , distance_msc: Float
                  , distance_msc_upper: Float
                  , distance_msc_lower: Float
                  , teff_msc1: Float
                  , teff_msc1_upper: Float
                  , teff_msc1_lower: Float
                  , teff_msc2: Float
                  , teff_msc2_upper: Float
                  , teff_msc2_lower: Float
                  , logg_msc1: Float
                  , logg_msc1_upper: Float
                  , logg_msc2: Float
                  , logg_msc2_upper: Float
                  , logg_msc2_lower: Float
                  , ag_msc: Float
                  , ag_msc_upper: Float
                  , ag_msc_lower: Float
                  , logposterior_msc: Float
                  , mcmcaccept_msc: Float
                  , mcmcdrift_msc: Float
                  , flags_msc: String
                  , neuron_oa_id: Long
                  , neuron_oa_dist: Float
                  , neuron_oa_dist_percentile_rank: Float
                  , flags_oa: String*/
                 ) {
  //---------------------------------------------------------------------------
  def getAsCsvLine(sep: String) =
    this.getClass.getDeclaredFields.map { f=>
      f.setAccessible(true)
      val v = f.get(this).toString
      f.setAccessible(false)
      v
    }.mkString(sep)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Source.scala
//=============================================================================
