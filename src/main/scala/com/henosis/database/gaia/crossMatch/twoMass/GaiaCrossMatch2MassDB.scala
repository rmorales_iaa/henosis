/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:49m
 * Description: https://gea.esac.esa.int/archive/documentation/GEDR3/Gaia_archive/chap_datamodel/sec_dm_crossmatches/ssec_dm_tmass_psc_xsc_best_neighbour.html
 */
//=============================================================================
package com.henosis.database.gaia.crossMatch.twoMass
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers._
import com.common.database.mongoDB.MongoDB
import org.mongodb.scala.model.Filters.{equal}
//=============================================================================
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object GaiaCrossMatch2MassDB {
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): GaiaCrossMatch2MassDB = {
    GaiaCrossMatch2MassDB(conf.getString("Database.name")
                    , conf.getString("Database.crossMatch.2Mass.collectionName"))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class GaiaCrossMatch2MassDB(databaseName: String, collectionName: String ) extends MongoDB {
  //---------------------------------------------------------------------------
  // class variable
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = init()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3

  connected = true
  //-------------------------------------------------------------------------
  def matchSource(twoMassID: String) : Seq[Document] = {
    collection.find(
        equal("original_ext_source_id", twoMassID)
      )
      .getResultDocumentSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GaiaCrossMatch2MassDB.scala
//=============================================================================
