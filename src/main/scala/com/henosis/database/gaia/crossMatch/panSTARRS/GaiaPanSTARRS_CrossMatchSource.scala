/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Aug/2021
 * Time:  10h:19m
 * Description: None
 */
//=============================================================================
package com.henosis.database.gaia.crossMatch.panSTARRS
//=============================================================================
import com.henosis.database.gaia.crossMatch.panSTARRS.FileEntry.COL_NAME_PANSTARRS_1_ID
//=============================================================================
import org.mongodb.scala.Document
//=============================================================================
//=============================================================================
object GaiaPanSTARRS_CrossMatchSource {
  //---------------------------------------------------------------------------
  def apply(doc: Document): GaiaPanSTARRS_CrossMatchSource = GaiaPanSTARRS_CrossMatchSource(
    doc("source_id").asInt64().getValue
    , doc("clean_panstarrs1_oid").asInt64().getValue
    , doc(COL_NAME_PANSTARRS_1_ID).asInt64().getValue
    , doc("angular_distance").asDouble().getValue
    , doc("number_of_neighbours").asInt32().getValue
    , doc("number_of_mates").asInt32().getValue
    , doc("xm_flag").asInt32().getValue
  )
  //-------------------------------------------------------------------------
  def getInfo(docSeq: Seq[Document]) = {
    if (docSeq.isEmpty) s"\n\t'GAIA EDR3 cross match PanSTARSS' is EMPTY!!!"
    else {
      docSeq.zipWithIndex.map { case (doc,i) =>
        val source = GaiaPanSTARRS_CrossMatchSource(doc)
        s"\n\t'------ PanSTARSS candidate source $i -------" +
          s"\n\t'GAIA EDR3 2Mass cross match' GaiaDR3 ID         : ${source.source_id}" +
          s"\n\t'GAIA EDR3 2Mass cross match' 2Mass ID        : ${source.original_ext_source_id}" +
          s"\n\t'GAIA EDR3 2Mass cross match' Neighbours      : ${source.number_of_neighbours}" +
          s"\n\t'GAIA EDR3 2Mass cross match' Number of mates : ${source.number_of_mates}" +
          s"\n\t'--------------------------------------------"
      }.mkString
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class GaiaPanSTARRS_CrossMatchSource(source_id: Long
                                          , clean_panstarrs1_oid: Long
                                          , original_ext_source_id: Long
                                          , angular_distance: Double
                                          , number_of_neighbours: Int
                                          , number_of_mates: Int
                                          , xm_flag:Int
                                       )
//=============================================================================
//End of file GaiaPanSTARRS_CrossMatchSource.scala
//=============================================================================
