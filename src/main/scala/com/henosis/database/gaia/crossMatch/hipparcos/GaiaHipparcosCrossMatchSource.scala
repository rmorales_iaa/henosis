/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:35m
 * Description: None
 */
//=============================================================================
package com.henosis.database.gaia.crossMatch.hipparcos
//=============================================================================
import org.mongodb.scala.Document
//=============================================================================
//=============================================================================
object GaiaHipparcosCrossMatchSource {
  //---------------------------------------------------------------------------
  def apply(doc: Document): GaiaHipparcosCrossMatchSource = GaiaHipparcosCrossMatchSource(
    doc("source_id").asInt64().getValue
    , doc("original_ext_source_id").asString().getValue
    , doc("angular_distance").asDouble().getValue
    , doc("number_of_neighbours").asInt32().getValue
    , doc("xm_flag").asInt32().getValue
  )
  //-------------------------------------------------------------------------
  def getInfo(hipparcosDocSec: Seq[Document]) = {
    if (hipparcosDocSec.isEmpty) s"\n\t'Hipparcos' candidate sources is EMPTY!!!"
    else {
      hipparcosDocSec.zipWithIndex.map { case (doc,i) =>
        val source = GaiaHipparcosCrossMatchSource(doc)
        s"\n\t'------ Hipparcos candidate source $i -------" +
          s"\n\t'Hipparcos' ID             : ${source.original_ext_source_id}" +
          s"\n\t'Hipparcos' GaiaDR3 EDR 3 ID  : ${source.source_id}" +
          s"\n\t'Hipparcos' Neighbours     : ${source.number_of_neighbours}" +
          s"\n\t'Hipparcos' xm flag        : ${source.xm_flag}" +
          s"\n\t'--------------------------------------------"
      }.mkString
    }
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
case class GaiaHipparcosCrossMatchSource(source_id : Long
                                         , original_ext_source_id : String
                                         , angular_distance : Double
                                         , number_of_neighbours: Int
                                         , xm_flag : Int)
//=============================================================================
//End of file Gaia_2MassCrossMatchSource.scala
//=============================================================================
