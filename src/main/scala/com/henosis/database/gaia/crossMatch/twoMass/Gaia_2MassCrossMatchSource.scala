/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  21h:35m
 * Description: None
 */
//=============================================================================
package com.henosis.database.gaia.crossMatch.twoMass
//=============================================================================
import com.henosis.database.gaia.crossMatch.twoMass.FileEntry.COL_NAME_2MASS_ID
//=============================================================================
import org.mongodb.scala.Document
//=============================================================================
//=============================================================================
object Gaia_2MassCrossMatchSource {
  //---------------------------------------------------------------------------
  def apply(doc: Document) : Gaia_2MassCrossMatchSource = Gaia_2MassCrossMatchSource(
    doc("source_id").asInt64().getValue
    , doc(COL_NAME_2MASS_ID).asString().getValue
    , doc("angular_distance").asDouble().getValue
    , doc("xm_flag").asInt32().getValue
    , doc("clean_tmass_psc_xsc_oid").asInt32().getValue
    , doc("number_of_neighbours").asInt32().getValue
    , doc("number_of_mates").asInt32().getValue
  )
  //-------------------------------------------------------------------------
  def getInfo(docSeq: Seq[Document]) = {
    if (docSeq.isEmpty) s"\n\t'GAIA EDR3 cross match 2Mass' is EMPTY!!!"
    else {
      docSeq.zipWithIndex.map { case (doc,i) =>
        val source = Gaia_2MassCrossMatchSource(doc)
        s"\n\t'------ GaiaDR3 source candidate $i ---------" +
          s"\n\t'GAIA EDR3 2Mass cross match' GaiaDR3 ID         : ${source.source_id}" +
          s"\n\t'GAIA EDR3 2Mass cross match' 2Mass ID        : ${source.original_ext_source_id}" +
          s"\n\t'GAIA EDR3 2Mass cross match' Neighbours      : ${source.number_of_neighbours}" +
          s"\n\t'GAIA EDR3 2Mass cross match' Number of mates : ${source.number_of_mates}" +
          s"\n\t'----------------------------------------"
      }.mkString
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class Gaia_2MassCrossMatchSource(source_id : Long
                                      , original_ext_source_id : String
                                      , angular_distance : Double
                                      , xm_flag : Int
                                      , clean_tmass_psc_xsc_oid : Int
                                      , number_of_neighbours: Int
                                      , number_of_mates : Int)
//=============================================================================
//End of file Gaia_2MassCrossMatchSource.scala
//=============================================================================
