/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:49m
 * Description: https://gea.esac.esa.int/archive/documentation/GEDR3/Gaia_archive/chap_datamodel/sec_dm_crossmatches/ssec_dm_panstarrs1_best_neighbour.html
 */
//=============================================================================
package com.henosis.database.gaia.crossMatch.panSTARRS
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers._
import com.common.database.mongoDB.MongoDB
import org.mongodb.scala.model.Filters.equal
//=============================================================================
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object  GaiaCrossMatchPanSTARRS_DB {
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): GaiaCrossMatchPanSTARRS_DB = {
    GaiaCrossMatchPanSTARRS_DB(conf.getString("Database.name")
      , conf.getString("Database.crossMatch.panstarrs.collectionName"))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class GaiaCrossMatchPanSTARRS_DB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  // class variable
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = init()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  connected = true
  //-------------------------------------------------------------------------
  def matchSource(gaiaID: Long) : Seq[Document] = {
    collection.find(
      equal("source_id", gaiaID)
    )
    .getResultDocumentSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GaiaCrossMatch2MassDB.scala
//=============================================================================
