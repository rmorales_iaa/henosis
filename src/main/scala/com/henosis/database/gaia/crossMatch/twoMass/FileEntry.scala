package com.henosis.database.gaia.crossMatch.twoMass
//=============================================================================
import com.common.logger.MyLogger
import com.common.LineEntry
//=============================================================================
//=============================================================================

//https://gea.esac.esa.int/archive/documentation/GEDR3/Gaia_archive/chap_datamodel/sec_dm_crossmatches/ssec_dm_tmass_psc_xsc_join.html
object FileEntry extends MyLogger {
  //---------------------------------------------------------------------------
  final val COL_NAME_2MASS_ID = "original_ext_source_id"
  //---------------------------------------------------------------------------
  val COL_DEFINITION_SEQ = Seq(
      LineEntry("source_id","long")
    , LineEntry("original_ext_source_id","string")
    , LineEntry("angular_distance","double")
    , LineEntry("xm_flag","int")
    , LineEntry("clean_tmass_psc_xsc_oid","int")
    , LineEntry("gaia_number_of_neighbours","int")
    , LineEntry("gaia_number_of_mates","int")
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Line.scala
//=============================================================================