/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Aug/2021
 * Time:  14h:46m
 * Description: None
 */
//=============================================================================
package com.henosis.database.gaia.crossMatch.hipparcos
//=============================================================================
import com.common.DatabaseImporter
import com.common.configuration.MyConf
import com.common.hardware.cpu.CPU
//=============================================================================
//=============================================================================
case class HipparcosDB_Importer(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = GaiaCrossMatchHipparcosDB(conf)
  val maxBufferedDocument = conf.getInt("Database.crossMatch.hipparcos.maxBufferedDocument")
  val colNameSeq = FileEntry.COL_DEFINITION_SEQ
  val lineDivider: String = ","
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    mongoDB.createIndexes(Array("source_id"))
  }
  //---------------------------------------------------------------------------
  run()
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file TwoMassDB_Importer.scala
//=============================================================================
