package com.henosis.database.gaia.crossMatch.panSTARRS
//=============================================================================
import com.common.logger.MyLogger
import com.common.LineEntry
//=============================================================================
//=============================================================================
//https://gea.esac.esa.int/archive/documentation/GEDR3/Gaia_archive/chap_datamodel/sec_dm_crossmatches/ssec_dm_tmass_psc_xsc_join.html
object FileEntry extends MyLogger {
  //---------------------------------------------------------------------------
  final val COL_NAME_PANSTARRS_1_ID = "original_ext_source_id"
  //---------------------------------------------------------------------------
  val COL_DEFINITION_SEQ = Seq(
      LineEntry("source_id","long")
    , LineEntry("clean_panstarrs1_oid","long")
    , LineEntry("original_ext_source_id","long")
    , LineEntry("angular_distance","double")
    , LineEntry("gaia_number_of_neighbours","int")
    , LineEntry("gaia_number_of_mates","int")
    , LineEntry("xm_flag","int")
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Line.scala
//=============================================================================