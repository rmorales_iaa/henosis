/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  27/Aug/2021
 * Time:  14h:46m
 * Description: None
 */
//=============================================================================
package com.henosis.database.gaia.crossMatch.twoMass
//=============================================================================
import com.common.configuration.MyConf
import com.common.DatabaseImporter
import com.common.hardware.cpu.CPU
//=============================================================================
//=============================================================================
case class TwoMassDB_Importer(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = GaiaCrossMatch2MassDB(conf)
  val maxBufferedDocument = MyConf.c.getInt("Database.mongoDB.gedr3.crossMatch.hipparcos.maxBufferedDocument")
  val colNameSeq = FileEntry.COL_DEFINITION_SEQ
  val lineDivider: String = ","
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    info(s"Please create index manually on '${FileEntry.COL_NAME_2MASS_ID}'")
  }
  //---------------------------------------------------------------------------
  run()
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file TwoMassDB_Importer.scala
//=============================================================================
