package com.henosis.database.gaia.crossMatch.hipparcos
//=============================================================================
import com.common.LineEntry
import com.common.logger.MyLogger
//=============================================================================
//=============================================================================
//https://gea.esac.esa.int/archive/documentation/GEDR3/Gaia_archive/chap_datamodel/sec_dm_crossmatches/ssec_dm_hipparcos2_best_neighbour.html
object FileEntry extends MyLogger {
  //---------------------------------------------------------------------------
  final val COL_NAME_HIPPARCOS_ID = "original_ext_source_id"
  //---------------------------------------------------------------------------
  val COL_DEFINITION_SEQ = Seq(
      LineEntry("source_id","long")
    , LineEntry("original_ext_source_id","string")
    , LineEntry("angular_distance","double")
    , LineEntry("number_of_neighbours","int")
    , LineEntry("xm_flag","int")
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Line.scala
//=============================================================================