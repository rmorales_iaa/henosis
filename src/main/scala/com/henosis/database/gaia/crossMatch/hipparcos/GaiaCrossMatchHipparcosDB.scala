/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:49m
 * Description: https://gea.esac.esa.int/archive/documentation/GEDR3/Gaia_archive/chap_datamodel/sec_dm_crossmatches/ssec_dm_tmass_psc_xsc_best_neighbour.html
 */
//=============================================================================
package com.henosis.database.gaia.crossMatch.hipparcos
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers._
import com.common.database.mongoDB.MongoDB
import org.mongodb.scala.model.Filters.equal
//=============================================================================
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object GaiaCrossMatchHipparcosDB {
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): GaiaCrossMatchHipparcosDB = {
    GaiaCrossMatchHipparcosDB(conf.getString("Database.name")
      , conf.getString("Database.crossMatch.hipparcos.collectionName"))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class GaiaCrossMatchHipparcosDB(databaseName: String, collectionName: String ) extends MongoDB {
  //---------------------------------------------------------------------------
  // class variable
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = init()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3

  connected = true
  //-------------------------------------------------------------------------
  def matchSource(gaiaID: Long) : Seq[Document] = {
    collection.find(
        equal("source_id", gaiaID)
      )
      .getResultDocumentSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GaiaCrossMatch2MassDB.scala
//=============================================================================
