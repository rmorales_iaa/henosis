/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  17/Jun/2022
  * Time:  13h:22m
  * Description: None
  */
//=============================================================================
package com.henosis.database.gaia.source.gaiaDR4.gaia_source
//=============================================================================
import com.common.DatabaseImporter.GZ_EXTENSION_FILE
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.{DatabaseImporter, LineEntry}
import com.common.hardware.cpu.CPU
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import java.io.{BufferedReader}
import scala.collection.mutable.ArrayBuffer
import java.io.{FileInputStream, InputStreamReader}
import java.util.zip.GZIPInputStream
//=============================================================================
//=============================================================================
object  DB_Importer {
  //---------------------------------------------------------------------------
  def processFile(path: String
                  , coll: MongoCollection[Source]
                  , maxBufferedDocument:  Int): Boolean = {

    val decoder =  new InputStreamReader(new GZIPInputStream(new FileInputStream(path)))
    val br = new BufferedReader(decoder)
    val sourceBuffer = ArrayBuffer[Source]()
    var line = br.readLine()
    line = br.readLine()  //avoid header
    while(line != null &&  line.length()!=0 ) {
      if (!line.isEmpty && !line.startsWith("#") && !line.startsWith("solution_id")) {
        val source = Source.getDocument(line)
        sourceBuffer += source
        if (sourceBuffer.length > maxBufferedDocument) {
          coll.insertMany(sourceBuffer.toArray).results()
          sourceBuffer.clear()
        }
      }
      line = br.readLine()
    }
    br.close()

    //store the remain buffered docs
    if (sourceBuffer.length > 0) {
      coll.insertMany(sourceBuffer.toArray).results()
      sourceBuffer.clear()
    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class DB_Importer(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = DB(conf)
  val maxBufferedDocument = conf.getInt("Database.source.maxBufferedDocument")
  val colNameSeq = Array[LineEntry]() //not used
  val lineDivider: String = ","
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[Source] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Please create indexes manually using a local connection to the database:\nuse GAIA_3\ndb.gaia_source.createIndex( { \"ra\": 1, \"dec\": 1 } , {unique: true})")
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing file: '$path'")
    DB_Importer.processFile(path, collWithRegistry, maxBufferedDocument)
  }
  //---------------------------------------------------------------------------
  run(fileExtension = GZ_EXTENSION_FILE)
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file DB_Importer.scala
//=============================================================================
