/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  06/Sep/2021
 * Time:  13h:23m
 * Description: http://faculty.washington.edu/ivezic/sdssmoc/sdssmoc.html#catalogs
 */
//=============================================================================
package com.henosis.database.sdsssMoc_4
//=============================================================================
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.MongoDB
import com.common.logger.MyLogger
import com.henosis.database.MPO_DB
import com.common.database.mongoDB.fortran.FortranColumn
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import org.mongodb.scala.bson.BsonValue
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
//=============================================================================
object SdssMoc_4_ToMongo extends MyLogger {
  //---------------------------------------------------------------------------
  private val mpoColumnData = Array(
    //-- SDSS identification --
    FortranColumn("_id","A1")  //moID
    , FortranColumn("run", "I1")
    , FortranColumn("col", "I1")
    , FortranColumn("field", "I1")
    , FortranColumn("object", "I1")
    , FortranColumn("rowc", "F1")
    , FortranColumn("colc","F1")
      //--  Astrometry --
    , FortranColumn("mjd","F1")
    , FortranColumn("ra","F1")
    , FortranColumn("dec","F1")
    , FortranColumn("lambda","F1")
    , FortranColumn("beta","F1")
    , FortranColumn("phi","F1")
    , FortranColumn("vMu","F1")
    , FortranColumn("vMu_error","F1")
    , FortranColumn("vNu","F1")
    , FortranColumn("vNu_error","F1")
    , FortranColumn("vLambda","F1")
    , FortranColumn("vBeta","F1")
    //-- Photometry --
    , FortranColumn("u","F1")
    , FortranColumn("uErr","F1")
    , FortranColumn("g","F1")
    , FortranColumn("gErr","F1")
    , FortranColumn("r","F1")
    , FortranColumn("rErr","F1")
    , FortranColumn("i","F1")
    , FortranColumn("iErr","F1")
    , FortranColumn("z","F1")
    , FortranColumn("zErr","F1")
    , FortranColumn("a","F1")
    , FortranColumn("aErr","F1")
    , FortranColumn("V","F1")
    , FortranColumn("B","F1")
    //-- Identification --
    , FortranColumn("identification_flag","I1")
    , FortranColumn("numeration","I1")
    , FortranColumn("designation","A1")
    , FortranColumn("detection_counter","I1")
    , FortranColumn("total_detection_count","I1")
    , FortranColumn("flags","I1")
      //-- Matching information --
    , FortranColumn("computed_ra","F1")
    , FortranColumn("computed_dec","F1")
    , FortranColumn("computed_app_mag","F1")
    , FortranColumn("r","F1")
    , FortranColumn("geocentric","F1")
    , FortranColumn("phase","F1")
    //-- Osculating elements --
    , FortranColumn("catalogID","A1")
    , FortranColumn("H","F1")
    , FortranColumn("G","F1")
    , FortranColumn("arc","I1")
    , FortranColumn("epoch","F1")
    , FortranColumn("a","F1")
    , FortranColumn("e","F1")
    , FortranColumn("i","F1")
    , FortranColumn("lon_asc_node","F1")
    , FortranColumn("arg_of_perihelion","F1")
    , FortranColumn("M","F1")
     // -- Proper elements --
    , FortranColumn("proper_elements_catalog_ID","A1")
    , FortranColumn("a_","F1")
    , FortranColumn("e_","F1")
    , FortranColumn("sin(i_)","F1")
    , FortranColumn("binary_processing_flags","A1")
  )
  //---------------------------------------------------------------------------
  private def readAsteroidInfo(line: String) : Option[Document] = {
    val row = ArrayBuffer[(String, BsonValue)]()
    val tokenSeq = line.replaceAll(",",".").split(" ")
    mpoColumnData.zipWithIndex.foreach { case (t,i) =>
      val s = if (i == 60) tokenSeq.drop(i).mkString("{",",","}")  //unify 'binary_processing_flags'
              else tokenSeq(i)
      t.addColumnToRow(s.trim, row)
    }
    Some(Document(row))
  }
  //---------------------------------------------------------------------------
  private def processFile(fileName: String, db: MongoDB, coll: MongoCollection[Document]): Boolean = {
    info(s"Processing 'SDSS MOC 4' file: '$fileName'")
    val mpoBuffer = ArrayBuffer[Document]()
    val maxBufferedMPO  = 64 * 1024 //to minimize the write operations in mongo

    //read the mpo info and store it
    val sourceFile = Source.fromFile(fileName)
    for (line <- sourceFile.getLines) {
      val doc = readAsteroidInfo(line)
      if (doc.isEmpty) {
        sourceFile.close()
        return false
      }
      else {
        //store the mpo in the buffer and store the mpo sequence if is needed
        mpoBuffer += doc.get
        if (mpoBuffer.length > maxBufferedMPO) {
          coll.insertMany(mpoBuffer.toArray).results()
          mpoBuffer.clear()
        }
      }
    }
    sourceFile.close()

    //store the remain buffered mpo's
    if (mpoBuffer.length > 0) {
      coll.insertMany(mpoBuffer.toArray).results()
      mpoBuffer.clear()
    }

    info("Creating indexes")
    db.createIndex("catalogID", coll, _unique = false)
    true
  }
  //---------------------------------------------------------------------------
  def importData(db: MPO_DB, inputFile: String) = {
    val coll = db.collSdssMoc_4
    db.dropCollection(coll)
    processFile(inputFile, db, coll)
    db.createIndex("designation", _unique = false)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SdssMoc_4_ToMongo.scala
//=============================================================================
