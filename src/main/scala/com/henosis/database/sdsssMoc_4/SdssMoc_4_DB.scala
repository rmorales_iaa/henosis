/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Sep/2021
 * Time:  19h:12m
 * Description: None
 */
//=============================================================================
package com.henosis.database.sdsssMoc_4
//=============================================================================
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.henosis.database.MPO_DB
import com.henosis.database.sdsssMoc_4.MPO.CSV_HEADER
//=============================================================================
import org.mongodb.scala.model.Filters.equal
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
case class SdssMoc_4_DB(mpoDB: MPO_DB) {
  //---------------------------------------------------------------------------
  val database = mpoDB
  val collection = database.collSdssMoc_4
  //---------------------------------------------------------------------------
  def getDocSeq(nameSeq: Seq[String]) = {
    nameSeq map { name =>
      collection.find(equal("designation", name)).results
    }
  }
  //---------------------------------------------------------------------------
  def saveFile(nameSeq: Seq[String], mpcInfo: Seq[String], outputCsv: String, sep: String = "\t") = {
    val docSeq = getDocSeq(nameSeq)
    val bw = new BufferedWriter(new FileWriter(new File(outputCsv)))
    bw.write("seq" + sep + CSV_HEADER.mkString(sep) + sep + "sdss_moc_4_mpc_name" + "\n")
    var seqCount: Long = 0
    docSeq.zipWithIndex.foreach { case (seq,i) =>
      seq foreach { doc =>
        seqCount += 1
        val mpo = MPO(doc)
        bw.write(seqCount.toString + sep)
        bw.write(mpo.getAsCsvRow(sep) + sep)
        bw.write(mpcInfo(i) + sep)
        bw.write("\n")
      }
    }
    bw.flush()
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SdssMoc_4_DB.scala
//=============================================================================
