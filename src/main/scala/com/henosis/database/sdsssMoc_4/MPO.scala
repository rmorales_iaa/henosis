/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Sep/2021
 * Time:  19h:31m
 * Description: None
 */
//=============================================================================
package com.henosis.database.sdsssMoc_4
//=============================================================================
import com.henosis.algoritms.paper.mpo.MPO_Common
//=============================================================================
import org.mongodb.scala.Document
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object MPO {
  //---------------------------------------------------------------------------
  final val CSV_HEADER =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = "sdss_moc_4_" + f.getName
      f.setAccessible(false)
      res
    }

  //---------------------------------------------------------------------------
  def apply(doc: Document): MPO =  MPO(
      doc("_id").asString().getValue
    , doc("run").asInt32().getValue
    , doc("col").asInt32().getValue
    , doc("field").asInt32().getValue
    , doc("object").asInt32().getValue
    , doc("rowc").asDouble().getValue
    , doc("colc").asDouble().getValue
    , doc("mjd").asDouble().getValue
    , doc("ra").asDouble().getValue
    , doc("dec").asDouble().getValue
    , doc("lambda").asDouble().getValue
    , doc("beta").asDouble().getValue
    , doc("phi").asDouble().getValue
    , doc("vMu").asDouble().getValue
    , doc("vMu_error").asDouble().getValue
    , doc("vNu").asDouble().getValue
    , doc("vNu_error").asDouble().getValue
    , doc("vLambda").asDouble().getValue
    , doc("vBeta").asDouble().getValue
    , doc("u").asDouble().getValue
    , doc("uErr").asDouble().getValue
    , doc("g").asDouble().getValue
    , doc("gErr").asDouble().getValue
    , doc("r").asDouble().getValue
    , doc("rErr").asDouble().getValue
    , doc("i").asDouble().getValue
    , doc("iErr").asDouble().getValue
    , doc("z").asDouble().getValue
    , doc("zErr").asDouble().getValue
    , doc("a").asDouble().getValue
    , doc("aErr").asDouble().getValue
    , doc("V").asDouble().getValue
    , doc("B").asDouble().getValue
    , doc("identification_flag").asInt32().getValue
    , doc("numeration").asInt32().getValue
    , Seq(doc("designation").asString().getValue)
    , doc("detection_counter").asInt32().getValue
    , doc("total_detection_count").asInt32().getValue
    , doc("flags").asInt32().getValue
    , doc("computed_ra").asDouble().getValue
    , doc("computed_dec").asDouble().getValue
    , doc("computed_app_mag").asDouble().getValue
    , doc("geocentric").asDouble().getValue
    , doc("phase").asDouble().getValue
    , doc("catalogID").asString().getValue
    , doc("H").asDouble().getValue
    , doc("G").asDouble().getValue
    , doc("arc").asInt32().getValue
    , doc("epoch").asDouble().getValue
    , doc("e").asDouble().getValue
    , doc("lon_asc_node").asDouble().getValue
    , doc("arg_of_perihelion").asDouble().getValue
    , doc("M").asDouble().getValue
    , doc("proper_elements_catalog_ID").asString().getValue
    , doc("a_").asDouble().getValue
    , doc("e_").asDouble().getValue
    , doc("sin(i_)").asDouble().getValue
    , doc("binary_processing_flags").asString().getValue
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MPO(id: String
               , run: Int
               , col: Int
               , field: Int
               , objectID: Int
               , rowc: Double
               , colc: Double
               , mjd: Double
               , ra: Double
               , dec: Double
               , lambda: Double
               , beta: Double
               , phi: Double
               , vMu: Double
               , vMu_error: Double
               , vNu: Double
               , vNu_error: Double
               , vLambda: Double
               , vBeta: Double
               , u: Double
               , uErr: Double
               , g: Double
               , gErr: Double
               , r: Double
               , rErr: Double
               , i: Double
               , iErr: Double
               , z: Double
               , zErr: Double
               , a: Double
               , aErr: Double
               , V: Double
               , B: Double
               , identification_flag: Int
               , numeration: Int
               , name: Seq[String]
               , detection_counter: Int
               , total_detection_count: Int
               , flags: Int
               , computed_ra: Double
               , computed_dec: Double
               , computed_app_mag: Double
               , geocentric: Double
               , phase: Double
               , catalogID: String
               , H: Double
               , G: Double
               , arc: Int
               , epoch: Double
               , e: Double
               , lon_asc_node: Double
               , arg_of_perihelion: Double
               , M: Double
               , proper_elements_catalog_ID: String
               , aprime: Double
               , eprime: Double
               , siniprime: Double
               , binary_processing_flags: String) extends MPO_Common {
  //-------------------------------------------------------------------------
 def getAsCsvRow(sep: String = "\t") = {
    val seq = ArrayBuffer[String]()
    seq ++= productIterator.mkString(sep).split(sep)
    seq(35) = name.mkString(sep)
    seq.mkString(sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO.scala
//=============================================================================
