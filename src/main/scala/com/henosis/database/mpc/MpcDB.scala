/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  11h:18m
 * Description: None
 */
//=============================================================================
package com.henosis.database.mpc
//=============================================================================
import com.common.database.mongoDB.MongoDB
import com.common.database.mongoDB.Helpers._
import com.common.database.mongoDB.MongoDB.getProjectionColList
import com.henosis.database.MPO_DB
import com.henosis.ephemeris.OrbitalElement
//=============================================================================
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.bson.codecs.configuration.CodecRegistries.{fromRegistries, fromProviders}
import com.mongodb.client.model.CollationStrength
import org.mongodb.scala.model.Collation
import org.mongodb.scala.model.Filters.equal
import java.io.BufferedWriter
//=============================================================================
object MpcDB {
  //---------------------------------------------------------------------------
  //http://mongodb.github.io/mongo-scala-driver/2.3/getting-started/quick-tour-case-classes/
  //https://jannikarndt.de/blog/2017/08/writing_case_classes_to_mongodb_in_scala/
  val codecRegistry = fromRegistries(fromProviders(classOf[MPO]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  val collationCaseInsensitive = Collation
    .builder()
    .locale("en")
    .collationStrength(CollationStrength.PRIMARY).build
  //--------------------------------------------------------------------------
  final val MPC_BASIC_INFO_COL_SEQ  = Seq(
    "Number"
    , "Name"
    , "H"
    , "G"
    , "Principal_desig"
    , "Epoch"
    , "M"
    , "Num_obs")
  //--------------------------------------------------------------------------
  final val MPC_ORBITAL_ELEMENT_COL_SEQ = Seq(
     "Node"
   , "i"
   , "Peri"
   , "M"
   , "a"
   , "e"
   , "Epoch"
   , "n"
   , "Number"
   , "Principal_desig" )

  //--------------------------------------------------------------------------
  final val MPC_BASIC_INFO_CSV_HEADER = MPC_BASIC_INFO_COL_SEQ map ("MPC_" + _)
  //--------------------------------------------------------------------------
  def writeCsvMocInfo(doc: Document, bw: BufferedWriter, sep: String = "\t") = {
    bw.write(doc("Number").asString().getValue + sep)

    if (doc("Name").isNull)  bw.write("-1" + sep) else bw.write(doc("Name").asString.getValue + sep)

    bw.write(doc("H").asDouble.getValue + sep)
    bw.write(doc("G").asDouble.getValue + sep)
    bw.write(doc("Principal_desig").asString.getValue + sep)
    bw.write(doc("Epoch").asDouble.getValue + sep)
    bw.write(doc("M").asDouble.getValue + sep)
    bw.write(doc("Num_obs").asInt32().getValue + sep)
  }
  //--------------------------------------------------------------------------
}
//=============================================================================
import MpcDB._
case class MpcDB() extends MongoDB {
  //---------------------------------------------------------------------------
  val databaseName  = MPO_DB.DATABASE_NAME
  val collectionName = MPO_DB.COLLECTION_MPC
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection =  r._3
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
  def existName(name: String) =
    collection.find(equal("Name", name))
      .collation(collationCaseInsensitive)
      .resultCount > 0
  //---------------------------------------------------------------------------
  def existNumber(id: Int) =
    collection.find(equal("Number", id))
      .resultCount > 0
  //---------------------------------------------------------------------------
  def getByName(name: String, colList:Seq[String] = MPC_BASIC_INFO_COL_SEQ, verbose: Boolean = true) : Option[Document] = {
    val r = collection
      .find(equal("Name", name))
      .collation(collationCaseInsensitive)
      .projection(getProjectionColList(colList))
      .results()
    if (r.length == 0) {
      if (verbose) error(s"The mpo with name: '$name' does not exist")
      None
    }
    else Some(r.head)
  }
  //---------------------------------------------------------------------------
  def getByNumber(mpcID: String, colList:Seq[String] = MPC_BASIC_INFO_COL_SEQ, verbose: Boolean = true) = {
    val r = collection
      .find(equal("Number", mpcID))
      .projection(getProjectionColList(colList))
      .results()

    if (r.length == 0) {
      if (verbose) error(s"The mpo with number: '$mpcID' does not exist")
      None
    }
    else Some(r.head)
  }
  //---------------------------------------------------------------------------
  def getOrbitalElement(name: String) : Option[OrbitalElement] = {
    val doc = getByName(name, MPC_ORBITAL_ELEMENT_COL_SEQ).getOrElse(return None)
    getOrbitalElement(doc)
  }
  //---------------------------------------------------------------------------
  def getOrbitalElement(mpcID: Int) : Option[OrbitalElement] = {
    val doc = getByNumber(mpcID.toString, MPC_ORBITAL_ELEMENT_COL_SEQ).getOrElse(return None)
    getOrbitalElement(doc)
  }
  //---------------------------------------------------------------------------
  def getOrbitalElement(doc:Document) : Option[OrbitalElement] = {
    Some(OrbitalElement(
        doc("Node").asDouble().getValue  //longitude of the ascending node                                 (deg)
      , doc("i").asDouble().getValue     //inclination to the ecliptic (plane of the Earth's orbit)        (deg)
      , doc("Peri").asDouble().getValue  //argument (aka longitude) of perihelion                          (deg)
      , doc("M").asDouble().getValue     //mean anomaly (0 at perihelion; increases uniformly with time)   (deg)
      , doc("a").asDouble().getValue     //semi-major axis, or mean distance from Sun (aka mean distance)  (au)
      , doc("e").asDouble().getValue     //eccentricity (0=circle, 0-1=ellipse, 1=parabola)                (no units)
      , doc("Epoch").asDouble().getValue // epoch                                                          (Julian day)
      , doc("n").asDouble().getValue               //Optional: mpc mean motion (degrees per sidereal day)                                                   (Julian day)
      , doc("Principal_desig").asString().getValue //Optional: mpc main designation
      , doc("Number").asString().getValue          //Optional: mpc ID
    ))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpcDB.scala
//=============================================================================
