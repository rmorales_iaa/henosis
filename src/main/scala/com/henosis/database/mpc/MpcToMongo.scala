/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  09/Aug/2021
 * Time:  20h:25m
 * Description: Import data from mpc database (minor planet center database) to mongo
 * https://minorplanetcenter.net/data
 */
//=============================================================================
package com.henosis.database.mpc
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.logger.MyLogger
import com.common.util.util.Util
import com.henosis.database.MPO_DB
import org.mongodb.scala.MongoClient
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.model.{IndexModel, IndexOptions, Indexes}
//=============================================================================
import com.github.plokhotnyuk.jsoniter_scala.core.{JsonValueCodec, scanJsonArrayFromStream}
import com.github.plokhotnyuk.jsoniter_scala.macros.JsonCodecMaker
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoCollection
import org.mongodb.scala.bson.codecs.Macros._

import java.io.{File, FileInputStream}
import java.nio.charset.StandardCharsets
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Success, Try}
//=============================================================================
object MpcToMongo extends MyLogger {
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
  private final val conf = MyConf(MyConf.c.getString("Database.mpc"))
  private final val updateScript = conf.getString("MPC.script.updateScript")
  //---------------------------------------------------------------------------
  final val COL_NAME_NAME              = "Name"
  final val COL_NAME_MPC_ID            = "Number"
  final val COL_NAME_OTHER_DESIGNATION = "Other_desigs"
  final val COL_NAME_SPK_ID            = "spk_id"
  //---------------------------------------------------------------------------
  final val AsteroidShortCsvHeader = Seq(
    "mpc_ID"
    , "mpc_name"
    , "mpc_other_designation")
  //---------------------------------------------------------------------------
  //https://www.amitph.com/java-parse-large-json-files/
  private def processFile(fileName: String): Boolean = {
    //--------------------------------------------------------------------------
    val uri = MyConf.c.getString("Database.mongoDB.uri")
    val mongoClient = MongoClient(uri)
    val codecRegistry = fromRegistries(fromProviders(classOf[MPO]), DEFAULT_CODEC_REGISTRY )
    val database = mongoClient.getDatabase(MPO_DB.DATABASE_NAME).withCodecRegistry(codecRegistry)
    val collection: MongoCollection[MPO] = database.getCollection(MPO_DB.COLLECTION_MPC)
    collection.drop().results()
    val mpoBuffer = ArrayBuffer[MPO]()
    val maxBufferedAsteroid  = 64 * 1024 //to minimize the write operations in mongo
    //-------------------------------------------------------------------------
    def createIndex(collName: String, _unique: Boolean = true, ascending: Boolean = true): Unit = {
      collection.createIndexes(
        Seq(
          IndexModel(
            if (ascending) Indexes.ascending(collName) else Indexes.descending(collName),
            IndexOptions().background(false).unique(_unique)
          ))).results()
    }
    //-------------------------------------------------------------------------
    def processMPO(mpo: MPO) : Boolean = {
      //store the mpo in the buffer and store the mpo sequence if is needed
      mpoBuffer += mpo
      if (mpoBuffer.length > maxBufferedAsteroid) {
        collection.insertMany(mpoBuffer.toArray).results()
        mpoBuffer.clear()
      }
      true
    }
    //-------------------------------------------------------------------------
    info(s"Processing mpc file: '$fileName'")
    implicit val codec: JsonValueCodec[MPO] = JsonCodecMaker.make

    Try {
      scanJsonArrayFromStream(new FileInputStream(fileName))(processMPO)
    }
    match {
      case Success(_) => info(s"Success processing json file: '$fileName'")
      case Failure(ex) => error(s"Error processing json file: '$fileName'" + ex.toString)
    }

    //store the remain buffered mpo's
    if (mpoBuffer.length > 0) {
      collection.insertMany(mpoBuffer.toArray).results()
      mpoBuffer.clear()
    }

    info("Creating indexes")

    createIndex(COL_NAME_MPC_ID,_unique = false) //due to unnumbered mpo
    createIndex(COL_NAME_NAME,_unique = false)
    createIndex(COL_NAME_OTHER_DESIGNATION,_unique = false)
    createIndex(COL_NAME_SPK_ID,_unique = false)
    true
  }
  //---------------------------------------------------------------------------
  def importData() = {
    val r = Util.runShellCommandAndGetBinaryOutput_2(updateScript)
    val inputFile = new String(r, StandardCharsets.UTF_8).split("\n").last
    processFile(inputFile)
    new File(inputFile).delete()
    info(s"File: '$inputFile' deleted")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MpcToMongo.scala
//=============================================================================
