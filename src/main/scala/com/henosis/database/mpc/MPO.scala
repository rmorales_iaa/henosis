/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  05/Sep/2021
 * Time:  15h:06m
 * Description: None
 */
//=============================================================================
package com.henosis.database.mpc
//=============================================================================
//=============================================================================
object MPO {  //minor planet object
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//https://minorplanetcenter.net/Extended_Files/Extended_MPCORB_Data_Format_Manual.pdf
//Table 4
//get the scheme and use it to generate the class. It can be reordered regarding the schema order
//pip3 install genson
//python3 /home/rafa/.local/bin/genson ./mpcorb_extended.json > mpcorb_extended_schema.json
case class MPO(Number: Option[String]
               , Name: Option[String]
               , H: Option[Float]
               , G: Option[Float]
               , Principal_desig: String
               , Epoch: Float
               , M: Float
               , Peri: Float
               , Node: Float
               , i: Float
               , e: Float
               , n: Float
               , a: Float
               , Ref: String
               , Num_opps: Int
               , Computer: String
               , Hex_flags: String
               , Last_obs: String
               , Tp: Float
               , Orbital_period: Float
               , Perihelion_dist: Float
               , Aphelion_dist: Float
               , Semilatus_rectum: Float
               , Synodic_period: Float
               , Orbit_type: String
               , Num_obs: Option[Int]
               , rms: Option[Float]
               , U: Option[String]
               , Arc_years: Option[String]
               , Perturbers: Option[String]
               , Perturbers_2: Option[String]
               , Other_desigs: Option[List[String]]
               , NEO_flag: Option[Int]
               , One_km_NEO_flag: Option[Int]
               , PHA_flag: Option[Int]
               , Critical_list_Floated_object_flag: Option[Int]
               , One_opposition_obj: Option[Int]
               , Arc_length: Option[Int])
//=============================================================================
//End of file MPO.scala
//=============================================================================
