/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Jul/2021
 * Time:  20h:04m
 * Description: None
 */
//=============================================================================
package com.henosis.database
//=============================================================================
import com.common.database.mongoDB.MongoDB
import com.henosis.database.MPO_DB.DATABASE_NAME
//=============================================================================
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object MPO_DB{
  //---------------------------------------------------------------------------
  final val DATABASE_NAME          = "mpo_catalog"
  //---------------------------------------------------------------------------
  final val COLLECTION_ASTORB      = "astorb"
  final val COLLECTION_MPC         = "mpc"
  final val COLLECTION_2_MASS_MOC  = "2massMoc"
  final val COLLECTION_GAIA_2_MOC  = "gaia_2_moc"
  final val COLLECTION_K_2_MOC     = "k2_Moc"
  final val COLLECTION_SDSS_MOC    = "sdssMoc_4"
  final val COLLECTION_TESS_MOC    = "tessMoc"
  final val COLLECTION_TNO_COLOR   = "tnoColor"
  //---------------------------------------------------------------------------
}
//=============================================================================
import MPO_DB._
case class MPO_DB(databaseName: String = DATABASE_NAME
                  , collectionName: String = "None") extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = init()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  //---------------------------------------------------------------------------

  val collAstorb = database.getCollection(COLLECTION_ASTORB)

  val collMpc = database.getCollection(COLLECTION_MPC)

  //moc = moving object catalog
  val coll2MassMoc = database.getCollection(COLLECTION_2_MASS_MOC)

  val collGaiaMoc = database.getCollection(COLLECTION_GAIA_2_MOC)
  val collK_2_Moc = database.getCollection(COLLECTION_K_2_MOC)

  val collSdssMoc_4 = database.getCollection(COLLECTION_SDSS_MOC)

  val collTessMoc = database.getCollection(COLLECTION_TESS_MOC)

  val tnoColor = database.getCollection(COLLECTION_TNO_COLOR)
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO_DB.scala
//=============================================================================
