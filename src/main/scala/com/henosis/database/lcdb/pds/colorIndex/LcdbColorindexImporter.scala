/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  02/Jun/2022
  * Time:  11h:40m
  * Description: None
  */
//=============================================================================
package com.henosis.database.lcdb.pds.colorIndex
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.hardware.cpu.CPU
import com.common.pds.nasa.PDS
import com.common.{DatabaseImporter, LineEntry}
import com.common.database.mongoDB.database.lcdb.pds.colorIndex._
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
//=============================================================================
//=============================================================================
case class LcdbColorindexImporter(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = LcdbColorIndexDB(conf)
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[LcdbColorIndexSource] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    mongoDB.createIndexes(Array("Name"),_unique = false)
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    val pds = PDS(path)
    val table = pds.getTableSeq().head
    val sourceSeq = table.rowSeq.map { row => LcdbColorIndexSource(row) }
    collWithRegistry.insertMany(sourceSeq).results()
    true
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true, fileExtension = ".xml")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file LcdbColorindexImporter.scala
//=============================================================================
