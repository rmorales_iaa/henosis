/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  02/Jun/2022
  * Time:  11h:40m
  * Description: None
  */
//=============================================================================
package com.henosis.database.lcdb.mpc.colorIndex

//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.database.lcdb.mpc.colorIndex._
import com.common.hardware.cpu.CPU
import com.common.{DatabaseImporter, LineEntry}

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
//=============================================================================
//=============================================================================
case class LcdbColorindexImporter(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = LcdbColorIndexDB(conf)
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[LcdbColorIndexSource] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    mongoDB.createIndexes(Array("Name"),_unique = false)
  }
  //---------------------------------------------------------------------------
  private def groupByBlankLines(lines: Seq[String]): List[List[String]] = {
    lines.foldLeft(List.empty[List[String]]) { (acc, line) =>
      if (line.isEmpty) {
        // Start a new group with the blank line
        List.empty[String] :: acc
      } else {
        // Add line to current group (or create a new group if empty)
        if (acc.isEmpty) List(line) :: acc else (line :: acc.head) :: acc.tail
      }
    }.reverse // Reverse the final result to maintain the original order of groups
  }
  //---------------------------------------------------------------------------
  private def processColorIndex(fileName: String): Array[LcdbColorIndexSource] = {
    info(s"Processing '$fileName'")

    val sourceSeq = ArrayBuffer[LcdbColorIndexSource]()

    val bs = Source.fromFile(fileName)
    val entreSeq = groupByBlankLines(bs.getLines().drop(10).toList)
    bs.close()

    entreSeq.map { entryList =>
      sourceSeq ++= LcdbColorIndexSource.build(entryList.reverse)
    }

    sourceSeq.toArray
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {

    if (path.endsWith("lc_colorindex_pub.txt")) {
      val sourceSeq = processColorIndex(path)
      collWithRegistry.insertMany(sourceSeq).results()
    }
    else warning(s"Ignoring: '$path'")
    true
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true, fileExtension = ".txt")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file LcdbColorindexImporter.scala
//=============================================================================
