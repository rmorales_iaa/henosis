/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Jun/2022
 * Time:  11h:40m
 * Description: None
 */
//=============================================================================
package com.henosis.database.carmencita
//=============================================================================
import com.common.csv.CsvRead
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.database.carmencita.{CarmencitaDB, CarmencitaSource}
import com.common.hardware.cpu.CPU
import com.common.{DatabaseImporter, LineEntry}
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
//=============================================================================
//=============================================================================
object CarmencitaImporter {
  //---------------------------------------------------------------------------
  private def loadSeqFromCsv(csvFileName: String) = {
    //-------------------------------------------------------------------------
    val csv = CsvRead(csvFileName, hasHeader = true, itemDivider = ",")
    csv.read()
    csv.getRowSeq.map { row =>
      CarmencitaSource(
        row.getString("_id"),
        row.getString("name"),
        row.getString("comp"),
        row.getString("gj"),
        row.getDouble("ra_j2016_deg"),
        row.getDouble("de_j2016_deg"),
        row.getString("ra_j2000"),
        row.getString("de_j2000"),
        row.getString("l_j2016_deg"),
        row.getString("b_j2016_deg"),
        row.getString("ref01"),
        row.getString("spt"),
        row.getDouble("sptnum"),
        row.getString("ref02"),
        row.getString("teff_k"),
        row.getString("eteff_k"),
        row.getString("logg"),
        row.getString("elogg"),
        row.getString("feh"),
        row.getString("efeh"),
        row.getString("ref03"),
        row.getString("l_lsol"),
        row.getString("el_lsol"),
        row.getString("ref04"),
        row.getString("r_rsol"),
        row.getString("er_rsol"),
        row.getString("ref05"),
        row.getString("m_msol"),
        row.getString("em_msol"),
        row.getString("ref06"),
        row.getDouble("mura_masa1"),
        row.getDouble("emura_masa1"),
        row.getDouble("mude_masa1"),
        row.getDouble("emude_masa1"),
        row.getString("ref07"),
        row.getString("pi_mas"),
        row.getString("epi_mas"),
        row.getString("ref08"),
        row.getDouble("d_pc"),
        row.getDouble("ed_pc"),
        row.getString("ref09"),
        row.getString("vr_kms1"),
        row.getString("evr_kms1"),
        row.getString("ref10"),
        row.getString("ruwe"),
        row.getString("ref11"),
        row.getString("u_kms1"),
        row.getString("eu_kms1"),
        row.getString("v_kms1"),
        row.getString("ev_kms1"),
        row.getString("w_kms1"),
        row.getString("ew_kms1"),
        row.getString("ref12"),
        row.getString("sa_msa"),
        row.getString("esa_msa"),
        row.getString("ref13"),
        row.getString("skg"),
        row.getString("ref14"),
        row.getString("skg_lit"),
        row.getString("ref14_lit"),
        row.getString("pop"),
        row.getString("ref15"),
        row.getString("vsini_flag"),
        row.getString("vsini_kms1"),
        row.getString("evsini_kms1"),
        row.getString("ref16"),
        row.getString("p_d"),
        row.getString("ep_d"),
        row.getString("ref17"),
        row.getString("pewhalpha_a"),
        row.getString("epewhalpha_a"),
        row.getString("ref18"),
        row.getString("loglhalphalbol"),
        row.getString("eloglhalphalbol"),
        row.getString("ref19"),
        row.getString("_1rxs"),
        row.getString("crt_s1"),
        row.getString("ecrt_s1"),
        row.getString("hr1"),
        row.getString("ehr1"),
        row.getString("hr2"),
        row.getString("ehr2"),
        row.getString("flux_x_e13_ergcm2s1"),
        row.getString("eflux_x_e13_ergcm2s1"),
        row.getString("lxlj"),
        row.getString("elxlj"),
        row.getString("ref20"),
        row.getString("activity"),
        row.getString("ref21"),
        row.getString("fuv_mag"),
        row.getString("efuv_mag"),
        row.getString("nuv_mag"),
        row.getString("enuv_mag"),
        row.getString("ref22"),
        row.getString("u_mag"),
        row.getString("eu_mag"),
        row.getString("ref23"),
        row.getString("bt_mag"),
        row.getString("ebt_mag"),
        row.getString("ref24"),
        row.getString("b_mag"),
        row.getString("eb_mag"),
        row.getString("ref25"),
        row.getDouble("bp_mag"),
        row.getDouble("ebp_mag"),
        row.getString("ref26"),
        row.getString("g_mag"),
        row.getString("eg_mag"),
        row.getString("ref27"),
        row.getString("vt_mag"),
        row.getString("evt_mag"),
        row.getString("ref28"),
        row.getString("v_mag"),
        row.getString("ev_mag"),
        row.getString("ref29"),
        row.getString("ra_mag"),
        row.getString("ref30"),
        row.getString("r_mag"),
        row.getString("er_mag"),
        row.getString("ref31"),
        row.getDouble("gg_mag"),
        row.getDouble("egg_mag"),
        row.getString("ref32"),
        row.getString("i_mag"),
        row.getString("ei_mag"),
        row.getString("ref33"),
        row.getDouble("rp_mag"),
        row.getDouble("erp_mag"),
        row.getString("ref34"),
        row.getString("in_mag"),
        row.getString("ref35"),
        row.getDouble("j_mag"),
        row.getString("ej_mag"),
        row.getString("h_mag"),
        row.getString("eh_mag"),
        row.getDouble("ks_mag"),
        row.getString("eks_mag"),
        row.getString("qflag_2m"),
        row.getString("ref36"),
        row.getString("w1_mag"),
        row.getString("ew1_mag"),
        row.getString("w2_mag"),
        row.getString("ew2_mag"),
        row.getString("w3_mag"),
        row.getString("ew3_mag"),
        row.getString("w4_mag"),
        row.getString("ew4_mag"),
        row.getString("qflag_wise"),
        row.getString("ref37"),
        row.getString("multiplicity"),
        row.getString("widewds"),
        row.getString("widerho_arcsec"),
        row.getString("ewiderho_arcsec"),
        row.getString("ref38"),
        row.getString("widecompanionname"),
        row.getString("widecompanionspt"),
        row.getString("closewds"),
        row.getString("closerho_arcsec"),
        row.getString("ecloserho_arcsec"),
        row.getString("ref39"),
        row.getString("closecompanionspt"),
        row.getString("rv"),
        row.getString("planet"),
        row.getDouble("planetnum"),
        row.getString("ref40"),
        row.getString("lores_spectrum"),
        row.getString("hires_spectrum"),
        row.getString("lores_imaging"),
        row.getString("hires_imaging"),
        row.getString("_class"),
        row.getString("survey"),
        row.getString("notes"),
        row.getDouble("gaia_id_1"),
        row.getString("_2mass_id"),
        row.getString("tic_id"),
        row.getString("toi")
      )
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class CarmencitaImporter(inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = CarmencitaDB()
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[CarmencitaSource] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    mongoDB.createIndexes(Array("name"))
    mongoDB.createIndexes(Array("ra_j2016_deg","de_j2016_deg"))
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    val sourceSeq = CarmencitaImporter.loadSeqFromCsv(path)
    collWithRegistry.insertMany(sourceSeq).results()
    true
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true, fileExtension = ".csv")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CarmencitaImporter.scala
//=============================================================================
