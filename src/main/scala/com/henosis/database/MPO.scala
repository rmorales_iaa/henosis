/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  05/Sep/2021
  * Time:  14h:27m
  * Description: None
  */
//=============================================================================
package com.henosis.database
//=============================================================================
import com.henosis.algoritms.paper.mpo.MPO_Common
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object MPO {
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[MPO].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class MPO(mpcID: String
               , name: Seq[String]
               , rotation_period: Double
               , max_amplitude_object: Double
               , publication: String
               , utc: String
               , rotation_reported: Double
               , rotation_reported_error: Double
               , max_amplitude_light_curve: Double
               , max_amplitude_light_curve_error: Double
               , B_V: Double
               , B_V_error: Double
               , B_R: Double
               , B_R_error: Double
               , V_R: Double
               , V_R_error: Double
               , V_I: Double
               , V_I_error: Double
               , sloan_g_r: Double
               , sloan_g_r_error: Double
               , sloan_r_i: Double
               , sloan_r_i_error: Double
               , sloan_i_z: Double
               , sloan_i_z_error: Double
                             ) extends MPO_Common {
  //-------------------------------------------------------------------------
  def getAsCsvRow(sep: String = "\t") = {
    val seq = ArrayBuffer[String]()
    seq ++= productIterator.mkString(sep).split(sep)
    seq.insert(1, name.mkString("|"))  //fix equivalent designation
    seq.mkString(sep)
  }
  //-------------------------------------------------------------------------
}
//=============================================================================
//End of file MPO.scala
//=============================================================================