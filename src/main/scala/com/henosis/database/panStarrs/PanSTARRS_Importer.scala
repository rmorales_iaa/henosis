/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Aug/2021
 * Time:  02h:17m
 * Description: None
 */
//=============================================================================
package com.henosis.database.panStarrs
//=============================================================================
import com.common.DatabaseImporter
//=============================================================================
//=============================================================================
case class PanSTARRS_Importer(inputDir: String, mongoDB: PanSTARRS_DB) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val maxBufferedDocument = 10
  val colNameSeq = FileEntry.COL_DEFINITION_SEQ
  val lineDivider: String = ","
  val headerInCsv: Boolean = true
  val coreCount = 1
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    info("Please crate index manually: objID")
  }
  //---------------------------------------------------------------------------
  run(false)
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PanSTARRS_Importer.scala
//=============================================================================
