/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  31/Aug/2021
  * Time:  02h:11m
  * Description: None
  */
//=============================================================================
package com.henosis.database.panStarrs
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.MongoDB
import com.common.geometry.point.Point2D_Double
import com.common.util.path.Path
//=============================================================================
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
import scalaj.http.{Http, HttpResponse}
import java.io.{BufferedWriter, File, FileWriter}
import org.apache.commons.csv.{CSVFormat, CSVParser}
import org.mongodb.scala.model.Filters.equal
import java.io.{BufferedReader, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
//=============================================================================
//=============================================================================
object PanSTARRS_DB {
  //---------------------------------------------------------------------------
  final val conf = MyConf(MyConf.c.getString("Database.panSTARRS"))
  //---------------------------------------------------------------------------
  final val PANSTARRS_REFERENCE_EPOCH      = "2019-01-01T00:00:00"
  //---------------------------------------------------------------------------
  private val queryUrl = conf.getString("PanSTARRS.query.url")
  //---------------------------------------------------------------------------
  val queryTemporalDir = Path.resetDirectory(conf.getString("PanSTARRS.query.cacheDir"))
  private val queryConnectionTimeoutMs = conf.getInt("PanSTARRS.query.connectionTimeoutMs")
  private val queryReadTimeoutMs = conf.getInt("PanSTARRS.query.readTimeoutMs")
  //---------------------------------------------------------------------------
  final val PANSTARRS_QUERY_BY_POSITION_SEARCH_RADIUS    = conf.getDouble("PanSTARRS.query.byPosition.searchRadius")
  final val PANSTARRS_QUERY_BY_POSITION_MIN_DETECTIONS   = conf.getInt("PanSTARRS.query.byPosition.minDetections")
  final val PANSTARRS_QUERY_BY_POSITION_MAX_RESULT_COUNT = conf.getInt("PanSTARRS.query.byPosition.maxResultCount")
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): PanSTARRS_DB =
    PanSTARRS_DB(conf.getString("PanSTARRS.databaseName")
      , conf.getString("PanSTARRS.mean.collectionName"))
  //---------------------------------------------------------------------------
}
//=============================================================================
import PanSTARRS_DB._
case class PanSTARRS_DB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = init()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3

  val collStack = database.getCollection(conf.getString("PanSTARRS.stack.collectionName"))
  val collForcedMean = database.getCollection(conf.getString("PanSTARRS.forcedMean.collectionName"))

  connected = true
  //---------------------------------------------------------------------------
  def existSource(id: Long) = collection.find(equal("objID", id)).resultCount > 0
  //---------------------------------------------------------------------------
  private def httpGet(objectId: Long) : Option[String] = {
    info(s"Querying PanSTARRS: '${queryUrl}objID=$objectId?format=csv'")
    val result: HttpResponse[String] =  Http(queryUrl)
      .charset("UTF-8")
      .timeout(queryConnectionTimeoutMs,queryReadTimeoutMs)
      .param("objID",objectId.toString)
      .param("format","csv")
      .asString
    if (result.code != 200) return None
    if (result.body.isEmpty) None
    else Some(result.body)
  }
  //---------------------------------------------------------------------------
  private def httpGet(raDec: Point2D_Double, searchRadius: Double, detections: Int, pagesize: Int) : Option[String] = {
    info(s"Querying PanSTARRS by position: $raDec")
    val result: HttpResponse[String] =  Http(queryUrl)
      .charset("UTF-8")
      .timeout(queryConnectionTimeoutMs,queryReadTimeoutMs)
      .param("ra",raDec.x.toString)
      .param("dec",raDec.y.toString)
      .param("radius",searchRadius.toString)
      .param("Detections",detections.toString)
      .param("pagesize",pagesize.toString)
      .param("format","csv")
      .asString
    if (result.code != 200) return None
    if (result.body.isEmpty) None
    else Some(result.body)
  }
  //---------------------------------------------------------------------------
  def downloadAndAdd(panSTARRS_ID: Long) = {
    val outputDir = Path.ensureEndWithFileSeparator(Path.createDirectoryIfNotExist(queryTemporalDir + panSTARRS_ID))
    val r = httpGet(panSTARRS_ID)
    if(r.isEmpty) false
    else {
      val bw = new BufferedWriter(new FileWriter(new File(outputDir + "tmp.csv")))
      bw.write(r.get)
      bw.close()
      PanSTARRS_Importer(outputDir, this)
      true
    }
  }
  //---------------------------------------------------------------------------
  private def getDocID_Sec(fileName: String) = {
    val br = new BufferedReader(new FileReader(new File(fileName)))
    val inputFormat = CSVFormat
      .DEFAULT
      .withHeader()
      .withSkipHeaderRecord()
    val r = new CSVParser(br, inputFormat).getRecords.asScala map { r =>
      val id = r.get(4).trim.toLong
      collection.find(equal("objID", id)).results.head
    }
    br.close()
    r
  }
  //---------------------------------------------------------------------------
  def downloadAndAdd(starName: String,raDec: Point2D_Double) : Array[Document] = {
    val outputDir = Path.ensureEndWithFileSeparator(Path.createDirectoryIfNotExist(queryTemporalDir + starName))
    val r = httpGet(raDec
      , PANSTARRS_QUERY_BY_POSITION_SEARCH_RADIUS
      , PANSTARRS_QUERY_BY_POSITION_MIN_DETECTIONS
      , PANSTARRS_QUERY_BY_POSITION_MAX_RESULT_COUNT)
    if(r.isEmpty) Array[Document]()
    else {
      val csvFileName = outputDir + starName + ".csv"
      val bw = new BufferedWriter(new FileWriter(new File(csvFileName)))
      bw.write(r.get)
      bw.close()
      PanSTARRS_Importer(outputDir, this)
      getDocID_Sec(csvFileName).toArray
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PanSTARRS_DB.scala
//=============================================================================
