/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  29/Aug/2021
 * Time:  20h:11m
 * Description: None
 */
//=============================================================================
package com.henosis.database.panStarrs
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.MongoDB.{getProjectionColList}
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import org.mongodb.scala.model.Filters.equal
import java.io.BufferedWriter
//=============================================================================
//=============================================================================
object PanSTARRS_Source {
  //---------------------------------------------------------------------------
  val PANSTARRS_PHOTOMETRY_COL_SEQ = Seq(
      "_id"
    , "objID"
    , "raStack"
    , "decStack"
    , "raMean"
    , "decMean"
    , "epochMean"
    , "ng"
    , "nr"
    , "ni"
    , "nz"
    , "ny"
    , "gMeanPSFMag"
    , "gMeanPSFMagErr"
    , "gMeanPSFMagStd"
    , "gMeanKronMag"
    , "gMeanKronMagErr"
    , "gMeanKronMagStd"
    , "gMeanApMag"
    , "gMeanApMagErr"
    , "gMeanApMagStd"
    , "rMeanPSFMag"
    , "rMeanPSFMagErr"
    , "rMeanPSFMagStd"
    , "rMeanKronMag"
    , "rMeanKronMagErr"
    , "rMeanKronMagStd"
    , "rMeanApMag"
    , "rMeanApMagErr"
    , "rMeanApMagStd"
    , "iMeanPSFMag"
    , "iMeanPSFMagErr"
    , "iMeanPSFMagStd"
    , "iMeanKronMag"
    , "iMeanKronMagErr"
    , "iMeanKronMagStd"
    , "iMeanApMag"
    , "iMeanApMagErr"
    , "iMeanApMagStd"
    , "zMeanPSFMag"
    , "zMeanPSFMagErr"
    , "zMeanPSFMagStd"
    , "zMeanKronMag"
    , "zMeanKronMagErr"
    , "zMeanKronMagStd"
    , "zMeanApMag"
    , "zMeanApMagErr"
    , "zMeanApMagStd"
    , "yMeanPSFMag"
    , "yMeanPSFMagErr"
    , "yMeanPSFMagStd"
    , "yMeanKronMag"
    , "yMeanKronMagErr"
    , "yMeanKronMagStd"
    , "yMeanApMag"
    , "yMeanApMagErr"
    , "yMeanApMagStd"
  )
  //---------------------------------------------------------------------------
  final val PANSTARRS_CSV_PHOTOMETRY_HEADER = PANSTARRS_PHOTOMETRY_COL_SEQ map ("PanSTARRS_" + _)
  //---------------------------------------------------------------------------
  def writePhotometricInfo(doc: Document, bw: BufferedWriter, sep: String = "\t") = {
    bw.write(doc("_id").asString.getValue + sep)
    bw.write(doc("objID").asInt64.getValue + sep)
    bw.write(doc("raStack").asDouble.getValue + sep)
    bw.write(doc("decStack").asDouble.getValue + sep)
    bw.write(doc("raMean").asDouble.getValue + sep)
    bw.write(doc("decMean").asDouble.getValue + sep)
    bw.write(doc("epochMean").asDouble.getValue + sep)
    bw.write(doc("ng").asInt32.getValue + sep)
    bw.write(doc("nr").asInt32.getValue + sep)
    bw.write(doc("ni").asInt32.getValue + sep)
    bw.write(doc("nz").asInt32.getValue + sep)
    bw.write(doc("ny").asInt32.getValue + sep)
    bw.write(doc("gMeanPSFMag").asDouble.getValue + sep)
    bw.write(doc("gMeanPSFMagErr").asDouble.getValue + sep)
    bw.write(doc("gMeanPSFMagStd").asDouble.getValue + sep)
    bw.write(doc("gMeanKronMag").asDouble.getValue + sep)
    bw.write(doc("gMeanKronMagErr").asDouble.getValue + sep)
    bw.write(doc("gMeanKronMagStd").asDouble.getValue + sep)
    bw.write(doc("gMeanApMag").asDouble.getValue + sep)
    bw.write(doc("gMeanApMagErr").asDouble.getValue + sep)
    bw.write(doc("gMeanApMagStd").asDouble.getValue + sep)
    bw.write(doc("rMeanPSFMag").asDouble.getValue + sep)
    bw.write(doc("rMeanPSFMagErr").asDouble.getValue + sep)
    bw.write(doc("rMeanPSFMagStd").asDouble.getValue + sep)
    bw.write(doc("rMeanKronMag").asDouble.getValue + sep)
    bw.write(doc("rMeanKronMagErr").asDouble.getValue + sep)
    bw.write(doc("rMeanKronMagStd").asDouble.getValue + sep)
    bw.write(doc("rMeanApMag").asDouble.getValue + sep)
    bw.write(doc("rMeanApMagErr").asDouble.getValue + sep)
    bw.write(doc("rMeanApMagStd").asDouble.getValue + sep)
    bw.write(doc("iMeanPSFMag").asDouble.getValue + sep)
    bw.write(doc("iMeanPSFMagErr").asDouble.getValue + sep)
    bw.write(doc("iMeanPSFMagStd").asDouble.getValue + sep)
    bw.write(doc("iMeanKronMag").asDouble.getValue + sep)
    bw.write(doc("iMeanKronMagErr").asDouble.getValue + sep)
    bw.write(doc("iMeanKronMagStd").asDouble.getValue + sep)
    bw.write(doc("iMeanApMag").asDouble.getValue + sep)
    bw.write(doc("iMeanApMagErr").asDouble.getValue + sep)
    bw.write(doc("iMeanApMagStd").asDouble.getValue + sep)
    bw.write(doc("zMeanPSFMag").asDouble.getValue + sep)
    bw.write(doc("zMeanPSFMagErr").asDouble.getValue + sep)
    bw.write(doc("zMeanPSFMagStd").asDouble.getValue + sep)
    bw.write(doc("zMeanKronMag").asDouble.getValue + sep)
    bw.write(doc("zMeanKronMagErr").asDouble.getValue + sep)
    bw.write(doc("zMeanKronMagStd").asDouble.getValue + sep)
    bw.write(doc("zMeanApMag").asDouble.getValue + sep)
    bw.write(doc("zMeanApMagErr").asDouble.getValue + sep)
    bw.write(doc("zMeanApMagStd").asDouble.getValue + sep)
    bw.write(doc("yMeanPSFMag").asDouble.getValue + sep)
    bw.write(doc("yMeanPSFMagErr").asDouble.getValue + sep)
    bw.write(doc("yMeanPSFMagStd").asDouble.getValue + sep)
    bw.write(doc("yMeanKronMag").asDouble.getValue + sep)
    bw.write(doc("yMeanKronMagErr").asDouble.getValue + sep)
    bw.write(doc("yMeanKronMagStd").asDouble.getValue + sep)
    bw.write(doc("yMeanApMag").asDouble.getValue + sep)
    bw.write(doc("yMeanApMagErr").asDouble.getValue + sep)
    bw.write(doc("yMeanApMagStd").asDouble.getValue.toString)
  }
  //-------------------------------------------------------------------------
  def getPhotometricDoc(gaiaID : Long, coll: MongoCollection[Document]) =
    Some(coll
      .find(equal("objID", gaiaID))
      .projection(getProjectionColList(PANSTARRS_PHOTOMETRY_COL_SEQ))
      .getResultDocumentSeq.head)
  //-------------------------------------------------------------------------
  def getInfo(doc: Document) = {
    val id = doc("objID").asInt64.getValue
    val ra = doc("raMean").asDouble.getValue
    val dec = doc("decMean").asDouble.getValue
    val r = Conversion.DD_to_HMS(ra)
    val d = Conversion.DD_to_DMS(dec)
    s"\n\t'PanSTARRS' ID                   : $id" +
    s"\n\t'PanSTARRS' (ra,dec)             : $ra $dec" +
    s"\n\t'PanSTARRS' (ra,dec)             : ${r._1 + ":" + r._2 + ":" + r._3} ${d._1 + ":" + d._2 + ":" + d._3} "
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file PanSTARRS_Source.scala
//=============================================================================
