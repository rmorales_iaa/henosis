/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Aug/2021
 * Time:  19h:04m
 * Description: Complete 2Mass database
 *
 */
//=============================================================================
package com.henosis.database.twoMass
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.DocumentObservable
import com.common.database.mongoDB.MongoDB
import com.common.database.mongoDB.MongoDB.getProjectionColList
import org.mongodb.scala.model.Filters.{and, equal, gte, lte}
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object TwoMassDB {
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): TwoMassDB =
    TwoMassDB(conf.getString("Database.name")
            , conf.getString("Database.collectionName"))
  //---------------------------------------------------------------------------
}
//=============================================================================
case class TwoMassDB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = init()
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  //---------------------------------------------------------------------------
  connected = true
  //-------------------------------------------------------------------------
  def findInBoxWithLimit(minRa: Double
                         , maxRa: Double
                         , minDec: Double
                         , maxDec: Double
                         , colList: Seq[String]
                         , limitResult: Int) : Seq[Document] = {
    collection.find(and(
      gte("ra", minRa)
      , lte("ra", maxRa)
      , gte("dec", minDec)
      , lte("dec", maxDec)
    )).limit(limitResult)
      .projection(getProjectionColList(colList))
      .getResultDocumentSeq
  }
  //-------------------------------------------------------------------------
  def findByDesignation(designation: String, colList: Seq[String]) : Seq[Document] = {
    collection.find( equal("designation", designation))
      .projection(getProjectionColList(colList))
      .getResultDocumentSeq
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file TwoMassDB.scala
//=============================================================================
