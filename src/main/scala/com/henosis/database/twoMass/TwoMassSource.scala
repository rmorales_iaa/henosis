/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:59m
 * Description: None
 */
//=============================================================================
package com.henosis.database.twoMass
//=============================================================================
import com.common.coordinate.conversion.Conversion
import com.common.logger.MyLogger
import org.mongodb.scala.Document
//=============================================================================
import java.io.BufferedWriter
//=============================================================================
//=============================================================================
object TwoMassSource extends MyLogger{
  //---------------------------------------------------------------------------
  val TWO_MASS_COL_PHOTOMETRY_SEQ = Seq(
    "ra"
    , "dec"
    , "designation"
    , "j_m"
    , "j_cmsig"
    , "j_msigcom"
    , "j_snr"
    , "h_m"
    , "h_cmsig"
    , "h_msigcom"
    , "h_snr"
    , "k_m"
    , "k_cmsig"
    , "k_msigcom"
    , "k_snr"
    , "ph_qual"

    , "date"
    , "jdate"

    , "j_m_stdap"
    , "j_msig_stdap"
    , "h_m_stdap"
    , "h_msig_stdap"
    , "k_m_stdap"
    , "k_msig_stdap"

    , "a"

    , "b_m_opt"
    , "vr_m_opt"
  )
  //---------------------------------------------------------------------------
  final val TWO_MASS_COL_CSV_PHOTOMETRY_HEADER = TWO_MASS_COL_PHOTOMETRY_SEQ map ("2MASS_" + _)
  //---------------------------------------------------------------------------
  def writePhotometricInfo(doc: Document, bw: BufferedWriter, sep: String = "\t") = {
    bw.write(doc("ra").asDouble.getValue + sep)
    bw.write(doc("dec").asDouble.getValue + sep)
    bw.write(doc("designation").asString.getValue+ sep)
    bw.write(doc("j_m").asDouble.getValue + sep)
    bw.write(doc("j_cmsig").asDouble.getValue + sep)
    bw.write(doc("j_msigcom").asDouble.getValue + sep)
    bw.write(doc("j_snr").asDouble.getValue + sep)
    bw.write(doc("h_m").asDouble.getValue + sep)
    bw.write(doc("h_cmsig").asDouble.getValue + sep)
    bw.write(doc("h_msigcom").asDouble.getValue + sep)
    bw.write(doc("h_snr").asDouble.getValue + sep)
    bw.write(doc("k_m").asDouble.getValue + sep)
    bw.write(doc("k_cmsig").asDouble.getValue + sep)
    bw.write(doc("k_msigcom").asDouble.getValue + sep)
    bw.write(doc("k_snr").asDouble.getValue + sep)
    bw.write(doc("ph_qual").asString.getValue + sep)
    bw.write(doc("date").asString.getValue + sep)
    bw.write(doc("jdate").asDouble.getValue + sep)
    bw.write(doc("j_m_stdap").asDouble.getValue + sep)
    bw.write(doc("j_msig_stdap").asDouble.getValue + sep)
    bw.write(doc("h_m_stdap").asDouble.getValue + sep)
    bw.write(doc("h_msig_stdap").asDouble.getValue + sep)
    bw.write(doc("k_m_stdap").asDouble.getValue + sep)
    bw.write(doc("k_msig_stdap").asDouble.getValue + sep)
    bw.write(doc("a").asString.getValue + sep)
    bw.write(doc("b_m_opt").asDouble.getValue + sep)
    bw.write(doc("vr_m_opt").asDouble.getValue + sep)
  }
  //---------------------------------------------------------------------------
  def getInfo(doc: Document) = {
    val id = doc("designation").asString.getValue
    val ra = doc("ra").asDouble.getValue
    val dec = doc("dec").asDouble.getValue
    val r = Conversion.DD_to_HMS(ra)
    val d = Conversion.DD_to_DMS(dec)
    s"\n\t'2Mass' ID                   : $id" +
    s"\n\t'2Mass' (ra,dec)             : $ra $dec" +
    s"\n\t'2Mass' (ra,dec)             : ${r._1 + ":" + r._2 + ":" + r._3} ${d._1 + ":" + d._2 + ":" + d._3} "
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file TwoMassSource.scala
//=============================================================================
