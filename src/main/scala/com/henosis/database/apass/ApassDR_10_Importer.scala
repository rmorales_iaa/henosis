/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  02/Jun/2022
  * Time:  11h:40m
  * Description: None
  */
//=============================================================================
package com.henosis.database.apass
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.database.mongoDB.database.apass.{ApassDB, ApassSource}
import com.common.hardware.cpu.CPU
import com.common.{DatabaseImporter, LineEntry}
import com.common.csv.CsvFile.{getDoubleOrDefault, getIntOrDefault}
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import scala.io.Source
//=============================================================================
//=============================================================================
object ApassDR_10_Importer {
  //---------------------------------------------------------------------------
  private def loadSeqFromCsv(csvFileName: String) = {
    //-------------------------------------------------------------------------
    var index = -1
    //-------------------------------------------------------------------------
    def getNewIndex() = {
      index += 1
      index
    }
    //---------------------------------------------------------------------------
    val bufferedSource = Source.fromFile(csvFileName)

    val result = bufferedSource.getLines.zipWithIndex.flatMap { case (line,i) =>
      if (i == 0) None
      else {
        val r = line.split(",")
        index = -1
        Some(ApassSource(
          getIntOrDefault(r(getNewIndex())) // id  : Int
          , r(getNewIndex()) // idstr: String
          , r(getNewIndex()) // field_id: String
          , getDoubleOrDefault(r(getNewIndex())) // ra      :Double
          , getDoubleOrDefault(r(getNewIndex())) // ra_sig  :Double
          , getDoubleOrDefault(r(getNewIndex())) // dec     :Double
          , getDoubleOrDefault(r(getNewIndex())) // dec_sig :Double
          , getIntOrDefault(r(getNewIndex())) // zone_id : Int
          , getIntOrDefault(r(getNewIndex())) // node_id :Int
          , getIntOrDefault(r(getNewIndex())) // container_id : Int
          , getDoubleOrDefault(r(getNewIndex())) // container_width :Double
          , getDoubleOrDefault(r(getNewIndex())) // container_height :Double
          , getDoubleOrDefault(r(getNewIndex())) // container_area  :Double
          , getIntOrDefault(r(getNewIndex())) // good_obs : Int
          , getIntOrDefault(r(getNewIndex())) // num_obs_B: Int
          , getIntOrDefault(r(getNewIndex())) // num_obs_V: Int
          , getIntOrDefault(r(getNewIndex())) // num_obs_su: Int
          , getIntOrDefault(r(getNewIndex())) // num_obs_sg: Int
          , getIntOrDefault(r(getNewIndex())) // num_obs_sr: Int
          , getIntOrDefault(r(getNewIndex())) // num_obs_si: Int
          , getIntOrDefault(r(getNewIndex())) // num_obs_sz: Int
          , getIntOrDefault(r(getNewIndex())) // num_obs_Ha: Int
          , getIntOrDefault(r(getNewIndex())) // num_obs_ZS: Int
          , getIntOrDefault(r(getNewIndex())) // num_obs_Y: Int
          , getIntOrDefault(r(getNewIndex())) // num_nights_B: Int
          , getIntOrDefault(r(getNewIndex())) // num_nights_V: Int
          , getIntOrDefault(r(getNewIndex())) // num_nights_su: Int
          , getIntOrDefault(r(getNewIndex())) // num_nights_sg: Int
          , getIntOrDefault(r(getNewIndex())) // num_nights_sr: Int
          , getIntOrDefault(r(getNewIndex())) // num_nights_si: Int
          , getIntOrDefault(r(getNewIndex())) // num_nights_sz: Int
          , getIntOrDefault(r(getNewIndex())) // num_nights_Ha: Int
          , getIntOrDefault(r(getNewIndex())) // num_nights_ZS: Int
          , getIntOrDefault(r(getNewIndex())) // num_nights_Y: Int
          , getDoubleOrDefault(r(getNewIndex())) // B :Double
          , getDoubleOrDefault(r(getNewIndex())) // V :Double
          , getDoubleOrDefault(r(getNewIndex())) // su :Double
          , getDoubleOrDefault(r(getNewIndex())) // sg :Double
          , getDoubleOrDefault(r(getNewIndex())) // sr :Double
          , getDoubleOrDefault(r(getNewIndex())) // si :Double
          , getDoubleOrDefault(r(getNewIndex())) // sz :Double
          , getDoubleOrDefault(r(getNewIndex())) // Ha :Double
          , getDoubleOrDefault(r(getNewIndex())) // ZS :Double
          , getDoubleOrDefault(r(getNewIndex())) // Y :Double
          , getDoubleOrDefault(r(getNewIndex())) // B_sig :Double
          , getDoubleOrDefault(r(getNewIndex())) // V_sig :Double
          , getDoubleOrDefault(r(getNewIndex())) // su_sig :Double
          , getDoubleOrDefault(r(getNewIndex())) // sg_sig :Double
          , getDoubleOrDefault(r(getNewIndex())) // sr_sig :Double
          , getDoubleOrDefault(r(getNewIndex())) // si_sig :Double
          , getDoubleOrDefault(r(getNewIndex())) // sz_sig :Double
          , getDoubleOrDefault(r(getNewIndex())) // Ha_sig :Double
          , getDoubleOrDefault(r(getNewIndex())) // ZS_sig :Double
          , getDoubleOrDefault(r(getNewIndex())) //Y_sig :Double
        ))
      }
    }
   // bufferedSource.close
    result.toArray
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class ApassDR_10_Importer(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = ApassDB(conf)
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[ApassSource] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    val sourceSeq = ApassDR_10_Importer.loadSeqFromCsv(path)
    collWithRegistry.insertMany(sourceSeq).results()
    true
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true, fileExtension = ".csv")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ApassDR_10_Importer.scala
//=============================================================================
