/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Jun/2022
 * Time:  11h:40m
 * Description: None
 */
//=============================================================================
package com.henosis.database.wise.allSky
//=============================================================================
import com.common.DatabaseImporter.CSV_EXTENSION_FILE
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.hardware.cpu.CPU
import com.common.util.path.Path
import com.common.{DatabaseImporter, LineEntry}
import com.henosis.database.wise.allSky.WiseAllSky.COL_DEFINITION_SEQ
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object WiseAllSkyImporter {
  //---------------------------------------------------------------------------
  private val colIndexSeq = COL_DEFINITION_SEQ map(_.colPos)
  private val maxColIndexSeq = colIndexSeq.max
  private val maxBufferedDocument = 5 * 1000
  //---------------------------------------------------------------------------
  def getInt(s: String) =
    if (s.trim.isEmpty) Int.MinValue
    else s.trim.toInt
  //---------------------------------------------------------------------------
  def getLong(s: String) =
    if (s.trim.isEmpty) Long.MinValue
    else s.trim.toLong
  //---------------------------------------------------------------------------
  def getDouble(s: String) =
    if (s.trim.isEmpty) Double.NaN
    else s.trim.toDouble
  //---------------------------------------------------------------------------
  private def processFile(path: String
                         , collWithRegistry: MongoCollection[WiseAllSky]): Boolean = {
    val br = new BufferedReader(new FileReader(new File(path)))
    val sourceSeq = ArrayBuffer[WiseAllSky]()
    var line = br.readLine()
    while(line != null &&  line.length()!=0 ) {
      sourceSeq += getSource(line)
      if (sourceSeq.length > maxBufferedDocument) {
        collWithRegistry.insertMany(sourceSeq.toArray).results()
        sourceSeq.clear()
      }
      line = br.readLine()
    }
    br.close()

    //store the remain buffered docs
    if (sourceSeq.length > 0) {
      collWithRegistry.insertMany(sourceSeq.toArray).results()
      sourceSeq.clear()
    }
    true
  }
  //---------------------------------------------------------------------------
  private def getSource(line: String): WiseAllSky = {
    val _itemSeq = line.split("\\|")

    val itemSeq =
      if(_itemSeq.length < maxColIndexSeq)
        _itemSeq ++ Array.fill[String](maxColIndexSeq - _itemSeq.length + 1)("")
      else
        _itemSeq

    var index = -1
    WiseAllSky(
      { index += 1; getLong(itemSeq(colIndexSeq(index))) },   // _id (Long)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // ra (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // dec (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w1mpro (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w1snr (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w2mpro (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w2snr (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w3mpro (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w3snr (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w4mpro (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w4snr (Double)
      { index += 1; itemSeq(colIndexSeq(index)) },            // cc_flags (String)
      { index += 1; itemSeq(colIndexSeq(index)) },            // var_flg (String)
      { index += 1; itemSeq(colIndexSeq(index)) },            // ph_qual (String)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w1mag (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w1sigm (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w2mag (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w2sigm (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w3mag (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w3sigm (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w4mag (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w4sigm (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w1magp (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w1sigp1 (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w1mjdmean (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w2magp (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w2sigp1 (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w2mjdmean (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w3magp (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w3sigp1 (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w3mjdmean (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w4magp (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w4sigp1 (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // w4mjdmean (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // xscprox (Double)
      { index += 1; getInt(itemSeq(colIndexSeq(index))) },    // tmass_key (Integer)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // r_2mass (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // n_2mass (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // j_m_2mass (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // j_msig_2mass (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // h_m_2mass (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // h_msig_2mass (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }, // k_m_2mass (Double)
      { index += 1; getDouble(itemSeq(colIndexSeq(index))) }  // k_msig_2mass (Double)
    )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class WiseAllSkyImporter(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = WiseAllSkyDB(conf)
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[WiseAllSky] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    mongoDB.createIndexes(Array("ra","dec"))
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    WiseAllSkyImporter.processFile(path, collWithRegistry)
    true
  }
  //---------------------------------------------------------------------------
  override  def run(dropCollection : Boolean = true, fileExtension: String = CSV_EXTENSION_FILE) : Unit = {
    if (Path.directoryExist(iDir)) {
      if (dropCollection) mongoDB.dropCollection()
      val pathSeq = Path.getSortedFileList(iDir)
        .map(_.getAbsolutePath)
        .filter(_.contains("cat-part"))
      new MyParallelTask(pathSeq, mongoDB.getCollection)
      info(s"Total documents inserted in: ${mongoDB.getBasicConnectionInfo()}: ${mongoDB.getCollection.countDocuments().results.head}")
      createIndexes
    }
    else error(s"Input directory '$iDir' does not exists")
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true, fileExtension = "cat-part")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file WiseAllSkyImporter.scala
//=============================================================================
