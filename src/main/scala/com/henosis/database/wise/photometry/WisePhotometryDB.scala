/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Aug/2021
 * Time:  19h:04m
 * Description:
 * https://irsa.ipac.caltech.edu/data/download/
 * https://irsa.ipac.caltech.edu/data/download/wise-allwise/wise-allwise-cat-schema.txt
 */
//=============================================================================
package com.henosis.database.wise.photometry
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.MongoDB
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object WisePhotometryDB {
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): WisePhotometryDB =
    WisePhotometryDB(conf.getString("Database.name")
               , conf.getString("Database.photometry.collectionName"))
  //---------------------------------------------------------------------------
}
//=============================================================================
case class WisePhotometryDB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(WisePhotometry.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[WisePhotometry] =  database.getCollection(collectionName)
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file WiseAllSkyDB.scala
//=============================================================================
