/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:59m
 * Description: WISE source
 * https://irsa.ipac.caltech.edu/data/download/
 * https://irsa.ipac.caltech.edu/data/download/wise-allwise/wise-allwise-cat-schema.txt
 */
package com.henosis.database.wise.photometry
//=============================================================================
//=============================================================================
import com.common.LineEntry
import com.common.logger.MyLogger
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._

import scala.language.existentials
//=============================================================================
//=============================================================================
object WisePhotometry extends MyLogger {
  //---------------------------------------------------------------------------
  //http://mongodb.github.io/mongo-scala-driver/2.3/getting-started/quick-tour-case-classes/
  //https://jannikarndt.de/blog/2017/08/writing_case_classes_to_mongodb_in_scala/
  val codecRegistry = fromRegistries(fromProviders(classOf[WisePhotometry]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[WisePhotometry].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[WisePhotometry].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private final val COMPLETE_COL_DEFINITION_SEQ = Seq(
      LineEntry("source_id_mf","string")
    , LineEntry("ra","integer")
    , LineEntry("dec","integer")
    , LineEntry("w1x_ep","integer")
    , LineEntry("w1y_ep","integer")
    , LineEntry("w1mpro_ep","integer")
    , LineEntry("w1sigmpro_ep","integer")
    , LineEntry("w1rchi2_ep","float")
    , LineEntry("w1flux_ep","float")
    , LineEntry("w1sigflux_ep","float")
    , LineEntry("w2x_ep","double")
    , LineEntry("w2y_ep","double")
    , LineEntry("w2mpro_ep","double")
    , LineEntry("w2sigmpro_ep"," double")
    , LineEntry("w2rchi2_ep","double")
    , LineEntry("w2flux_ep","double")
    , LineEntry("w2sigflux_ep","double")
    , LineEntry("w3x_ep","double")
    , LineEntry("w3y_ep","double")
    , LineEntry("w3mpro_ep","double")
    , LineEntry("w3sigmpro_ep","double")
    , LineEntry("w3rchi2_ep","double")
    , LineEntry("w3flux_ep","double")
    , LineEntry("w3sigflux_ep","double")
    , LineEntry("w4x_ep","double")
    , LineEntry("w4y_ep","double")
    , LineEntry("w4mpro_ep","double")
    , LineEntry("w4sigmpro_ep","double")
    , LineEntry("w4rchi2_ep","double")
    , LineEntry("w4flux_ep","double")
    , LineEntry("w4sigflux_ep","double")
    , LineEntry("mjd","double")
    , LineEntry("frame_id","string")
    , LineEntry("nb","integer")
    , LineEntry("na","integer")
    , LineEntry("cc_flags","string")
    , LineEntry("cntr_mf","long")
    , LineEntry("cat","integer")
    , LineEntry("qi_fact","integer")
    , LineEntry("saa_sep","integer")
    , LineEntry("moon_masked","string")
    , LineEntry("load_id","integer")
    , LineEntry("cntr","Long")
    , LineEntry("x","double")
    , LineEntry("y","double")
    , LineEntry("z","double")
    , LineEntry("spt_ind","integer")
    , LineEntry("htm20"," integer")
  )
  //---------------------------------------------------------------------------
  private final val COL_NAME_DEFINITION_SEQ = Seq(
      "cntr"
    , "cntr_mf"
    , "nb"
    , "cc_flags"
    , "mjd"
    , "w1mpro_ep"
    , "w1sigmpro_ep"
    , "w2mpro_ep"
    , "w2sigmpro_ep"
    , "w3mpro_ep"
    , "w3sigmpro_ep"
    , "w4mpro_ep"
    , "w4sigmpro_ep"
  )
  //---------------------------------------------------------------------------
  final val COL_DEFINITION_SEQ = COL_NAME_DEFINITION_SEQ.flatMap { colName=>
    COMPLETE_COL_DEFINITION_SEQ.find(_.name == colName)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class WisePhotometry(_id: Long //cntr
                          , cntr_mf: Long
                          , nb: Int
                          , cc_flags:String
                          , mjd: Double
                          , w1mpro_ep: Double
                          , w1sigmpro_ep: Double
                          , w2mpro_ep: Double
                          , w2sigmpro_ep: Double
                          , w3mpro_ep: Double
                          , w3sigmpro_ep: Double
                          , w4mpro_ep: Double
                          , w4sigmpro_ep: Double)
//=============================================================================
//End of file WisePhotometry.scala
//=============================================================================

