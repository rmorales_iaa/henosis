/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Jun/2022
 * Time:  11h:40m
 * Description:
 * https://irsa.ipac.caltech.edu/data/download/
 * https://irsa.ipac.caltech.edu/data/download/wise-allwise/wise-allwise-cat-schema.txt
 */
//=============================================================================
package com.henosis.database.wise.photometry
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.hardware.cpu.CPU
import com.common.{DatabaseImporter, LineEntry}
import com.henosis.database.wise.photometry.WisePhotometry.COL_DEFINITION_SEQ
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object WisePhotometryImporter {
  //---------------------------------------------------------------------------
  private val colIndexSeq = COL_DEFINITION_SEQ map(_.colPos)
  private val maxBufferedDocument = 5 * 1000
  //---------------------------------------------------------------------------
  def getInt(s: String) =
    if (s.trim.isEmpty) Int.MinValue
    else s.trim.toInt
  //---------------------------------------------------------------------------
  def getLong(s: String) =
    if (s.trim.isEmpty) Long.MinValue
    else s.trim.toLong
  //---------------------------------------------------------------------------
  def getDouble(s: String) =
    if (s.trim.isEmpty) Double.NaN
    else s.trim.toDouble
  //---------------------------------------------------------------------------
  private def processFile(path: String
                         , collWithRegistry: MongoCollection[WisePhotometry]): Boolean = {
    val br = new BufferedReader(new FileReader(new File(path)))
    val sourceSeq = ArrayBuffer[WisePhotometry]()
    var line = br.readLine()
    while(line != null &&  line.length()!=0 ) {
      sourceSeq += getSource(line)
      if (sourceSeq.length > maxBufferedDocument) {
        collWithRegistry.insertMany(sourceSeq.toArray).results()
        sourceSeq.clear()
      }
      line = br.readLine()
    }
    br.close()

    //store the remain buffered docs
    if (sourceSeq.length > 0) {
      collWithRegistry.insertMany(sourceSeq.toArray).results()
      sourceSeq.clear()
    }
    true
  }
  //---------------------------------------------------------------------------
  private def getSource(line: String) = {
    val itemSeq = line.split("\\|")

    WisePhotometry(
        getLong(itemSeq(colIndexSeq(0)))
      , getLong(itemSeq(colIndexSeq(1)))
      , getInt(itemSeq(colIndexSeq(2)))
      , itemSeq(colIndexSeq(3))
      , getDouble(itemSeq(colIndexSeq(4)))
      , getDouble(itemSeq(colIndexSeq(5)))
      , getDouble(itemSeq(colIndexSeq(6)))
      , getDouble(itemSeq(colIndexSeq(7)))
      , getDouble(itemSeq(colIndexSeq(8)))
      , getDouble(itemSeq(colIndexSeq(9)))
      , getDouble(itemSeq(colIndexSeq(10)))
      , getDouble(itemSeq(colIndexSeq(11)))
      , getDouble(itemSeq(colIndexSeq(12)))
    )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class WisePhotometryImporter(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = WisePhotometryDB(conf)
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[WisePhotometry] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    mongoDB.createIndexes(Array("ra","dec"))
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    WisePhotometryImporter.processFile(path, collWithRegistry)
    true
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true, fileExtension = ".unl")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file WisePhotometryImporter.scala
//=============================================================================
