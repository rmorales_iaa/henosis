/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Jun/2022
 * Time:  11h:40m
 * Description: None
 */
//=============================================================================
package com.henosis.database.wise.source
//=============================================================================
import com.common.DatabaseImporter.CSV_EXTENSION_FILE
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.hardware.cpu.CPU
import com.common.util.path.Path
import com.common.{DatabaseImporter, LineEntry}
import com.henosis.database.wise.source.WiseSource.COL_DEFINITION_SEQ
//=============================================================================
import org.mongodb.scala.{Document, MongoCollection}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object WiseSourceImporter {
  //---------------------------------------------------------------------------
  private val colIndexSeq = COL_DEFINITION_SEQ map(_.colPos)
  private val maxBufferedDocument = 5 *  1000
  //---------------------------------------------------------------------------
  def getInt(s: String) =
    if (s.trim.isEmpty) Int.MinValue
    else s.trim.toInt
  //---------------------------------------------------------------------------
  def getLong(s: String) =
    if (s.trim.isEmpty) Long.MinValue
    else s.trim.toLong
  //---------------------------------------------------------------------------
  def getDouble(s: String) =
    if (s.trim.isEmpty) Double.NaN
    else s.trim.toDouble
  //---------------------------------------------------------------------------
  private def processFile(path: String
                         , collWithRegistry: MongoCollection[WiseSource]): Boolean = {
    val br = new BufferedReader(new FileReader(new File(path)))
    val sourceSeq = ArrayBuffer[WiseSource]()
    var line = br.readLine()
    while(line != null &&  line.length()!=0 ) {
      sourceSeq += getSource(line)
      if (sourceSeq.length > maxBufferedDocument) {
        collWithRegistry.insertMany(sourceSeq.toArray).results()
        sourceSeq.clear()
      }
      line = br.readLine()
    }
    br.close()

    //store the remain buffered docs
    if (sourceSeq.length > 0) {
      collWithRegistry.insertMany(sourceSeq.toArray).results()
      sourceSeq.clear()
    }
    true
  }
  //---------------------------------------------------------------------------
  private def getSource(line: String) = {
    val itemSeq = line.split("\\|")
    WiseSource(
        getLong(itemSeq(colIndexSeq(0)))
      , getDouble(itemSeq(colIndexSeq(1)))
      , getDouble(itemSeq(colIndexSeq(2)))
      , getDouble(itemSeq(colIndexSeq(3)))
      , getDouble(itemSeq(colIndexSeq(4)))
      , getDouble(itemSeq(colIndexSeq(5)))
      , getDouble(itemSeq(colIndexSeq(6)))
      , getDouble(itemSeq(colIndexSeq(7)))
      , getDouble(itemSeq(colIndexSeq(8)))
      , getDouble(itemSeq(colIndexSeq(9)))
      , getDouble(itemSeq(colIndexSeq(10)))
      , itemSeq(colIndexSeq(11))
      , getDouble(itemSeq(colIndexSeq(12)))
      , getDouble(itemSeq(colIndexSeq(13)))
      , getDouble(itemSeq(colIndexSeq(14)))
      , getDouble(itemSeq(colIndexSeq(15)))
      , getDouble(itemSeq(colIndexSeq(16)))
      , getDouble(itemSeq(colIndexSeq(17)))
      , getDouble(itemSeq(colIndexSeq(18)))
      , getDouble(itemSeq(colIndexSeq(19)))
      , getDouble(itemSeq(colIndexSeq(20)))
      , getDouble(itemSeq(colIndexSeq(21)))
      , getDouble(itemSeq(colIndexSeq(22)))
      , getDouble(itemSeq(colIndexSeq(23)))
      , getDouble(itemSeq(colIndexSeq(24)))
      , getInt(itemSeq(colIndexSeq(25)))
      , getDouble(itemSeq(colIndexSeq(26)))
      , getInt(itemSeq(colIndexSeq(27)))
      , getDouble(itemSeq(colIndexSeq(28)))
      , getDouble(itemSeq(colIndexSeq(29)))
      , getDouble(itemSeq(colIndexSeq(30)))
      , getDouble(itemSeq(colIndexSeq(31)))
      , getDouble(itemSeq(colIndexSeq(32)))
      , getDouble(itemSeq(colIndexSeq(33)))
    )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class WiseSourceImporter(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = WiseSourceDB(conf)
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[WiseSource] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    mongoDB.createIndexes(Array("ra","dec"))
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    WiseSourceImporter.processFile(path, collWithRegistry)
    true
  }
  //---------------------------------------------------------------------------
  override  def run(dropCollection : Boolean = true, fileExtension: String = CSV_EXTENSION_FILE) : Unit = {
    if (Path.directoryExist(iDir)) {
      if (dropCollection) mongoDB.dropCollection()
      val pathSeq = Path.getSortedFileList(iDir)
        .map(_.getAbsolutePath)
        .filter(_.contains("cat-part"))
      new MyParallelTask(pathSeq, mongoDB.getCollection)
      info(s"Total documents inserted in: ${mongoDB.getBasicConnectionInfo()}: ${mongoDB.getCollection.countDocuments().results.head}")
      createIndexes
    }
    else error(s"Input directory '$iDir' does not exists")
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true, fileExtension = "cat-part")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file WisePhotometryImporter.scala
//=============================================================================
