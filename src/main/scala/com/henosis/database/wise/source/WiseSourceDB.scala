/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Aug/2021
 * Time:  19h:04m
 * Description: Complete 2Mass database
 *
 */
//=============================================================================
package com.henosis.database.wise.source
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.MongoDB
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object WiseSourceDB {
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): WiseSourceDB =
    WiseSourceDB(conf.getString("Database.name")
               , conf.getString("Database.source.collectionName"))
  //---------------------------------------------------------------------------
}
//=============================================================================
case class WiseSourceDB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(WiseSource.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[WiseSource] =  database.getCollection(collectionName)
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file WiseSourceDB.scala
//=============================================================================
