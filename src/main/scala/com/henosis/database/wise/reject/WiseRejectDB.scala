/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/Aug/2021
 * Time:  19h:04m
 * Description: Complete 2Mass database
 *
 */
//=============================================================================
package com.henosis.database.wise.reject

//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.database.mongoDB.MongoDB
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
object WiseRejectDB {
  //---------------------------------------------------------------------------
  def apply(conf: MyConf): WiseRejectDB =
    WiseRejectDB(conf.getString("Database.name")
               , conf.getString("Database.reject.collectionName"))
  //---------------------------------------------------------------------------
}
//=============================================================================
case class WiseRejectDB(databaseName: String, collectionName: String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) = initWithCodeRegister(WiseReject.codecRegistry)
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  val collWithRegistry: MongoCollection[WiseReject] =  database.getCollection(collectionName)
  //---------------------------------------------------------------------------
  connected = true
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file WiseRejectDB.scala
//=============================================================================
