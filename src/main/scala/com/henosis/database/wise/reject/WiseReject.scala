/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/Aug/2021
 * Time:  20h:59m
 * Description: WISE source
 * https://irsa.ipac.caltech.edu/data/download/
 * https://irsa.ipac.caltech.edu/data/download/wise-allwise/wise-allwise-cat-schema.txt
 */
package com.henosis.database.wise.reject
//=============================================================================
//=============================================================================
import com.common.LineEntry
import com.common.logger.MyLogger
//=============================================================================
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._

import scala.language.existentials
//=============================================================================
//=============================================================================
object WiseReject extends MyLogger {
  //---------------------------------------------------------------------------
  //http://mongodb.github.io/mongo-scala-driver/2.3/getting-started/quick-tour-case-classes/
  //https://jannikarndt.de/blog/2017/08/writing_case_classes_to_mongodb_in_scala/
  val codecRegistry = fromRegistries(fromProviders(classOf[WiseReject]), DEFAULT_CODEC_REGISTRY)
  //---------------------------------------------------------------------------
  def getColNameSeq(prefix: String) =
    classOf[WiseReject].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  def getColNameValueSeq(prefix: String = "") =
    classOf[WiseReject].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private final val COMPLETE_COL_DEFINITION_SEQ = Seq(
    LineEntry("designation", "string"),
    LineEntry("ra", "double"),
    LineEntry("dec", "double"),
    LineEntry("sigra", "double"),
    LineEntry("sigdec", "double"),
    LineEntry("sigradec", "double"),
    LineEntry("glon", "double"),
    LineEntry("glat", "double"),
    LineEntry("elon", "double"),
    LineEntry("elat", "double"),
    LineEntry("wx", "double"),
    LineEntry("wy", "double"),
    LineEntry("cntr", "long"),
    LineEntry("source_id", "string"),
    LineEntry("coadd_id", "string"),
    LineEntry("src", "integer"),
    LineEntry("w1mpro", "double"),
    LineEntry("w1sigmpro", "double"),
    LineEntry("w1snr", "double"),
    LineEntry("w1rchi2", "float"),
    LineEntry("w2mpro", "double"),
    LineEntry("w2sigmpro", "double"),
    LineEntry("w2snr", "double"),
    LineEntry("w2rchi2", "float"),
    LineEntry("w3mpro", "double"),
    LineEntry("w3sigmpro", "double"),
    LineEntry("w3snr", "double"),
    LineEntry("w3rchi2", "float"),
    LineEntry("w4mpro", "double"),
    LineEntry("w4sigmpro", "double"),
    LineEntry("w4snr", "double"),
    LineEntry("w4rchi2", "float"),
    LineEntry("rchi2", "float"),
    LineEntry("nb", "integer"),
    LineEntry("na", "integer"),
    LineEntry("w1sat", "double"),
    LineEntry("w2sat", "double"),
    LineEntry("w3sat", "double"),
    LineEntry("w4sat", "double"),
    LineEntry("satnum", "string"),
    LineEntry("ra_pm", "double"),
    LineEntry("dec_pm", "double"),
    LineEntry("sigra_pm", "double"),
    LineEntry("sigdec_pm", "double"),
    LineEntry("sigradec_pm", "double"),
    LineEntry("pmra", "integer"),
    LineEntry("sigpmra", "integer"),
    LineEntry("pmdec", "integer"),
    LineEntry("sigpmdec", "integer"),
    LineEntry("w1rchi2_pm", "float"),
    LineEntry("w2rchi2_pm", "float"),
    LineEntry("w3rchi2_pm", "float"),
    LineEntry("w4rchi2_pm", "float"),
    LineEntry("rchi2_pm", "float"),
    LineEntry("pmcode", "string"),
    LineEntry("cc_flags", "string"),
    LineEntry("rel", "string"),
    LineEntry("ext_flg", "integer"),
    LineEntry("var_flg", "string"),
    LineEntry("ph_qual", "string"),
    LineEntry("det_bit", "integer"),
    LineEntry("moon_lev", "integer"),
    LineEntry("w1nm", "integer"),
    LineEntry("w1m", "integer"),
    LineEntry("w2nm", "integer"),
    LineEntry("w2m", "integer"),
    LineEntry("w3nm", "integer"),
    LineEntry("w3m", "integer"),
    LineEntry("w4nm", "integer"),
    LineEntry("w4m", "integer"),
    LineEntry("w1cov", "double"),
    LineEntry("w2cov", "double"),
    LineEntry("w3cov", "double"),
    LineEntry("w4cov", "double"),
    LineEntry("w1cc_map", "integer"),
    LineEntry("w1cc_map_str", "string"),
    LineEntry("w2cc_map", "integer"),
    LineEntry("w2cc_map_str", "string"),
    LineEntry("w3cc_map", "integer"),
    LineEntry("w3cc_map_str", "string"),
    LineEntry("w4cc_map", "integer"),
    LineEntry("w4cc_map_str", "string"),
    LineEntry("use_src", "smallint"),
    LineEntry("best_use_cntr", "int8"),
    LineEntry("ngrp", "smallint"),
    LineEntry("w1flux", "float"),
    LineEntry("w1sigflux", "float"),
    LineEntry("w1sky", "double"),
    LineEntry("w1sigsk", "double"),
    LineEntry("w1conf", "double"),
    LineEntry("w2flux", "float"),
    LineEntry("w2sigflux", "float"),
    LineEntry("w2sky", "double"),
    LineEntry("w2sigsk", "double"),
    LineEntry("w2conf", "double"),
    LineEntry("w3flux", "float"),
    LineEntry("w3sigflux", "float"),
    LineEntry("w3sky", "double"),
    LineEntry("w3sigsk", "double"),
    LineEntry("w3conf", "double"),
    LineEntry("w4flux", "float"),
    LineEntry("w4sigflux", "float"),
    LineEntry("w4sky", "double"),
    LineEntry("w4sigsk", "double"),
    LineEntry("w4conf", "double"),
    LineEntry("w1mag", "double"),
    LineEntry("w1sigm", "double"),
    LineEntry("w1flg", "integer"),
    LineEntry("w1mcor", "double"),
    LineEntry("w2mag", "double"),
    LineEntry("w2sigm", "double"),
    LineEntry("w2flg", "integer"),
    LineEntry("w2mcor", "double"),
    LineEntry("w3mag", "double"),
    LineEntry("w3sigm", "double"),
    LineEntry("w3flg", "integer"),
    LineEntry("w3mcor", "double"),
    LineEntry("w4mag", "double"),
    LineEntry("w4sigm", "double"),
    LineEntry("w4flg", "integer"),
    LineEntry("w4mcor", "double"),
    LineEntry("w1mag_1", "double"),
    LineEntry("w1sigm_1", "double"),
    LineEntry("w1flg_1", "integer"),
    LineEntry("w2mag_1", "double"),
    LineEntry("w2sigm_1", "double"),
    LineEntry("w2flg_1", "integer"),
    LineEntry("w3mag_1", "double"),
    LineEntry("w3sigm_1", "double"),
    LineEntry("w3flg_1", "integer"),
    LineEntry("w4mag_1", "double"),
    LineEntry("w4sigm_1", "double"),
    LineEntry("w4flg_1", "integer"),
    LineEntry("w1mag_2", "double"),
    LineEntry("w1sigm_2", "double"),
    LineEntry("w1flg_2", "integer"),
    LineEntry("w2mag_2", "double"),
    LineEntry("w2sigm_2", "double"),
    LineEntry("w2flg_2", "integer"),
    LineEntry("w3mag_2", "double"),
    LineEntry("w3sigm_2", "double"),
    LineEntry("w3flg_2", "integer"),
    LineEntry("w4mag_2", "double"),
    LineEntry("w4sigm_2", "double"),
    LineEntry("w4flg_2", "integer"),
    LineEntry("w1mag_3", "double"),
    LineEntry("w1sigm_3", "double"),
    LineEntry("w1flg_3", "integer"),
    LineEntry("w2mag_3", "double"),
    LineEntry("w2sigm_3", "double"),
    LineEntry("w2flg_3", "integer"),
    LineEntry("w3mag_3", "double"),
    LineEntry("w3sigm_3", "double"),
    LineEntry("w3flg_3", "integer"),
    LineEntry("w4mag_3", "double"),
    LineEntry("w4sigm_3", "double"),
    LineEntry("w4flg_3", "integer"),
    LineEntry("w1mag_4", "double"),
    LineEntry("w1sigm_4", "double"),
    LineEntry("w1flg_4", "integer"),
    LineEntry("w2mag_4", "double"),
    LineEntry("w2sigm_4", "double"),
    LineEntry("w2flg_4", "integer"),
    LineEntry("w3mag_4", "double"),
    LineEntry("w3sigm_4", "double"),
    LineEntry("w3flg_4", "integer"),
    LineEntry("w4mag_4", "double"),
    LineEntry("w4sigm_4", "double"),
    LineEntry("w4flg_4", "integer"),
    LineEntry("w1mag_5", "double"),
    LineEntry("w1sigm_5", "double"),
    LineEntry("w1flg_5", "integer"),
    LineEntry("w2mag_5", "double"),
    LineEntry("w2sigm_5", "double"),
    LineEntry("w2flg_5", "integer"),
    LineEntry("w3mag_5", "double"),
    LineEntry("w3sigm_5", "double"),
    LineEntry("w3flg_5", "integer"),
    LineEntry("w4mag_5", "double"),
    LineEntry("w4sigm_5", "double"),
    LineEntry("w4flg_5", "integer"),
    LineEntry("w1mag_6", "double"),
    LineEntry("w1sigm_6", "double"),
    LineEntry("w1flg_6", "integer"),
    LineEntry("w2mag_6", "double"),
    LineEntry("w2sigm_6", "double"),
    LineEntry("w2flg_6", "integer"),
    LineEntry("w3mag_6", "double"),
    LineEntry("w3sigm_6", "double"),
    LineEntry("w3flg_6", "integer"),
    LineEntry("w4mag_6", "double"),
    LineEntry("w4sigm_6", "double"),
    LineEntry("w4flg_6", "integer"),
    LineEntry("w1mag_7", "double"),
    LineEntry("w1sigm_7", "double"),
    LineEntry("w1flg_7", "integer"),
    LineEntry("w2mag_7", "double"),
    LineEntry("w2sigm_7", "double"),
    LineEntry("w2flg_7", "integer"),
    LineEntry("w3mag_7", "double"),
    LineEntry("w3sigm_7", "double"),
    LineEntry("w3flg_7", "integer"),
    LineEntry("w4mag_7", "double"),
    LineEntry("w4sigm_7", "double"),
    LineEntry("w4flg_7", "integer"),
    LineEntry("w1mag_8", "double"),
    LineEntry("w1sigm_8", "double"),
    LineEntry("w1flg_8", "integer"),
    LineEntry("w2mag_8", "double"),
    LineEntry("w2sigm_8", "double"),
    LineEntry("w2flg_8", "integer"),
    LineEntry("w3mag_8", "double"),
    LineEntry("w3sigm_8", "double"),
    LineEntry("w3flg_8", "integer"),
    LineEntry("w4mag_8", "double"),
    LineEntry("w4sigm_8", "double"),
    LineEntry("w4flg_8", "integer"),
    LineEntry("w1magp", "double"),
    LineEntry("w1sigp1", "double"),
    LineEntry("w1sigp2", "double"),
    LineEntry("w1k", "double"),
    LineEntry("w1ndf", "integer"),
    LineEntry("w1mlq", "double"),
    LineEntry("w1mjdmin", "double"),
    LineEntry("w1mjdmax", "double"),
    LineEntry("w1mjdmean", "double"),
    LineEntry("w2magp", "double"),
    LineEntry("w2sigp1", "double"),
    LineEntry("w2sigp2", "double"),
    LineEntry("w2k", "double"),
    LineEntry("w2ndf", "integer"),
    LineEntry("w2mlq", "double"),
    LineEntry("w2mjdmin", "double"),
    LineEntry("w2mjdmax", "double"),
    LineEntry("w2mjdmean", "double"),
    LineEntry("w3magp", "double"),
    LineEntry("w3sigp1", "double"),
    LineEntry("w3sigp2", "double"),
    LineEntry("w3k", "double"),
    LineEntry("w3ndf", "integer"),
    LineEntry("w3mlq", "double"),
    LineEntry("w3mjdmin", "double"),
    LineEntry("w3mjdmax", "double"),
    LineEntry("w3mjdmean", "double"),
    LineEntry("w4magp", "double"),
    LineEntry("w4sigp1", "double"),
    LineEntry("w4sigp2", "double"),
    LineEntry("w4k", "double"),
    LineEntry("w4ndf", "integer"),
    LineEntry("w4mlq", "double"),
    LineEntry("w4mjdmin", "double"),
    LineEntry("w4mjdmax", "double"),
    LineEntry("w4mjdmean", "double"),
    LineEntry("rho12", "integer"),
    LineEntry("rho23", "integer"),
    LineEntry("rho34", "integer"),
    LineEntry("q12", "integer"),
    LineEntry("q23", "integer"),
    LineEntry("q34", "integer"),
    LineEntry("xscprox", "double"),
    LineEntry("w1rsemi", "double"),
    LineEntry("w1ba", "double"),
    LineEntry("w1pa", "double"),
    LineEntry("w1gmag", "double"),
    LineEntry("w1gerr", "double"),
    LineEntry("w1gflg", "integer"),
    LineEntry("w2rsemi", "double"),
    LineEntry("w2ba", "double"),
    LineEntry("w2pa", "double"),
    LineEntry("w2gmag", "double"),
    LineEntry("w2gerr", "double"),
    LineEntry("w2gflg", "integer"),
    LineEntry("w3rsemi", "double"),
    LineEntry("w3ba", "double"),
    LineEntry("w3pa", "double"),
    LineEntry("w3gmag", "double"),
    LineEntry("w3gerr", "double"),
    LineEntry("w3gflg", "integer"),
    LineEntry("w4rsemi", "double"),
    LineEntry("w4ba", "double"),
    LineEntry("w4pa", "double"),
    LineEntry("w4gmag", "double"),
    LineEntry("w4gerr", "double"),
    LineEntry("w4gflg", "integer"),
    LineEntry("tmass_key", "integer"),
    LineEntry("r_2mass", "double"),
    LineEntry("pa_2mass", "double"),
    LineEntry("n_2mass", "integer"),
    LineEntry("j_m_2mass", "double"),
    LineEntry("j_msig_2mass", "double"),
    LineEntry("h_m_2mass", "double"),
    LineEntry("h_msig_2mass", "double"),
    LineEntry("k_m_2mass", "double"),
    LineEntry("k_msig_2mass", "double"),
    LineEntry("x", "double"),
    LineEntry("y", "double"),
    LineEntry("z", "double"),
    LineEntry("spt_ind", "integer"),
    LineEntry("htm20", "integer")
  )
  //---------------------------------------------------------------------------
  private final val COL_NAME_DEFINITION_SEQ = Seq(
      "cntr"
    , "ra"
    , "dec"
    , "w1mpro"
    , "w1snr"
    , "w2mpro"
    , "w2snr"
    , "w3mpro"
    , "w3snr"
    , "w4mpro"
    , "w4snr"
    , "ph_qual"
    , "w1mag"
    , "w1sigm"
    , "w2mag"
    , "w2sigm"
    , "w3mag"
    , "w3sigm"
    , "w4mag"
    , "w4sigm"
    , "w1mjdmean"
    , "w2mjdmean"
    , "w3mjdmean"
    , "w4mjdmean"
    , "xscprox"
    , "tmass_key"
    , "r_2mass"
    , "n_2mass"
    , "j_m_2mass"
    , "j_msig_2mass"
    , "h_m_2mass"
    , "h_msig_2mass"
    , "k_m_2mass"
    , "k_msig_2mass"
  )
  //---------------------------------------------------------------------------
  final val COL_DEFINITION_SEQ = COL_NAME_DEFINITION_SEQ.flatMap { colName=>
    COMPLETE_COL_DEFINITION_SEQ.find(_.name == colName)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class WiseReject(_id: Long //cntr
                      , ra:Double
                      , dec:Double
                      , w1mpro:Double
                      , w1snr:Double
                      , w2mpro:Double
                      , w2snr:Double
                      , w3mpro:Double
                      , w3snr:Double
                      , w4mpro:Double
                      , w4snr:Double
                      , ph_qual:String
                      , w1mag:Double
                      , w1sigm:Double
                      , w2mag:Double
                      , w2sigm:Double
                      , w3mag:Double
                      , w3sigm:Double
                      , w4mag:Double
                      , w4sigm:Double
                      , w1mjdmean:Double
                      , w2mjdmean:Double
                      , w3mjdmean:Double
                      , w4mjdmean:Double
                      , xscprox:Double
                      , tmass_key:Int
                      , r_2mass:Double
                      , n_2mass:Double
                      , j_m_2mass:Double
                      , j_msig_2mass:Double
                      , h_m_2mass:Double
                      , h_msig_2mass:Double
                      , k_m_2mass:Double
                      , k_msig_2mass: Double)
//=============================================================================
//End of file WiseReject.scala
//=============================================================================

