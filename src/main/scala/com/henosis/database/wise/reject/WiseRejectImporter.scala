/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  02/Jun/2022
 * Time:  11h:40m
 * Description: None
 */
//=============================================================================
package com.henosis.database.wise.reject
//=============================================================================
import com.common.DatabaseImporter.CSV_EXTENSION_FILE
import com.common.configuration.MyConf
import com.common.database.mongoDB.Helpers.GenericObservable
import com.common.hardware.cpu.CPU
import com.common.util.path.Path
import com.common.{DatabaseImporter, LineEntry}
import com.henosis.database.wise.reject.WiseReject.COL_DEFINITION_SEQ
import org.mongodb.scala.Document
//=============================================================================
import org.mongodb.scala.MongoCollection
import java.io.{BufferedReader, File, FileReader}
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object WiseRejectImporter {
  //---------------------------------------------------------------------------
  private val colIndexSeq = COL_DEFINITION_SEQ map(_.colPos)
  private val maxColIndexSeq = colIndexSeq.max
  private val maxBufferedDocument = 5 *  1000
  //---------------------------------------------------------------------------
  def getInt(s: String) =
    if (s.trim.isEmpty) Int.MinValue
    else s.trim.toInt
  //---------------------------------------------------------------------------
  def getLong(s: String) =
    if (s.trim.isEmpty) Long.MinValue
    else s.trim.toLong
  //---------------------------------------------------------------------------
  def getDouble(s: String) =
    if (s.trim.isEmpty) Double.NaN
    else s.trim.toDouble
  //---------------------------------------------------------------------------
  private def processFile(path: String
                         , collWithRegistry: MongoCollection[WiseReject]): Boolean = {
    val br = new BufferedReader(new FileReader(new File(path)))
    val sourceSeq = ArrayBuffer[WiseReject]()
    var line = br.readLine()
    while(line != null &&  line.length()!=0 ) {
      sourceSeq += getSource(line)
      if (sourceSeq.length > maxBufferedDocument) {
        collWithRegistry.insertMany(sourceSeq.toArray).results()
        sourceSeq.clear()
      }
      line = br.readLine()
    }
    br.close()

    //store the remain buffered docs
    if (sourceSeq.length > 0) {
      collWithRegistry.insertMany(sourceSeq.toArray).results()
      sourceSeq.clear()
    }
    true
  }
  //---------------------------------------------------------------------------
  private def getSource(line: String) = {
    val _itemSeq = line.split("\\|")

    val itemSeq =
      if(_itemSeq.length < maxColIndexSeq)
        _itemSeq ++ Array.fill[String](maxColIndexSeq - _itemSeq.length + 1)("")
      else
        _itemSeq

    var index = -1
    WiseReject(
      {index += 1; getLong(itemSeq(colIndexSeq(index)))},     // _id
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // ra
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // dec
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w1mpro
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w1snr
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w2mpro
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w2snr
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w3mpro
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w3snr
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w4mpro
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w4snr
      {index += 1; itemSeq(colIndexSeq(index))},              // ph_qual
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w1mag
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w1sigm
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w2mag
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w2sigm
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w3mag
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w3sigm
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w4mag
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w4sigm
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w1mjdmean
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w2mjdmean
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w3mjdmean
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // w4mjdmean
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // xscprox
      {index += 1; getInt(itemSeq(colIndexSeq(index)))},      // tmass_key
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // r_2mass
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // n_2mass
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // j_m_2mass
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // j_msig_2mass
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // h_m_2mass
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // h_msig_2mass
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))},   // k_m_2mass
      {index += 1; getDouble(itemSeq(colIndexSeq(index)))}    // k_msig_2mass
    )
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
case class WiseRejectImporter(conf: MyConf, inputDir: String) extends DatabaseImporter {
  //---------------------------------------------------------------------------
  val mongoDB = WiseRejectDB(conf)
  val maxBufferedDocument = 0       //not used
  val colNameSeq = Seq[LineEntry]() //not used
  val lineDivider: String = ","     //not used
  val headerInCsv: Boolean = true
  val coreCount = CPU.getCoreCount()
  val collWithRegistry: MongoCollection[WiseReject] = mongoDB.database.getCollection(mongoDB.collectionName)
  //---------------------------------------------------------------------------
  def createIndexes() = {
    info("Creating indexes")
    mongoDB.createIndexes(Array("ra","dec"), _unique = false)
  }
  //---------------------------------------------------------------------------
  override def run(dropCollection : Boolean = true, fileExtension: String = CSV_EXTENSION_FILE) : Unit = {
    if (Path.directoryExist(iDir)) {
      if (dropCollection) mongoDB.dropCollection()
      val pathSeq = Path.getSortedFileList(iDir)
        .map(_.getAbsolutePath)
        .filter(_.contains("rej-part"))
      new MyParallelTask(pathSeq, mongoDB.getCollection)
      info(s"Total documents inserted in: ${mongoDB.getBasicConnectionInfo()}: ${mongoDB.getCollection.countDocuments().results.head}")
      createIndexes
    }
    else error(s"Input directory '$iDir' does not exists")
  }
  //---------------------------------------------------------------------------
  override protected def processFile(path: String, coll: MongoCollection[Document]): Boolean = {
    WiseRejectImporter.processFile(path, collWithRegistry)
    true
  }
  //---------------------------------------------------------------------------
  run(dropCollection = true, fileExtension = "cat-part")
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file WiseRejectImporter.scala
//=============================================================================
