#!/bin/bash
#----------------
LOCAL_DIR=/home/rafa/images/tools/spice/horizons/xfr/
#----------------
echo "Prepare directories"
rm -fr $LOCAL_DIR
mkdir $LOCAL_DIR
cd $LOCAL_DIR

echo "Downloading and unzip"
rm dastcom5.zip
rm -fr dastcom5
curl -O ftp://ssd.jpl.nasa.gov/pub/xfr/dastcom5.zip
unzip dastcom5.zip -d .
rm dastcom5.zip
#----------------
echo "Creating the configuration file dxlook.inp"

printf "DB    ../dat/dast5_le.dat
DB    ../dat/dcom5_le.dat
INDEX ../dat/dastcom.idx
PAGER /usr/bin/less" > dastcom5/exe/dxlook.inp
#----------------
#now compile, but remember to modify the makefile to use flag:  -fallow-argument-mismatch (more details un zim page)
#cd dastcom5/src
#make
#----------------
#end of script
