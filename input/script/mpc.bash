#!/bin/bash
#-----------------------------------------------------
F=mpcorb_extended.json
TEMP_DIR=/tmp
#-----------------------------------------------------
cd $TEMP_DIR
rm -fr $F
wget https://minorplanetcenter.net/Extended_Files/$F.gz
gzip -d ./$F

#remove the parethesis that surrounds the name
sed -i 's/[()]//g' $F
#-----------------------------------------------------
echo $TEMP_DIR/$F
