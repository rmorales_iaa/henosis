#!/bin/bash
#-----------------------------------------------------
F=astorb.dat
TEMP_DIR=/tmp
#-----------------------------------------------------
cd $TEMP_DIR
rm -fr $F
wget https://ftp.lowell.edu/pub/elgb/$F.gz
gzip -d ./$F
#-----------------------------------------------------
echo $TEMP_DIR/$F
