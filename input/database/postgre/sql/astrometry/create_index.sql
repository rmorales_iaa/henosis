DROP INDEX IF EXISTS idx_m2_astrometry_radec;
CREATE INDEX idx_m2_astrometry_radec ON m2_astrometry (raMin, raMax, decMin, decMax);
