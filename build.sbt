//-----------------------------------------------------------------------------
//henosis settings
//-----------------------------------------------------------------------------
lazy val programName    = "henosis"
lazy val programVersion = "0.0.1"
lazy val authorList     = "Rafael Morales Muñoz (rmorales@iaa.es)"
lazy val license        = "This project is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)"
//-----------------------------------------------------------------------------
//common settings
//version will be calculated using git repository
val myScalaVersion = "2.13.14"
lazy val commonSettings = Seq(
  name                 := programName
  , version            := programVersion
  , scalaVersion       := myScalaVersion
  , description        := "database hub"
  , organization       := "IAA-CSIC"
  , homepage           := Some(url("http://www.iaa.csic.es"))
)

//-----------------------------------------------------------------------------
//Main class
lazy val mainClassName = "com.henosis.Main"
//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
  //  "-Xfatal-warnings"   //generate an error when warning raises. Enable when stable version was published
)
//-----------------------------------------------------------------------------
//dependencies versions
val apacheCommonsCsv         = "1.11.0"
val apacheCommonsIO          = "2.16.1"
val apacheCommonsMath        = "3.6.1"
val apacheLogCoreVersion     = "2.19.0"
val apacheLogScalaVersion    = "13.1.0"
val commandLineParserVersion = "5.1.0"
val fastParseVersion         = "3.1.1"
val hdf_5_Version            = "19.04.1"  //see https://maven.scijava.org/content/repositories/public/cisd/jhdf5/
val jamaVersion              = "1.0.3"
val javaFitsVersion          = "1.20.1"
val javaxMailVersion         = "1.6.2"
val jniLibraryVersion        = "0.4.4"
val jSofaVersion             = "20210512"
val jsonParsingUpickleVersion = "4.0.1"
val jsonParsingJsoniterVersion = "2.30.9"
val mongoScalaDriverVersion  = "5.2.0"
val orekitVersion            = "12.1.3"
val postgreSQL_Version       = "42.7.4"
val qosLogbackVersion        = "1.5.8"
val pdsVersion               = "2.8.4"
val scalaSqlVersion          = "0.1.9"
val rTree2DVersion           = "0.11.13"
val scalaReflectVersion      = myScalaVersion
val scalaTestVersion         = "3.3.0-SNAP3"
val scalaHttpVersion         = "2.4.2"
val typeSafeVersion          = "1.4.3"
//-----------------------------------------------------------------------------
//assembly
assembly / mainClass := Some(mainClassName)
assembly / logLevel  := Level.Warn

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}
//-----------------------------------------------------------------------------
//tests
test / parallelExecution := false
//-----------------------------------------------------------------------------
//resolvers
resolvers ++= Resolver.sonatypeOssRepos("public")
resolvers ++= Seq(
    "ImageJ Public Repository" at "https://maven.scijava.org/content/repositories/public/"  //required by jhdf5
    , "JBoss" at "https://repository.jboss.org/nexus/content/groups/public"  //required by pdsLCDB
)

//-----------------------------------------------------------------------------
//git versioning: https://github.com/sbt/sbt-git

git.useGitDescribe := true
git.uncommittedSignifier := None         //do not append suffix is some changes are not pushed to git repo
git.gitDescribePatterns := Seq("v*.*")   //format of the tag version

//https://stackoverflow.com/questions/56588228/how-to-get-git-gittagtoversionnumber-value
git.gitTagToVersionNumber := { tag: String =>
  if (tag.isEmpty)  None
  else Some(tag.split("-").toList.head)
}

//See configuration options in: https://github.com/sbt/sbt-buildinfo

//package and name of the scala code auto-generated
buildInfoPackage := "BuildInfo"
buildInfoObject := "BuildInfo"

//append build time
buildInfoOptions += BuildInfoOption.BuildTime

//append extra options to be generated
buildInfoKeys ++= Seq[BuildInfoKey](
  buildInfoBuildNumber
  , scalaVersion
  , sbtVersion
  , name
  , version
  , "authorList" -> authorList
  , "license" -> license
  , description
  , organization
  , homepage
  , BuildInfoKey.action("myGitCurrentBranch") {git.gitCurrentBranch.value}
  , BuildInfoKey.action("myGitHeadCommit")    {git.gitHeadCommit.value.get}
)
//-----------------------------------------------------------------------------

excludeFilter in (Compile, unmanagedSources) := {
  new SimpleFileFilter(f=> {
    val p = f.getCanonicalPath

      p.contains("dataType/array") ||
      p.contains("fits/metaData") ||
      p.contains("fits/standard") ||
      p.contains("fits/wcs/fit") ||
      p.contains("image/astrometry") ||
      p.contains("image/catalog") ||
      p.contains("image/estimator/flux/") ||
      p.contains("image/fumo/") ||
      p.contains("image/myImage/dir") ||
      p.contains("image/nsaImage") ||
      p.contains("image/occultation") ||
      p.contains("image/photometry") ||
      p.contains("image/registration") ||
      p.contains("image/script") ||
      p.contains("image/web") ||
      p.contains("myJava/nom/tam") ||
      p.contains("stream/remote") ||
      p.contains("util/computerInfo")
  })}

//-----------------------------------------------------------------------------
//dependencies list
lazy val dependenceList = Seq(

  //manage configuration files. https://github.com/typesafehub/config
  "com.typesafe" % "config" % typeSafeVersion

  //logging: https://logging.apache.org/log4j/2.x/manual/scala-api.html
  , "org.apache.logging.log4j" %% "log4j-api-scala" % apacheLogScalaVersion
  , "org.apache.logging.log4j" % "log4j-core" % apacheLogCoreVersion % Runtime
  , "ch.qos.logback" % "logback-classic" % qosLogbackVersion //required by mongoDB

  //https://mvnrepository.com/artifact/commons-io/commons-io
  , "commons-io" % "commons-io" % apacheCommonsIO

  //command line argument parser: https://github.com/scallop/scallop
  , "org.rogach" %% "scallop" % commandLineParserVersion

  //mongoDB
  , "org.mongodb.scala" %% "mongo-scala-driver" % mongoScalaDriverVersion

  //csv management: https://mvnrepository.com/artifact/org.apache.commons/commons-csv
  , "org.apache.commons" % "commons-csv" % apacheCommonsCsv

  //https://mvnrepository.com/artifact/org.apache.commons/commons-math3
  , "org.apache.commons" % "commons-math3" % apacheCommonsMath

  //scala reflect
  , "org.scala-lang" % "scala-reflect" % scalaReflectVersion

  //Java translation of the International Astronomical Union's C SOFA software library : http://javastro.github.io/jsofa/
  , "org.javastro" % "jsofa" % jSofaVersion

  //jni: https://mvnrepository.com/artifact/ch.jodersky/jni-library
  // remember to add: 'addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.10")' in plugin.sbt
  , "ch.jodersky" % "jni-library_2.12.0-M4" % jniLibraryVersion

  //mail agent: https://javaee.github.io/javamail/
  , "com.sun.mail" % "javax.mail" % javaxMailVersion

  // https://mvnrepository.com/artifact/org.orekit/orekit
  , "org.orekit" % "orekit" % orekitVersion

   //https://github.com/com-lihaoyi/fastparse
  , "com.lihaoyi" %% "fastparse" %  fastParseVersion

   //json parsing: //https://mvnrepository.com/artifact/com.lihaoyi/upickle
  , "com.lihaoyi" %% "upickle" % jsonParsingUpickleVersion

   //postgreSQL: http://www.lihaoyi.com/post/ScalaSqlaNewSQLDatabaseQueryLibraryforthecomlihaoyiScalaEcosystem.html#the-missing-database-library-in-the-com-lihaoyi-ecosystem
  , "com.lihaoyi" %% "scalasql" % scalaSqlVersion
  , "com.lihaoyi" %% "scalasql-core" % scalaSqlVersion
  , "com.lihaoyi" %% "scalasql-operations" % scalaSqlVersion
  , "com.lihaoyi" %% "scalasql-query"  % scalaSqlVersion
  , "org.postgresql" % "postgresql" % postgreSQL_Version

  //json parsing: https://github.com/plokhotnyuk/jsoniter-scala
  , "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-core" % jsonParsingJsoniterVersion
  , "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-macros" % jsonParsingJsoniterVersion % "provided"

  //http get and post:  https://github.com/scalaj/scalaj-http
  , "org.scalaj" %% "scalaj-http" % scalaHttpVersion

  // basic linear algebra package for Java.  https://mvnrepository.com/artifact/gov.nist.math/jama
  , "gov.nist.math" % "jama" % jamaVersion

  //R-tree on 2d: https://github.com/plokhotnyuk/rtree2d
  , "com.github.plokhotnyuk.rtree2d" %% "rtree2d-core" % rTree2DVersion

  //java FITS: https://mvnrepository.com/artifact/gov.nasa.gsfc.heasarc/nom-tam-fits
  , "gov.nasa.gsfc.heasarc" % "nom-tam-fits" % javaFitsVersion

  //hdf5 version: http://portal.hdfgroup.org/display/HDF5
  , "cisd" % "jhdf5" % hdf_5_Version

  //Planetary Data System (pds4): https://pds.nasa.gov/
  , "gov.nasa.pds" % "pds4-jparser" % pdsVersion

   //scala test
  , "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)
//=============================================================================
//root project
lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(AssemblyPlugin, BuildInfoPlugin, GitVersioning)
  .settings(libraryDependencies ++= dependenceList)
//-----------------------------------------------------------------------------
//End of file 'build.sbt'
//-----------------------------------------------------------------------------
